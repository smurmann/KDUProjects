#pragma once
#include "player.h"
#include "ItemList.h"
#include "ArmorList.h"
#include "PlayerList.h"

PlayerList *PlayerList::CreatePlayer(PLAYER choosePlayer)
{
	if (choosePlayer == PLAYER::HUMAN)
	{
		return new Human();
	}
	else if (choosePlayer == PLAYER::SOLDIER)
	{
		return new Soldier();
	}
}
void PlayerList::CreateWeapon(ItemList iList)
{
	weapon.push_back(iList);

}

void PlayerList::CreateArmor(ArmorList aList)
{
	armor.push_back(aList);

}

void PlayerList::PrintWeapon()
{
	for (vector<ItemList>::iterator iter = weapon.begin(); iter != weapon.end(); ++iter)
	{
		cout << "-" << (*iter).getName() << endl;
	}

}

void PlayerList::PrintArmor()
{
	for (vector<ArmorList>::iterator iter = armor.begin(); iter != armor.end(); ++iter)
	{
		cout << "-" << (*iter).GetName() << endl;
	}

}