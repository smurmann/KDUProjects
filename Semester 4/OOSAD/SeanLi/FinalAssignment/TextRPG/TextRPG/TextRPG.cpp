// TextRPG.cpp : Defines the entry point for the console application.
//
#include<iostream>
#include<string>
#include<Windows.h>
#include<vector>
#include<time.h>
#include "stdafx.h"
#include <cstdlib>

#include "PlayerClass.h"
#include "CreatorPattern.h"
#include "BuilderWarrior.h"
#include "PlayerBuilder.h"
#include "BuilderMage.h"
#include "BuilderThief.h"
#include "BuilderEnemy.h"
#include "WeaponFactory.h"
#include "EquipmentDecorator.h"

using namespace std;



int _tmain(int argc, _TCHAR* argv[])
{
	CreatorPattern c;
	srand(time(0));
	int inputSelection;
	bool inputCheck = false;
	bool done = false;
	Player* p;
	PlayerBuilder* pb;
	EQ* head = new Headgear();
	EQ* chest = new Shirt();
	EQ* pants = new Pants();
	EQ* shoes = new Shoes();
	int evadechance;

	// choose class
	do
	{

		system("CLS");
		cout << "Welcome to Sean-Li's final assignment battle simulator program." << endl;
		cout << "Select a class of your choice." << endl;
		cout << endl;
		cout << "1. Warrior" << endl;
		cout << "2. Mage" << endl;
		cout << "3. Thief" << endl;
		cout << endl;
		cout << "Your input: ";
		cin >> inputSelection;

		if (inputSelection == 1)
		{
			inputSelection = -1;

			pb = new BuilderWarrior;
			c.setPlayerBuilder(pb);
			c.createPlayer();
			p = c.getPlayer();
			inputCheck = true;
		}
		else if (inputSelection == 2)
		{
			inputSelection = -1;

			pb = new BuilderMage;
			c.setPlayerBuilder(pb);
			c.createPlayer();
			p = c.getPlayer();
			inputCheck = true;
		}
		else if (inputSelection == 3)
		{
			inputSelection = -1;
			pb = new BuilderThief;
			c.setPlayerBuilder(pb);
			c.createPlayer();
			p = c.getPlayer();
			inputCheck = true;
		}
		else if (inputSelection != 1 && inputSelection != 2 && inputSelection != 3)
		{
			system("CLS");
			cout << "Sorry wrong Input. Please reenter your choice." << endl;
			system("Pause");
		}

	} while (!inputCheck);



	// skillpoint destribution
	do
	{
		system("CLS");
		p->PlayerFullStats();
		cout << endl;
		cout << "Now destribute your remaining skillpoints" << endl;
		cout << "Skillpoints remaining: " << p->getSkillPoints() << endl;
		cout << endl;
		cout << "1. Strength     : +HP 20, +MP 5 , +PDMG 10, +MDMG 3,  -EVADE 2, +DEF 10" << endl;
		cout << "2. Dexterity    : +HP 10, +MP 10, +PDMG 5,  +MDMG 5,  +EVADE 5, +DEF 5" << endl;
		cout << "3. Intelligence : +HP 5,  +MP 20, +PDMG 10, +MDMG 10, +EVADE 2, +DEF 3" << endl;
		cout << "4. To Continue" << endl;
		cout << endl;
		cout << "Your input: ";
		cin >> inputSelection;

		if (inputSelection == 1)
		{
			inputSelection = -1;
			p->bAddStr();
		}
		else if (inputSelection == 2)
		{
			inputSelection = -1;
			p->bAddDex();
		}
		else if (inputSelection == 3)
		{
			inputSelection = -1;
			p->bAddInt();
		}
		else if (inputSelection != 1 && inputSelection != 2 && inputSelection != 3 && inputSelection != 4)
		{
			system("CLS");
			cout << "Sorry wrong Input. Please reenter your choice." << endl;
			system("Pause");
		}
		system("PAUSE");

	} while (inputSelection != 4);

	p->setSkillPoints(0);
	inputSelection = -1;
	//cheaty way of adding points for weapon
	p->setSkillPoints(p->getSkillPoints() + 2);
	do
	{
		system("CLS");
		cout << "TIME TO SELECT YOUR WEAPONS" << endl;
		cout << "You can select: " << p->getSkillPoints() << " Weapons." << endl;
		for (int i = 0; WeaponShop.size() > i; i++)
		{
			cout << i + 1;  WeaponShop[i].showWeaponStats();
		}
		cin >> inputSelection;

		if (inputSelection == 1)
		{
			Rapier rapier;
			p->addToWeapons(rapier);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 2)
		{
			Bloodsword bloodsword;
			p->addToWeapons(bloodsword);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 3)
		{
			TheDefender thedefender;
			p->addToWeapons(thedefender);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 4)
		{
			Butterknife butterknife;
			p->addToWeapons(butterknife);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 5)
		{
			Karambit karambit;
			p->addToWeapons(karambit);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 6)
		{
			Dagger dagger;
			p->addToWeapons(dagger);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 7)
		{
			Wand wand;
			p->addToWeapons(wand);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 8)
		{
			Staff staff;
			p->addToWeapons(staff);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else if (inputSelection == 9)
		{
			Book book;
			p->addToWeapons(book);
			p->setSkillPoints(p->getSkillPoints() - 1);
		}
		else
		{
			system("CLS");
			cout << "Sorry wrong Input. Please reenter your choice." << endl;
			system("Pause");
			inputSelection = -1;
		}

	} while (p->getSkillPoints() > 0);

	p->addCurrentWeaponToStat();

	do
	{
		system("CLS");
		cout << "ITS TIME TO CUSTOMIZE YOUR GEAR" << endl;
		cout << head->getDesc()<< "\t" << ": Weight: " << head->getWeight() << ", Health: " << head->getHealth() << ", Def: " << head->getDef() << endl;
		cout << chest->getDesc()<< "\t\t" << ": Weight: " << chest->getWeight() << ", Health: " << chest->getHealth() << ", Def: " << chest->getDef() << endl;
		cout << pants->getDesc() << "\t\t" << ": Weight: " << pants->getWeight() << ", Health: " << pants->getHealth() << ", Def: " << pants->getDef() << endl;
		cout << shoes->getDesc() << "\t\t" << ": Weight: " << shoes->getWeight() << ", Health: " << shoes->getHealth() << ", Def: " << shoes->getDef() << endl;
		cout << endl;
		cout << "Decorate your clothes with shiny upgrade decorator crystals!" << endl;
		cout << "Choose 1. to give all your gear HP Crystals" << endl;
		cout << "or choose 2. to give all your gear DEF Crystals" << endl;
		cout << endl;
		cout << "Your input: ";
		cin >> inputSelection;

		if (inputSelection == 1)
		{
			head = new HealthDecorator(new SubDecorator(head));
			chest = new HealthDecorator(new SubDecorator(chest));
			pants = new HealthDecorator(new SubDecorator(pants));
			shoes = new HealthDecorator(new SubDecorator(shoes));
			done = true;
			p->setHealth(p->getHealth() + head->getHealth() + chest->getHealth() + pants->getHealth() + shoes->getHealth());
		}
		else if (inputSelection == 2)
		{
			head = new DefDecorator(new SubDecorator(head));
			chest = new DefDecorator(new SubDecorator(chest));
			pants = new DefDecorator(new SubDecorator(pants));
			shoes = new DefDecorator(new SubDecorator(shoes));
			done = true;
			p->setDefense(p->getDefense() + head->getDef() + chest->getDef() + pants->getDef() + shoes->getDef());
		}
		else
		{
			system("CLS");
			cout << "Sorry wrong Input. Please reenter your choice." << endl;
			system("Pause");
			inputSelection = -1;
		}

	} while (!done);


	system("CLS");
	cout << "This is you finished character!!" << endl;
	cout << "====================================" << endl;
	p->PlayerFullStats();
	cout << endl;
	p->getCurrentWeapon();
	cout << endl;
	cout << "This is your gear: " << endl;
	cout << head->getDesc() << "Weight: " << head->getWeight() << "Health: " << head->getHealth() << "Def: " << head->getDef() << endl;
	cout << chest->getDesc() << "Weight: " << chest->getWeight() << "Health: " << chest->getHealth() << "Def: " << chest->getDef() << endl;
	cout << pants->getDesc() << "Weight: " << pants->getWeight() << "Health: " << pants->getHealth() << "Def: " << pants->getDef() << endl;
	cout << shoes->getDesc() << "Weight: " << shoes->getWeight() << "Health: " << shoes->getHealth() << "Def: " << shoes->getDef() << endl;
	cout << endl;

	system("PAUSE");

	Player* e;
	PlayerBuilder* enemyBuilder;

	enemyBuilder = new BuilderEnemy;
	c.setPlayerBuilder(enemyBuilder);
	c.createPlayer();
	e = c.getPlayer();
	done = false;
	do
	{
		inputSelection = -1;
		system("CLS");
		cout << "TIME TO FIGHT!" << endl;
		cout << "=======================" << endl;
		p->PlayerCombatStats();
		cout << "=======================" << endl;
		e->PlayerCombatStats();
		cout << endl;
		cout << "Your skills" << endl;
		cout << "1. Physical Attack" << endl;
		cout << "2. Magic Attack (10 MP)" << endl;
		cout << endl;
		cout << "Your input: ";
		cin >> inputSelection;
		system("CLS");
		if (inputSelection == 1)
		{
			evadechance = rand() % 101;
			if (evadechance <= e->getEvadeChance())
			{
				cout << "Oh no!! " << e->getName() << " has evaded your attack!" << endl;
				system("Pause");
			}
			else
			{
				if (e->getDefense() > 0)
				{
					e->setDefense(e->getDefense() - (p->getDmgPhysical()));
					cout << "You managed to damage his shields with your weapon!!" << endl;

				}
				else
				{
					e->setHealth(e->getHealth() - (p->getDmgPhysical()));
					cout << "You managed to damage his health with your weapon!!" << endl;

				}
			}

		}
		else if (inputSelection == 2)
		{
			if (p->getMana() < 10)
			{
				cout << "Sorry! No more mana!!" << endl;
			}
			else
			{
				evadechance = rand() % 101;
				if (evadechance >= e->getEvadeChance())
				{
					cout << "Oh no!! " << e->getName() << " has evaded your attack!" << endl;
					system("Pause");
				}
				else
				{
					if (e->getDefense() > 0)
					{
						e->setDefense(e->getDefense() - (p->getDmgMagical()));
						p->setMana(p->getMana() - 10);
						cout << "You managed to damage his shields with your magic!" << endl;
					}
					else
					{
						e->setHealth(e->getHealth() - (p->getDmgMagical()));
						p->setMana(p->getMana() - 10);
						cout << "You managed to damage his health with your magic!" << endl;

					}
				}
			}
			system("PAUSE");

		}

		evadechance = rand() % 101;
		if (evadechance <= p->getEvadeChance())
		{
			cout << "Sweet!! " << p->getName() << " has evaded " << e->getName() << "'s attack!" << endl;
			system("Pause");
		}
		else
		{
			evadechance = rand() % 3;
			if (evadechance == 0)
			{
				if (p->getDefense() > 0)
				{
					p->setDefense(p->getDefense() - (e->getDmgPhysical()));
				}
				else
				{
					p->setHealth(p->getHealth() - (e->getDmgPhysical()));
				}
				cout << e->getName() << " has punched you with all his might!" << endl;
			}
			else if (evadechance == 1)
			{
				if (p->getDefense() > 0)
				{
					p->setDefense(p->getDefense() - (e->getDmgMagical()));
				}
				else
				{
					p->setHealth(p->getHealth() - (e->getDmgMagical()));
				}
				cout << e->getName() << " has cast an evil spell onto you!" << endl;
			}
			else if (evadechance == 2)
			{
				if (p->getDefense() > 0)
				{
					p->setDefense(p->getDefense() - (e->getDmgMagical()) - e->getDmgPhysical());
				}
				else
				{
					p->setHealth(p->getHealth() - (e->getDmgMagical()) - e->getDmgPhysical());
				}
				cout << e->getName() << " managed to hit you TWICE!!" << endl;
			}
			system("PAUSE");

		}

		if (e->getHealth() <= 0 || p->getHealth() <= 0)
		{
			done = true;
		}
	
	} while (!done);


	system("CLS");
	cout << "TIME TO FIGHT!" << endl;
	cout << "=======================" << endl;
	p->PlayerCombatStats();
	cout << "=======================" << endl;
	e->PlayerCombatStats();
	cout << endl;
	if (p->getHealth() <= 0)
	{
		cout << "YOU GOT REKT! Better luck next time!" << endl;
	}

	if (e->getHealth() <= 0)
	{
		cout << "Congratulations! You beat the game!" << endl;
	}




	system("PAUSE");
	return 0;
}

