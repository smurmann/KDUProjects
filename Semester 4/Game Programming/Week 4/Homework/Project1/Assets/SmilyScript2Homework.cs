﻿//Speed is not constant T_T
// HELP

using UnityEngine;
using System.Collections;

public class SmilyScript2Homework : MonoBehaviour {
			
	public float speed = 1.5f;
	public float stop = 0.0f;
	public float go = 1.5f;
	private Vector3 target;
	private Vector3 lastpos;
	private float m_smileyHalfW;
	private float m_smileyHalfH;
	
	
	private SpriteRenderer m_spriteRenderer;

		
	void Start () {
		m_spriteRenderer = gameObject.renderer as SpriteRenderer;
		Sprite mySprite = m_spriteRenderer.sprite;
		
		m_smileyHalfW = mySprite.bounds.size.x * 0.5f;
		m_smileyHalfH = mySprite.bounds.size.y * 0.5f;

		target = transform.position;

		lastpos = transform.position;



	}
		
	void Update () {
		Vector3 WorldSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0.0f));
		float halfWidth = WorldSize.x;
		float halfHeight = WorldSize.y;



		Vector3 myPosition = transform.position;

		if (Input.GetMouseButtonDown(0)) {
			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			target.z = transform.position.z;
			lastpos = transform.position;
			speed = go;
		}
		//var heading = (target - myPosition);
		//var distance = heading.magnitude;
		//var direction = heading /distance;

		myPosition += (target - lastpos) * speed *Time.deltaTime;


		if(myPosition.x > halfWidth - m_smileyHalfW)
		{
			myPosition.x = halfWidth - m_smileyHalfW - 0.001f;
			speed = stop;
			target.x = myPosition.x;
			target.y = myPosition.y;
		}
		if (myPosition.y > halfHeight - m_smileyHalfH)
		{
			myPosition.y = halfHeight - m_smileyHalfW - 0.001f;
			speed = stop;
			target.y = myPosition.y;
			target.x = myPosition.x;
		}
		
		if (myPosition.y < -halfHeight + m_smileyHalfH)
		{
			myPosition.y = -halfHeight + m_smileyHalfW + 0.001f;
			speed = stop;
			target.y = myPosition.y;
			target.x = myPosition.x;
		}
		if (myPosition.x < -halfWidth + m_smileyHalfW)
		{
			myPosition.x = -halfWidth + m_smileyHalfW + 0.001f;
			speed = stop;
			target.x = myPosition.x;
			target.y = myPosition.y;
		}

		transform.position = myPosition;

	}    
} 

