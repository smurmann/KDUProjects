﻿using UnityEngine;
using System.Collections;

public class ufoScript : MonoBehaviour {

	private SpriteRenderer m_spriteRenderer;
	private Sprite mySprite;
	float dirX = 0.1f;

	public int health = 20;


	// Use this for initialization
	void Start () {

		m_spriteRenderer = gameObject.renderer as SpriteRenderer;
		mySprite = m_spriteRenderer.sprite;
	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 myPosition = transform.position;


		myPosition.x += dirX;

		if(myPosition.x > 5.0f)
		{
			dirX = -dirX;
		}

		if(myPosition.x < -5.0f)
		{
			dirX = -dirX;
		}

		transform.position = myPosition;

		if(health <= 0)
		{
			Destroy (this.gameObject);
		}
	
	}
}
