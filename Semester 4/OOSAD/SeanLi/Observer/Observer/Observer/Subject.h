#ifndef _SUBJECT_H
#define _SUBJECT_H
#include "Observer.h"
#include <vector>

using namespace std;
class Subject
{
protected:
	//! collection of registered listener
	typedef vector<Observer*> ObserverVector;
	ObserverVector mObserver;
public:
	//! Add observer to subject
	void AddObserver(Observer* observer)
	{
		mObserver.push_back(observer);
	}
	void removeObserver(Observer* observer)
	{
		ObserverVector::iterator iter = mObserver.begin();
		while (iter != mObserver.end())
		{
			if (*iter == observer)
			{
				mObserver.erase(iter);
			}
			++iter;
		}
	}
	void Notify()
	{
		ObserverVector::iterator iter = mObserver.begin();
		while (iter != mObserver.end())
		{
			(*iter)->update(this);
			++iter;
		}
	}
};

#endif