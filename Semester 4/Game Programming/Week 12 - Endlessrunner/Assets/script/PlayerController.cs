﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	public float m_speed;
	public GameObject m_explosionPrefab;

	private Rigidbody2D m_rb;
	private GameObject m_particleSysNode;
	private ParticleSystem m_particleSys;
	private bool m_killed;

    private Animator anim;

	// Use this for initialization
	void Start ()
	{
		m_rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

		m_particleSysNode = GameObject.Find("/contactFloorParticle");
		m_particleSys = m_particleSysNode.GetComponent<ParticleSystem> ();

		m_killed = false;

        anim.Play("AlpacaRun");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_killed)
			return;

		// 1st raycast to check if it's hitting ground or landing on any physics object
		Vector2 downDirection = new Vector2 (0.0f, -1.0f);
		RaycastHit2D downHit = Physics2D.Raycast (transform.position, downDirection, 0.58f); //0.58f : make it easier to jump
		if (downHit == true)
		{
            
			Vector3 particleNodePos = transform.position;
			particleNodePos.y -= 0.3f;
			m_particleSysNode.transform.position = particleNodePos;
			m_particleSys.Play();

			if(Input.anyKeyDown)
			{
				Vector3 velocity = m_rb.velocity;
				velocity.x = 0.0f;
				velocity.y = 10.5f;
				m_rb.velocity = velocity;
			}

			//reset the box's rotation
			transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 0.0f));
		}
		else
		{
			m_particleSys.Stop();
			transform.Rotate(0, 0, -400.0f*Time.deltaTime);
		}
	
		// 2nd raycast to check if the player has hit any obstacle from the front.
		Vector2 frontDirection = new Vector2 (1.0f, 0.0f);
		RaycastHit2D frontHit = Physics2D.Raycast (transform.position, frontDirection, 0.36f);
		if (frontHit == true)
		{
			Kill();
		}
			
		// increase the speed over time
		//m_speed += 0.1f*Time.deltaTime;
        //if(anim)
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag == "SpikeObstacle")
		{
			Kill();
		}
	}

	void Kill()
	{
		m_killed = true;
		m_speed = 0.0f;

		m_particleSys.Stop();

		//=========== lazy way to "hide" the object, more proper way is to disable the renderer
		Vector3 scale = new Vector3(0.0f, 0.0f, 0.0f);
		this.gameObject.transform.localScale = scale;
		//==================
		
		GameObject newExplosion = Instantiate(m_explosionPrefab, transform.position, transform.rotation) as GameObject;
		
		StartCoroutine(ResetLevel());
	}
	
	//coroutine function to reset the level after 1.2 second
	IEnumerator ResetLevel()
	{
		float timeElapsed = 0.0f;	
		while (timeElapsed < 1.2f)
		{
			timeElapsed += Time.deltaTime;
			yield return null;
		}

		Application.LoadLevel(Application.loadedLevel);
	}

}
