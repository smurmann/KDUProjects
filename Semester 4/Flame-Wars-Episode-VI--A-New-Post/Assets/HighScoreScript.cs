﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour {

    Text text;
    int score;
    int score2;

	// Use this for initialization
	void Start () {

        text = GetComponent<Text>();
        //score = PlayerPrefs.GetInt("Highscore");
        //score2 = PlayerPrefs.GetInt("Highscore");
	
	}
	
	// Update is called once per frame
	void Update () {

        text.text = "HIGHSCORE: " + PlayerPrefs.GetInt("Highscore"); 

        if(GameObject.FindGameObjectWithTag("InputBox").GetComponent<SendInputOnSpace>().playerLikes > PlayerPrefs.GetInt("Highscore"))
        {
            PlayerPrefs.SetInt("Highscore", GameObject.FindGameObjectWithTag("InputBox").GetComponent<SendInputOnSpace>().playerLikes);
        }
	
	}
}
