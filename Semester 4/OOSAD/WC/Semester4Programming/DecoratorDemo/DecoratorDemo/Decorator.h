#ifndef DECORATOR_H
#define DECORATOR_H

#include <iostream>
#include <string>
#include "windows.h"

using namespace std;

class Sandwich
{

public:
	virtual double getCost() = 0;
	virtual string getDesc() = 0;
};

class WhileGrainBread :public Sandwich
{
public:
	string getDesc()
	{
		return "Whole Grain Bread";
		
	}
	double getCost()
	{
		return 3.0;

	}

};

class ItalianBread :public Sandwich

{
public:
	string getDesc()
	{
		return "Italian Bread";

	}
	double getCost()
	{
		return 4.0;

	}

};

class WheatBread :public Sandwich

{
public:
	string getDesc()
	{
		return "Wheat Bread";

	}
	double getCost()
	{
		return 3.5;

	}

};

class SubDecorator : public Sandwich
{
	Sandwich *sandwich;
public:
	SubDecorator(Sandwich *sandwichRef)
	{
		sandwich = sandwichRef;
	}
	string getDesc()
	{
		return sandwich->getDesc();
	}
	double getCost()
	{
		return sandwich->getCost();
	}

};

class CheeseDecorator : public SubDecorator
{
private:
	double cheese_cost;
	string cheese_desc()
	{
		return " + Cheese ";
	}
public:
	CheeseDecorator(Sandwich *sandwich):
	SubDecorator(sandwich)
	{
		cheese_cost = 3.0;
	}
	string getDesc()
	{
		return SubDecorator::getDesc().append(cheese_desc());
	}
	double getCost()
	{
		return SubDecorator::getCost() + cheese_cost;
	}
};

class VegeDecorator : public SubDecorator
{
private:
	double vege_cost;
	string vege_desc()
	{
		return " + Vege ";
	}
public:
	VegeDecorator(Sandwich *sandwich):
	SubDecorator(sandwich)
	{
		vege_cost = 1.0;
	}
	string getDesc()
	{
		return SubDecorator::getDesc().append(vege_desc());
	}
	double getCost()
	{
		return SubDecorator::getCost() + vege_cost;
	}
};

class SauceDecorator : public SubDecorator
{
private:
	double sauce_cost;
	string sauce_desc()
	{
		return " + Sauce ";
	}
public:
	SauceDecorator(Sandwich *sandwich):
	SubDecorator(sandwich)
	{
		sauce_cost = 3.0;
	}
	string getDesc()
	{
		return SubDecorator::getDesc().append(sauce_desc());
	}
	double getCost()
	{
		return SubDecorator::getCost() + sauce_cost;
	}
};

#endif