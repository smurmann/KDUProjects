#include <iostream>
#include "windows.h"
//#include <cstlib>
#include <ctime>

using namespace std;

//! roll a six sided dice 6,000,000 times

int main ()
{
    const int arraySize = 7;
    int frequency[arraySize] = {}; //!initialize the element to 0

    srand(time(0)); //!seed random number gen

    //! roll dice 6mil times.

    for (int roll = 1; roll<=600000000; ++roll)
    {
        ++frequency[1+rand()%6];
    }
    cout << "Face\t\t" << "frequency" << endl;

    for (int face = 1; face < arraySize; ++face)
    {
        cout << face << "\t\t" << frequency[face] << endl;
    }
}
