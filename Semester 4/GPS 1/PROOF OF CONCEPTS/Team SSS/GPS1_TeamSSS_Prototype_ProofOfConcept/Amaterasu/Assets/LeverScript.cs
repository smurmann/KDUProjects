﻿using UnityEngine;
using System.Collections;

public class LeverScript : MonoBehaviour 
{
	[SerializeField] Sprite LeverOnSprite;
	[SerializeField] GameObject TargetObject;
	[SerializeField] Vector3 targetPosition2;
	bool collidingWithPlayer;
	private bool activated;
	Rect textArea;
	private GameObject player;
	Player playerScript;
	// Use this for initialization
	void Start () 
	{
		collidingWithPlayer=false;
		activated=false;

		Vector2 screenPos= Camera.main.WorldToScreenPoint(transform.position);
		screenPos.y*=2;
		textArea=new Rect(screenPos.x,screenPos.y,200,200);	
	}
	
	// Update is called once per frame
	void Update () 
	{
		player = GameObject.Find("/Player");
		playerScript=player.GetComponent<Player>();
		if(activated)
		{
			TargetObject.gameObject.transform.position=Vector3.MoveTowards(TargetObject.gameObject.transform.position,targetPosition2,3f*Time.deltaTime);
		}

	}

	void OnTriggerStay2D(Collider2D other) 
	{
		if(other.gameObject.tag=="Player")
		{
			collidingWithPlayer=true;
		}

	}
	void OnTriggerExit2D(Collider2D other) 
	{
		if(other.gameObject.tag=="Player")
		{
			collidingWithPlayer=false;
		}
	}
	void OnGUI()
	{
		if(collidingWithPlayer&!activated)
		GUI.Label(textArea,"Activate");
	}
	void OnMouseOver()
	{
		if(Input.GetMouseButtonDown(0)&!activated)
		{
			if(collidingWithPlayer)
			{
				activated=true;
				gameObject.GetComponent<SpriteRenderer>().sprite = LeverOnSprite;
				playerScript.targetX = playerScript.transform.position.x;
			}
		}
	}
}
