// Facade1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

class Engine
{
public:
	void start()
	{
		cout << "Start" << endl;
	}
};

class Headlights
{
public:
	void turnOn()
	{
		cout << "Turn On" << endl;
	}
};

//Facade class
class Car
{
private:
	Engine engine;
	Headlights headlights;
public:
	void turnKeyOn()
	{
		engine.start();
		headlights.turnOn();
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	Car myCar;
	myCar.turnKeyOn();
	system("PAUSE");
	return 0;
}

