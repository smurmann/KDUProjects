// Observer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MusicBox.h"
#include <iostream>
#include <windows.h>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Trigger trigger1;
	Trigger trigger2;

	MusicBox mBox;
	trigger1.AddObserver(&mBox);
	bool isExit = true;

	while (true)
	{
		char input;
		cin >> input;
		if (input == '1')
		{
			trigger1.setState(true);
			isExit = true;
		}
		else if (input == 'q')
		{
			trigger1.setState(false);
			isExit = true;
		}
	}while (!isExit)
	{

	}


	system("PAUSE");
	return 0;
}

