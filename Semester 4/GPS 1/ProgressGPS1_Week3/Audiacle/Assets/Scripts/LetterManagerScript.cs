﻿using UnityEngine;
using System.Collections;

public class LetterManagerScript : MonoBehaviour 
{
	public Sprite Untyped_A;
	public Sprite Untyped_B;
	public Sprite Untyped_C;
	public Sprite Untyped_D;
	public Sprite Untyped_E;
	public Sprite Untyped_F;
	public Sprite Untyped_G;
	public Sprite Untyped_H;
	public Sprite Untyped_I;
	public Sprite Untyped_J;
	public Sprite Untyped_K;
	public Sprite Untyped_L;
	public Sprite Untyped_M;
	public Sprite Untyped_N;
	public Sprite Untyped_O;
	public Sprite Untyped_P;
	public Sprite Untyped_Q;
	public Sprite Untyped_R;
	public Sprite Untyped_S;
	public Sprite Untyped_T;
	public Sprite Untyped_U;
	public Sprite Untyped_V;
	public Sprite Untyped_W;
	public Sprite Untyped_X;
	public Sprite Untyped_Y;
	public Sprite Untyped_Z;

	public Sprite Typed_A;
	public Sprite Typed_B;
	public Sprite Typed_C;
	public Sprite Typed_D;
	public Sprite Typed_E;
	public Sprite Typed_F;
	public Sprite Typed_G;
	public Sprite Typed_H;
	public Sprite Typed_I;
	public Sprite Typed_J;
	public Sprite Typed_K;
	public Sprite Typed_L;
	public Sprite Typed_M;
	public Sprite Typed_N;
	public Sprite Typed_O;
	public Sprite Typed_P;
	public Sprite Typed_Q;
	public Sprite Typed_R;
	public Sprite Typed_S;
	public Sprite Typed_T;
	public Sprite Typed_U;
	public Sprite Typed_V;
	public Sprite Typed_W;
	public Sprite Typed_X;
	public Sprite Typed_Y;
	public Sprite Typed_Z;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
