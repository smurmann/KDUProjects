#include <iostream>
#include <windows.h>

using namespace std;

int main ()
{
    int a = 10;
    int b = 3;
    cout << "a " << a << endl;
    cout << "b: " << b << endl;
    cout << endl;

    //!arimethic
    cout << "a + b: " << a+b << endl;
    cout << "a - b: " << a-b << endl;
    cout << "a * b: " << a*b << endl;
    cout << "a / b: " << a/b << endl;
    cout << "a % b: " << a%b << endl;

    //! ++ --

    cout << "a++:" << a++ << endl;
    cout <<"a:" << a << endl;

    cout << "a--:" << a-- << endl;
    cout <<"a:" << a << endl;
\
    cout << "++a:" << ++a << endl;
    cout << "--a:" << --a << endl;

    cout << endl;
    a +=5;
    cout << "a+=5:" << a << endl;
    a -=5;
    cout << "a-=5:" << a << endl;
    a *=5;
    cout << "a*=5:" << a << endl;
    a /=5;
    cout << "a/=5:" << a << endl;
    a %=5;
    cout << "a%=5:" << a << endl;
    cout << endl;

    a = 5;
    cout << "a:" << a << endl;
    a<<=1;
    cout << "a <<=1:" << a << endl;
    a>>=2;
    cout << "a >>=2:" << a << endl;

    //! Not
    bool isGameAwesome = true;
    cout << "isGameAwesome: " << isGameAwesome << endl;
    //! negation or toggle
    isGameAwesome = !isGameAwesome;
    cout << "isGameAWesome: " << isGameAwesome << endl;
    bool isExit = false;
    do
    {
        for (int i=0; i<5; i++)
        {
            cout << "yes!" << endl;
        }
        isExit = true;
    }while(!isExit);


    system("PAUSE");
    return 0;
}
