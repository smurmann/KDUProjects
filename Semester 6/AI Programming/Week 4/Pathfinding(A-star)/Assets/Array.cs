﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Array : MonoBehaviour
{

    public int X, Z;
    public GameObject tile;

    public GameObject[,] array;
    public List<GameObject> openList = new List<GameObject>();

    public bool endFound = false;

    public bool diagonal = false;


    bool pathFound = false;
    void SpawnArray(int x, int z)
    {
        var size = tile.GetComponent<MeshRenderer>().bounds.size.x;
        var container = new GameObject();

        array = new GameObject[x, z];
        container.name = "ArrayContainer";
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < z; j++)
            {
                var NewObject = Instantiate(tile, new Vector3(size * i - (size * x - 1) / 2, size * j - (size * z - 1) / 2, 0), Quaternion.identity) as GameObject;
                NewObject.transform.parent = container.transform;

                NewObject.GetComponent<Tile>().TilePosX = i;
                NewObject.GetComponent<Tile>().TilePosZ = j;
                NewObject.GetComponent<Tile>().distance = 99999;

                array[i, j] = NewObject;
            }
        }
    }



    // Use this for initialization
    void Start()
    {
        SpawnArray(X, Z);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            FindStartandEnd();
        }

    }

    void FindStartandEnd()
    {
        var StartFound = false;
        var EndFound = false;

        var startPos = new int[2];
        var endPos = new int[2];
        openList = new List<GameObject>();

        foreach (GameObject tile in array)
        {
            var tileScript = tile.GetComponent<Tile>();
            if (tileScript.state == Tile.TileState.StartNode)
            {
                StartFound = true;
                startPos[0] = tileScript.TilePosX;
                startPos[1] = tileScript.TilePosZ;
                openList.Add(tile);
                tileScript.distance = 0;
                tileScript.addedToOpenList = true;
            }
            else if (tileScript.state == Tile.TileState.EndNode)
            {
                EndFound = true;
                endPos[0] = tileScript.TilePosX;
                endPos[1] = tileScript.TilePosZ;
            }
        }

        if (!StartFound || !EndFound)
        {
            Debug.LogError("either start or end is missing");
            return;
        }
        else
        {
            foreach (GameObject tile in array)
            {
                var tileScript = tile.GetComponent<Tile>();
                tileScript.distance = Mathf.Infinity;
            }
        }

        //PathFind(tileInArray(startPos[0], startPos[1]), null, tileInArray(startPos[0], startPos[1]), tileInArray(endPos[0], endPos[1]), array);
        do
        {

            AStar();
            //PathFind();

        } while (openList.Count > 0 && endFound == false);




    }

    void AStar()
    {
        //int lowestValue = openList.Min(car => car.GetComponent<Tile>().distance);

        //Debug.Log(lowestValue);

      //  openList.OrderBy(temp => temp.GetComponent<Tile>().distance);
        var tempNode = openList[0];

        //openList.RemoveAt(openList.First<GameObject>());
        openList.Remove(openList.First<GameObject>());

        if (tempNode.GetComponent<Tile>().state == Tile.TileState.EndNode)
        {
            endFound = true;
            Debug.Log("found end");
            //return;
        }

        tempNode.GetComponent<Tile>().state = Tile.TileState.Settled;
        var nodeScript = tempNode.GetComponent<Tile>();


        // if (nodeScript.TilePosX < array.GetLength(0) - 1 || nodeScript.TilePosZ < array.GetLength(1) - 1 ||
        //     nodeScript.TilePosX > 0 || nodeScript.TilePosZ > 0)
        {
            var tempNeighbours = new List<GameObject>();
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ));
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ));
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX, nodeScript.TilePosZ + 1));
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX, nodeScript.TilePosZ - 1));

            if (diagonal)
            {
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ + 1));
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ - 1));
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ + 1));
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ - 1));
            }



            foreach (GameObject tempObj in tempNeighbours)
            {
                if (tempObj != null)
                {
//                    Debug.Log(tempObj);
                    var childNode = tempObj;
                    var childNodeScript = tempObj.GetComponent<Tile>();
                    if (childNodeScript.state != Tile.TileState.Settled && childNodeScript.state != Tile.TileState.Obstacle)
                    {
                        if (childNodeScript.distance >= nodeScript.distance)
                        {
                            childNodeScript.distance = nodeScript.distance + 1;
                            childNodeScript.parentNode = tempNode;


                        }
                        if (!childNodeScript.addedToOpenList)
                        {
                            openList.Add(childNode);
                        }

                        if (childNodeScript.state == Tile.TileState.EndNode)
                        {
                            endFound = true;
                            TracePath(childNode);
                        }
                    }
                }
                //             
            }


        }
    }


    void PathFind()
    {
        //int lowestValue = openList.Min(car => car.GetComponent<Tile>().distance);

        //Debug.Log(lowestValue);

        var tempNode = openList[0];
        //openList.RemoveAt(openList.First<GameObject>());
        openList.Remove(openList.First<GameObject>());

        if (tempNode.GetComponent<Tile>().state == Tile.TileState.EndNode)
        {
            endFound = true;
            Debug.Log("found end");
            //return;
        }

        tempNode.GetComponent<Tile>().state = Tile.TileState.Settled;
        var nodeScript = tempNode.GetComponent<Tile>();


        // if (nodeScript.TilePosX < array.GetLength(0) - 1 || nodeScript.TilePosZ < array.GetLength(1) - 1 ||
        //     nodeScript.TilePosX > 0 || nodeScript.TilePosZ > 0)
        {
            var tempNeighbours = new List<GameObject>();
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ));
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ));
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX, nodeScript.TilePosZ + 1));
            tempNeighbours.Add(tileInArray(nodeScript.TilePosX, nodeScript.TilePosZ - 1));

            if (diagonal)
            {
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ + 1));
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ - 1));
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ + 1));
                tempNeighbours.Add(tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ - 1));
            }



            foreach (GameObject tempObj in tempNeighbours)
            {
                if (tempObj != null)
                {
                    Debug.Log(tempObj);
                    var childNode = tempObj;
                    var childNodeScript = tempObj.GetComponent<Tile>();
                    if (childNodeScript.state != Tile.TileState.Settled && childNodeScript.state != Tile.TileState.Obstacle)
                    {
                        if (childNodeScript.distance >= nodeScript.distance)
                        {
                            childNodeScript.distance = nodeScript.distance + 1;
                            childNodeScript.parentNode = tempNode;


                        }
                        if (!childNodeScript.addedToOpenList)
                        {
                            openList.Add(childNode);
                        }

                        if (childNodeScript.state == Tile.TileState.EndNode)
                        {
                            endFound = true;
                            TracePath(childNode);
                        }
                    }
                }
                //             
            }


        }
    }





    //void PathFind(GameObject StartNode, GameObject previousNode, GameObject node, GameObject endNode, GameObject[,] tileArray)
    //{

    //    if (node == null)
    //    {
    //        return;
    //    }
    //    var endScript = endNode.GetComponent<Tile>();
    //    var nodeScript = node.GetComponent<Tile>();
    //    nodeScript.parentNode = previousNode;

    //    //        Debug.Log("Searching X:" + nodeScript.TilePosX + " Y:" + nodeScript.TilePosZ);

    //    if (nodeScript.parentNode != null)
    //    {
    //        return;
    //    }

    //    if (endScript.TilePosX == nodeScript.TilePosX && endScript.TilePosZ == nodeScript.TilePosZ)
    //    {
    //        nodeScript.parentNode = previousNode;

    //        if(!pathFound)
    //        {
    //            TracePath(tileInArray(endScript.TilePosX, endScript.TilePosZ));
    //        }
    //        Debug.Log("PathFound");
    //        return;
    //    }




    //    if (node != StartNode)
    //    {
    //        if (nodeScript.parentNode == null)
    //        {
    //            //nodeScript.parentNode = previousNode;
    //        }
    //    }
    //    else if (node == endNode)
    //    {
    //        nodeScript.parentNode = previousNode;
    //        TracePath(endNode);
    //        Debug.Log("PathFound");
    //        return;
    //    }

    //if (nodeScript.TilePosX<array.GetLength(0) - 1 || nodeScript.TilePosZ<array.GetLength(1) - 1 ||
    //    nodeScript.TilePosX > 0 || nodeScript.TilePosZ > 0)
    //{
    //    PathFind(StartNode, node, tileInArray(nodeScript.TilePosX + 1, nodeScript.TilePosZ), endNode, tileArray);
    //    PathFind(StartNode, node, tileInArray(nodeScript.TilePosX - 1, nodeScript.TilePosZ), endNode, tileArray);
    //    PathFind(StartNode, node, tileInArray(nodeScript.TilePosX, nodeScript.TilePosZ + 1), endNode, tileArray);
    //    PathFind(StartNode, node, tileInArray(nodeScript.TilePosX, nodeScript.TilePosZ - 1), endNode, tileArray);
    //}


    //}

    void TracePath(GameObject end)
    {
        var temp = end.GetComponent<Tile>().parentNode;
        if (temp == null)
        {
//            Debug.Log(end.GetComponent<Tile>().TilePosX + "" + end.GetComponent<Tile>().TilePosZ);
            end.GetComponent<Tile>().currentState = 3;
            return;
        }
        temp.GetComponent<Tile>().state = Tile.TileState.Path;
        temp.GetComponent<Tile>().currentState = 4;
        temp.GetComponent<MeshRenderer>().material.color = Color.yellow;
        TracePath(temp);
    }

    GameObject tileInArray(int x, int z)
    {
        if (x > array.GetLength(0) - 1 || z > array.GetLength(1) - 1 ||
            x < 0 || z < 0)
        {
            return null;
        }
        else
        {
            return array[x, z];

        }
    }
}
