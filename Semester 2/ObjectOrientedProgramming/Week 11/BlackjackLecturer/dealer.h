#include "random.h"

class Dealer
{
private:
    int mTotal;
    int mInsurance;
public:
    //! Construcor
    Dealer();
    int deal(int);
    int game(int);
    int getTotal();
    void setTotal(int);
    void setInsurance(int);
    int getInsurance();
    //! OOP concept - encapsulation
    //! make all the data attribute to private
    //! provide public set get method to manipulate the
    //! the private data attribute
};
