﻿using UnityEngine;
using GoogleMobileAds.Api;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class mainScript : MonoBehaviour
{

    Camera camera;
    public GameObject m_projectilePrefab;
    int count = 0;

    private InterstitialAd m_interstitial;
    bool authenticateSuccess = false;

    private void RequestBanner()
    {
        string adUnitId = "ca-app-pub-1862735857055195/7253248660";

        BannerView bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

        AdRequest request = new AdRequest.Builder().Build();

        bannerView.LoadAd(request);
    }

    // Use this for initialization
    void Start()
    {

        // Build google play config
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        PlayGamesPlatform.InitializeInstance(config);

        // Activate the google play games platform
        PlayGamesPlatform.Activate();

        // authenticate user:
        Social.localUser.Authenticate((bool success) =>
        {
            // handle success or failure
            if (success)
            {
                //Social.ShowAchievementsUI();
                Social.ShowLeaderboardUI();
            }
        });

        camera = GetComponent<Camera>();
        RequestBanner();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.GetTouch(0);

            foreach (Touch touch in Input.touches)
            //  if(touch.phase == TouchPhase.Began)
            {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);

                GameObject newObject = Instantiate(m_projectilePrefab, ray.origin, Quaternion.identity) as GameObject;
                count++;
                Debug.Log(count);
                Rigidbody newObjectRigidBody = newObject.GetComponent<Rigidbody>();
                newObjectRigidBody.velocity = ray.direction * 15.0f;

            }
        }

        if (Input.touchCount == 2)
        {
            Social.ReportScore((long)Random.Range(0, 10000), "CgkItamoz68OEAIQBg", (bool success) =>
            {
                if(success)
                {
                    Social.ShowLeaderboardUI();
                }
                else
                {

                }

            });

        }

    }
}
