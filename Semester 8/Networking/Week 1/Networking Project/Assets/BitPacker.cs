﻿using UnityEngine;
using System.Collections;

public class BitPacker
{
    uint m_data;
    uint m_bitCount;

    public bool pack(uint value, int bitCount)
    {
        m_data = m_data << bitCount;
        m_data = m_data | value;

        //m_bitCount = m_bitCount | bitCount;
        //m_bitCount = m_bitCount << 4;

        //Debug.Log(System.Convert.ToString(m_data, 2));
        return true;
    }

    public uint extract(int bitCount)
    {
        var returnVal = m_data & (uint)(Mathf.Pow(2,bitCount)-1);
        //Debug.Log(System.Convert.ToString((uint)(Mathf.Pow(2,bitCount) - 1)));
        //Debug.Log(System.Convert.ToString(m_data, 2));
        m_data = m_data >> bitCount;
        //Debug.Log(System.Convert.ToString(m_data, 2));

        return returnVal;
    }

    public BitPacker()
    {
        m_bitCount = new uint();
        m_data = new uint();
    }
}
