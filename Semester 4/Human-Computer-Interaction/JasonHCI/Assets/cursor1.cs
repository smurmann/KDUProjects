﻿using UnityEngine;
using System.Collections;

public class cursor1 : MonoBehaviour
{
    public Texture2D cursorChange;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotspot = Vector2.zero;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
       
        Cursor.SetCursor(cursorChange, hotspot, cursorMode);
    }

}