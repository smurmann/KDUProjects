#include "DefaultHeaderFiles.h"

class Enemy : public GameObject
{
public:
    int killScore;
    bool isInTheScreen;

    //! type of bullet
    BULLET_TYPE bulletType;
    bool enableShooting;
    int shootDelayTimer;
    int ShootDelayDuration;

    Enemy();
    //! pure virtual
    virtual void Update(int elapsedTime) = 0;
};

