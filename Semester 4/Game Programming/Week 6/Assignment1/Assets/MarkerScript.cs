﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshCollider))]

public class MarkerScript : MonoBehaviour 
{
	
	private Vector3 screenPoint;
	private Vector3 offset;

	public Vector3 MarketPos;
	
	void OnMouseDown()
	{
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
		
	}
	
	void OnMouseDrag()
	{
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		
		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
		transform.position = curPosition;
		
	}

	void Update()
	{
		MarketPos = transform.position;

	}
	
}