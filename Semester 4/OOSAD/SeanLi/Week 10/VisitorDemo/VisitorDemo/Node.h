#ifndef _NODE_H
#define _NODE_H

#include "Actor.h"
#include <vector>

using namespace std;
class Node
{
protected:
	vector<Node*> mChildren;
	Node* mParent;
	vector<Actor*> mActor;
public:
	Node() : mParent(NULL){}
	//! add node as child
	void addChild(Node* node);
	//! append actor to node
	void appendActor(Actor* actor);
	//! return parent node
	inline Node* getParent()
	{
		return mParent;
	}
	//! traverse through hierarchy and notify visitor
	void accept(ActorVisitor* visitor);
};

#endif