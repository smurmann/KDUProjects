﻿using UnityEngine;
using System.Collections;

public class KeyScript : MonoBehaviour {
	GameObject player;
	Player playerScript;
	bool collidingWithPlayer=false;
	[SerializeField] ItemList ItemType;
	Rect textArea;
	// Use this for initialization
	void Start () 
	{
		player=GameObject.Find("/Player");
		playerScript = player.GetComponent<Player>();
		Vector2 screenPos= Camera.main.WorldToScreenPoint(transform.position);
		screenPos.y*=2;
		textArea=new Rect(screenPos.x,screenPos.y,200,200);	
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerStay2D(Collider2D other)
	{
		if(other.tag =="Player")
		{
			collidingWithPlayer=true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag =="Player")
		{
			collidingWithPlayer=false;
		}
	}
	void OnMouseDown()
	{
		if(collidingWithPlayer)
		{
			if(playerScript.currentItem==ItemList.Empty)
			{
				playerScript.currentItem=ItemType;
				Destroy(this.gameObject);
			}
		}
	}
	void OnGUI()
	{
		if(collidingWithPlayer)
			GUI.Label(textArea,"Pick up");
	}
}
