#include <string>
#include "Employee.h"
using namespace std;

class Manager : public Employee
{
protected:
    bool isSalaried;
public:
    Manager (string n, float rate, bool salary);

    bool getSalaried() const;

    //! poly - overloading
    float pay (float hoursWork) const;
};
