﻿using UnityEngine;
using System.Collections;

public class SwitchScript : MonoBehaviour {

	bool enable = true;

	public Sprite Up;
	public Sprite Pressed;

	SpriteRenderer button;

	public GameObject newAtom;
	public GameObject marker;

	Vector3 originalPos;
	public bool activeAtoms;

	
	// Use this for initialization
	void Start () {
		enable = true;
		button = GetComponent<SpriteRenderer> ();
		activeAtoms = false;

		originalPos = GameObject.Find ("Atom").GetComponent<AtomScript>().originalPos;
	
	}
	
	// Update is called once per frame
	void Update () {

		if(enable)
		{
			button.sprite = Up;
		}
		if(!enable)
		{
			button.sprite = Pressed;
		}
	
	}

	void OnMouseDown()
	{
		Debug.Log ("MouseDown!");
		if(enable)
		{
			enable = false;
			GameObject.FindGameObjectWithTag ("Atom").GetComponent<AtomScript>().activeAtom = true;
			activeAtoms = true;

		}
		else if (!enable)
		{
			enable = true;
			GameObject[] gameObjects;
			gameObjects = GameObject.FindGameObjectsWithTag ("Atom");
			activeAtoms = false;

			for(var i = 0; i< gameObjects.Length; i++)
			{
				Destroy(gameObjects[i]);
			}

			GameObject.FindGameObjectWithTag ("Atom").GetComponent<AtomScript>().activeAtom = false;


			//GameObject.FindGameObjectsWithTag ("Atom").GetComponent<AtomScript>().Destroy();
			//GameObject spawnAtom = Instantiate(newAtom, originalPos, Quaternion.identity)as GameObject;
			GameObject spawnAtom = Instantiate(newAtom, new Vector3(0,0,0), Quaternion.identity)as GameObject;
		}
	}

	public void ToggleOff()
	{
		if(!enable)
		{
			enable = true;
			button.sprite = Up;
		}
	}

}
