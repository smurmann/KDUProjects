// PizzaBuilder.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include "stdafx.h"
#include "windows.h"
#include <string>

using namespace std;

class Pizza
{
private:
	string dough;
	string sauce;
	string topping;
public:
	void setDough(const string &d)
	{
		dough = d;
	}
	void setSauce(const string &s)
	{
		sauce = s;
	}
	void setTopping(const string &t)
	{
		topping = t;
	}
	void Display()
	{
		cout << "Dough : " << dough << endl;
		cout << "Sauce : " << sauce << endl;
		cout << "Topping : " << topping << endl;

	}
};

//!abstract pizza builder
class PizzaBuilder
{
protected:
	Pizza* pizza;
public:
	Pizza* getPizza()
	{
		return pizza;
	}
	//! create new Pizza
	void CreateNewPizza()
	{
		pizza = new Pizza();
	}
	virtual void buildDough() = 0;
	virtual void buildSauce() = 0;
	virtual void buildTopping() = 0;
};

class MamakPizzaBuilder :public PizzaBuilder
{
public:
	void buildDough()
	{
		pizza->setDough("naan");
	}
	void buildSauce()
	{
		pizza->setSauce("curry");

	}
	void buildTopping()
	{
		pizza->setTopping("cheese");

	}
};
//! Director
class Cook
{
private:
	PizzaBuilder* pizzaBuilder;
public:
	void SetPizzaBuilder(PizzaBuilder* pb)
	{
		pizzaBuilder = pb;

	}
	Pizza* getPizzaBuilder()
	{
		return pizzaBuilder->getPizza();
	}
	void cookPizza()
	{
		pizzaBuilder->CreateNewPizza();
		pizzaBuilder->buildDough();
		pizzaBuilder->buildSauce();
		pizzaBuilder->buildTopping();
	}
};
int _tmain(int argc, _TCHAR* argv[])
{
	Cook cook;
	PizzaBuilder *pizzaBuilder = new MamakPizzaBuilder();
	cout << "Mamak Pizza " << endl;
	cook.SetPizzaBuilder(pizzaBuilder);
	cook.cookPizza();
	Pizza* kris = cook.getPizzaBuilder();
	kris->Display();
	cout << endl;

	system("PAUSE");
	return 0;
}

