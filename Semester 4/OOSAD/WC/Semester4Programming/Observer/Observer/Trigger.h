#ifndef _TRIGGER_H
#define _TRIGGER_H

#include "Subject.h"

class Trigger :public Subject
{
protected:
	//! trigger state
	bool mIsOn;
public:
	Trigger() :mIsOn(false)
	{


	}
	//! GET  trigger
	inline bool getState()
	{

		return mIsOn;

	}
	//! SET trigger
	void setState(bool on)
	{
		if (mIsOn == on)return;
		mIsOn = on;
		Notify();

	}

};


#endif