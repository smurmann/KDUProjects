﻿using UnityEngine;
using System.Collections;

public class TileScript : MonoBehaviour {

	public Sprite m_SpriteState_0;
	public Sprite m_SpriteState_1;
	private int m_state = -1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	
	}

	public void setState(int state)
	{
		SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
		if(state == 0)
		{
			sr.sprite = m_SpriteState_0;
			m_state = 0;
		}
		else if(state == 1)
		{
			sr.sprite = m_SpriteState_1;
			m_state = 1;
		}

   }
}
