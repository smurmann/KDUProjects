/*
 * main.cpp
 *
 *  Created on: Jul 12, 2014
 *      Author: Sean-Li Murmann
 */
#include <iostream>
#include "CardDeck.h"

using namespace std;

int main() {
	CardDeck box;
	box.deal();
	cout << endl;
	box.shuffle();
	box.deal();
	return 0;
}
