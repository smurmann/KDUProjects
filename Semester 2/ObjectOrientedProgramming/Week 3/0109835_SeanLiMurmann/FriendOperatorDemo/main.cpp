#include <iostream>
#include <string>

using namespace std;

class Critter
{
    //! Make folowing global functions of the Critter Class
    friend void Peek(const Critter& aCritter);
    friend ostream& operator<<(ostream& os, const Critter& aCritter);

    public:
        Critter(const string& name="");
    private:
        string mName;
};
Critter::Critter(const string& name) :
    mName(name)
{}

void Peek(const Critter& aCritter);
ostream& operator<<(ostream& os, const Critter& aCritter);

int main()
{
    Critter crit("Pikachu");
    cout<<"Calling Peek() go access crit's private member: "<<endl;
    //! crit.GetName(); crit.name
    Peek(crit);
    cout<<endl<<"Sending crit object to cout with the << operator: "<<endl;
    cout<<crit<<endl;
    return 0;
}

//! global friend function which can
//! access all of the Critter's object's member
void Peek (const Critter& aCritter)
{
    cout<<aCritter.mName<<endl;
}
//! overload the << operator,
//! so you can send a critter object to cout
ostream& operator<<(ostream& os, const Critter& aCritter)
{
    //!cout<< , overload the cout operator
    os<<"Critter Object - "<<endl;
    os<<"Name "<<aCritter.mName << " in the farm";
    return os;
}
