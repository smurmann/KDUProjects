#include "Manager.h"
#include <string>
using namespace std;

Manager::Manager(string n, float rate,bool salary)
    : Employee(n,rate)
{
    isSalaried = salary;
}

bool Manager::getSalaried() const
{
    return isSalaried;
}

float Manager::pay(float hoursWork) const
{
    if(getSalaried())
    {
        return payRate;
    }
    return Employee::pay(hoursWork);
}
