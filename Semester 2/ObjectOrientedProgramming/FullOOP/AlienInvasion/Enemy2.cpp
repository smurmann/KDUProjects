#include "DefaultHeaderFiles.h"

Enemy02::Enemy02()
{
    x = 35;
    y= 5;
    row = 3;
    col = 3;
    speedX = 1;
    speedY = 1;
    colour = YELLOW;
    isInTheScreen = false;

    enableShooting = false;
    bulletType = DUAL;

    updateDelayTimer = 0;
    updateDelayDuration = 60;

    char tempSprite[3][3] = {
                                {'x','1','x'},
                                {'\\','1','/'},
                                {' ','v',' '}
                            };
    sprite = new char*[row];
    for(int i=0; i<row; i++)
    {
        sprite[i] = new char[col];
        for(int j=0; j<col; j++)
        {
            sprite[i][j] = tempSprite[i][j];
        }
    }
}
void Enemy02::Update(int elapsedTime)
{
    updateDelayTimer += elapsedTime;
    shootDelayTimer += elapsedTime;

    if(updateDelayTimer < updateDelayDuration)
    {
        return;
    }
    updateDelayTimer %= updateDelayDuration;

    MoveRight();

    if(shootDelayTimer > shootDelayDuration)
    {
        enableShooting = true;
        shootDelayTimer = 0;
    }
}


