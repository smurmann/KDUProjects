// StrategyDemo2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "windows.h"

using namespace std;

class QuackBehavior
{
public:
	virtual void quack() = 0;

};

class MuteQuack :public QuackBehavior
{
public:
	void quack()
	{
		cout << "The Duck says : Miao !" << endl;
	}

};

class Quack :public QuackBehavior
{
public:
	void quack()
	{
		cout << "The Duck says : Quack !" << endl;
	}

};

class FlyBehavior
{
public:
	void fly()
	{
		cout << "Duck flies into the bird nest " << endl;
	}
};

class FlyNoWay :public FlyBehavior
{
public:
	void fly()
	{
		cout << "Duck can't fly!! " << endl;
	}

};
class FlyWithRocket :public FlyBehavior
{
public:
	void fly()
	{
		cout << "Duck flies with rocket " << endl;
	}

};

class Duck
{
public:
	QuackBehavior *quackbehavior;
	FlyBehavior * flybehavior;

	void performQuack()
	{
		quackbehavior->quack();
	}
	void setQuackBehavior(QuackBehavior *qb)
	{
		cout << "changing quack behavior..." << endl;
		quackbehavior = qb;
	}
	void performFly()
	{
		flybehavior->fly();
	}
	void setFlyBehavior(FlyBehavior * fb)
	{
		cout << "changing fly behavior..." << endl;
		flybehavior = fb;
	}
	virtual void display() = 0;
};

class Sam :public Duck
{
public:
	Sam()
	{
		quackbehavior = new Quack();
		flybehavior = new FlyBehavior();
	}
	void display()
	{
		cout << "Sam is an adorable duck " << endl;
	}

};
class Mody :public Duck
{
public:
	Mody()
	{
		quackbehavior = new MuteQuack();
		flybehavior = new FlyNoWay();

	}
	void display()
	{
		cout << "Mody cant fly because the ball is too heavy" << endl;
	}

};

int _tmain(int argc, _TCHAR* argv[])
{
	Duck * kong = new Sam();
	kong->display();
	kong->performQuack();
	kong->performFly();
	kong->setQuackBehavior(new MuteQuack);
	kong->performQuack();
	kong->performFly();

	system("PAUSE");
	return 0;
}

