#include "DefaultHeaderFiles.h"

class Enemy : public GameObject
{
public:
    int killScore;
    bool isInTheScreen;

    //! type of bullet
    BULLET_TYPE bulletType;
    bool enableShooting;
    int shootDelayTimer;
    int shootDelayDuration;

    Enemy();
    //! pure virtual functions
    virtual void Update(int elapsedTime) = 0;
};
