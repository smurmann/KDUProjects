#ifndef BLENDING_DEMO_H
#define BLENDING_DEMO_H

#define TEXTURE_COUNT 3

#include "demo_base.h"
#include "bitmap.h"


class BlendingDemo : public DemoBase
{
private:
	//GLfloat mRectVertices[18];
	GLuint mTextureID[TEXTURE_COUNT];

	void loadTexture(const char* path, GLuint textureID)
	{
		CBitmap bitmap(path);

		//Create Linear Filtered Texture
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // apply texture wrapping along horizontal part.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // apply texture wrapping along vertical part.

		//old school (minecraft) filtering.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // near filtering. (For when texture needs to sscale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // far filtering. (For when texture needs to sscale down on screen)

		/*
		bilinear filtering.		// for testing bilinear filtering on our own time
		glTextParametri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // near filtering. (For when texture needs to scale up on screen)
		glTextParametri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // far filtering. (For when texture needs to scale down on screen)
		*/

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.GetWidth(), bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.GetBits());
	}

public:
	float m_rotation;
	Vector m_translation;
	virtual void init()
	{
		glGenTextures(TEXTURE_COUNT, mTextureID);
		loadTexture("../media/glass.bmp", mTextureID[0]);
		loadTexture("../media/background.bmp", mTextureID[1]);
		loadTexture("../media/rocks.bmp", mTextureID[2]);
	}

	virtual void deinit()
	{
	}

	void drawWorldOriginLines(const Matrix& viewMatrix)
	{

	}

	void drawBackground()
	{
		float halfSize = 5.0f;
		static GLfloat vertices[] =
		{
			-halfSize, -halfSize, 0,
			halfSize, -halfSize, 0,
			halfSize, halfSize, 0,
			halfSize, halfSize, 0,
			-halfSize, halfSize, 0,
			-halfSize, -halfSize, 0
		};

		static GLfloat texCoords[] =
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f
		};

		static GLubyte colors[] =
		{
			255, 255, 255,
			255, 255, 255,
			255, 255, 255,
			255, 255, 255,
			255, 255, 255,
			255, 255, 255,
		};

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mTextureID[1]);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
		glColorPointer(3, GL_UNSIGNED_BYTE, 0, colors);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glDisable(GL_TEXTURE_2D);
	}

	void drawForeground()
	{
		float halfSize = 1.0f;
		static GLfloat vertices[] =
		{
			//-halfSize, -halfSize, 1,
			//halfSize, -halfSize, 1,
			//halfSize, halfSize, 1,
			//halfSize, halfSize, 1,
			//-halfSize, halfSize, 1,
			//-halfSize, -halfSize, 1

			- 1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			-1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
		};

		static GLfloat texCoords[] =
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f
		};

		static GLubyte indicies[] =
		{
			0, 1, 2, //1st triangle
			2, 3, 0, //2nd triangle

			4, 5, 6,
			6, 7, 4,

			3, 2, 6,
			6, 7, 3,

			0, 1, 5,
			5, 4, 0,
		};

		static GLubyte colors[] =
		{
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
		};

		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mTextureID[0]);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
		glColorPointer(4, GL_UNSIGNED_BYTE, 0, colors);

		glDrawElements(GL_TRIANGLES, 6 * 4, GL_UNSIGNED_BYTE, indicies);

		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //alpha blending
		//glBlendFunc(GL_ONE, GL_ONE); //additive blending
		glBlendFunc(GL_DST_COLOR, GL_ZERO); //multiply blending
		//glBlendFunc(GL_ZERO, GL_SRC_COLOR); //also multiply blending

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisable(GL_BLEND);

		glDisable(GL_TEXTURE_2D);
	}

	void drawForeground1()
	{

		float halfSize = 1.0f;
		static GLfloat vertices[] =
		{
			//-halfSize, -halfSize, 1,
			//halfSize, -halfSize, 1,
			//halfSize, halfSize, 1,
			//halfSize, halfSize, 1,
			//-halfSize, halfSize, 1,
			//-halfSize, -halfSize, 1

			//-1.0f, -1.0f, 1.0f,
			//-1.0f, 1.0f, 1.0f,
			//1.0f, 1.0f, 1.0f,
			//1.0f, -1.0f, 1.0f,

			//-1.0f, -1.0f, -1.0f,
			//-1.0f, 1.0f, -1.0f,
			//1.0f, 1.0f, -1.0f,
			//1.0f, -1.0f, -1.0f,

			1,1,1, -1,1,1, -1,-1,1, 1,-1,1,
			1,1,1, 1,-1,1, 1,-1,-1, 1,1,-1,
			1,1,1, 1,1,-1, -1,1,-1, -1,1,1,
			-1,1,1, -1,1,-1, -1,-1,-1, -1,-1,1,
			1,-1,-1, -1,-1,-1, -1,1,-1, 1,1,-1
		};

		static GLfloat texCoords[] =
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f
		};

		static GLubyte indicies[] =
		{
			//0, 1, 2, //1st triangle
			//2, 3, 0, //2nd triangle

			//4, 5, 6,
			//6, 7, 4,

			//3, 2, 6,
			//6, 7, 3,

			//0, 1, 5,
			//5, 4, 0,
			0, 1, 2, 2, 3, 0, 
			4, 5, 6, 6, 7, 4,
			8, 9, 10, 10, 11,
			8, 12, 13, 14, 15, 12,
			16, 17, 18, 18, 19, 16, 
			20, 21, 22, 22, 23, 20
		};

		static GLubyte colors[] =
		{
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60,
			255, 255, 255, 60
		};

		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mTextureID[0]);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
		glColorPointer(4, GL_UNSIGNED_BYTE, 0, colors);

		glDrawElements(GL_TRIANGLES, 6 * 4, GL_UNSIGNED_BYTE, indicies);

		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //alpha blending
		glBlendFunc(GL_ONE, GL_ONE); //additive blending
		//glBlendFunc(GL_DST_COLOR, GL_ZERO); //multiply blending
		//glBlendFunc(GL_ZERO, GL_SRC_COLOR); //also multiply blending

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisable(GL_BLEND);

		glDisable(GL_TEXTURE_2D);
	}

	virtual void draw(const Matrix& viewMatrix)
	{
		//draw axis


		glLoadMatrixf((GLfloat*)viewMatrix.mVal);

		drawBackground();
		drawForeground();

		//m_rotation += 0.1f*;
		m_translation.set(cos(m_rotation), sin(m_rotation), -0.5f);

		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(0.0f, 0.0f, 0.0f));

		//translate to origin
		//Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);

		Matrix translate1 = Matrix::makeTranslationMatrix(3.0f, 0.0f, 0.0f);

		Matrix modelMatrix = translate1;

		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		drawForeground1();

	}
};




#endif