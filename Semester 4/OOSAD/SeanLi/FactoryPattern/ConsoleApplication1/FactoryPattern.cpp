// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "AEmployee.h"
#include <Windows.h>

int _tmain(int argc, _TCHAR* argv[])
{
	AEmployee *emp =
		AEmployee::CreateEmployee(AEmployee::EMP_TYPE::HUMAN);
	cout << "Receivable for Human" << endl;
	emp->PrintBenefits();
	cout << endl;
	delete emp;

	emp =
		AEmployee::CreateEmployee(AEmployee::EMP_TYPE::SOLDIER);
	cout << "Receivable for Soldier" << endl;
	emp->PrintBenefits();
	cout << endl;
	system("PAUSE");
	return 0;
}
