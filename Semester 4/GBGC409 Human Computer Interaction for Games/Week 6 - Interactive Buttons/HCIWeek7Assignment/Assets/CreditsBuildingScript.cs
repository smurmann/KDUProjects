﻿using UnityEngine;
using System.Collections;

public class CreditsBuildingScript : MonoBehaviour {

	public GameObject creditButton;
	public GameObject creditsBuildingClicked;

	public GameObject Background;


	ButtonScript creditsButtonScript;

	float currentTime;
	float waitTime;

	bool spawned;


	// Use this for initialization
	void Start () {
		spawned = false;
		creditsButtonScript = creditButton.GetComponent<ButtonScript>();
		//reloadScript = Background.GetComponent<ReloadScript>();

		//reloadScript.numberOfActive++;
		
	
	}

	// Update is called once per frame
	void Update () {

		if(spawned == false)
		{
			if(creditsButtonScript.Button == ButtonScript.ButtonState.Disabled)
			{
				Instantiate(creditsBuildingClicked, transform.position, Quaternion.identity);
				//creditsButtonScript.Button = ButtonScript.ButtonState.Disabled;
				spawned = true;
			}

		}

	}
}
