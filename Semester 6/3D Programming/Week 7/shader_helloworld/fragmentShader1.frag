      precision mediump float;
	  varying vec4 fColor;
	  uniform float Factor1;
	  uniform float Factor2;
	  
	  varying vec2 fTexCoord;
	  
	  uniform sampler2D sampler2d;
	  
	  
	   float imageWidth = 1.0/128.0;
	   float imageHeight = 1.0/128.0;
	  
	  const float boxSize = 5.0;
      void main()             
      {		
		vec4 texColor = texture2D(sampler2d, fTexCoord);
		vec4 combinedColor = texColor;
		float neighbourPixels = 0.0;
		
		for(float w = -imageWidth*boxSize; w <= imageWidth*boxSize; w += imageWidth)
		{
			for(float h = -imageHeight*boxSize; h <= imageHeight*boxSize; h += imageHeight)
			{
				//if(w >= 0.0 && w <= 1.0 && h >= 0.0 && h <= 1.0)
				{
					vec2 neighbourUV = vec2(fTexCoord.x + w, fTexCoord.y + h);
				
					vec4 thePixelColor = texture2D(sampler2d, neighbourUV);
					combinedColor += thePixelColor;
					neighbourPixels += 1.0;
				}

			}
		}
		
		
			
		vec4 resultColor;
		resultColor = combinedColor / neighbourPixels;

		
		gl_FragColor = resultColor;
		
		
		//gl_FragColor = texture2D(sampler2d, fTexCoord);
       }                                            