﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Node
{
    public GameObject visual;
    public Node parent;
    /// <summary>
    /// G = the movement cost to move from the starting point A to a given node on the grid, 
    /// following the path generated to get there.
    /// </summary>
    public float g;
    /// <summary>
    /// H = the estimated movement cost to move from that given node on the grid to the final destination, point B. 
    /// This is often referred to as the heuristic, the reason why it is called that is because it is a guess.
    /// </summary>
    public float h;

    public int X, Y;

    public bool diagonal;

    public NodeScript.TileState m_state;


    public Node(GameObject visualObj)
    {
        visual = visualObj;
    }
    public void SetVisualColour(Color colour)
    { 
        if(visual.GetComponent<SpriteRenderer>().color != Color.red||
            visual.GetComponent<SpriteRenderer>().color != Color.green)
        visual.GetComponent<SpriteRenderer>().color = colour;
    }

    public void Setup(Node node, Vector3 location, Transform parent,int x, int y)
    {
        visual.GetComponent<NodeScript>().parentNode = node;
        visual.transform.parent = parent;
        visual.transform.position = location;
        X = x;
        Y = y;
    }
 
}

public class ArrayPoint
{

    public int X, Y;


    public void SetArrayPoint(int x, int y)
    {
        X = x;
        Y = y;
    }

    public ArrayPoint()
    {
        X = -1;
        Y = -1;
    }

}
