#include "DefaultHeaderFiles.h"

//! implement the constrctor from Player class
//! construct a player -> Draw a Player
//!  ^
//!X C X
//!X C X
Player::Player()
{
    //! coordinates for the player
    x = 35;
    y = 20;
    //! row and col for the 2D array
    row = 3;
    col = 3;
    //! speed for x and y -> increase or decrease x and y
    speedX = 1;
    speedY = 1;
    //! color
    color = YELLOW;
    enableShooting = false;

    updateDelayDuration = 60;
    updateDelayTimer = 0;
    score = 0;
    shootDelayTimer = 0;
    shootDelayDuration = 10;
    bulletType = STRAIGHT;

    weaponTemp = 0;
    weaponTempMax = 3;
    cooldownTimer = 0;
    cooldownDuration = 100;
    cooldownDelayTimer = 0;
    cooldownDelayDuration = 1000;
    disableShooting = false;


    char tempSprite[3][3] = { {' ',(char)30,' '},
                              {' ', '#', ' '},
                              {'X', '#', 'X'}};
    sprite = new char*[row];
    for(int i = 0; i < row; i++)
    {
        sprite[i] = new char [col];
        for(int j = 0; j < col; j++)
        {
            sprite[i][j] = tempSprite[i][j];
        }
    }
}

//! impliment the update function from the player class
//! make the player move around in the game
void Player::Update(int elapsedTime)
{
    updateDelayTimer += elapsedTime;
    shootDelayTimer += elapsedTime;

    if(weaponTemp > 0)
    {
        cooldownTimer += elapsedTime;
    }

    if(disableShooting)
    {
        cooldownDelayTimer += elapsedTime;
    }


    if(updateDelayTimer < updateDelayDuration)
    {
        return;
    }
    updateDelayTimer %= updateDelayDuration;

    if(GetAsyncKeyState(VK_LEFT))
    {
        MoveLeft();
    }
    if(GetAsyncKeyState(VK_RIGHT))
    {
        MoveRight();
    }
    if(GetAsyncKeyState(VK_UP))
    {
        MoveUp();
    }
    if(GetAsyncKeyState(VK_DOWN))
    {
        MoveDown();
    }
    if(!disableShooting)
    {
        if(GetAsyncKeyState(VK_SPACE) && shootDelayTimer > shootDelayDuration)
        {
            enableShooting = true;
            shootDelayTimer = 0;
            weaponTemp++;
            if(weaponTemp >= weaponTempMax)
            {
                disableShooting = true;
            }
        }
        else if(cooldownTimer > cooldownDuration)
        {
            weaponTemp--;
            cooldownTimer = 0;
        }
    }
    else
    {
        if(cooldownDelayTimer > cooldownDelayDuration)
        {
            disableShooting = false;
            cooldownDelayTimer = 0;
            weaponTemp = 0;
        }
    }

    if(CheckOverBoundary())
    {
        x = xOld;
        y = yOld;
    }

    if(GetAsyncKeyState(0x31))
    {
        bulletType = DUAL;
    }
    if(GetAsyncKeyState(0x32))
    {
        bulletType = TRIPLE;
    }

}
