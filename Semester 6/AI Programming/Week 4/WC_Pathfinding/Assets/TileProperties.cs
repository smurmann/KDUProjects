﻿using UnityEngine;
using System.Collections;

public class TileProperties : MonoBehaviour {

	public float X;
	public float Y;
	TileMap script;
	bool isWalkable = true;
	public float movementCost = 1;

	Renderer color;
	// Use this for initialization
	void Start () 
	{
		color = GetComponent<Renderer> ();
		script = GameObject.Find ("Map").GetComponent<TileMap> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isWalkable)
		{
			color.material.color = Color.white;
			movementCost = 1;
		}
		else
		{
			color.material.color = Color.grey;
			movementCost = Mathf.Infinity;
		}
	}

	void OnMouseUp()
	{
		isWalkable = !isWalkable;
	}
}
