#include <cstdlib>
#include <iostream>
#include <ctime>
#include "windows.h"

using namespace std;
const int ROWS = 10;
const int COLS = 10;

void printBoard(char board[][COLS]);
void generateShip(char board[][COLS], int size);
void checkBoard(char board[][COLS], bool &victory);

int main()
{
    //! randomize
    srand(time(NULL));
    char board[ROWS][COLS], playerBoard[ROWS][COLS];
    int sizeShip, nShips;

    //! initialize the boards
    for(int i = 0; i<ROWS; i++)
    {
        for(int j = 0; j<COLS; j++)
        {
            board[i][j] = '.';
            playerBoard[i][j] = '.';
        }
    }
    cout << "Choose your preferred difficulty level!" << endl;

    {
        cout << "How many enemy battleships do you want? You can't have more than 5." << endl;
        cout << "Min 2 - Max 5" << endl;
        cin >> nShips;
    }while(nShips < 1 || nShips > 5);
    //! get each ship's size
    for(int i=1; i<=nShips; i++)
    {
        do
        {
            cout << "Size of enemy battleship # " << i  << endl;
            cin >> sizeShip;

        }while(sizeShip < 2  || sizeShip > 5);
        generateShip(board,sizeShip);

    }
    //printBoard(board);
    //! gameplay
    int nBombs = 50, iGuess, jGuess;
    bool victory = false;
    if(nShips != 1)
    {
        cout << "\nThe board is below. There are ";
        cout << nShips << "enemy battleships hidden here." << endl;
    }
   else
    {
        cout << "\nThe board is below. There is ";
        cout << nShips << "enemy battleships hidden here." << endl;
    }
    cout << "You have 50 bombs. Make your first move now." << endl;
    //! check the player's bomb count
    for(int n = 1; n <= nBombs && !victory; n++)
    {
        printBoard(playerBoard);
        cout << "\nBomb #" << n <<", i and j: ";
        cin >> iGuess;
        cin >> jGuess;

        if(board[iGuess-1][jGuess-1]=='S')
        {
            cout << "You made a hit!" << endl;
            board[iGuess-1][jGuess-1] = '*';
            playerBoard[iGuess-1][jGuess-1] = '*';
        }
        else
        {
            cout << "You missed!" << endl;
            board[iGuess-1][jGuess-1] = 'o';
            playerBoard[iGuess-1][jGuess-1] = '0';
        }

    }
    victory = true;
    checkBoard(board,victory);
    //!display the final board
    cout<<"The final board:"<<endl;
    printBoard(board);

    if (victory)
    {
        cout << "Congratulations! You sunk all the enemy's battleship" << endl;
    }
    else
    {
        cout << "You didn't sink every battleship. Please try again." << endl;
    }
    system("PAUSE");
    return 0;
}

void printBoard(char board[][COLS])
{
    for(int i=0; i<ROWS; i++)
    {
        for(int j=0; j<COLS; j++)
        {
            cout << board[i][j];
        }
        cout << endl;
    }
}
void generateShip(char board[][COLS], int size)
{
    //! ort -> horizontal or vertical
    int iStart = 0, jStart = 0, ort;
    //! generate random start point and the orientation
    Randomize:
        do
        {
            iStart = rand()%ROWS;
            jStart = rand()%COLS;
            ort = rand()&3;
        }while(board[iStart][jStart]=='S');

        if(ort == 0)
        {
            //! make sure the ship wont go off the board
            while(jStart + size >= COLS)
            {
                jStart = rand()%COLS;
            }
            //! no duplication
            for(int j = jStart; j<jStart + size;j++)
            {
                if(board[iStart][j] == 'S')
                {
                    goto Randomize;
                }
            }
            for(int j = jStart; j<jStart + size;j++)
            {
                if(board[iStart][j] = 'S');
            }
        }
        if(ort == 1)
        {
            //! make sure the ship wont go off the board
            while(iStart + size >= COLS)
            {
                iStart = rand()%COLS;
            }
            //! no duplication
            for(int i = iStart; i<iStart + size;i++)
            {
                if(board[i][jStart] == 'S')
                {
                    goto Randomize;
                }
            }
            for(int i = iStart; i<iStart + size;i++)
            {
                if(board[i][jStart] = 'S');
            }
        }
        if(ort == 2)
        {
            //! make sure the ship wont go off the board
            while(iStart + size >= COLS)
            {
                iStart = rand()%COLS;
            }
            //! no duplication
            for(int i = iStart, j = jStart; i<iStart + size;i++, j--)
            {
                if(board[i][j] == 'S')
                    {
                        goto Randomize;
                    }
            }
            for(int i = iStart, j = jStart; i<iStart + size;i++, j--)
            {
                if(board[i][j] = 'S');

            }

        }
        if(ort == 3)
        {
            //! make sure the ship wont go off the board
            while(jStart + size >= COLS)
            {
                jStart = rand()%COLS;
            }
            //! no duplication
            for(int i = iStart, j = jStart; j<jStart + size;i++, j++)
            {
                if(board[i][j] == 'S')
                    {
                        goto Randomize;
                    }
            }
            for(int i = iStart, j = jStart; j<jStart + size;i++, j++)
            {
                if(board[i][j] = 'S');

            }

        }


        return;
}

void checkBoard(char board[][COLS], bool &victory)
{
    for(int i=0; i<ROWS; i++)
    {
        for(int j = 0; j < COLS; j++)
        {
            if(board[i][j] == 'S')
            {
                victory = false;
            }
        }
    }
}
