﻿using UnityEngine;
using System.Collections;

public class newScript : MonoBehaviour {

	public GameObject m_object;
	public GameObject m_projectilePrefab;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (0)) {
			Ray theRay = Camera.main.ScreenPointToRay(Input.mousePosition);

			//GameObject newObject = Instantiate(m_projectilePrefab,theRay.origin,Quaternion.identity) as GameObject;
			GameObject newObject = pooler.m_instance.newObject();
			if(newObject != null)
			{
				newObject.transform.position = theRay.origin;
				newObject.transform.rotation = Quaternion.identity;
				newObject.SetActive(true);

				Rigidbody newObjectRigidbody = newObject.GetComponent<Rigidbody>();
				newObjectRigidbody.velocity = theRay.direction*15.0f;
			}
		}
	}
}
