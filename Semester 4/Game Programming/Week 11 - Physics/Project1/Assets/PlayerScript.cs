﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        Vector3 velocity = rb.velocity;
        velocity.x = moveHorizontal * 5.0f;

        if(Input.GetButtonDown("Jump"))
        {
            velocity.y = 7.0f;
        }

        rb.velocity = velocity;
	
	}
}
