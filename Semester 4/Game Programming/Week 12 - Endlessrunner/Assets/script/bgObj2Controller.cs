﻿using UnityEngine;
using System.Collections;

public class bgObj2Controller : MonoBehaviour {

    
	private PlayerController m_playerScript;
    private GameObject player;
    Animator anim;
	// Use this for initialization
	void Start ()
	{
		GameObject player = GameObject.FindGameObjectWithTag("Player");
        anim = player.GetComponent<Animator>();
        m_playerScript = (PlayerController)player.GetComponent(typeof(PlayerController));
	}
	
	// Update is called once per frame
	void Update ()
	{
        //float speed = m_playerScript.m_speed * 0.2f * Time.deltaTime;
        float speed = anim.GetFloat("Speed")* 0.2f * Time.deltaTime;

		Vector3 position = transform.position;
		position.x -= speed;

		transform.position = position;

		if (transform.position.x < -8.0f)
			Destroy(this.gameObject);
	}
}
