#include <iostream>
#include "windows.h"
using namespace std;

int main()
{
	cout<<"Variables"<<endl;
	
	//! boolean type -> flag
	//! it can store the value of true or false only
	bool isAmale = true;
	
	cout<<"isAmale value : "<<isAmale<<endl;
	// & for calling the address
	cout<<"isAmale address: "<<&isAmale<<endl;
	
	char yourGrade = 1337;
	cout<<"yourGrade value : "<<yourGrade<<endl;
	cout<<"yourGrade address : "<<(void*)&yourGrade<<endl;
	
	int yourAge = 20;
	cout<<"yourAge value : "<<yourAge<<endl;
	cout<<"yourAge address : "<<&yourAge<<endl;
	
	float yourHeight = 1.64;
	cout<<"yourHeight value : "<<yourHeight<<endl;
	cout<<"yourHeight address : "<<&yourHeight<<endl;
	
	double yourWeight = 100.8;
	cout<<"yourWeight value : "<<yourWeight<<endl;
	cout<<"yourWeight address : "<<&yourWeight<<endl;
	
	
	
	
	
	
	}

