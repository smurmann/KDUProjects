#ifndef _WEAPONS_H
#define _WEAPONS_H

#include <string>

using namespace std;
//enum WEAPONTYPE
//{
//	SWORD = 0,
//	SHIELD,
//	MASK,
//	GUN
//};

class Weapons
{
	string className;
	string name;
	int dmg;
public:
	Weapons(string cname, string n, int x) : className(cname), name(n), dmg(x){}
	string GetName()
	{

		return name;

	}
	int GetDmg()
	{
		return dmg;
	}
	string GetclassName()
	{
		return className;
	}
};

class Sword : public Weapons
{

public:
	Sword(string name, int dmg) : Weapons("Sword",name,dmg){}
};

class Shield : public Weapons
{
public:
	Shield(string name, int defense) : Weapons("Shield",name, defense){}
};

class Gun : public Weapons
{
public:
	Gun(string name, int dmg) : Weapons("Gun",name, dmg){}
};

class Mask : public Weapons
{
public: 
	Mask(string name, int buffstat) : Weapons("Mask",name, buffstat){}
};

#endif