// Vector.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <vector>
#include <iostream>
#include <stack>
#include <time.h>

using namespace std;

vector<int>v;

void Display(vector<int> tempVector)
{
	cout<<"=================="<<endl;
	if(tempVector.empty())
	{
		cout<<"Vector is EMPTY!!"<<endl;
	}
	else
	{
		for(int i=0; i<tempVector.size(); i++)
		{
			cout<<tempVector[i]<<endl;
		}
	}
}

void ReverseVector(vector<int>& realVector, vector<int> tempVector)
{
	realVector.clear();
	for(int i= tempVector.size() -1; i>=0; i--)
	{
		realVector.push_back(tempVector[i]);
	}
}
void AddRandomNumber(vector<int>& realVector, vector<int> tempVector)
{
	//realVector.clear();
	int a = rand() % 100 + 1;
	int b = rand() % realVector.size() + 1;
	cout << a << b << endl;
//	v = realVector.begin();
	realVector.insert(v.begin(),b,a);	
}
void RemoveRandomNumber(vector<int>& realVector, vector<int> tempVector)
{
	realVector.clear();
	int b = rand() % tempVector.size() + 1;
	//realVector.pop_back(b);
}
void SwapRandomStuff()
{

}

int _tmain(int argc, _TCHAR* argv[])
{
	//random Gen
	srand(time(NULL));
	bool isExit = false;

	do
	{
		int choice = -1;
		int value = 0;
		cout<<"Vector Program"<<endl;
		Display(v);
		
		cout<<"1. Add to the back of the vector"<<endl;
		cout<<"2. Remove from the back of the vector"<<endl;
		cout<<"3. Reverse the vector"<<endl;
		cout<<"4. Add Random number to the vector"<<endl;
		cout<<"5. Remove Random number to the vector"<<endl;
		cout<<"6. Spaw random stuff in the vector"<<endl;
		cout<<"7. Exit the program"<<endl;
		cout<<"Your choice is:"<<endl;
		cin>>choice;
		if(choice==1)
		{
			cout<<"Value to be added"<<endl;
			cin>>value;
			v.push_back(value);
		}
		else if(choice==2)
		{
			if(!v.empty())
			{
				v.pop_back();
			}
			else
			{
				cout<<"Nothing to pop"<<endl;
			}
		}
		else if (choice==3)
		{
			ReverseVector(v, v);
		}
		else if (choice==4)
		{
			AddRandomNumber(v, v);
		}
		else if (choice==5)
		{
			RemoveRandomNumber(v, v);
		}
		else if (choice==6)
		{
			SwapRandomStuff();
		}
		else if (choice==7)
		{
			isExit = true;
		}
	
		system("PAUSE");
		system("CLS");
	}while(!isExit);

	return 0;
}

