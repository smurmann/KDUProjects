#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

class GlobalClass
{
	int value;
public:
	GlobalClass(int v = 0)
	{
		value = v;
	}
	int getValue()
	{
		return value;
	}
	void setValue(int v)
	{
		value = v;
	}
};

//! Default initialization
GlobalClass *global_ptr = 0;

void foo(void)
{
	// initialization on first time
	if(!global_ptr)
	{
		global_ptr = new GlobalClass;
	}
	global_ptr->setValue(1);
	cout << "foo: global_ptr is " << global_ptr->getValue()<<endl;
}

void bar(void)
{
	// initialization on second time
	if(!global_ptr)
	{
		global_ptr = new GlobalClass;
	}
	global_ptr->setValue(2);
	cout << "bar: global_ptr is " << global_ptr->getValue()<<endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if(!global_ptr)
		global_ptr = new GlobalClass;
	cout << "main: global_ptr is " << global_ptr->getValue() << endl;
	foo();
	//bar();
	cout << global_ptr->getValue() << endl;
	system("PAUSE");
	return 0;
}