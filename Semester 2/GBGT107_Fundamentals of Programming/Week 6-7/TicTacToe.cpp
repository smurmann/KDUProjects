#include <iostream>
#include "windows.h"
using namespace std;

char charArray[3][3];
bool isCrossTurn = true;
bool isGameEnd = false;

void DisplayTicTacToe()
{
    //! update the isGameEnd to false
    //! when we achieve the Winning Condition
     for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            cout << charArray[i][j];
        }
        cout << endl;
    }
}

void GetInput()
{
    int row, column;
    char whoseTurn = 'O';

    if(isCrossTurn)
    {
        whoseTurn = 'X';
    }
    cout << whoseTurn << " please enter the row." << endl;
    cin >> row;
    cout << whoseTurn << " please enter the column." << endl;
    cin >> column;

    if(charArray[row][column] == '#' && row < 3 && row >= 0 && column >= 0 && column < 3)
    {
        if(isCrossTurn)
        {
            charArray[row][column] = 'X';
        }
        else
        {
            charArray[row][column] = 'O';
        }
        isCrossTurn = !isCrossTurn;
    }
    else
    {
        cout << "=================================" << endl;
        cout << "Invalid position, please try again" << endl;
        cout << "=================================" << endl;
    }
}

void CheckWinningCondition()
{
    for(int i = 0; i < 3; i++)
    {
        for(int j=0; j<3; j++)
        {
        if(charArray [j][i] == 'X'
        && charArray [j][i+1] == 'X'
        && charArray [j][i+2] == 'X'
        )
        {
            cout << "X wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }

        else if(charArray [j][i] == 'O'
        && charArray [j][i+1] == 'O'
        && charArray [j][i+2] == 'O'
        )
        {
            cout << "O wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        else if(charArray [j][i] == 'O'
        && charArray [j+1][i] == 'O'
        && charArray [j+2][i] == 'O'
        )
        {
            cout << "O wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        else if(charArray [j][i] == 'X'
        && charArray [j+1][i] == 'X'
        && charArray [j+2][i] == 'X'
        )
        {
            cout << "X wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        //! Diagonal
        else if(charArray [j][i] == 'X'
        && charArray [j+1][i+1] == 'X'
        && charArray [j+2][i+2] == 'X'
        )
        {
            cout << "X wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        else if(charArray [j][i] == 'O'
        && charArray [j+1][i+1] == 'O'
        && charArray [j+2][i+2] == 'O'
        )
        {
            cout << "O wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        else if(charArray [j+2][i] == 'O'
        && charArray [j-1][i+1] == 'O'
        && charArray [j-1][i+2] == 'O'
        )
        {
            cout << "O wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        else if(charArray [j+2][i] == 'X'
        && charArray [j+1][i+1] == 'X'
        && charArray [j][i+2] == 'X'
        )
        {
            cout << "X wins the game!" << endl;
            isGameEnd = true;
            DisplayTicTacToe();
            break;
        }
        }

    }
}

int main()
{
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            charArray[i][j] = '#';
        }
    }

    do
    {
        DisplayTicTacToe();
        GetInput();
        CheckWinningCondition();
    }while(!isGameEnd);


    system("PAUSE");
    return 0;
}
