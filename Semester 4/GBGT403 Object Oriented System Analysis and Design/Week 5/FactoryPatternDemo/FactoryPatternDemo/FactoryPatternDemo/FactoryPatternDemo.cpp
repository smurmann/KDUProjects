// FactoryPatternDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <Windows.h>

using namespace std;

class Stooge
{
public:
	static Stooge *make_stooge(int choice);
	virtual void slap_stick() = 0;
};
class Larry : public Stooge
{
public:
	void slap_stick()
	{
		cout << "Larry: poke eyes" << endl;
	}
};

class Sam : public Stooge
{
public:
	void slap_stick()
	{
		cout << "Sam: slap ass" << endl;
	}
}; 
class Curly : public Stooge
{
public:
	void slap_stick()
	{
		cout << "Curly: suffer abuse" << endl;
	}
};
Stooge* Stooge::make_stooge(int choice)
{
	if (choice == 1)
	{
		return new Larry;
	}
	else if (choice == 2)
	{
		return new Sam;
	}
	else if (choice == 3)
	{
		return new Curly;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	vector<Stooge*> roles;
	int choice;
	while (true)
	{
		cout << "Larry(1), Sam(2), Curly(3), Go(0) : ";
		cin >> choice;
		if (choice == 0)
			break;
		roles.push_back(Stooge::make_stooge(choice));
	}
	for (int i = 0; i < roles.size(); i++)
	{
		roles[i]->slap_stick();
	}
	system("PAUSE");
	return 0;
}

