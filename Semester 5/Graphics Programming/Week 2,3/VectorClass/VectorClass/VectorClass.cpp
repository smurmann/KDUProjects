// VectorClass.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <Windows.h>
#include <math.h>

using namespace std;

class Vector3
{
public:
	Vector3(void);
	float x, y, z;
	Vector3(float X, float Y, float Z);
	Vector3 Addition(Vector3 A, Vector3 B);
	Vector3 Substraction(Vector3 A, Vector3 B);
	//float Length();
	Vector3 scalarMultiply(float multp, Vector3 A);
	float scalarDivide();
	float magnitude();
	float dotProduct(Vector3 A, Vector3 B);
	Vector3 crossProduct(Vector3 A, Vector3 B);
	Vector3 normalize(Vector3 A);

	
};

Vector3::Vector3(void)
{

}

Vector3 Vector3::Substraction(Vector3 A, Vector3 B)
{
	Vector3 newVec;
	newVec.x = A.x - B.x;
	newVec.y = A.y - B.y;
	newVec.z = A.z - B.z;
	return newVec;
}

Vector3::Vector3(float X, float Y, float Z)
{
	x = X;
	y = Y;
	z = Z;
}

Vector3 Vector3::Addition(Vector3 A, Vector3 B)
{
	Vector3 newVec;
	newVec.x = A.x + B.x;
	newVec.y = A.y + B.y;
	newVec.z = A.z + B.z;
	return newVec;
}

Vector3 Vector3::scalarMultiply(float mulp, Vector3 A)
{
	Vector3 newVect;
	newVect.x = A.x*mulp;
	newVect.y = A.y*mulp;
	newVect.z = A.z*mulp;
	return newVect;
}

float Vector3::scalarDivide()
{
	return (x/2.0f, y/2.0f, z/2.0f);
}

float Vector3::magnitude()
{
	return sqrt(x*x + y*y + z*z);
}

float Vector3::dotProduct(Vector3 A, Vector3 B)
{
	return ((A.x*B.x) + (A.y*B.y) + (A.z*B.z));
}

Vector3 Vector3::crossProduct(Vector3 A, Vector3 B)
{
	Vector3 newVec;
	newVec.x = A.y*B.z - A.z*B.y;
	newVec.y = A.z*B.x - A.x*B.z;
	newVec.z = A.x*B.y - A.y*B.x;
	return newVec;
}

Vector3 Vector3::normalize(Vector3 A)
{
	Vector3 newVec;
	newVec.x = A.x / A.magnitude();
	newVec.y = A.y / A.magnitude();
	newVec.z = A.z / A.magnitude();
	return newVec;
}

void TestVectors()
{
	Vector3 vector1(10.0f, 20.0f, 5.0f);
	printf("length of vector 1 : %f \n", vector1.magnitude());

	Vector3 vector2(3.0f, 3.0f, 10.0f);
	printf("length of vector 2 : %f \n", vector2.magnitude());

	Vector3 vector3 = vector3.Addition(vector1, vector2);
	printf("vector 3 : %f, %f, %f \n", vector3.x, vector3.y, vector3.x);

	Vector3 vector4 = vector2.scalarMultiply(1.4f, vector2);
	printf("vector 4 : %f, %f, %f \n", vector4.x, vector4.y, vector4.x);

	//Vector 5 with normalize
	Vector3 vector5 = vector4;
	vector5 = vector5.normalize(vector5);
	printf("normliazed vector4: %f, %f, %f | length after normalization: %f \n", vector5.x, vector5.y, vector5.z, vector5.magnitude());

	printf("dot product value of vector 1 with vector 2: %f \n", vector2.dotProduct(vector1, vector2));

	Vector3 vector6(0.0f, 1.0f, 0.0f);
	Vector3 vector7(1.0f, 0.0f, 0.0f);
	Vector3 crossVect = crossVect.crossProduct(vector6, vector7);
	printf("cross vector : %f, %f, %f \n", crossVect.x, crossVect.y, crossVect.x);
}

int _tmain(int argc, _TCHAR* argv[])
{
	TestVectors();
	
	//Vector3 C(10.0, 10.0, 10.0);
	system("PAUSE");
	return 0;
}

