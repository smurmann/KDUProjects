﻿using UnityEngine;
using System.Collections;

public class GameBoard : Photon.MonoBehaviour
{
    static int PLAYER_1 = 1;
    static int PLAYER_2 = 2;

    int player = 0;

    int[,] boardState;

    bool boardInitalized = false;
    bool gameStarted = false;

    int randomizedNumber = 0;

    void Awake()
    {
        boardInitalized = false;
    }

    void OnBoardInitalized()
    {
        boardInitalized = true;
    }

    void Start()
    {
        InitalizeBoard();

        PhotonNetwork.ConnectUsingSettings("1.0");
    }

    void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");
        PhotonNetwork.JoinOrCreateRoom("TicTacToe", new RoomOptions(), TypedLobby.Default);
    }

    void OnJoinedRoom()
    {
        if (PhotonNetwork.playerList.Length == 2)
        {
            photonView.RPC("RandomizeStart", PhotonTargets.All);
        }
    }

    [PunRPC]
    void RandomizeStart()
    {
        randomizedNumber = Random.Range(0, 999999);
    }


    void InitalizeBoard()
    {
        boardState = new int[3, 3];

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                boardState[i, j] = new int();
                boardState[i, j] = 0;
            }
        }
        OnBoardInitalized();
    }


    void DisplayBoard()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (GUI.Button(new Rect((Screen.width - 100) / 3 * i, (Screen.height - 100) / 3 * j,
                                        (Screen.width - 100) / 3, (Screen.height - 100) / 3),
                                        returnXO(i, j)))
                {
                    if (boardState[i, j] == 0)
                        boardState[i, j] = player;
                    else
                        Debug.Log("INVALID MOVE");
                }

            }
        }
    }

    string returnXO(int i, int j)
    {
        string returnString = "";
        switch (boardState[i, j])
        {
            case 1:
                returnString = "O";
                break;
            case 2:
                returnString = "X";
                break;
        }

        return returnString;
    }


    void OnGUI()
    {
        if (boardInitalized && gameStarted)
        {
            DisplayBoard();
        }
    }
}
