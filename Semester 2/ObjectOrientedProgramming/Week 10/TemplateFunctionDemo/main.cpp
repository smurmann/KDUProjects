#include <iostream>
#include "maximum.h"

using namespace std;

int main()
{
    int int1, int2, int3;
    cout << "Input three integer values: ";
    cin >> int1 >> int2 >> int3;

    cout << "The maximum integer value is: " << endl;
    cout << maximum(int1,int2,int3);
    cout << endl;

    double d1, d2, d3;
    cout << "Input three double values: ";
    cin >> d1 >> d2 >> d3;
    cout << "The maximum double value is " << endl;
    cout << maximum(d1,d2,d3);

    char c1,c2,c3;
    cout << "Input three char values: ";
    cin >> c1 >> c2 >> c3;
    cout << "The maximum char value is " << endl;
    cout << maximum(c1,c2,c3);
    cout << endl;

    return 0;
}
