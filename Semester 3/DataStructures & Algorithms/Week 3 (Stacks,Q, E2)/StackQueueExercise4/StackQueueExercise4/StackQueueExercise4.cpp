// StackQueueExercise4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <stack>
#include <string>
#include <queue>

using namespace std;

queue<char> Q1;
queue<char> Q2;

int _tmain(int argc, _TCHAR* argv[])
{
	string str;
	bool isIdentical = true;
	cout<<"enter a word: "<<endl;
	getline(cin,str);

	if(str.length() % 2 == 0)
	{
		for(int i=0; i<(str.length()/2);i++)
		{
			Q1.push(str[i]);
			Q2.push(str[i+str.length()/2]);
		}
	

	while(Q1.size()>0 && Q2.size()>0)
	{
		if(Q1.front()!=Q2.front())
		{
			isIdentical = false;
			break;
		}
		Q1.pop();
		Q2.pop();
	}

	}else{
	
		isIdentical = false;
	}

	if(isIdentical)
	{
		cout<<"The " << str <<" is identical"<<endl;
	}
	else
	{
		cout<<"The " << str <<" is not identical"<<endl;
	}
	system("PAUSE");
	return 0;
}

