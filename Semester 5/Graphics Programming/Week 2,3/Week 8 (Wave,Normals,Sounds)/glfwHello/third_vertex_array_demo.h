#ifndef _THIRD_VERTEX_ARRAY_DEMO_H
#define _THIRD_VERTEX_ARRAY_DEMO_H

#include "demo_base.h"

class ThirdVertexArrayDemo : public DemoBase
{
private:
	//GLfloat mRectVertices[18];
public:
	void init()
	{

	}

	void deinit()
	{

	}
	void draw(const Matrix& viewMatrix)
	{
		//drawaxis
		glLoadMatrixf((GLfloat*)viewMatrix.mVal);
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.3f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.3f, 1.0f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.3f, 0.3f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();

		Matrix rotate1 = Matrix::makeRotateMatrix(0, Vector(0.0f, 0.0f, 1.0f));

		Matrix modelMatrix = rotate1;

		Matrix viewSpaceMatrix = viewMatrix;// *modelMatrix;


		// Rectangle
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		//drawPyramid();
		//drawCube();
		glColor3f(1.0f, 1.0f, 1.0f);

		static GLfloat vertices[] =
		{
			-1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			-1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
		};

		static GLubyte indicies[] =
		{
			0, 1, 2, //1st triangle
			2, 3, 0, //2nd triangle

			4,5,6,
			6,7,4,

			3,2,6,
			6,7,3,

			0,1,5,
			5,4,0,
		};
		// activate and specify pointer to vertext array
		glEnableClientState(GL_VERTEX_ARRAY);

		//tell opengl which vertex array is going to be used (set active)
		// in this case, vertices is the name of the vertices array going to be used
		glVertexPointer(3, GL_FLOAT, 0, vertices);

		int numberOfVertices = 3 * 12;
		// draw rectangle
		glDrawElements(GL_TRIANGLES, 6*4, GL_UNSIGNED_BYTE, indicies);

		//deactivate vertex arrats after drawing
		glDisableClientState(GL_VERTEX_ARRAY);

	}
};


#endif