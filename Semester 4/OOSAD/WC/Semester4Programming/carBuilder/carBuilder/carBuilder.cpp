// carBuilder.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <string>

using namespace std;

//! car parts
class Wheel
{
public:
	int size;
};
class Engine
{
public:
	int horsePower;
};
class Body
{
public:
	string shape;
};

//! final Object 
class Car
{
public:
	Engine* engine;
	Body* body;
	Wheel* wheels[4];
	void specification()
	{
		cout << "Body : " << body->shape << endl;
		cout << "Engine Horsepower : " << engine->horsePower << endl;
		cout << "Tire size : " << wheels[0]->size << "  " << endl;
	}
};

class CarBuilder
{
public:
	virtual Wheel* getWheel() = 0;
	virtual Engine* getEngine() = 0;
	virtual Body* getBody() = 0;

};
//! concreate builder
class ToyotaBuilder :public CarBuilder
{
	Wheel* getWheel()
	{
		Wheel* wheel = new Wheel();
		wheel->size = 22;
		return wheel;
	}
	Engine* getEngine()
	{
		Engine* engine = new Engine();
		engine->horsePower = 400;
		return engine;
	}
	Body* getBody()
	{
		Body* body = new Body;
		body->shape = "SUV";
		return body;
	}
};

class ProtonBuilder :public CarBuilder
{
	Wheel* getWheel()
	{
		Wheel* wheel = new Wheel();
		wheel->size = 16;
		return wheel;
	}
	Engine* getEngine()
	{
		Engine* engine = new Engine();
		engine->horsePower = 100;
		return engine;
	}
	Body* getBody()
	{
		Body* body = new Body;
		body->shape = "Sedan";
		return body;
	}
};

class Director
{
	CarBuilder* carbuilder;
public:
	void setCarBuilder(CarBuilder * newCarBuilder)
	{
		carbuilder = newCarBuilder;
	}
	Car* getCar()
	{
		Car * car = new Car();
		car->body = carbuilder->getBody();
		car->engine = carbuilder->getEngine();
		car->wheels[0] = carbuilder->getWheel();
		car->wheels[1] = carbuilder->getWheel();
		car->wheels[2] = carbuilder->getWheel();
		car->wheels[3] = carbuilder->getWheel();
		return car;

	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	Car *car;
	Director director;
	ToyotaBuilder toyota;
	ProtonBuilder proton;

	cout << "Toyota" << endl;
	director.setCarBuilder(&toyota);
	car = director.getCar();
	car->specification();
	cout << endl;

	cout << "Proton " << endl;
	director.setCarBuilder(&proton);
	car = director.getCar();
	car->specification();
	cout << endl;
	
	system("PAUSE");
	return 0;
}

