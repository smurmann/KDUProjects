#include <iostream>
#define _WIN32_WINNT 0x0500
#include <windows.h>
#include "conio_yp.h"
#include <cstdlib>
#include <ctime>

using namespace std;

//! global variable
const int mapRow = 27;
const int mapCol = 36;
char map[mapRow][mapCol];
bool isExit = false;
int mapOffSetX = 4;
int mapOffSetY = 3;
int startPosX;
int startPosY;
int selection;
int charSelect;
char block = 219;
char space = 32;

void BattleSimulation();
void ItemCollision();
void KeyCollision();
void RandomItem();
void TrapCollision();
void DoorCollision();
void ShopMenu();
void CashCollision();
void HomeCollision();

#define tree 5
#define wall 64
#define trap 84
#define e1 69
//#define e1 6
#define item 43
#define home 72
#define key 75
#define shop 1
#define door 219
#define rItem 63
#define cash 36


//! enumeration
enum TYPE
{
    WARRIOR= 0,
    ARCHER,
    MAGE,
    TOTAL
};

struct Hero
{
    TYPE type;
    string heroType;
    char avatar;
    int row;
    int column;
    int rowOld;
    int columnOld;
    int health;
    int mana;
    int attack;
    int money;
    int potHP;
    int potMP;
    int keyAmount;
    bool playerhurt;
    bool playerhappy;
    bool playersurprised;
    bool playerdead;
    bool playercash;

    void Initialize(TYPE v);
};
player.heroType
void Hero::Initialize(TYPE v)
{
    type = v;
    row = 5;
    column = 5;
    keyAmount = 0;
    potHP = 2;
    potMP = 2;
    money = 50;

    playerhurt = false;
    playerhappy = false;
    playersurprised = false;
    playerdead = false;
    playercash = false;


    if(type == WARRIOR)
    {
        heroType = "WARRIOR";
        avatar = 'W';
        health = 100;
        attack = 35;
        mana = 25;
    }
    else if(type == ARCHER)
    {
        heroType = "ARCHER";
        avatar = 'A';
        health = 80;
        attack = 30;
        mana = 25;
    }
    else if(type == MAGE)
    {
        heroType = "MAGE";
        avatar = 'M';
        health = 50;
        attack = 50;
        mana = 100;
    }

};

Hero player;

struct Enemy
{

    TYPE type;
    char avatar;
    int row;
    int column;
    int rowOld;
    int columnOld;
    int maxHealth;
    int health;
    int attack;
    int rAttack;

    void Initialize();
};
void Enemy::Initialize()
{
    rAttack = 0;
    type = (TYPE)(rand()%TOTAL);
    if(type == WARRIOR)
    {
        avatar = 'Z';
        row = 5;
        column = 5;
        maxHealth = 50;
        health = 50;
        attack = 50;
    }
    else if(type == ARCHER)
    {
        avatar = 'Z';
        row = 5;
        column = 5;
        maxHealth = 50;
        health = 50;
        attack = 50;

    }
    else if(type == MAGE)
    {
        avatar = 'Z';
        row = 5;
        column = 5;
        maxHealth = 50;
        health = 50;
        attack = 50;

    }
};

Enemy theEnemy;

void InitializeMap()
{
    char tempMap[mapRow][mapCol] =
    {
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
        {'#',wall,wall,wall,wall,wall,wall,wall,wall,wall,key,cash,tree,wall,wall,wall,wall,wall,wall,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#',wall,'0','0','0','0','0',wall,cash,wall,'0',tree,e1,tree,wall,'0',rItem,rItem,wall,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#',wall,'0',trap,trap,trap,'0',wall,e1,wall,'0',e1,'0',tree,wall,'0',rItem,rItem,wall,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#',wall,'0',key,trap,trap,'0',wall,'0',wall,tree,tree,e1,shop,'0','0',e1,key,wall,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#',wall,trap,trap,trap,'0','0','0','0',wall,'0',tree,'0',tree,wall,door,wall,wall,wall,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#',wall,'0','0','0','0','0','0','0',wall,tree,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',e1,'0','0','0','0','#'},
        {'#',wall,'0',rItem,'0','0','0','0','0',door,'0','0',tree,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',e1,'0','0','0','0','0','0','#'},
        {'#',wall,'0',rItem,'0','0','0','0',item,wall,tree,'0','0','0','0','0','0','0','0','0','0',tree,tree,tree,tree,tree,tree,e1,'0',e1,'0','0','0','0','0','#'},
        {'#',wall,wall,wall,wall,wall,wall,wall,wall,wall,'0',tree,'0',tree,'0',tree,'0','0','0','0','0',tree,'0','0','0','0',door,e1,'0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0',tree,'0',key,'0',tree,'0','0','0','0',tree,'0',home,'0','0',tree,e1,'0',e1,'0',e1,'0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0',tree,'0','0','0','0','0','0','0',tree,'0','0','0','0',tree,e1,'0',e1,'0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0',tree,'0','0','0','0','0','0','0','0','0',tree,tree,tree,tree,tree,tree,'0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}
    };
    for (int i=0; i<mapRow; i++)
    {
        for(int j=0; j<mapCol; j++)
            {
                map[i][j] = tempMap[i][j];
            }
    }

}

void ShowMap()
{
    //int mapOffSetX = 4;
    //int mapOffSetY = 3;

    for(int i=0; i<mapRow; i++)
    {
        for(int j=0; j<mapCol; j++)
        {
            gotoxy(mapOffSetX + j, mapOffSetY + i);


            if(i<player.row - 5 ||
               i>player.row + 5 ||
               j<player.column - 5||
               j>player.column + 5)
            {
                textcolor(GREEN);
                cout << " ";

                textcolor(WHITE);
                continue;
            }
            if(map[i][j] == tree)
            {
                textcolor(GREEN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == trap)
            {
                textcolor(LIGHTRED);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == '#')
            {
                textcolor(YELLOW);
                cout << map[i][j];
                textcolor(WHITE);
            }
            /*else if (map[i][j] == player)
            {
                textcolor(LIGHTCYAN);
                cout << map[i][j];
                textcolor(WHITE);
            }*/
            else if (map[i][j] == e1)
            {
                textcolor(DARKGRAY);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] ==(char) door)
            {
                textcolor(BROWN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == item)
            {
                textcolor(LIGHTGREEN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == home)
            {
                textcolor(LIGHTCYAN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == wall)
            {
                textcolor(MAGENTA);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == shop)
            {
                textcolor(BROWN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == rItem)
            {
                textcolor(LIGHTBLUE);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == key)
            {
                textcolor(YELLOW);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == cash)
            {
                textcolor(LIGHTGREEN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else
            {
                textcolor(WHITE);
                cout << map[i][j];
                textcolor(WHITE);
            }
        }
        cout << endl;
    }
    //! draw player
    gotoxy(mapOffSetX + player.column,
           mapOffSetY + player.row);
    textcolor(GREEN);
    cout << player.avatar;
    textcolor(WHITE);
}

void ShowPlayerInfo()
{
    gotoxy(42,3);
    cout << "Player Information";
    gotoxy(42,4);
    cout << "==================";

    gotoxy(42,5);
    cout << "Job    : " << player.heroType;

    gotoxy(42,6);
    cout << "Health : " << player.health <<"  ";
    gotoxy(42,7);
    cout << "Mana   : " << player.mana <<"  ";
    gotoxy(42,8);
    cout << "Attack : " << player.attack  << " ";

    gotoxy(42,10);
    cout << "HP Pots : " << player.potHP << " ";
    gotoxy(42,11);
    cout << "MP Pots : " << player.potMP << " ";

    gotoxy(42,13);
    cout << "Gold   : " << player.money << " ";

    gotoxy(42,14);
    cout << "Key    : " << player.keyAmount << " ";

    gotoxy(42,17);
    cout << "==================";
    gotoxy(42,16);
    cout << "Avatar Description";

    gotoxy(42,18);
    cout << "You         : ";
    textcolor(GREEN);
    cout << player.avatar;
    textcolor(WHITE);

    gotoxy(42,21);
    cout << "Enemy       : ";
    textcolor(DARKGRAY);
    cout << (char) e1;
    textcolor(WHITE);

    gotoxy(42,19);
    cout << "Shopkeeper  : ";
    textcolor(BROWN);
    cout << (char) shop;
    textcolor(WHITE);

    gotoxy(42,25);
    cout << "Key         : ";
    textcolor(YELLOW);
    cout << (char) key;
    textcolor(WHITE);

    gotoxy(42,27);
    cout << "Health      : ";
    textcolor(LIGHTGREEN);
    cout << (char) item;
    textcolor(WHITE);

    gotoxy(42,28);
    cout << "Random Item : ";
    textcolor(LIGHTBLUE);
    cout << (char) rItem;
    textcolor(WHITE);

    gotoxy(42,24);
    cout << "Home        : ";
    textcolor(LIGHTCYAN);
    cout << (char) home;
    textcolor(WHITE);

    gotoxy(60,20);
    cout << "Border      : ";
    textcolor(YELLOW);
    cout << '#';
    textcolor(WHITE);

    gotoxy(60,22);
    cout << "Goal : Make it Home!";


    gotoxy(60,19);
    cout << "Wall        : ";
    textcolor(MAGENTA);
    cout << (char) wall;
    textcolor(WHITE);

    gotoxy(42,22);
    cout << "Trap        : ";
    textcolor(LIGHTRED);
    cout << (char) trap;
    textcolor(WHITE);

    gotoxy(60,18);
    cout << "Tree        : ";
    textcolor(GREEN);
    cout << (char) tree;
    textcolor(WHITE);

    gotoxy(42,26);
    cout << "Door Closed : ";
    textcolor(BROWN);
    cout << (char) door;
    textcolor(WHITE);



if(player.playerhurt)
    {
        gotoxy(55,6);
        cout << " ,    ,    /\\   /\\ ";
        gotoxy(55,7);
        cout << "/( /\\ )\\  _\\ \\_/ /_ ";
        gotoxy(55,8);
        cout << "|\\_||_/| < \\_   _/ >";
        gotoxy(55,9);
        cout << "\\______/  \\\\>   <//";
        gotoxy(55,10);
        cout << "  _\\/_   _(_  ^  _)_";
        gotoxy(55,11);
        cout << " ( () ) /`\\|IIIII|/`\\";
        gotoxy(55,12);
        cout << "   {}   \\  \\_____/  /";
        gotoxy(55,13);
        cout << "   ()   /\\   )=(   /\\ ";
        gotoxy(55,14);
        cout << "   {}  /  \\_/\\=/\\_/  \\ ";
        gotoxy(65,15);
        cout <<" OUCH!!";
        player.playerhurt = false;
    }
    else if(player.playerhappy)
    {
        gotoxy(55,6);
        cout << " ,    ,    /\\   /\\ ";
        gotoxy(55,7);
        cout << "/( /\\ )\\  _\\ \\_/ /_ ";
        gotoxy(55,8);
        cout << "|\\_||_/| < \\_   _/ >";
        gotoxy(55,9);
        cout << "\\______/  \\|^   ^|/";
        gotoxy(55,10);
        cout << "  _\\/_   _(_  ^  _)_";
        gotoxy(55,11);
        cout << " ( () ) /`\\|\\___/|/`\\";
        gotoxy(55,12);
        cout << "   {}   \\  \\_____/  /";
        gotoxy(55,13);
        cout << "   ()   /\\   )=(   /\\ ";
        gotoxy(55,14);
        cout << "   {}  /  \\_/\\=/\\_/  \\ ";
        gotoxy(55,15);
        cout << "  Mhhh, Homebrew potions!  ";
        player.playerhappy = false;
    }
    else if(player.playersurprised)
    {
        gotoxy(55,6);
        cout << " ,    ,    /\\   /\\ ";
        gotoxy(55,7);
        cout << "/( /\\ )\\  _\\ \\_/ /_ ";
        gotoxy(55,8);
        cout << "|\\_||_/| < \\_   _/ >";
        gotoxy(55,9);
        cout << "\\______/  \\|O   O|/";
        gotoxy(55,10);
        cout << "  _\\/_   _(_  ^  _)_";
        gotoxy(55,11);
        cout << " ( () ) /`\\|  O  |/`\\";
        gotoxy(55,12);
        cout << "   {}   \\  \\_____/  /";
        gotoxy(55,13);
        cout << "   ()   /\\   )=(   /\\ ";
        gotoxy(55,14);
        cout << "   {}  /  \\_/\\=/\\_/  \\ ";
        gotoxy(55,15);
        cout << "  Ohh, what is this?  ";
        player.playersurprised = false;
    }
    else if(player.playerdead)
    {
        gotoxy(55,6);
        cout << " ,    ,    /\\   /\\ ";
        gotoxy(55,7);
        cout << "/( /\\ )\\  _\\ \\_/ /_ ";
        gotoxy(55,8);
        cout << "|\\_||_/| < \\_   _/ >";
        gotoxy(55,9);
        cout << "\\______/  \\|X   X|/";
        gotoxy(55,10);
        cout << "  _\\/_   _(_  ^  _)_";
        gotoxy(55,11);
        cout << " ( () ) /`\\|/---\\|/`\\";
        gotoxy(55,12);
        cout << "   {}   \\  \\_____/  /";
        gotoxy(55,13);
        cout << "   ()   /\\   )=(   /\\ ";
        gotoxy(55,14);
        cout << "   {}  /  \\_/\\=/\\_/  \\ ";
        gotoxy(55,15);
        cout << "  Blurghhhhh  ";
        player.playerdead = false;
    }
    else if(player.playercash)
    {
        gotoxy(55,6);
        cout << " ,    ,    /\\   /\\ ";
        gotoxy(55,7);
        cout << "/( /\\ )\\  _\\ \\_/ /_ ";
        gotoxy(55,8);
        cout << "|\\_||_/| < \\_   _/ >";
        gotoxy(55,9);
        cout << "\\______/  \\|$   $|/";
        gotoxy(55,10);
        cout << "  _\\/_   _(_  ^  _)_";
        gotoxy(55,11);
        cout << " ( () ) /`\\|\\___/|/`\\";
        gotoxy(55,12);
        cout << "   {}   \\  \\_____/  /";
        gotoxy(55,13);
        cout << "   ()   /\\   )=(   /\\ ";
        gotoxy(55,14);
        cout << "   {}  /  \\_/\\=/\\_/  \\ ";
        gotoxy(55,15);
        cout << "  KA CHING!! $$  ";
        player.playercash = false;
    }
    else
    {
        gotoxy(55,6);
        cout << " ,    ,    /\\   /\\ ";
        gotoxy(55,7);
        cout << "/( /\\ )\\  _\\ \\_/ /_ ";
        gotoxy(55,8);
        cout << "|\\_||_/| < \\_   _/ >";
        gotoxy(55,9);
        cout << "\\______/  \\|0   0|/";
        gotoxy(55,10);
        cout << "  _\\/_   _(_  ^  _)_";
        gotoxy(55,11);
        cout << " ( () ) /`\\|V^^^V|/`\\";
        gotoxy(55,12);
        cout << "   {}   \\  \\_____/  /";
        gotoxy(55,13);
        cout << "   ()   /\\   )=(   /\\ ";
        gotoxy(55,14);
        cout << "   {}  /  \\_/\\=/\\_/  \\ ";
        gotoxy(55,15);
        cout << "                            ";
    }
}

void UpdateInput()
{
    //! store the old position before the input
    player.rowOld = player.row;
    player.columnOld = player.column;

    if(GetAsyncKeyState(VK_LEFT))
    {
        if(player.column != 1)
        {
                player.column--;
        }

    }
    else if(GetAsyncKeyState(VK_RIGHT))
    {
        if(player.column != mapCol - 2)
        {
                player.column++;
        }

    }
    else if(GetAsyncKeyState(VK_UP))
    {
        if(player.row != 1)
        {
            player.row--;
        }
    }
    else if(GetAsyncKeyState(VK_DOWN))
    {
        if(player.row != mapRow - 2)
        {
                player.row++;
        }

    }
    else if(GetAsyncKeyState(VK_ESCAPE))
    {
        isExit = true;
    }
}

void BattleMenu()
{
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
    cout << "BATTLE MENU";

    gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
    cout << "===========";

    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "1. FIGHT";

    gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
    cout << "2. RUN";

battleselection:
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
    cout << "Your Choice : ";

    float choice;
    cin >> choice;

        if(choice == 1)
        {
            //! call battle system
            BattleSimulation();
        }
        else if(choice == 2)
        {
            player.row = player.rowOld;
            player.column = player.columnOld;
        }
        else if(choice != 1 || choice != 2)
        {
            goto battleselection;
        }

}

void ClearMenuArea()
{




    for(int i=0; i<15; i++)
    {
        gotoxy(mapOffSetX,
                mapOffSetY + mapRow + i + 1);
               cout <<"                                                                     ";
    }
}
void ShowEnemyInfo()
{
    gotoxy(42,31);
    cout << "Enemy Information";
    gotoxy(42,32);
    cout << "=================";
    gotoxy(42,33);
    cout << "Health : " << theEnemy.health;
    gotoxy(42,34);
    cout << "Attack : " << theEnemy.attack;
}

void BattleSimulation()
{

    bool isBattleEnded = false;
    while(!isBattleEnded)
    {
        ClearMenuArea();
        ShowPlayerInfo();
        ShowEnemyInfo();
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
        cout << "BATTLE SIMULATION";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
        cout << "=================";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "1. ATTACK";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
        cout << "2. EVADE";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
        cout << "3. FLEE";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 6);
        cout << "4. HP POT";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 7);
        cout << "5. MANA POT";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
        cout << "Your Choice : ";
        int choice;
        cin >> choice;
    if(theEnemy.health<= 0 || player.health<= 0)
    {
                isBattleEnded = true;
    }else{
        if (choice == 1)
        {
            theEnemy.health -= player.attack;
            gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
            cout << "You Inflict " << player.attack << " damage to the enemy.";
            if(theEnemy.health<= 0 || player.health<= 0)
            {
                isBattleEnded = true;
            }
            else
            {
                player.health -= theEnemy.attack;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 10);
                cout << "Enemy inflict " << theEnemy.attack<<" damage to you.";
                player.playerhurt = true;
                ShowPlayerInfo();
            }
        }
        else if (choice == 2)
        {
            if(rand()%2 == 0)
            {
                player.health -= theEnemy.attack;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "Enemy inflicted " << theEnemy.attack<<" damage to you.";

            }
            else
            {
                player.health += 5;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "Evade Successful, some health gained.";
            }

        }
        else if (choice == 3)
        {
            int randNum = rand()&10;

            if(randNum >= 0 && randNum <= 7)
            {
                player.row = player.rowOld;
                player.column = player.columnOld;
                isBattleEnded = true;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "You have successfully escaped.";
            }
            else
            {
                theEnemy.rAttack = rand()%70;
                player.health -= theEnemy.rAttack;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "You tried to flee, but you failed!";
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 10);
                cout << "Enemy inflicted " << theEnemy.rAttack<<" damage to you.";
                player.playerhurt = true;
                ShowPlayerInfo();

            }

        }
        else if (choice == 4)
        {
            if(player.potHP > 0)
            {
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "Your HP Potions restored 100 HP.";
                player.health += 100;
                player.playerhappy = true;
                player.potHP --;
                ShowPlayerInfo();

            }else
            {
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "You don't have any HP Potions left.";
            }
        }
        else if (choice == 5)
        {
            if(player.potMP > 0)
            {
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "Your HP Potions restored 100 HP.";
                player.mana += 100;
                player.playerhappy = true;
                player.potHP --;
                ShowPlayerInfo();

            }else
            {
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "You don't have any HP Potions left.";
            }
        }
    }
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 13);
    system("PAUSE");
    }


//! result screen

    ClearMenuArea();
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
    cout << "BATTLE RESULT";
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
    cout << "=============";
    if(player.health > 0)
    {
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        RandGold:
        int randNum = rand()&150;
        if(randNum < 35)
        {
            goto RandGold;
        }
        player.money += randNum;
        cout << "You Win and looted " << randNum << " Gold!";
    }
    else
    {
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "You were beaten, the village saved you and has sent you back home!";
        player.playerdead = true;
        ShowPlayerInfo();
        player.row = startPosX;
        player.column = startPosY;
        player.health = 100;
    }
    map[player.row][player.column] = '0';
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 13);
    system("PAUSE");

    theEnemy.Initialize();
}

void ItemCollision()
{
    player.playerhappy = true;
    player.health += 50;
    map[player.row][player.column] = '0';
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "You found a potion! Plus 50 HP!";
    //system("PAUSE");
}

void DoorCollision()
{
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
        cout << "DOOR MENU";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
        cout << "=================";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "1. OPEN";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
        cout << "2. Cancel";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
        cout << "Your Choice : ";
        int choice;
        cin >> choice;
        if(choice == 1)
        {
            if(player.keyAmount != 0)
            {
                map[player.row][player.column] = '0';
                player.keyAmount--;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
                cout << "Click! The door opened!";
            }
            else
            {
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
                cout << "Looks like you don't have a key!";
                player.row = player.rowOld;
                player.column = player.columnOld;
                ShowMap();
            }
        }else{
                player.row = player.rowOld;
                player.column = player.columnOld;
                ShowMap();
        }
}

void RandomItem()
{
    ShowPlayerInfo();
    int randNum = rand()&10;

     if(randNum >= 0 && randNum <= 4)
     {
        ItemCollision();
     }
     else if(randNum > 4 && randNum <= 8)
     {
         CashCollision();
     }
     else
     {
        TrapCollision();
     }


}

void TrapCollision()
{
    int randPoision = rand()&50;
    player.playerhurt = true;
    player.health -= randPoision;
    map[player.row][player.column] = '0';
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "You have been poisoned for " << randPoision << " damage!";
    if(player.health <= 0)
    {
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "You were beaten, the village saved you and has sent you back home!";
        player.playerdead = true;
        ShowPlayerInfo();
        player.row = startPosX;
        player.column = startPosY;
        player.health = 100;
    }
}

void KeyCollision()
{
    player.playerhappy = true;
    player.keyAmount++;
    map[player.row][player.column] = '0';
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "You found a key!";
}

void CollisionDection()
{
    if(map[player.row][player.column]==(char)item)
    {
        ItemCollision();
    }
    if(map[player.row][player.column]==(char)home)
    {
        HomeCollision();
    }
    if(map[player.row][player.column]==(char)cash)
    {
        CashCollision();
    }

    if(map[player.row][player.column]==(char)tree)
    {
        player.row = player.rowOld;
        player.column = player.columnOld;
        ShowMap();
    }
    if(map[player.row][player.column]==(char)wall)
    {
        player.row = player.rowOld;
        player.column = player.columnOld;
        ShowMap();
    }
    if(map[player.row][player.column]==(char)home)
    {
        isExit = true;
    }
    if(map[player.row][player.column]==(char)key)
    {
        KeyCollision();
    }
    if(map[player.row][player.column]==(char)shop)
    {
        ShopMenu();
    }
    if(map[player.row][player.column]==(char)rItem)
    {
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "Oh a mysterious item! I wonder what it is.";
        player.playersurprised = true;
        ShowPlayerInfo();
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
        system("PAUSE");
        ClearMenuArea();
        RandomItem();
    }
    if(map[player.row][player.column]==(char)trap)
    {
        TrapCollision();
    }
        if(map[player.row][player.column]==(char)door)
    {
        DoorCollision();
    }
    if(map[player.row][player.column]==(char)e1)
    {
        BattleMenu();
    }

}

void TitleScreen()
{
    gotoxy(10,4);
    cout <<"Welcome to Sean-Li's \n";
    textcolor(LIGHTCYAN);

    gotoxy(10,6);
    cout    << block << block << block << block << block << block << block << space //t
            << block << block << block << block << block << block << block << space //e
            << block << space << space << space << space << space << block << space //x
            << block << block << block << block << block << block << block << space //t

            << space << space

            << space << block << block << block << block << block << space << space //g
            << space << space << block << block << block << space << space << space //a
            << block << space << space << space << space << space << block << space //m
            << block << block << block << block << block << block << block << space; //e

    gotoxy(10,7);
    cout    << space << space << space << block << space << space << space << space //t
            << block << space << space << space << space << space << space << space //e
            << space << block << space << space << space << block << space << space //x
            << space << space << space << block << space << space << space << space //t

            << space << space

            << block << space << space << space << space << space << block << space //g
            << space << block << space << space << space << block << space << space //a
            << block << block << space << space << space << block << block << space //m
            << block << space << space << space << space << space << space << space; //e

    gotoxy(10,8);
    cout    << space << space << space << block << space << space << space << space //t
            << block << space << space << space << space << space << space << space //e
            << space << space << block << space << block << space << space << space //x
            << space << space << space << block << space << space << space << space //t

            << space << space

            << block << space << space << space << space << space << space << space //g
            << space << block << space << space << space << block << space << space //a
            << block << block << space << space << space << block << block << space //m
            << block << space << space << space << space << space << space << space; //e

    gotoxy(10,9);
    cout    << space << space << space << block << space << space << space << space //t
            << block << block << block << block << block << space << space << space //e
            << space << space << space << block << space << space << space << space //x
            << space << space << space << block << space << space << space << space //t

            << space << space

            << block << space << space << block << block << block << block << space //g
            << block << block << block << block << block << block << block << space //a
            << block << space << block << block << block << space << block << space //m
            << block << block << block << block << space << space << space << space; //e

    gotoxy(10,10);
    cout    << space << space << space << block << space << space << space << space //t
            << block << space << space << space << space << space << space << space //e
            << space << space << block << space << block << space << space << space //x
            << space << space << space << block << space << space << space << space //t

            << space << space

            << block << space << space << space << space << space << block << space //g
            << block << space << space << space << space << space << block << space //a
            << block << space << space << space << space << space << block << space //m
            << block << space << space << space << space << space << space << space; //e

    gotoxy(10,11);
    cout    << space << space << space << block << space << space << space << space //t
            << block << space << space << space << space << space << space << space //e
            << space << block << space << space << space << block << space << space //x
            << space << space << space << block << space << space << space << space //t

            << space << space

            << block << space << space << space << space << space << block << space //g
            << block << space << space << space << space << space << block << space //a
            << block << space << space << space << space << space << block << space //m
            << block << space << space << space << space << space << space << space; //e

    gotoxy(10,12);
    cout    << space << space << space << block << space << space << space << space //t
            << block << block << block << block << block << block << block << space //e
            << block << space << space << space << space << space << block << space //x
            << space << space << space << block << space << space << space << space //t

            << space << space

            << space << block << block << block << block << block << space << space //g
            << block << space << space << space << space << space << block << space //a
            << block << space << space << space << space << space << block << space //m
            << block << block << block << block << block << block << block << space; //e

    cout << endl;
    textcolor(WHITE);
}

void WinScreen()
{
    gotoxy(10,6);

}

void TitleSelection()
{
    gotoxy(15,15);
    cout << "1. Start Game                      " << endl;
    gotoxy(15,16);
    cout << "2. Controls                        " << endl;
    gotoxy(15,17);
    cout << "3. Credits                         " << endl;
    gotoxy(15,18);
    cout << "4. Exit                            " << endl;
    gotoxy(15,19);
    cout << "                                     ";
    gotoxy(15,20);
    cout << "                                     ";
    gotoxy(15,21);
    cout << "Please enter your selection : ";
    cin >> selection;


switch(selection)
{
                case 1:
                    gotoxy(15,15);
                    cout << "1. Warrior                     " << endl;
                    gotoxy(15,16);
                    cout << "2. Archer                      " << endl;
                    gotoxy(15,17);
                    cout << "3. Mage                        " << endl;
                    gotoxy(15,18);
                    cout << "4. Return to main menu.        " << endl;
                    gotoxy(15,19);
                    cout << "                               ";
                    gotoxy(15,20);
                    cout << "                                    ";
                    gotoxy(15,21);
                    cout << "Please enter your selection : ";
                    cin >> charSelect;
                    switch(charSelect)
                        {
            case 1:
                player.Initialize(WARRIOR);
                gotoxy(15,20);
                cout << "You have chosen " << player.heroType << endl;
                break;
            case 2:
                player.Initialize(ARCHER);
                gotoxy(15,20);
                cout << "You have chosen " << player.heroType << endl;
                break;
            case 3:
                player.Initialize(MAGE);
                gotoxy(15,20);
                cout << "You have chosen " << player.heroType << endl;
                break;
            case 4:
                TitleSelection();
                break;
            default:
                gotoxy(15,20);
                cout << "Invalid Input, returning to Title Selection" << endl;
                TitleSelection();
                break;


            }
    break;
                case 2:
                    gotoxy(15,15);
                    cout << "ArrowUp    = Up             " << endl;
                    gotoxy(15,16);
                    cout << "ArrowDown  = Down           " << endl;
                    gotoxy(15,17);
                    cout << "ArrowLeft  = Left           " << endl;
                    gotoxy(15,18);
                    cout << "ArrowRight = Right          " << endl;
                    gotoxy(15,19);
                    cout << "Enter '1' to Return to main menu.     " << endl;
                    gotoxy(15,20);
                    cout << "                            " << endl;
                    gotoxy(15,21);
                    cout << "Please enter your selection : ";
                    cin >> charSelect;
                        switch(charSelect)
                            {
                            case 1:
                                TitleSelection();
                                break;
                            default:
                                TitleSelection();
                                break;
                            }
    break;
                case 3:
                    gotoxy(15,15);
                    cout << "This game was created by:   " << endl;
                    gotoxy(15,16);
                    cout << "Sean-Li Joachim Murmann     " << endl;
                    gotoxy(15,17);
                    cout << "In Fundamentals of Programming " << endl;
                    gotoxy(15,18);
                    cout << "under supervision of                    " << endl;
                    gotoxy(15,19);
                    cout << "Jason YS                                    " << endl;
                    gotoxy(15,20);
                    cout << "Enter '1' to Return to main menu.     " << endl;
                    gotoxy(15,21);
                    cout << "Please enter your selection : ";
                    cin >> charSelect;
                        switch(charSelect)
                            {
                            case 1:
                                TitleSelection();
                                break;
                            default:
                                TitleSelection();
                                break;
                            }

                case 4:
                    isExit = true;
                    break;


                default:
                TitleSelection();
                break;
}


}

void ShopMenu()
{
    bool inShop = true;
       do
        {
        ClearMenuArea();
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
        cout << "SHOP MENU - Welcome young adventurer! Look around! ";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
        cout << "=================";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "1. HP Potion           :  20 ";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
        cout << "2. MP Potion           :  20";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
        cout << "3. Attack Upgrade + 25 : 150";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 6);
        cout << "4. Exit";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 7);
        cout << "Your Choice : ";
        int choice;
        cin >> choice;
        int potAmount;
            if(choice == 1)
            {
                gotoxy(mapOffSetX, mapOffSetX + mapRow + 9);
                cout << "How many would you like? : ";
                cin >> potAmount;

                if(player.money > 20*potAmount)
                {
                    for(int i=0; i<potAmount; i++)
                    {
                        player.potHP++;
                        player.money -= 20;
                    }
                    player.playercash = true;
                    ShowPlayerInfo();
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 10);
                    cout << "You now have " << player.potHP << " Health Potions.";
                }
                else
                {
                    player.playersurprised = true;
                    ShowPlayerInfo();
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 10);
                    cout << "Sorry, you have insufficient funds.";
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 11);
                    system("PAUSE");
                }
            }
            else if(choice == 2)
            {
                gotoxy(mapOffSetX, mapOffSetX + mapRow + 9);
                cout << "How many would you like? : ";
                cin >> potAmount;
                ClearMenuArea();

                if(player.money > 20*potAmount)
                {
                    for(int i=0; i<potAmount; i++)
                    {
                        player.potMP++;
                        player.money -= 20;
                    }
                    player.playercash = true;
                    ShowPlayerInfo();
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 10);
                    cout << "You now have " << player.potMP << " Mana Potions.";
                }
                else
                {
                    player.playersurprised = true;
                    ShowPlayerInfo();
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 10);
                    cout << "Sorry, you have insufficient funds.";
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 11);
                    system("PAUSE");
                }
            }
            else if(choice == 3)
            {
                gotoxy(mapOffSetX, mapOffSetX + mapRow + 9);
                cout << "How many would you like? : ";
                cin >> potAmount;
                ClearMenuArea();

                if(player.money > 150*potAmount)
                {
                    for(int i=0; i<potAmount; i++)
                    {
                        player.attack += 25;
                        player.money -= 150;
                    }
                    player.playercash = true;
                    ShowPlayerInfo();
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 10);
                    cout << "Your attack now deals " << player.attack << " Damage.";
                }
                else
                {
                    player.playersurprised = true;
                    ShowPlayerInfo();
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 10);
                    cout << "Sorry, you have insufficient funds.";
                    gotoxy(mapOffSetX, mapOffSetX + mapRow + 11);
                    system("PAUSE");
                }
            }
            else
            {
                gotoxy(mapOffSetX, mapOffSetX + mapRow + 9);
                cout << "Please come again!";
                inShop = false;
            }
        }while(inShop);

}

void CashCollision()
{
    int cashamount = 0;
    while(cashamount < 150)
    {
        cashamount = rand()&550;
    }
    player.money += cashamount;
    player.playercash = true;
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "You have aquired " << cashamount << " gold!";
    map[player.row][player.column] = '0';
    ShowPlayerInfo();

}

void HomeCollision()
{
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "Congratulation you completed the game!";
    isExit = true;
}

int main(int argc, char** argv)
{
    system("mode con: cols=80 lines=50");
    textcolor(WHITE);
    srand(time(NULL));
//    player.Initialize(WARRIOR);
    theEnemy.Initialize();
    InitializeMap();


    HWND console = GetConsoleWindow();
    RECT ConsoleRect;
    GetWindowRect(console, &ConsoleRect);
    MoveWindow(console, ConsoleRect.right, ConsoleRect.top, 800, 600, TRUE);

    TitleScreen();
    TitleSelection();

    while(!isExit)
    {

        system("CLS");
        UpdateInput();
        ShowMap();
        ShowPlayerInfo();
        CollisionDection();
        ShowPlayerInfo();
        gotoxy(mapOffSetX, mapOffSetY + mapRow);
        system("PAUSE");
    }
    //system("PAUSE");
    return 0;
}
