﻿using UnityEngine;
using System.Collections;

public class SoundScript : MonoBehaviour {

    AudioSource ads;

	// Use this for initialization
	void Start () {
        ads = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.anyKeyDown)
        {
            ads.Play();
        }
	
	}

}
