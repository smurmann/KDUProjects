﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AtomCounterScript : MonoBehaviour {

	public int atomCounter = 0;

	GameObject[] atoms;

	public Text ScoreText;


	// Use this for initialization
	void Start () {

	ScoreText = GetComponent<Text>();


	}
	
	// Update is called once per frame
	void Update () {

	ScoreText.text = "Current Atoms : " + atomCounter;


	
	}
}
