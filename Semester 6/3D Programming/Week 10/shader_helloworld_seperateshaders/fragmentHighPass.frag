precision mediump float;
varying vec4 fColor;
varying vec2 fTexCoord;

uniform sampler2D sampler2d;

void main()
{
	
	//high pass filter
	vec4 texColor = texture2D(sampler2d, fTexCoord);
	float colorR = (texColor.r + texColor.g + texColor.b)/3.0;
	if(colorR <= 0.55)
	{
		texColor.r = 0.0;
		texColor.g = 0.0;
		texColor.b = 0.0;
	}
	
	gl_FragColor = texColor;
	
	/*
	vec4 texColor = texture2D(sampler2d, fTexCoord);
	float highPass = (texColor.r + texColor.g + texColor.b)/3.0;
	
	if(highPass > 0.2)
	{
		gl_FragColor = texColor;
	}
	*/

}