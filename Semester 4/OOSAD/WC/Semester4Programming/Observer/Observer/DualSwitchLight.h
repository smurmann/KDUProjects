#ifndef _DUALSWITCHLIGHT_H
#define _DUALSWITVHLIGHT_H

#include "Observer.h"
#include "Trigger.h"

#include <iostream>
using namespace std;
class DualSwitchLight :public Observer
{
protected:
	//! lightSwitch01
	Trigger * mSwitch1;

	//! lightSwitch02
	Trigger * mSwitch2;

	//! stage of light(on or off)
	int mLightState;

	//! Light enable flag
	bool mEnable;

public:
	DualSwitchLight(Trigger * switch1, Trigger* switch2) :mSwitch1(switch1), mSwitch2(switch2), mLightState(0), mEnable(true)
	{
		switch1->AddObserver(this);
		switch2->AddObserver(this);

	}
	//!enable/disable light
	//!add/remove Observer
	void setEneble(bool enable)
	{
		if (mEnable == enable)return;
		mEnable = enable;
		if (enable)
		{
			mSwitch1->AddObserver(this);
			mSwitch2->AddObserver(this);
			cout << "LightBulb has been Connected" << endl;
			update(NULL);
		}
		else
		{

			mSwitch1->RemoveObserver(this);
			mSwitch2->RemoveObserver(this);
			cout << "LightBulb has not Connected" << endl;
			if (mLightState)
			{
				cout << "The Room is Dark Again" << endl;
			}
		}
	}
protected:
	//!update function called when subject has notification event
	void update(Subject*subject)
	{
		int switchState1 = mSwitch1->getState() ? 1 : 0;
		int switchState2 = mSwitch2->getState() ? 1 : 0;
		mLightState = switchState1 ^ switchState2;
		if (mLightState == true)
		{
			cout << "The Room is Lit!" << endl;
		}
		else
		{
			cout << "The room is Dark Again" << endl;
		}

	}

};

#endif // !_DUALSWITCHLIGHT_H
