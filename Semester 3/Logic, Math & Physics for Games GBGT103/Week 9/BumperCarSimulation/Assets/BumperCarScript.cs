﻿using UnityEngine;
using System.Collections;

public class BumperCarScript : MonoBehaviour 
{
	float moveSpeed = 3000.0f;
	float resistance = 100.0f;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//! Initiate Movement
		if(Input.GetKey(KeyCode.A))
		{
			//transform.Translate(new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
			rigidbody.AddForce(new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.D))
		{
			//transform.Translate(new Vector3(moveSpeed, 0.0f, 0.0f)  * Time.deltaTime);
			rigidbody.AddForce(new Vector3(moveSpeed, 0.0f, 0.0f)  * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.W))
		{
			//transform.Translate(new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime);
			rigidbody.AddForce(new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.S))
		{
			//transform.Translate(new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime);
			rigidbody.AddForce(new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.R))
		{
			//transform.Translate(new Vector3(0.0f, moveSpeed, 0.0f) * Time.deltaTime);
			rigidbody.AddForce(new Vector3(0.0f, moveSpeed, 0.0f) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.F))
		{
			//transform.Translate(new Vector3(0.0f, -moveSpeed, 0.0f) * Time.deltaTime);
			rigidbody.AddForce(new Vector3(0.0f, -moveSpeed, 0.0f) * Time.deltaTime);
		}
		//! Initiate Friction when release

	}
}
