#include "week3.h"

using namespace osg;
int Week3()
{
	 //return Code1();
	 return Homework();
	//return Code2();
}

void Week3Void()
{
	Code3();
}

class Manager
{
public:
	Manager(){}

	virtual ~Manager()
	{
		std::cout<<"Manager deinit"<<std::endl;
	}

	virtual void interfaceFunction1() = 0;

	virtual void doSomething()
	{ 
		std::cout<<"from Manager"<<std::endl;
	}

};

class AudioManager : public Manager
{
public:
	AudioManager()
	{}

	// ~ is a destructor ALWAYS VIRTUAL
	virtual ~AudioManager()
	{
		std::cout<<"AudioManager deinit"<<std::endl;
	}

	virtual void interfaceFunction1()
	{
		std::cout<<"interface function audiomanager"<<std::endl;

	}
	
	virtual void doSomething()
	{ 
		//needs namespace
		//Manager::doSomething();
		std::cout<<"from AudioManager"<<std::endl;
	}
};

void Code3()
{
	AudioManager* theManager = new AudioManager();
	theManager->doSomething();
	theManager->interfaceFunction1();

	delete theManager;

	system("PAUSE");
}


int Code1()
{

	ref_ptr<ShapeDrawable> shape1 = new ShapeDrawable;
	shape1->setShape(new Box(Vec3(-3.0f,0.0f,0.0f),2.0f,2.0f,1.0f));

	ref_ptr<ShapeDrawable> shape2 = new ShapeDrawable;
	shape2->setShape(new Sphere(Vec3(3.0f,0.0f,0.0f),1.0f));
	shape2->setColor(Vec4(0.0f,0.0f,1.0f,1.0f));

	ref_ptr<ShapeDrawable> shape3 = new ShapeDrawable;
	shape3->setShape(new Cone(Vec3(0.0f,0.0f,0.0f),1.0f,1.0f));
	shape3->setColor(Vec4(0.0f,1.0f,0.0f,1.0f));

	ref_ptr<Geode> root = new Geode;
	root->addDrawable(shape1.get());
	root->addDrawable(shape2.get());
	root->addDrawable(shape3.get());

	osgViewer::Viewer viewer;

	viewer.setSceneData(root.get());

	return viewer.run();
}

int Code2()
{	
	ref_ptr<Node> model = osgDB::readNodeFile("../media/cow.osg");

	ref_ptr<MatrixTransform> transform1 = new MatrixTransform;
	transform1->setMatrix(Matrix::translate(-25.0f,0.0f,0.0f));
	transform1->addChild(model.get());

	ref_ptr<MatrixTransform> transform2 = new MatrixTransform;
	transform2->setMatrix(Matrix::translate(25.0f,0.0f,0.0f));
	transform2->addChild(model.get());

	ref_ptr<Group> root = new Group;
	root->addChild(transform1.get());
	root->addChild(transform2.get());


	osgViewer::Viewer viewer;
	viewer.setSceneData(root.get());
	return viewer.run();
}

osg::Geode* createPlanet(const std::string& name, double radius, const osg::Vec4& color)
{
	osg::Sphere* unitSphere = new osg::Sphere(osg::Vec3(0,0,0),radius);
	
	osg::ShapeDrawable* unitSphereDrawable = new osg::ShapeDrawable(unitSphere);
	unitSphereDrawable->setColor(color);
	
	osg::PositionAttitudeTransform* sphereXForm = new osg::PositionAttitudeTransform();
	sphereXForm->setPosition(osg::Vec3(2.5,0,0));

	osg::Geode* unitSphereGeode = new osg::Geode();

	sphereXForm->addChild(unitSphereGeode);
	unitSphereGeode->addDrawable(unitSphereDrawable);

	return(unitSphereGeode);
}

osg::MatrixTransform* createRotation(double orbit, double speed)
{
	osg::Vec3 center(0.0,0.0,0.0);
	float animationLenght = 10.0f;
	osg::AnimationPath* animationPath = createAnimationPath(center, orbit, animationLenght);

	osg::MatrixTransform* rotation = new osg::MatrixTransform;
	rotation->setUpdateCallback(new osg::AnimationPathCallback(animationPath, 0.0f, speed));
	return rotation;
}

osg::AnimationPath* createAnimationPath(const osg::Vec3& center,float radius,double looptime)
{
    // set up the animation path
    osg::AnimationPath* animationPath = new osg::AnimationPath;
    animationPath->setLoopMode(osg::AnimationPath::LOOP);

    int numSamples = 1000;
    float yaw = 0.0f;
    float yaw_delta = -2.0f*osg::PI/((float)numSamples-1.0f);
    float roll = osg::inDegrees(30.0f);

    double time=0.0f;
    double time_delta = looptime/(double)numSamples;
    for(int i=0;i<numSamples;++i)
    {
        osg::Vec3 position(center+osg::Vec3(sinf(yaw)*radius,cosf(yaw)*radius,0.0f));
        osg::Quat rotation(osg::Quat(roll,osg::Vec3(0.0,1.0,0.0))*osg::Quat(-(yaw+osg::inDegrees(90.0f)),osg::Vec3(0.0,0.0,1.0)));

        animationPath->insert(time,osg::AnimationPath::ControlPoint(position,rotation));

        yaw += yaw_delta;
        time += time_delta;

    }
    return animationPath;
}// end createAnimationPath

osg::MatrixTransform* createTranslationAndTilt( double /*translation*/, double tilt )
{
    osg::MatrixTransform* moonPositioned = new osg::MatrixTransform;
    moonPositioned->setMatrix(osg::Matrix::translate(osg::Vec3( 0.0, 1.0, 0.0 ) )*
                                 osg::Matrix::scale(1.0, 1.0, 1.0)*
                                 osg::Matrix::rotate(osg::inDegrees( tilt ),0.0f,0.0f,1.0f));

    return moonPositioned;
}// end SolarSystem::createTranslationAndTilt

class SolarSystem
{

public:
    double _radiusSpace;
    double _radiusSun;
    double _radiusMercury;
    double _radiusVenus;
    double _radiusEarth;
    double _radiusMoon;
    double _radiusMars;
    double _radiusJupiter;
	double _radiusSaturn;
	double _radiusUranus;
	double _radiusNeptune;
	double _radiusPluto;

    double _RorbitMercury;
    double _RorbitVenus;
    double _RorbitEarth;
    double _RorbitMoon;
    double _RorbitMars;
    double _RorbitJupiter;
    double _RorbitSaturn;
    double _RorbitUranus;
    double _RorbitNeptune;
    double _RorbitPluto;

    double _rotateSpeedSun;
    double _rotateSpeedMercury;
    double _rotateSpeedVenus;
    double _rotateSpeedEarthAndMoon;
    double _rotateSpeedEarth;
    double _rotateSpeedMoon;
    double _rotateSpeedMars;
    double _rotateSpeedJupiter;
    double _rotateSpeedSaturn;
    double _rotateSpeedUranus;
    double _rotateSpeedNeptune;
    double _rotateSpeedPluto;

    double _tiltEarth;

    std::string _mapSpace;
    std::string _mapSun;
    std::string _mapVenus;
    std::string _mapMercury;
    std::string _mapEarth;
    std::string _mapEarthNight;
    std::string _mapMoon;
    std::string _mapMars;
    std::string _mapJupiter;

    double _rotateSpeedFactor;
    double _RorbitFactor;
    double _radiusFactor;

    SolarSystem()
    {
        _radiusSpace    = 500.0;
        _radiusSun      = 109.0;
        _radiusMercury  = 0.38;
        _radiusVenus    = 0.95;
        _radiusEarth    = 1.0;
        _radiusMoon     = 0.27;
        _radiusMars     = 0.53;
        _radiusJupiter  = 11.0;
		_radiusSaturn   = 9.45;
		_radiusUranus   = 4.01;
		_radiusNeptune  = 3.88;
		_radiusPluto    = 0.186;
		 

        _RorbitMercury  = 11.7;
        _RorbitVenus    = 21.6;
        _RorbitEarth    = 30.0;
        _RorbitMoon     = 1.0;
        _RorbitMars     = 45.0;
        _RorbitJupiter  = 156.0;
		_RorbitSaturn   = 285.0;
		_RorbitUranus	= 576.0;
		_RorbitNeptune  = 906.0;
		_RorbitPluto    = 1185.0;

                                                // orbital period in days
        _rotateSpeedSun             = 0.0;      // should be 11.97;  // 30.5 average
        _rotateSpeedMercury         = 4.15;     // 87.96
        _rotateSpeedVenus           = 1.62;     // 224.70
        _rotateSpeedEarthAndMoon    = 1.0;      // 365.25
        _rotateSpeedEarth           = 1.0;      //
        _rotateSpeedMoon            = 0.95;     //
        _rotateSpeedMars            = 0.53;     // 686.98
        _rotateSpeedJupiter         = 0.08;     // 4332.71
		_rotateSpeedSaturn			= 0.04;		// 10,755
		_rotateSpeedUranus			= 0.0134;
		_rotateSpeedNeptune			= 0.00615;
		_rotateSpeedPluto			= 0.00444;

        _tiltEarth                  = 23.45; // degrees

        _mapSpace       = "Images/spacemap2.jpg";
        _mapSun         = "SolarSystem/sun256128.jpg";
        _mapMercury     = "SolarSystem/mercury256128.jpg";
        _mapVenus       = "SolarSystem/venus256128.jpg";
        _mapEarth       = "Images/land_shallow_topo_2048.jpg";
        _mapEarthNight  = "Images/land_ocean_ice_lights_2048.jpg";
        _mapMoon        = "SolarSystem/moon256128.jpg";
        _mapMars        = "SolarSystem/mars256128.jpg";
        _mapJupiter     = "SolarSystem/jupiter256128.jpg";

        _rotateSpeedFactor = 0.5;
        _RorbitFactor   = 15.0;
        _radiusFactor   = 10.0;
    }
};

osg::Group* createSunLight()
{

    osg::LightSource* sunLightSource = new osg::LightSource;

    osg::Light* sunLight = sunLightSource->getLight();
    sunLight->setPosition( osg::Vec4( 0.0f, 0.0f, 0.0f, 1.0f ) );
    sunLight->setAmbient( osg::Vec4( 0.0f, 0.0f, 0.0f, 1.0f ) );

    sunLightSource->setLight( sunLight );
    sunLightSource->setLocalStateSetModes( osg::StateAttribute::ON );
    sunLightSource->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::ON);

    osg::LightModel* lightModel = new osg::LightModel;
    lightModel->setAmbientIntensity(osg::Vec4(0.0f,0.0f,0.0f,1.0f));
    sunLightSource->getOrCreateStateSet()->setAttribute(lightModel);


    return sunLightSource;
}// end SolarSystem::createSunLight


int Homework()
{
	SolarSystem solarSystem;
	osg::Group* root = new osg::Group;

  osg::ClearNode* clearNode = new osg::ClearNode;
    clearNode->setClearColor(osg::Vec4(0.0f,0.0f,0.0f,1.0f));
    root->addChild(clearNode);

   

    // create the sun
    osg::Node* solarSun = createPlanet("Sun", solarSystem._radiusSun/10, osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f));
    osg::StateSet* sunStateSet = solarSun->getOrCreateStateSet();
    osg::Material* material = new osg::Material;
    material->setEmission( osg::Material::FRONT_AND_BACK, osg::Vec4( 1.0f, 1.0f, 0.0f, 0.0f ) );
    sunStateSet->setAttributeAndModes( material, osg::StateAttribute::ON );

	root->addChild(solarSun);

	
	///EARTH AND MOON STUFF
	osg::Geode* earth = createPlanet("Earth", solarSystem._radiusEarth, osg::Vec4(0.0,1.0,0.5,1.0));
	osg::Geode* moon = createPlanet("Moon", solarSystem._radiusMoon, osg::Vec4(0.0,1.0,0.5,1.0));

	osg::MatrixTransform* aroundSunRotationEarthMoonGroup = createRotation(solarSystem._RorbitEarth, solarSystem._rotateSpeedEarthAndMoon);
	osg::MatrixTransform* earthMoonGroupPosition = createTranslationAndTilt(solarSystem._RorbitEarth,0.0);

	osg::Group* earthMoonGroup = new osg::Group;

	osg::MatrixTransform* earthAroundItselfRotation = createRotation(0.0, solarSystem._rotateSpeedEarth);


	osg::MatrixTransform* moonAroundEarthRotation = createRotation(solarSystem._RorbitMoon, solarSystem._rotateSpeedMoon);
	osg::MatrixTransform* moonTranslation = createTranslationAndTilt(solarSystem._RorbitMoon, 0.0);

	moonTranslation->addChild(moon);
	moonAroundEarthRotation->addChild(moonTranslation);
	earthMoonGroup->addChild(moonAroundEarthRotation);

	earthAroundItselfRotation->addChild(earth);
	earthMoonGroup->addChild(earthAroundItselfRotation);

	earthMoonGroupPosition->addChild(earthMoonGroup);

	aroundSunRotationEarthMoonGroup->addChild(earthMoonGroupPosition);

	root->addChild(aroundSunRotationEarthMoonGroup);
	///////////////////////////////////////////////////////////////////////////
	///MERCURY

	osg::Node* mercury = createPlanet("Mercury", solarSystem._radiusMercury, osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f));

    osg::MatrixTransform* aroundSunRotationMercury = createRotation( solarSystem._RorbitMercury, solarSystem._rotateSpeedMercury );
    osg::MatrixTransform* mercuryPosition = createTranslationAndTilt( solarSystem._RorbitMercury, 0.0f );

    mercuryPosition->addChild( mercury );
    aroundSunRotationMercury->addChild( mercuryPosition );

    root->addChild( aroundSunRotationMercury );
	////////////////////////////////

	osg::Node* venus = createPlanet("Venus", solarSystem._radiusVenus,  osg::Vec4( 1.0f, 0.0f, 0.0f, 1.0f ));

	///VENUS
    osg::MatrixTransform* aroundSunRotationVenus = createRotation( solarSystem._RorbitVenus, solarSystem._rotateSpeedVenus );
    osg::MatrixTransform* venusPosition = createTranslationAndTilt( solarSystem._RorbitVenus, 0.0f );

    venusPosition->addChild( venus );
    aroundSunRotationVenus->addChild( venusPosition );

    root->addChild( aroundSunRotationVenus );
	//////////////
	///Mars
	osg::Node* mars = createPlanet("Mars", solarSystem._radiusMars,  osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f ));

    osg::MatrixTransform* aroundSunRotationMars = createRotation( solarSystem._RorbitMars, solarSystem._rotateSpeedMars );
    osg::MatrixTransform* marsPosition = createTranslationAndTilt( solarSystem._RorbitMars, 0.0f );

    marsPosition->addChild( mars );
    aroundSunRotationMars->addChild( marsPosition );

    root->addChild( aroundSunRotationMars );
	///
	///Jupiter
	osg::Node* jupiter = createPlanet("Jupiter", solarSystem._radiusJupiter,  osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f ));

    osg::MatrixTransform* aroundSunRotationJupiter = createRotation( solarSystem._RorbitJupiter, solarSystem._rotateSpeedJupiter );
    osg::MatrixTransform* jupiterPosition = createTranslationAndTilt( solarSystem._RorbitJupiter, 0.0f );

    jupiterPosition->addChild( jupiter );
    aroundSunRotationJupiter->addChild( jupiterPosition );

    root->addChild( aroundSunRotationJupiter );
	/////////////////////////////////
	///Saturn
	osg::Node* saturn = createPlanet("Saturn", solarSystem._radiusSaturn,  osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f ));

    osg::MatrixTransform* aroundSunRotationSaturn = createRotation( solarSystem._RorbitSaturn, solarSystem._rotateSpeedSaturn );
    osg::MatrixTransform* saturnPosition = createTranslationAndTilt( solarSystem._RorbitSaturn, 0.0f );

    saturnPosition->addChild( saturn );
    aroundSunRotationSaturn->addChild( saturnPosition );

    root->addChild( aroundSunRotationSaturn );
	///
	///Uranus
		osg::Node* uranus = createPlanet("Uranus", solarSystem._radiusUranus,  osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f ));

    osg::MatrixTransform* aroundSunRotationUranus = createRotation( solarSystem._RorbitUranus, solarSystem._rotateSpeedUranus );
    osg::MatrixTransform* uranusPosition = createTranslationAndTilt( solarSystem._RorbitUranus, 0.0f );

    uranusPosition->addChild( uranus );
    aroundSunRotationUranus->addChild( uranusPosition );

    root->addChild( aroundSunRotationUranus );
	///
	///Neptune
		osg::Node* neptune = createPlanet("Neptune", solarSystem._radiusNeptune,  osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f ));

    osg::MatrixTransform* aroundSunRotationNeptune = createRotation( solarSystem._RorbitNeptune, solarSystem._rotateSpeedNeptune );
    osg::MatrixTransform* neptunePosition = createTranslationAndTilt( solarSystem._RorbitNeptune, 0.0f );

    neptunePosition->addChild( neptune );
    aroundSunRotationNeptune->addChild( neptunePosition );

    root->addChild( aroundSunRotationNeptune );
	///
	///PLUTO
		osg::Node* pluto = createPlanet("Pluto", solarSystem._radiusPluto,  osg::Vec4( 1.0f, 1.0f, 1.0f, 1.0f ));

    osg::MatrixTransform* aroundSunRotationPluto = createRotation( solarSystem._RorbitPluto, solarSystem._rotateSpeedPluto );
    osg::MatrixTransform* plutoPosition = createTranslationAndTilt( solarSystem._RorbitPluto, 0.0f );

    plutoPosition->addChild( pluto );
    aroundSunRotationPluto->addChild( plutoPosition );

    root->addChild( aroundSunRotationPluto );
	///

	osgViewer::Viewer viewer;
	viewer.setSceneData(root);
	return viewer.run();
}


/// HOMEWORK

//class SolarSystem
//{
//
//public:
//    double _radiusSpace;
//    double _radiusSun;
//    double _radiusMercury;
//    double _radiusVenus;
//    double _radiusEarth;
//    double _radiusMoon;
//    double _radiusMars;
//    double _radiusJupiter;
//
//    double _RorbitMercury;
//    double _RorbitVenus;
//    double _RorbitEarth;
//    double _RorbitMoon;
//    double _RorbitMars;
//    double _RorbitJupiter;
//
//    double _rotateSpeedSun;
//    double _rotateSpeedMercury;
//    double _rotateSpeedVenus;
//    double _rotateSpeedEarthAndMoon;
//    double _rotateSpeedEarth;
//    double _rotateSpeedMoon;
//    double _rotateSpeedMars;
//    double _rotateSpeedJupiter;
//
//    double _tiltEarth;
//
//    std::string _mapSpace;
//    std::string _mapSun;
//    std::string _mapVenus;
//    std::string _mapMercury;
//    std::string _mapEarth;
//    std::string _mapEarthNight;
//    std::string _mapMoon;
//    std::string _mapMars;
//    std::string _mapJupiter;
//
//    double _rotateSpeedFactor;
//    double _RorbitFactor;
//    double _radiusFactor;
//
//    SolarSystem()
//    {
//        _radiusSpace    = 500.0;
//        _radiusSun      = 109.0;
//        _radiusMercury  = 0.38;
//        _radiusVenus    = 0.95;
//        _radiusEarth    = 1.0;
//        _radiusMoon     = 0.1;
//        _radiusMars     = 0.53;
//        _radiusJupiter  = 5.0;
//
//        _RorbitMercury  = 11.7;
//        _RorbitVenus    = 21.6;
//        _RorbitEarth    = 30.0;
//        _RorbitMoon     = 1.0;
//        _RorbitMars     = 45.0;
//        _RorbitJupiter  = 156.0;
//
//                                                // orbital period in days
//        _rotateSpeedSun             = 0.0;      // should be 11.97;  // 30.5 average
//        _rotateSpeedMercury         = 4.15;     // 87.96
//        _rotateSpeedVenus           = 1.62;     // 224.70
//        _rotateSpeedEarthAndMoon    = 1.0;      // 365.25
//        _rotateSpeedEarth           = 1.0;      //
//        _rotateSpeedMoon            = 0.95;     //
//        _rotateSpeedMars            = 0.53;     // 686.98
//        _rotateSpeedJupiter         = 0.08;     // 4332.71
//
//        _tiltEarth                  = 23.45; // degrees
//
//        _mapSpace       = "Images/spacemap2.jpg";
//        _mapSun         = "SolarSystem/sun256128.jpg";
//        _mapMercury     = "SolarSystem/mercury256128.jpg";
//        _mapVenus       = "SolarSystem/venus256128.jpg";
//        _mapEarth       = "Images/land_shallow_topo_2048.jpg";
//        _mapEarthNight  = "Images/land_ocean_ice_lights_2048.jpg";
//        _mapMoon        = "SolarSystem/moon256128.jpg";
//        _mapMars        = "SolarSystem/mars256128.jpg";
//        _mapJupiter     = "SolarSystem/jupiter256128.jpg";
//
//        _rotateSpeedFactor = 0.5;
//        _RorbitFactor   = 15.0;
//        _radiusFactor   = 10.0;
//    }
//
//    osg::MatrixTransform* createTranslationAndTilt( double translation, double tilt );
//    osg::MatrixTransform* createRotation( double orbit, double speed );
//
//    osg::Geode* createSpace( const std::string& name, const std::string& textureName );
//	//without textures
//    osg::Geode* createPlanet( double radius, const std::string& name, const osg::Vec4& color );
//    osg::Geode* createPlanet( double radius, const std::string& name, const osg::Vec4& color , const std::string& textureName );
//    osg::Geode* createPlanet( double radius, const std::string& name, const osg::Vec4& color , const std::string& textureName1, const std::string& textureName2);
//    osg::Group* createSunLight();
//
//    void rotateSpeedCorrection()
//    {
//        _rotateSpeedSun             *= _rotateSpeedFactor;
//        _rotateSpeedMercury         *= _rotateSpeedFactor;
//        _rotateSpeedVenus           *= _rotateSpeedFactor;
//        _rotateSpeedEarthAndMoon    *= _rotateSpeedFactor;
//        _rotateSpeedEarth           *= _rotateSpeedFactor;
//        _rotateSpeedMoon            *= _rotateSpeedFactor;
//        _rotateSpeedMars            *= _rotateSpeedFactor;
//        _rotateSpeedJupiter         *= _rotateSpeedFactor;
//
//        std::cout << "rotateSpeed corrected by factor " << _rotateSpeedFactor << std::endl;
//    }
//
//    void RorbitCorrection()
//    {
//        _RorbitMercury  *= _RorbitFactor;
//        _RorbitVenus    *= _RorbitFactor;
//        _RorbitEarth    *= _RorbitFactor;
//        _RorbitMoon     *= _RorbitFactor;
//        _RorbitMars     *= _RorbitFactor;
//        _RorbitJupiter  *= _RorbitFactor;
//
//        std::cout << "Rorbits corrected by factor " << _RorbitFactor << std::endl;
//    }
//
//    void radiusCorrection()
//    {
//        _radiusSpace    *= _radiusFactor;
//        //_radiusSun      *= _radiusFactor;
//        _radiusMercury  *= _radiusFactor;
//        _radiusVenus    *= _radiusFactor;
//        _radiusEarth    *= _radiusFactor;
//        _radiusMoon     *= _radiusFactor;
//        _radiusMars     *= _radiusFactor;
//        _radiusJupiter  *= _radiusFactor;
//
//        std::cout << "Radius corrected by factor " << _radiusFactor << std::endl;
//    }
//    void printParameters();
//
//};  // end SolarSystem
//
//osg::Group* SolarSystem::createSunLight()
//{
//	osg::LightSource* sunLightSource = new osg::LightSource;
//
//	osg::Light* sunLight = sunLightSource->getLight();
//	sunLight->setPosition(osg::Vec4(0.0f,0.0f,0.0f,1.0f));
//	sunLight->setAmbient(osg::Vec4(0.0f,0.0f,0.0f,1.0f));
//
//	sunLightSource->setLight(sunLight);
//	sunLightSource->setLocalStateSetModes(osg::StateAttribute::ON);
//	sunLightSource->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::ON);
//
//	//OSG LIGHTMODEL DOESNT EXSIST IN THIS NAMESPACE
//
//}
//
//osg::Geode* SolarSystem::createPlanet(double radius, const std::string& name, const osg::Vec4& color) //usually would include texture
//{
//	// container to draw a sphere
//	osg::Geometry *sPlanetSphere = new osg::Geometry();
//	{
//		// setting single color
//		osg::Vec4Array* colours = new osg::Vec4Array(1);
//		(*colours)[0] = color;
//		sPlanetSphere->setColorArray(colours);
//
//		//coords setup for normals and texture coords
//		unsigned int numX = 100;
//		unsigned int numY = 50;
//		unsigned int numVertices = numX* numY;
//
//		osg::Vec3Array* coords = new osg::Vec3Array(numVertices);
//		sPlanetSphere->setVertexArray(coords);
//
//		osg::Vec3Array* normals = new osg::Vec3Array(numVertices);
//		sPlanetSphere->setNormalArray(normals);
//
//		osg::Vec2Array* texcoords = new osg::Vec2Array(numVertices);
//		sPlanetSphere->setTexCoordArray(0,texcoords);
//		sPlanetSphere->setTexCoordArray(1,texcoords);
//
//		double delta_elevation = osg::PI/(double)(numY-1);
//		double delta_azim = 2.0*osg::PI/(double)(numX-1_;
//		float delta_tx = 1.0 / (float)(numX-1);
//		float delta_ty = 1.0 / (float)(numY-1);
//
//		 double elevation = -osg::PI*0.5;
//        float ty = 0.0;
//        unsigned int vert = 0;
//        unsigned j;
//        for(j=0;
//            j<numY;
//            ++j, elevation+=delta_elevation, ty+=delta_ty )
//        {
//            double azim = 0.0;
//            float tx = 0.0;
//            for(unsigned int i=0;
//                i<numX;
//                ++i, ++vert, azim+=delta_azim, tx+=delta_tx)
//            {
//                osg::Vec3 direction(cos(azim)*cos(elevation), sin(azim)*cos(elevation), sin(elevation));
//                (*coords)[vert].set(direction*radius);
//                (*normals)[vert].set(direction);
//                (*texcoords)[vert].set(tx,ty);
//            }
//        }
//
//        for(j=0;
//            j<numY-1;
//            ++j)
//        {
//            unsigned int curr_row = j*numX;
//            unsigned int next_row = curr_row+numX;
//            osg::DrawElementsUShort* elements = new osg::DrawElementsUShort(GL_QUAD_STRIP);
//            for(unsigned int i=0;
//                i<numX;
//                ++i)
//            {
//                elements->push_back(next_row + i);
//                elements->push_back(curr_row + i);
//            }
//            sPlanetSphere->addPrimitiveSet(elements);
//	}
//
//		 // set the object color
//    //sPlanetSphere->( color );
//
//    // create a geode object to as a container for our drawable sphere object
//    osg::Geode* geodePlanet = new osg::Geode();
//    geodePlanet->setName( name );
//
//   //no texture to wrap
//
//    // add our drawable sphere to the geode container
//    geodePlanet->addDrawable( sPlanetSphere );
//
//    return( geodePlanet );
//}

