#ifndef _MUSICBOX_H
#define _MUSICBOX_H

#include "Observer.h"
#include "Trigger.h"
#include <iostream>
using namespace std;

class MusicBox :public Observer
{

protected:
	//! update function
	//! subject has notification event
	void update(Subject * subject)
	{
		bool triggerIsOn = ((Trigger*)subject)->getState();
		if (triggerIsOn)
		{
			cout << "Music Box is Playing music !! " << endl;

		}
		else
		{
			cout << "Music Box stopped Playing Music !!" << endl;

		}
		Trigger* trigger = (Trigger*)subject;
		trigger->setState(!trigger->getState());

	}

};

#endif