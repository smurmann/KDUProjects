﻿using UnityEngine;
using System.Collections;

public class tutorialScript : MonoBehaviour {
    int state;
    SpriteRenderer sr;
    public Sprite s1;
    public Sprite s2;
    public Sprite s3;
    public Sprite s4;
    public Sprite s5;
   

	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        state = 1;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        ChangeState();
    }

    void ChangeState()
    {
        state++;
        if(state == 1)
        {
            sr.sprite = s1;
        }
        else if(state == 2)
        {
            sr.sprite = s2;
        }
        else if(state == 3)
        {
            sr.sprite = s3;
        }
        else if(state == 4)
        {
            sr.sprite = s4;            
        }
        else if(state == 5)
        {
            sr.sprite = s5;
        }
        else if(state == 6)
        {
            Application.LoadLevel("MainMenu");
        }
    }
}
