#ifndef PLAYERBUILDERCLASS_H
#define PLAYERBUILDERCLASS_H

#include<Windows.h>
#include "PlayerClass.h"

using namespace std;

class PlayerBuilder
{
protected:
	Player* player;
public:

	Player* getPlayer()
	{
		return player;
	}

	void createNewPlayer()
	{
		player = new Player();
	}
	// build virtual stats
	virtual void bStrength() = 0;
	virtual void bDexerity() = 0;
	virtual void bIntelligence() = 0;
	virtual void bHealth() = 0;
	virtual void bMana() = 0;
	virtual void bDmgPhysical() = 0;
	virtual void bDmgMagical() = 0;
	virtual void bEvadeChance() = 0;
	virtual void bDefense() = 0;
	virtual void bExperience() = 0;
	virtual void bLevel() = 0;
	virtual void bName() = 0;
	virtual void bSkillPoints() = 0;


};



#endif