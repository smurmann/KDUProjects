﻿using UnityEngine;
using System.Collections;

public class OrbScript : MonoBehaviour 
{
	Vector3 position;
	Vector3 targetPos;
	float speed;



	// Use this for initialization
	void Start () 
	{
		targetPos=transform.position;
		speed =2.5f;
	
	}
	
	// Update is called once per frame
	void Update () 
	{

		Vector3 worldMouse=Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,0));
		worldMouse.z=0;
		position = transform.position;
		if (Input.GetMouseButton(1))
			targetPos = worldMouse;

		transform.position=Vector2.MoveTowards(position, targetPos,speed*Time.deltaTime);

	}

	void OnTriggerStay2D(Collider2D other) 
	{
		if(other.gameObject.tag=="Ground")
		{
			targetPos=transform.position;
		}
	}
}
