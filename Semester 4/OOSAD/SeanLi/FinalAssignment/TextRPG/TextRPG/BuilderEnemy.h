#ifndef	BUILDERENEMY_H
#define BUILDERENEMY_H

#include "PlayerClass.h"
#include "PlayerBuilder.h"

using namespace std;

class BuilderEnemy : public PlayerBuilder
{
public:
	void bStrength()
	{
		player->setStrength(0);
	}
	void bDexerity()
	{
		player->setDexterity(0);
	}
	void bIntelligence()
	{
		player->setIntelligence(0);
	}
	void bHealth()
	{
		player->setHealth(300);
	}
	void bMana()
	{
		player->setMana(0);
	}
	void bDmgPhysical()
	{
		player->setDmgPhysical(50);
	}
	void bDmgMagical()
	{
		player->setDmgMagical(30);
	}
	void bEvadeChance()
	{
		player->setEvadeChance(10);
	}
	void bDefense()
	{
		player->setDefense(20);
	}
	void bExperience()
	{
		player->setExperience(0);
	}
	void bLevel()
	{
		player->setLevel(5);
	}
	void bName()
	{
		player->setName("Enemy");
	}
	void bSkillPoints()
	{
		player->setSkillPoints(0);
	}
};

#endif