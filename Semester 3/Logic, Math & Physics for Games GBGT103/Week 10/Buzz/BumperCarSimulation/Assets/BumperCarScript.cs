﻿using UnityEngine;
using System.Collections;

//! Instructions :
//! 1. Simulate velocity and acceleration (5%)
//! 2. Simulate friction & momentum (5%)
//! 3. Simulate collision response (10%)

public class BumperCarScript : MonoBehaviour 
{
	float moveSpeed = 1.5f;

	Vector3 moveDirection;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.A))
		{
			//transform.Translate(new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
			moveDirection += new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime;
		}		
		if(Input.GetKey(KeyCode.D))
		{
			//transform.Translate(new Vector3(moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
			moveDirection += new Vector3(moveSpeed, 0.0f, 0.0f) * Time.deltaTime;
		}		
		if(Input.GetKey(KeyCode.W))
		{
			//transform.Translate(new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime);
			///rigidbody.AddForce(new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime);
			moveDirection += new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime;
		}		
		if(Input.GetKey(KeyCode.S))
		{
			//transform.Translate(new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime);
			moveDirection += new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime;
		}
	}

	void FixedUpdate()
	{
		moveDirection *= 0.995f;

		rigidbody.transform.position += moveDirection;

		if(moveDirection.magnitude <= float.Epsilon)
		{
			moveDirection = Vector3.zero;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name == "WallVertical")
		{
			moveDirection.x = moveDirection.x * -1.0f;
			rigidbody.transform.position += 2.0f * moveDirection * Time.fixedDeltaTime;
		}
		if(other.gameObject.name == "WallHorizontal")
		{
			moveDirection.z = moveDirection.z * -1.0f;
			rigidbody.transform.position += 2.0f * moveDirection * Time.fixedDeltaTime;
		}

		if(other.gameObject.name == "BumperCarRED")
		{
			//! F = ma
			//! m -> rigidbody.mass
			//! a -> moveDirection
			
		}
	}
}









