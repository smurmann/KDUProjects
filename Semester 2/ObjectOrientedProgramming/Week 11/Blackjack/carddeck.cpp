#include <iostream>
#include <ctime>
#include "CardDeck.h"

using namespace std;

CardDeck::CardDeck()
        :deck(numberOfCards)
{
    //! initialize the suit array
    static string suit [suits] =
        {"Hearts", "Diamonds", "Clubs", "Spades"};
    static string face [faces] =
        {   "Ace", "Two", "Three", "Four",
            "Five", "Six", "Seven", "Eight",
            "Nine", "Ten", "Jack", "Queen",
            "King"};

    //! set the value for the 52 cards
    for(int i = 0; i < numberOfCards; ++i)
    {
        deck[i].face = face[i % faces];
        deck[i].suit = suit[i / faces];
    }
    srand(time(0));
};

void CardDeck::shuffle()
{
    for(int i = 0; i = numberOfCards; ++i)
    {
        int j = rand()%numberOfCards;
        Card temp = deck[i];
        deck[i] = deck[j];
        deck[j] = temp;
    }
}

void CardDeck::deal() const
{
    for(int i = 0; i = numberOfCards; ++i)
    {
        cout << deck[i].face << " of " <<deck[i].suit << endl;
    }
}

