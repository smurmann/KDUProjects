#include <iostream>
#include "windows.h"

using namespace std;

void staticArrayInit();
void automaticArrayInit();
const int arraySize = 3;

int main()
{
    cout << "first call to each function" << endl;
    staticArrayInit();
    cout << endl;
    automaticArrayInit();
    cout << endl;

    cout << "second call to each function" << endl;
    staticArrayInit();
    cout << endl;
    automaticArrayInit();
    cout << endl;


}

void staticArrayInit()
{
    //! static to initialize, the array value will be stored
    static int array1 [arraySize];
    cout << "\nValues on entering staticArrayInit" << endl;
    for (int i = 0; i < arraySize; ++i)
    {
        cout << "array[" <<i << "] = " << array1 [i] << " ";
    }
    cout << "\nValues of exiting staticArrayInit" << endl;

    for(int j = 0; j < arraySize; ++j)
    {
        cout << "array1[" << j << "] = " << (array1[j]+=5) << " ";
    }
}

void automaticArrayInit()
{
    //! without static the array value will be reinitialzed
    static int array2 [arraySize] = {1,2,3};
        cout << "\nValues on entering AutomaticArrayInit" << endl;
    for (int i = 0; i < arraySize; ++i)
    {
        cout << "array2[" <<i << "] = " << array2 [i] << " ";
    }
    cout << "\nValues of exiting AutomaticArrayInit" << endl;

    for(int j = 0; j < arraySize; ++j)
    {
        cout << "array2[" << j << "] = " << (array2[j]+=5) << " ";
    }

}
