// AlphabetList.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

class AlphabetNode
{
public:
	char alphabet;
	AlphabetNode *prev;
	AlphabetNode *next;

	//constructor
	AlphabetNode()
	{
		alphabet = '#';
		prev = 0;
		next = 0;
	}
};

void Initialize(AlphabetNode *node)
{
	for(int i=66; i<91; i++)
	{
		//create new alphabet node and assign subsequent char
		AlphabetNode *tempNode = new AlphabetNode();
		tempNode->alphabet = (char)i;

		//make passed in node next pointer point to the new node
		node->next = tempNode;

		//make new node previous pointer point to passed in code
		tempNode->prev = node;
		node = tempNode;

	}
}

AlphabetNode *Begin(AlphabetNode *node)
{
	if (node->prev != NULL)
	{
		node = Begin(node->prev);
	}
	return node;
}

AlphabetNode *End(AlphabetNode *node)
{ 
	if (node->next != NULL)
	{
		node = End(node->next);
	}
	return node;
}

void DisplayAll(AlphabetNode *node)
{
	node = Begin(node);
	if (node->prev != NULL)
	{
		DisplayAll(node->prev);
	}
	else
	{
		while(node != NULL)
		{
			cout << node->alphabet;
			node = node->next;
		}
		cout << endl;
	}
}

void addFront(AlphabetNode *node, char alphabet)
{
	AlphabetNode *firstNode = Begin(node);
	AlphabetNode *newNode = new AlphabetNode();
	newNode->alphabet = alphabet;
	firstNode->prev = newNode;
	newNode->next = firstNode;
	newNode->prev = NULL;
}

void addBehind(AlphabetNode *node, char alphabet)
{
	AlphabetNode *lastNode = End(node);
	AlphabetNode *newNode = new AlphabetNode();
	newNode->alphabet = alphabet;
	lastNode->next = newNode;
	newNode->prev = lastNode;
}

void addToPosition(AlphabetNode *node, char a, int position)
{
	AlphabetNode *firstNode = Begin(node);
	AlphabetNode *tempNode = new AlphabetNode();
	tempNode -> alphabet = a;

	for (int i = 1; i < position; i++)
	{
		firstNode = firstNode -> next;
	}

	firstNode->prev->next = tempNode;
	tempNode->prev = firstNode->prev;
	firstNode->prev = tempNode;
	tempNode->next = firstNode;
}

void removeFront(AlphabetNode *node)
{
	cout << "Node's Alphabet, Current, Next, Prev = " <<node->alphabet<<node<<node->next<<node->prev<<endl;
	AlphabetNode *delNode = Begin(node);
	node->prev = delNode->prev;
	node = node->next;
	//delete delNode;
	
	
	//cout << "Node's Alphabet, Current, Next, Prev = " <<del->alphabet<<del<<del->next<<del->prev<<endl;
	
	cout << "Node's Alphabet, Current, Next, Prev = " <<node->alphabet<<node<<node->next<<node->prev<<endl;
	//node = del;
	//delete[] del;
	//cout << "Node's Alphabet, Current, Next, Prev = " <<node->alphabet<<node<<node->next<<node->prev<<endl;
	
	

//	//AlphabetNode *del = Begin(node);
//	//cout << del->prev<<endl;
//
//	//del->next->prev = del->prev;
//
//	//cout << del->alphabet;
//	//cout << del<<endl;
//	//del = del->next;
//	//
//	//cout << del->alphabet;
//	//cout << del;
//	//cout << del->prev<<endl;
//
//	//del->next->prev = del;
//	//cout << del->next->alphabet;
//	//cout << del->next->prev<<endl;
//	//cout << node;
//
//	AlphabetNode *del = Begin(node);
//	cout << node->prev<<endl;
//
//	node->next->prev = node->prev;
//	cout<<"Node's next previous = node's prev = "<<node->prev<<endl;
//
//	cout << node->alphabet;
//	cout << node<<endl;
//	//delete[] node;
//	node = node->next;
//	//del->next->prev = del->prev;
//	cout << node->alphabet;
//	cout << node;
//	cout << "B's previous" << node->prev<<endl;
//	//delete[] node->prev;
//
//	node->next->prev = node;
//	cout << node->next->alphabet;
//	cout << node->next->prev<<endl;
//	cout << node<<endl;
//	cout << node->prev;
//
//	//delete[] del->prev;
////	del->prev->alphabet = NULL;
//	//del->prev->alphabet = NULL;
//	//del->prev->next = del;
//	//del->prev->prev = NULL;
//	//del->prev = del;
//
//	//delete[]del->prev;
//	//cout << del->prev->alphabet;
//	//del->prev->prev->next = del;

}

void removeBack(AlphabetNode *node)
{
	
}

void getIndex(AlphabetNode *node, int index)
{
	AlphabetNode *indexNode = Begin(node);
	for (int i = 1; i < index; i++)
	{
		indexNode = indexNode->next;
	}
	//does not work with last and first node
	indexNode->prev->next = indexNode->next;
	indexNode->next->prev = indexNode->prev;

}

void searchAlphabet(AlphabetNode *node, char alphabet)
{
	int indexCount = 1;
	AlphabetNode *searchNode = Begin(node);
	while(searchNode->alphabet != alphabet)
	{
		searchNode = searchNode->next;
		indexCount++;
	}
	cout<<"The alphabet's index is: "<<indexCount<<endl;
	
}


int _tmain(int argc, _TCHAR* argv[])
{
	AlphabetNode *alphabetList = new AlphabetNode();
	alphabetList->alphabet = (char)65;

	Initialize(alphabetList);

	bool isExit = false;
	

	do
	{
		int choice;		
		cout<<"============================================"<<endl;
		DisplayAll(alphabetList);
		cout<<"============================================"<<endl;
		cout << "1. Show the first node" << endl;
		cout << "2. Show the last node" << endl;
		cout << "3. Add to the front of the list" << endl;
		cout << "4. Add to the back of the list" << endl;
		cout << "5. Add alphabet to a position in the list. " << endl;
		cout << "6. Delete alphabet from front of the list" << endl;
		cout << "7. Delete alphabet from back of the list" << endl;
		cout << "8. Delete alphabet from specific index location"<<endl;
		cout << "9. Search for an alphabet." << endl;
		cout << "10. Exit" << endl;
		cout << "Your choice is: " << endl;
		cin >> choice;
		cout<<"============================================"<<endl;

		switch(choice){
		case 1:
			cout << "The first node: " << Begin(alphabetList)->alphabet << endl;
			break;
		case 2:
			cout << "The last node: " << End(alphabetList)->alphabet << endl;
			break;
		case 3:
			char alphabet;
			cout<<"The alphabet to add: ";
			cin>>alphabet;
			addFront(alphabetList, alphabet); //addFront(alphabetList, toupper(alphabet)); force lowercase needed?
			break;
		case 4:
			char alphabet1;
			cout << "The alphabet to add: ";
			cin >> alphabet1;
			addBehind(alphabetList, alphabet1);
			break;
		case 5:
			char a;
			int position;
			cout<<"Alphabet to add: ";
			cin>>a;
			cout<<endl;
			cout<<"Choose an index: ";
			cin>>position;
			addToPosition(alphabetList,a,position);
			break;
		case 6:
			removeFront(alphabetList); 
			break;
		case 7:
			//
			break;
		case 8:
			int i;
			cout <<"Delete an alphabet of an index"<<endl;
			cout <<"Enter index to delete: ";
			cin >>i;
			cout<<endl;
			getIndex(alphabetList,i);
			break;
		case 9:
			
			char b;
			cout <<"Search for index of alphabet"<<endl;
			cout <<"Enter alphabet to search: ";
			cin >>b;
			cout<<endl;
			searchAlphabet(alphabetList,b);
			break;
		case 10:
			isExit = true;
			break;
		default:
			cout<<"INVALID input. Please try again."<<endl;
			cin >> choice;

		}

		system("PAUSE");
		system("CLS");
		//isExit = true;
	} while (!isExit);
	return 0;
}

