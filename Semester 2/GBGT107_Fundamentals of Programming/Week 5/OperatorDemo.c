#include <iostream>
#include <windows.h>

using namespace std;

int main ()
{
    int a = 10;
    int b = 3;
    cout << "a " << a << endl;
    cout << "b: " << b << endl;

    //!arimethic
    cout << "a + b: " << a+b << endl;
    cout << "a - b: " << a-b << endl;
    cout << "a * b: " << a*b << endl;
    cout << "a / b: " << a/b << endl;
    cout << "a % b: " << a%b << endl;

    //! ++ --

    cout << "a++:" << a++ << endl;
    cout <<"a:" << a << endl;

    cout << "a--:" << a-- << endl;
    cout <<"a:" << a << endl;

    cout << "++a:" << ++a << endl;
    cout << "--a:" << --a << endl;

    system("PAUSE")
    return 0;
}
