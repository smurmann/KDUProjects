      precision mediump float;
	  varying vec4 fColor;
	  uniform float Factor1;
	  uniform float Factor2;
	  
	  varying vec2 fTexCoord;
	  
	  uniform sampler2D sampler2d;
      void main()             
      {
//	  	float r = cos(gl_FragCoord.y*0.05)*0.5-0.5 + fColor.r;
//		float g = cos(gl_FragCoord.y*0.05+2.0)*0.5-0.5 + fColor.g;
//		float b = cos(gl_FragCoord.y*0.05+4.0)*0.5-0.5 + fColor.b;
//		float a = fColor.a;

	    //float r = abs(sin(gl_FragCoord.y*0.05 + Factor1)+sin(gl_FragCoord.x*0.05 + Factor2));
		//float g = abs(cos(gl_FragCoord.y*0.05 + Factor2)+0.3);
		//float b = abs(sin(gl_FragCoord.y*0.05 + Factor1)+0.6);
		

		//float r = mod(fColor.r + Factor2,1.0);
		//float g = mod(fColor.g + Factor1,1.0);
		//float b = mod(fColor.b + Factor2,1.0);
		//float a = mod(fColor.g + Factor1,1.0);
		
		
	    //float r = abs(sin(gl_FragCoord.y*0.05 + Factor1 + fColor.r)+sin(gl_FragCoord.x*0.05 + Factor2));
		//float g = abs(cos(gl_FragCoord.y*0.05 + Factor2 + fColor.g)+0.3);
		//float b = abs(sin(gl_FragCoord.y*0.05 + Factor1 + fColor.b)+0.6);
		
		
		
		//gl_FragColor = vec4(r,g,b,a);
		
		/*
		vec4 texColor = texture2D(sampler2d, fTexCoord);
		vec4 combinedColor;
		combinedColor = fColor * texColor;
		
		vec4 resultColor;
		resultColor.r = mod(combinedColor.r+Factor1, 1.0);
		resultColor.g = mod(combinedColor.g+Factor1, 1.0);
		resultColor.b = mod(combinedColor.b+Factor1, 1.0);
		resultColor.a = combinedColor.a;
		
		gl_FragColor = resultColor;
		*/
		
		vec4 texColor = texture2D(sampler2d, fTexCoord);
		
		vec4 resultColor;
		float colorR = (texColor.r + texColor.g + texColor.b)/3.0;		
		

		/*
		resultColor.r = (colorR * 0.649);
		resultColor.g = (colorR * 0.386);    
		resultColor.b = (colorR * 0.168);
		*/
		
		if(colorR <= 0.25)
		{
			//dark blue
			resultColor.r = 0.0/255.0;
			resultColor.g = 50.0/255.0;    
			resultColor.b = 75.0/255.0;
		}
		else if(colorR > 0.25 && colorR <= 0.42)
		{
			//red 
			resultColor.r = 216.0/255.0;
			resultColor.g = 15.0/255.0;    
			resultColor.b = 33.0/255.0;
		}
		else if(colorR > 0.42 && colorR <= 0.55)
		{
			//light blue
			resultColor.r = 116.0/255.0;
			resultColor.g = 149.0/255.0;    
			resultColor.b = 158.0/255.0;
		}
		else if(colorR > 0.55 && colorR <= 0.6)
		{
			//dark yellow
			resultColor.r = 211.0/255.0;
			resultColor.g = 192.0/255.0;    
			resultColor.b = 152.0/255.0;
		}
		else if(colorR > 0.6 && colorR <= 1.0)
		{
			//light yellow
			resultColor.r = 252.0/255.0;
			resultColor.g = 228.0/255.0;    
			resultColor.b = 168.0/255.0;
		}
	
		//resultColor.r = colorR;
		//resultColor.g = colorR;
		//resultColor.b = colorR;
		resultColor.a = texColor.a;
		
		gl_FragColor = resultColor;
		
		//gl_FragColor = texture2D(sampler2d,fTexCoord);
       }                                            