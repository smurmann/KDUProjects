﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public GameObject targetObject;
    public Vector3 previous;
    public Vector3 sharedVector;
    public Vector3 Velocity;
    float timer = 0.1f;

    public float speed = 0.1f;

    private GameObject[] aiArray;

    
    void Start()
    {
        InitArray();
    }

    void InitArray()
    {
        aiArray = GameObject.FindGameObjectsWithTag("AI");
    }

    void Update()
    {
        UpdatePosition();
    }

    void UpdatePosition()
    {

        var pos = transform.position;
        var accel = (targetObject.transform.position - pos).normalized * Time.deltaTime;

        //var velocity = (pos - previous).normalized / Time.deltaTime;

        //sharedVector = ((avoidance(accel) + velocity) * 0.5f);
        //previous = transform.position;

        Velocity = Velocity + (avoidance(accel));

        if (Velocity.magnitude >= 0.3f)
        {
            Velocity = Velocity.normalized * 0.3f;
        }

        transform.position += Velocity;

        //interpolation method
        //transform.position += (sharedVector - previous) * speed * Time.deltaTime;
    }

    Vector3 avoidance(Vector3 accl)
    {
        
        foreach(GameObject ai in aiArray)
        {
            if(ai != this.gameObject)
            {
                var tr = ai.transform;
                float distanceSqr = (transform.position - tr.position).sqrMagnitude;
                if (distanceSqr < 10f)
                {
                    var heading = (this.transform.position - tr.position);
                    var distance = heading.magnitude;
                    var direction = heading / distance;
                    return (accl + direction / 15f) *0.5f;
                }
            }

        }
        return accl;
    }
}
