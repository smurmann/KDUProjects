#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

//! Singleton
class GlobalClass
{
	int value;
	//! static instance variable
	static GlobalClass *instance;

	//! private -> constructor
	private GlobalClass()
	{
		value = v;
	}
	
	int getValue()
	{
		return value;
	}
	void setValue(int v)
	{
		value = v;
	}
	static GlobalClass *getInstance()
	{
		if(!instance)
			instance = new GlobalClass;
		return instance;
	}
};

GlobalClass::instance = 0;

void foo(void)
{
	GlobalClass::getInstance()->setValue(1);
	cout << "foo: global_ptr is " << GlobalClass::getInstance()->getValue()<<endl;
}

void bar(void)
{
	GlobalClass::getInstance()->setValue(2);
	cout << "bar: global_ptr is " << GlobalClass::getInstance()->getValue()<<endl;
}

int _tmain(int argc, _TCHAR* argc[])
{
	cout << "main: instance value is " << GlobalClass::getInstance()->getValue()  << endl;
	foo();
	bar();
	system("PAUSE");
	return 0;
}