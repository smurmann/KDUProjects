﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class ArrayScript : MonoBehaviour
{

    public Node[,] nodeArray;
    public int X, Z;
    public bool readyToRunPathFind = false;

    public enum SearchMethod
    {
        Dijkstra = 0,
        Astar
    }

    public SearchMethod method;

    public class ImpNode
    {
        public int X, Y;


        public void SetImpNode(int x, int y)
        {
            X = x;
            Y = y;
        }

        public ImpNode()
        {
            X = -1;
            Y = -1;
        }
    }

    public void ToggleSearchMethod()
    {
        method++;
        if((int)method > 1)
        {
            method = 0;
        }
    }

    public void ReloadScene()
    {
        Application.LoadLevel(0);
    }


    public class Node
    {
        public List<Node> neighbourNodes;
        public GameObject visual;
        public int X, Y;
        public NodeScript.TileState currentState;
        public float distanceToSource;
        public Node previousNode;

        public Node()
        {
            neighbourNodes = new List<Node>();
            visual = Instantiate(Resources.Load("Node")) as GameObject;
            currentState = 0;
        }

        public void SetVisualLocation(Vector3 location)
        {
            visual.transform.position = location;
        }

        public void SetVisualColour(Color colour)
        {
            visual.GetComponent<SpriteRenderer>().color = colour;
        }

        public void SetParent(Transform parent)
        {
            visual.transform.parent = parent;
        }

        public void XY(int x, int y, ArrayScript script)
        {
            X = x;
            Y = y;
            var tempVisual = visual.GetComponent<NodeScript>();
            //tempVisual.TilePosX = x;
            //tempVisual.TilePosY = y;
            //tempVisual.array = script;
        }
    }

    void Start()
    {
        SpawnArray(X, Z);
    }

    void SpawnArray(int x, int z)
    {
        var tile = Resources.Load("Node") as GameObject;
        var size = tile.GetComponent<SpriteRenderer>().bounds.size.x;
        var container = new GameObject();

        nodeArray = new Node[x, z];
        container.name = "ArrayContainer";
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < z; j++)
            {
                var node = nodeArray[i, j] = new Node();

                node.SetVisualLocation(new Vector3(size * i - (size * x - 1) / 2, size * j - (size * z - 1) / 2, 0));
                node.SetParent(container.transform);
                node.XY(i, j, gameObject.GetComponent<ArrayScript>());
            }
        }
    }

    void TracePath(Node end)
    {
        if (end.previousNode == null)
        {
            return;
        }
        end.previousNode.SetVisualColour(Color.magenta);
        TracePath(end.previousNode);

    }

    IEnumerator Delay(float delay)
    {
        yield return null;
        //yield return new WaitForSeconds(delay);
    }

    IEnumerator PathFind(ImpNode startNode, ImpNode endNode)
    {
//        float distance = 0;

        Dictionary<Node, float> distanceToSource = new Dictionary<Node, float>();
        Dictionary<Node, float> heuristicDistance = new Dictionary<Node, float>();

        Node start = nodeArray[startNode.X, startNode.Y];
        Node end = nodeArray[endNode.X, endNode.X];

        List<Node> unvisitedNodes = new List<Node>();
        List<Node> openList = new List<Node>();

        distanceToSource[start] = 0;
        heuristicDistance[start] = 0;
        openList.Add(start);

        foreach (Node i in nodeArray)
        {
            i.previousNode = null;
            i.SetVisualColour(Color.white);
            if (i != start)
            {
                distanceToSource[i] = Mathf.Infinity;
                heuristicDistance[i] = Mathf.Infinity;
                //previousNode[i] = null;
            }
            unvisitedNodes.Add(i);
        }
        Debug.Log(openList.Count);

        while (openList.Count > 0)
        {
//            Debug.Log("OpenList Count: " + openList.Count);
            Node tempNode = null;
            foreach (Node lowest in openList)
            {
                if(method == SearchMethod.Dijkstra)
                {
                    //Dijkstra
                    if (tempNode == null || distanceToSource[lowest] < distanceToSource[tempNode])
                    {
                        if (unvisitedNodes.IndexOf(lowest) > -1)
                        {
                            tempNode = lowest;
                        }
                    }
                }
                else if(method == SearchMethod.Astar)
                {
                    //Astar
                    float fLowest = Mathf.Infinity;
                    float fTempNode = Mathf.Infinity;
                    if (tempNode != null)
                    {
                        fLowest = distanceToSource[lowest] + heuristicDistance[lowest];
                        fTempNode = distanceToSource[tempNode] + heuristicDistance[tempNode];

                    }
                    if (tempNode == null || fLowest < fTempNode)
                    {
                        if (unvisitedNodes.IndexOf(lowest) > -1)
                        {
                            tempNode = lowest;
                        }
                    }
                }          
            }

            tempNode.SetVisualColour(Color.grey);
            //var tempNode = openList[0];
            openList.Remove(tempNode);

            if (tempNode.X == endNode.X && tempNode.Y == endNode.Y)
            {
                TracePath(tempNode);
                Debug.Log("End Found");
                yield break;
            }
            unvisitedNodes.Remove(tempNode);

            tempNode.neighbourNodes = new List<Node>();

            if (tempNode.X > 0)
                tempNode.neighbourNodes.Add(nodeArray[tempNode.X - 1, tempNode.Y]);

            if (tempNode.Y > 0)
                tempNode.neighbourNodes.Add(nodeArray[tempNode.X, tempNode.Y - 1]);

            if (tempNode.X < nodeArray.GetLength(0) - 1)
                tempNode.neighbourNodes.Add(nodeArray[tempNode.X + 1, tempNode.Y]);

            if (tempNode.Y < nodeArray.GetLength(1) - 1)
                tempNode.neighbourNodes.Add(nodeArray[tempNode.X, tempNode.Y + 1]);

            foreach (Node v in tempNode.neighbourNodes)
            {
                if (unvisitedNodes.IndexOf(v) > -1 && v.currentState != NodeScript.TileState.Obstacle)
                {
                    if(method == SearchMethod.Dijkstra)
                    {

                        // Dijkstra
                        if (distanceToSource[tempNode]+10 < distanceToSource[v])
                        {
                            distanceToSource[v] = distanceToSource[tempNode] + 10;
                            v.previousNode = tempNode;
                        }

                        if (openList.IndexOf(v) == -1)
                        {
                            openList.Add(v);
                        }
                    }
                    else if(method == SearchMethod.Astar)
                    {
                        if (openList.IndexOf(v) == -1)
                        {
                            openList.Add(v);
                            v.previousNode = tempNode;
                            distanceToSource[v] = distanceToSource[tempNode] + 1;
                            heuristicDistance[v] = Mathf.Abs(v.X - endNode.X) + Mathf.Abs(v.Y - endNode.Y);
                        }
                        else
                        {
                            if (distanceToSource[v] < distanceToSource[tempNode])
                            {
                                v.previousNode = tempNode;
                                distanceToSource[tempNode] = distanceToSource[v];
                                heuristicDistance[v] = Mathf.Abs(v.X - endNode.X) + Mathf.Abs(v.Y - endNode.Y);
                            }
                        }
                    }
                    v.SetVisualColour(Color.yellow);

                }
            }
            yield return null;
            //yield return StartCoroutine(Delay(0.001f));
        }
    }

    void FindStartandEnd()
    {
        ImpNode startPoint = new ImpNode();
        ImpNode endPoint = new ImpNode();

        foreach (Node node in nodeArray)
        {
            node.distanceToSource = Mathf.Infinity;
            if (node.currentState == NodeScript.TileState.EndNode)
            {
                endPoint.SetImpNode(node.X, node.Y);
            }
            else if (node.currentState == NodeScript.TileState.StartNode)
            {
                startPoint.SetImpNode(node.X, node.Y);
            }
        }

        if (startPoint.X == -1 || endPoint.X == -1)
        {
            //Debug.Log("No end or start found");
            readyToRunPathFind = false;
            return;
        }
        else
        {
            readyToRunPathFind = true;

            StopAllCoroutines();
            StartCoroutine(PathFind(startPoint, endPoint));
            Debug.Log(startPoint.X);
            //Debug.Log("End or start found");
        }
    }


    public void SetNodeState(int x, int y, NodeScript.TileState state)
    {
        nodeArray[x, y].currentState = state;
        FindStartandEnd();
    }
}
