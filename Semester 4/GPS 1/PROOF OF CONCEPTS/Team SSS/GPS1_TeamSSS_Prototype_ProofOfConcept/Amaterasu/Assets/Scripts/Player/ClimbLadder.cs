﻿using UnityEngine;
using System.Collections;

public class ClimbLadder : MonoBehaviour {
	private Player player;
	private GameObject colLadder;
	private bool colWithLadder=false;
	private bool isClimbingLadder=false;
	private bool isOnTop=false;
	SpriteRenderer renderer;
	SpriteRenderer ladderRenderer;
	private Vector2 playerBounds;
	//private int climbingDirection=0;


	void startUpLadder()
	{
		
		isClimbingLadder=true;
		
		
	}
	void climbLadder()
	{
		
		if(isClimbingLadder)
		{


			if(player.currentAction==PlayerAction.climbingLadder)
			transform.Translate(new Vector3(0,0.08f,0));
			
			if(player.currentAction==PlayerAction.climbingDownLadder)
				transform.Translate(new Vector3(0,-0.08f,0));
			player.canMove=false;

		}
		if(!colWithLadder)
		{
			isClimbingLadder=false;
			player.canMove=true;
		}
	}
	void Start () 
	{
		player = gameObject.GetComponent<Player>();
	
	}

	void Update () 
	{

		renderer =(SpriteRenderer)gameObject.renderer;
		ladderRenderer = new SpriteRenderer();
		if(colLadder)
		{
			ladderRenderer =(SpriteRenderer)colLadder.gameObject.renderer;
		}
		playerBounds=renderer.bounds.size;
		player.position = transform.position;
		Vector3 worldMouse=Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,0));

		if (Input.GetMouseButton(0))
		{
			//climb up ladder
			if(!isOnTop&&colWithLadder&&worldMouse.x>transform.position.x-playerBounds.x&&worldMouse.x<transform.position.x+playerBounds.x&!isClimbingLadder)
			{
				if(colLadder)
				if(worldMouse.y>colLadder.transform.position.y+(ladderRenderer.bounds.size.y/2))
				{
					player.currentAction=PlayerAction.climbingLadder;
					player.canFall=false;
					isClimbingLadder=true;
					transform.position=new Vector2(colLadder.transform.position.x,transform.position.y);
				}
			}
			//climb down ladder
			else if(isOnTop&&colWithLadder&&worldMouse.x>transform.position.x-renderer.bounds.size.x&&worldMouse.x<transform.position.x+renderer.bounds.size.x&!isClimbingLadder&&player.currentAction!=PlayerAction.climbingDownLadder)
			{
				if(colLadder)
				if(worldMouse.y<colLadder.transform.position.y-(ladderRenderer.bounds.size.y/2)+0.5f)
				{
					player.currentAction=PlayerAction.climbingDownLadder;
					player.canFall=false;
					isClimbingLadder=true;
					transform.position=new Vector2(colLadder.transform.position.x,transform.position.y);
				}

			} 
			else if(!isClimbingLadder)
			{
				player.currentAction=PlayerAction.idle;
				player.canFall=true;;
				player.canMove=true;
			}   
		}
		climbLadder();


	
	}
	void OnTriggerStay2D(Collider2D other)
	{
		
		if(other.gameObject.tag =="Ladder")
		{
			//Debug.Log("On Trigger Stay");
			colWithLadder =true;
			colLadder=other.gameObject;
			//isOnTop=false;
		}
		
	}
	void OnCollisionStay2D(Collision2D other) 
	{
		if(other.gameObject.tag =="Ladder")
		{
			if(player.currentAction!=PlayerAction.climbingDownLadder)
			{
			 colWithLadder =true;
			 colLadder=other.gameObject;
			 ladderRenderer =(SpriteRenderer)colLadder.gameObject.renderer;
			 if(player.position.y-(playerBounds.y/2) > colLadder.transform.position.y+(ladderRenderer.bounds.size.y/2)-1)
			 isOnTop=true;
			}
		}
	}
	void OnCollisionEnter2D(Collision2D other) 
	{
		if(other.gameObject.tag=="Ground")
		{
			if(player.currentAction==PlayerAction.climbingDownLadder)
			{
				player.currentAction=PlayerAction.idle;
				player.canFall=true;
				player.canMove=true;
				isOnTop=false;
				isClimbingLadder=false;
			}
		}

	}
	void OnCollisionExit2D(Collision2D other) 
	{
		if(other.gameObject.tag =="Ladder")
		{
			//Debug.Log("On Collision Exit");

			colWithLadder=false;
			isClimbingLadder=false;
			player.currentAction=PlayerAction.idle;
			player.canFall=true;
			isOnTop=false;
		}
	}
}
