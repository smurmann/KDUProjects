﻿using UnityEngine;
using System.Collections;

public class ControlScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 position = transform.position;

		position.y += Input.GetAxisRaw ("Vertical") * 0.1f;
		position.x += Input.GetAxisRaw ("Horizontal") * 0.1f;



		if(Input.GetMouseButtonDown (0))
		{
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePos.z = 0.0f;
			position = mousePos;
			Debug.Log(position);
		}

		transform.position = position;
	
	}
}
