// AlphabetList.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <string>

using namespace std;

class AlphabetNode
{
	public:
		char alphabet;
		AlphabetNode* prev;
		AlphabetNode* next;

		// Constructor
		AlphabetNode()
			{
				alphabet = '#';
				prev = NULL;
				next = NULL;
			}
};

// Initialize the Link List
void Initialize(AlphabetNode* alphabetList)
{		
	for(int i=66; i<91; i++)
	{
		AlphabetNode* tempNode = new AlphabetNode();
		tempNode->alphabet = (char)i;

		tempNode->prev = alphabetList;
		alphabetList->next = tempNode;
		alphabetList = alphabetList->next;
	}
			
}

// Display the Link List
void Display(AlphabetNode* alphabetList)
{
	cout<<alphabetList->alphabet<<endl;
	if(alphabetList->next != NULL)
	{
		Display(alphabetList->next);
	}
}

//! Reset the node to beginning.
AlphabetNode* Begin(AlphabetNode* node)
{
	if(node->prev != NULL)
	{
		node = Begin(node->prev);
	}
	
	cout<<node->alphabet<<endl;

	return node;
}

//! Sets the node to the last node.
AlphabetNode* End(AlphabetNode* node)
{
	if(node->next != NULL)
	{
		node = End(node->next);
	}
	else
	{
		cout<<node->alphabet<<endl;
	}
	
	return node;
}

//! Insert node at the end 
void Push_Back(AlphabetNode* node, char tchar)
{
	AlphabetNode* tempNode = new AlphabetNode();

	do
	{
		node = node->next;
	}
	while(node->next != NULL);

	tempNode->alphabet = tchar;
	tempNode->next = NULL;
	tempNode->prev = node;
	node->next = tempNode;
}

//! Insert node at the front.
void Push_Front(AlphabetNode* node, char tchar)
{
	char tempChar = node->alphabet;
	AlphabetNode* tempNode = new AlphabetNode();

	node->alphabet = tchar;
	tempNode->alphabet = tempChar;
	
	tempNode->next = node->next;
	tempNode->prev = node;
	
	node->next = tempNode;

}

//! Pop the last node
void Pop_Back(AlphabetNode* node)
{
	if(node->next != NULL)
	{
		while(node->next != NULL)
		{
			node = node->next;
		}

		node = node->prev;
		cout<<node->alphabet<<" has been removed."<<endl;
		node->next = NULL;

	}else
	{
		cout<<"Node can't POP anymore."<<endl;
	}
	
}

void Pop_Front(AlphabetNode* node)
{
	if(node->next != NULL)
	{
		cout<<node->alphabet<<" has been removed."<<endl;
		node->alphabet = node->next->alphabet;
		if(node->next->next != NULL)
		{
			node->next = node->next->next;
		}else
		{
			node->next = NULL;
		}

		node->prev = NULL;
	}else
	{
		cout<<"Node can't be POP anymore."<<endl;
	}
	
}

void Insert(AlphabetNode* node, char tchar, int index)
{
	//! To Check if it is the first index that to be inserted.
	if(index == 0)
	{
		Push_Front(node, tchar);
	}
	else
	{
		char tempChar = node->alphabet;
		AlphabetNode* tempNode = new AlphabetNode();

		int i = 0;
		index--;
		bool lastIndex = false;

			while(i != index)
			{
				if(node->next->next != NULL)
				{
					node = node->next;
				}else
				{
					lastIndex = true;
					break;
				}
				i++;
			}

			if(lastIndex == false)
			{
				tempNode->alphabet = tchar;
				tempNode->next = node->next;
				tempNode->prev = node;
				node->next = tempNode;
			}
			else
			{
				Push_Back(node, tchar);
			}
	}
}

bool Search(AlphabetNode* node, char searchChar)
{
	bool isAvailable = false;

	if(node->alphabet == searchChar)
	{
		isAvailable = true;
	}
	else
	{
		//node = node->next;
		Search(node->next, searchChar);
	}

	return isAvailable;
}

void Remove(AlphabetNode* node, int index)
{
	if(index == 0)
	{
		Pop_Front(node);
	}
	else
	{
		int i = 0;
		index--;
		bool lastIndex = false;

			while(i != index)
			{
				if(node->next != NULL)
				{
					node = node->next;
				}
				else
				{
					lastIndex = true;
					break;
				}
				i++;
			}

			if(lastIndex == false)
			{
				if(node->next != NULL)
				{
					cout<<node->next->alphabet<<" has been removed."<<endl;
					node->next = node->next->next;
				}else
				{
					cout<<"It can't be pop"<<endl;
				}
			}
			else
			{
				Pop_Back(node);
			}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	 system("mode con: cols=100 lines=50");
	AlphabetNode* alphabetList = new AlphabetNode();
	alphabetList->alphabet = 'A';
	Initialize(alphabetList);
	
	bool isExit = false;
	
	do
	{
		system("CLS");
		int choice;
		cout<<"+===========================+"<<endl;
		Display(alphabetList);
		cout<<"+===========================+"<<endl;
		cout<<"1. Show the first Node"<<endl;
		cout<<"2. Show the last Node"<<endl;
		cout<<"3. Add to the back of the Node"<<endl;
		cout<<"4. Add to the front of the Node"<<endl;
		cout<<"5. Remove to the back of the Node"<<endl;
		cout<<"6. Remove to the front of the Node"<<endl;
		cout<<"7. Add specific location to the Node"<<endl;
		cout<<"8. Remove specific location to the Node"<<endl;
		cout<<"9. Search Alphabet"<<endl;
		cout<<"0. Exit"<<endl;

		cout<<endl;
		cin>>choice;
		if(choice == 1)
		{
			Begin(alphabetList);
		}
		else if (choice == 2)
		{
			End(alphabetList);
		}
		else if (choice == 3)
		{
			char tempChar;
			cout<<"Insert Character."<<endl;
			cin>>tempChar;
		
			Push_Back(alphabetList, tempChar);	
		}
		else if (choice == 4)
		{
			char tempChar;
			cout<<"Insert Character."<<endl;
			cin>>tempChar;
			
			Push_Front(alphabetList, tempChar);
		}
		else if (choice == 5)
		{
			Pop_Back(alphabetList);
		}
		else if (choice == 6)
		{
			Pop_Front(alphabetList);
		}
		else if (choice == 7)
		{
			int indexChoice;
			cout<<"Insert which index location that the character will be inserted."<<endl;
			cin>>indexChoice;
			char tempChar;
			cout<<"Insert Character."<<endl;
			cin>>tempChar;

			Insert(alphabetList, tempChar, indexChoice);
		}
		else if (choice == 8)
		{
			int indexChoice;
			cout<<"Insert which index location that the character will be removed."<<endl;
			cin>>indexChoice;

			Remove(alphabetList, indexChoice);
		}
		else if (choice == 9)
		{
			char sChar;
			cout<<"Please enter a character: "<<endl;
			cin>>sChar;

			bool cAvailable = Search(alphabetList, sChar);
			if(cAvailable)
			{
				cout<<"Character found!"<<endl;
			}
			else
			{
				cout<<"Please try to search again"<<endl;
			}
		}
		else{
			isExit = true;
		}
		
		system("PAUSE");
		cout<<"======================================="<<endl;

	}while(isExit == false);


	system("PAUSE");
	return 0;
}

