﻿using UnityEngine;
using System.Collections;

public class ButtonClicker : MonoBehaviour {

	DragObject dragObject;
	TileMap tileMap;
	// Use this for initialization
	void Start () 
	{
		dragObject = GameObject.Find ("Start").GetComponent<DragObject> ();
		tileMap = GameObject.Find ("Map").GetComponent<TileMap> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnMouseDown()
	{
		if(gameObject.name == "StartFind")
		{
			dragObject.isStart = !dragObject.isStart;
		}
		else if(gameObject.name == "Dijkstra")
		{
			tileMap.tempInt = 0;
			tileMap.isDijkstra = true;
			tileMap.isAstar = false;
		}
		else if(gameObject.name == "Astar")
		{
			tileMap.tempInt = 0;
			tileMap.isDijkstra = false;
			tileMap.isAstar = true;
		}
	}
}
