﻿using UnityEngine;
using System.Collections;

public class PushByPlayer : MonoBehaviour 
{
	GameObject player;
	Player playerScript;
	SpriteRenderer renderer;
	SpriteRenderer playerRenderer;
	bool playerInZone=false;

	// Use this for initialization
	void Start () 
	{
		player=GameObject.Find("/Player");
		playerScript = player.GetComponent<Player>();

	
	}
	
	// Update is called once per frame
	void Update () 
	{

		renderer =(SpriteRenderer)gameObject.renderer;
		playerRenderer=(SpriteRenderer)player.gameObject.renderer;

	}

	void OnCollisionStay2D(Collision2D other)
	{
		if(other.gameObject.tag =="Player")
		{
			Vector3 position=transform.position;
			if(position.y+(playerRenderer.bounds.size.y/2)-0.1f >playerScript.position.y-(playerRenderer.bounds.size.y/20))
			{
			if(other.transform.position.x >this.gameObject.transform.position.x)
			position.x-=2.5f*Time.deltaTime;
			else
				position.x +=2.5f*Time.deltaTime;
			}
			transform.position=position;

		}
	}


}
