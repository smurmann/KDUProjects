#ifndef BUILDERTHIEF_H
#define BUILDERTHIEF_H

#include "PlayerClass.h"
#include "PlayerBuilder.h"

using namespace std;

class BuilderThief : public PlayerBuilder
{
public:
	void bStrength()
	{
		player->setStrength(0);
	}
	void bDexerity()
	{
		player->setDexterity(0);
	}
	void bIntelligence()
	{
		player->setIntelligence(0);
	}
	void bHealth()
	{
		player->setHealth(150);
	}
	void bMana()
	{
		player->setMana(70);
	}
	void bDmgPhysical()
	{
		player->setDmgPhysical(60);
	}
	void bDmgMagical()
	{
		player->setDmgMagical(20);
	}
	void bEvadeChance()
	{
		player->setEvadeChance(20);
	}
	void bDefense()
	{
		player->setDefense(40);
	}
	void bExperience()
	{
		player->setExperience(0);
	}
	void bLevel()
	{
		player->setLevel(0);
	}
	void bName()
	{
		player->setName("Thief");
	}
	void bSkillPoints()
	{
		player->setSkillPoints(10);
	}
};

#endif