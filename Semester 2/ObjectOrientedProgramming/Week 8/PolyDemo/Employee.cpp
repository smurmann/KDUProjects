#include <iostream>
#include "Employee.h"

Employee::Employee(string n, float rate)
{
    name = n;
    payRate = rate;
}

string Employee::getName() const
{
    return name;
}

float Employee::getPayRate() const
{
    return payRate;
}

float Employee::pay(float hoursWork) const
{
    return hoursWork * payRate;
}
