#include <cstdlib>
#include <iostream>
#include <ctime>
#include "windows.h"

using namespace std;
const int ROWS = 10;
const int COLS = 10;

int playerX = 0;
int playerY = 0;

void printBoard(char board[][COLS]);
void spawnCrawler(char board[][COLS]);
void spawnTreasure(char board[][COLS]);
void spawnPlayer(char board[][COLS]);
void spawnTrap(char board[][COLS]);
void checkWin(char board[][COLS], bool &victory);
void checkDeath(char board[][COLS], bool &death);

int main()
{
    srand(time(NULL));
    char board[ROWS][COLS], playerBoard[ROWS][COLS];
    char direction = 'w';
    int nTraps = 0;

    for(int i = 0; i<ROWS; i++)
    {
        for(int j = 0; j<COLS; j++ )
        {
            board[i][j] = '.';
            playerBoard[i][j] = '.';
        }
    }
    cout << "Choose your preferred difficulty level" << endl;
    {
        cout << "How many traps would you like to face?" << endl;
        cout << "Min 2 - Max 6" << endl;
        cin >> nTraps;
    }while(nTraps < 1 || nTraps > 6);

    for(int i=1; i<=nTraps; i++)
    {
        spawnTrap(board);
    }

    spawnPlayer(board);
    spawnTreasure(board);

    do
    {   printBoard(board);
        cout << "Choose a direction. \nw = up\ns = down\na = left\nd = right" << endl;
        cin >> direction;
        cout << "You chose the follow direction: " << direction << endl;
        spawnCrawler(board);
        cout << "Player Coordinates are X: " << playerX+1 <<" Y: " << playerY+1 << endl;

        if(direction == 'w')
        {

        }


    }while(checkWin != false);
    //cin >>


    printBoard(board);


}

void printBoard(char board[][COLS])
{
    for(int i=0; i<ROWS; i++)
    {
        for(int j=0; j<COLS; j++)
        {
            cout << board[i][j];
        }
        cout << endl;
    }
}

void spawnCrawler(char board[][COLS])
{
    int iStart = 0, jStart = 0, mode;   //! mode for different spawn types,
                                        //! just in case
    Randomize:
        do
        {
            iStart = rand()%ROWS;
            jStart = rand()%COLS;
            //mode = rand()&0;
            mode = 0;
        }while(board[iStart][jStart] == 'C');

        if(mode == 0)
        {
            //! making sure crawler doesn't go off the board
            while(jStart >= COLS)
            {
                jStart = rand()%COLS;
            }
            while(iStart >= COLS)
            {
                iStart = rand()%COLS;
            }
            if(board[iStart][jStart] == 'C')
            {
                goto Randomize;
            }

            if(board[iStart][jStart] = 'C');

        }


}

void spawnPlayer(char board[][COLS])
{
    int iStart = 0, jStart = 0, mode;   //! mode for different spawn types,
                                        //! just in case
    Randomize:
        do
        {
            iStart = rand()%ROWS;
            jStart = rand()%COLS;
            playerX = iStart;
            playerY = jStart;
            //mode = rand()&0;
            mode = 0;
        }while(board[iStart][jStart] == 'i');

        if(mode == 0)
        {
            //! making sure crawler doesn't go off the board
            while(jStart >= COLS)
            {
                jStart = rand()%COLS;
            }
            while(iStart >= COLS)
            {
                iStart = rand()%COLS;
            }
            if(board[iStart][jStart] == 'i')
            {
                goto Randomize;
            }

            if(board[iStart][jStart] = 'i');

        }
}

void spawnTreasure(char board[][COLS])
{
    int iStart = 0, jStart = 0, mode;   //! mode for different spawn types,
                                        //! just in case
    Randomize:
        do
        {
            iStart = rand()%ROWS;
            jStart = rand()%COLS;
            //mode = rand()&0;
            mode = 0;
        }while(board[iStart][jStart] == 'X');

        if(mode == 0)
        {
            //! making sure crawler doesn't go off the board
            while(jStart >= COLS)
            {
                jStart = rand()%COLS;
            }
            while(iStart >= COLS)
            {
                iStart = rand()%COLS;
            }
            if(board[iStart][jStart] == 'X')
            {
                goto Randomize;
            }

            if(board[iStart][jStart] = 'X');

        }
}
void spawnTrap(char board[][COLS])
{
    int iStart = 0, jStart = 0, mode;   //! mode for different spawn types,
                                        //! just in case
    Randomize:
        do
        {
            iStart = rand()%ROWS;
            jStart = rand()%COLS;
            //mode = rand()&0;
            mode = 0;
        }while(board[iStart][jStart] == 'T');

        if(mode == 0)
        {
            //! making sure crawler doesn't go off the board
            while(jStart >= COLS)
            {
                jStart = rand()%COLS;
            }
            while(iStart >= COLS)
            {
                iStart = rand()%COLS;
            }
            if(board[iStart][jStart] == 'T')
            {
                goto Randomize;
            }

            if(board[iStart][jStart] = 'T');

        }


}

void checkWin(char board[][COLS], bool &victory)
{
    for(int i=0; i<ROWS; i++)
    {
        for(int j=0; j<COLS; j++)
        {
            if(board[i][j] == 'X')
            {
                victory = false;
            }
        }
    }
}

void checkDeath(char board[][COLS], bool &death)
{
    for(int i=0; i<ROWS; i++)
    {
        for(int j=0; j<COLS; j++)
        {
            if(board[i][j] == 'S')
            {
                death = false;
            }
        }
    }
}
