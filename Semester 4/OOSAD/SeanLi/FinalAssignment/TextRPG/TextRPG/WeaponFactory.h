#pragma once

#include <vector>
#include <string>
using namespace std;

class WeaponFactory
{
private:
	string className;
	string name;
	int health;
	int mana;
	int pdmg;
	int mdmg;
	int evadechance;
	int defense;
public: 
	WeaponFactory(string cn, string n, int hp, int mp, int dP, int dM, int e, int def)
	{
		className = cn;
		name = n;
		health = hp;
		mana = mp;
		pdmg = dP;
		mdmg = dM;
		evadechance = e;
		defense = def;
	}
	string getName()
	{
		return name;
	}

	int getHealth()
	{
		return health;
	}

	int getMana()
	{
		return mana;
	}

	int getPDmg()
	{
		return pdmg;
	}

	int getMDmg()
	{
		return mdmg;
	}

	int getEvadeChance()
	{
		return evadechance;
	}

	int getDefense()
	{
		return defense;
	}
	void showWeaponStats()
	{
		cout<< "<" << className << "> "<< name << "\t" <<": +HP " << health << ", +MP " << mana << ", +PDMG " << pdmg << ", +MDMG " << mdmg << ", +EVADE " << evadechance << ", +DEF " << defense << endl;
	}
};

class Rapier : public WeaponFactory
{
public: 
	Rapier() : WeaponFactory("WAR","Rapier", 15, 0, 30, 0, 0, 0)
	{}
};

class Bloodsword : public WeaponFactory
{
public:
	Bloodsword() : WeaponFactory("WAR", "BloodSWD", 30, 00, 15, 00, 00, 00)
	{}
};

class TheDefender : public WeaponFactory
{
public:
	TheDefender() : WeaponFactory("WAR", "Defender", 10, 00, 10, 00, 00, 10)
	{}
};

class Butterknife : public WeaponFactory
{
public:
	Butterknife() : WeaponFactory("THF", "ButterKN", 10, 00, 05, 00, 05, 00)
	{}
};

class Karambit : public WeaponFactory
{
public:
	Karambit() : WeaponFactory("THF", "Karambit", 00, 05, 05, 00, 10, 00)
	{}
};

class Dagger : public WeaponFactory
{
public:
	Dagger() : WeaponFactory("THF", "Dagger", 00, 05, 10, 00, 05, 00)
	{}
};

class Wand : public WeaponFactory
{
public:
	Wand() : WeaponFactory("MAGE", "Wand", 00, 15, 00, 15, 00, 00)
	{}
};

class Staff : public WeaponFactory
{
public:
	Staff() : WeaponFactory("MAGE", "Staff", 05, 20, 10, 20, 00, 05)
	{}
};

class Book : public WeaponFactory
{
public:
	Book() : WeaponFactory("MAGE", "Book", 10, 25, 0, 15, 5, 0)
	{}
};

//for showing the players the choices
Rapier wep_rp;
Bloodsword wep_bs;
TheDefender wep_td;
Butterknife wep_bk;
Karambit wep_kb;
Dagger wep_dg;
Wand wep_wd;
Staff wep_sf;
Book wep_book;

vector <WeaponFactory> WeaponShop{ wep_rp, wep_bs, wep_td, wep_bk, wep_kb, wep_dg, wep_wd, wep_sf, wep_book };




