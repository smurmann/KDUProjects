// Queue.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <iostream>
#include <queue>
#include <stack>

using namespace std;
queue<int> myQueue;

void Display(queue<int> tempQueue)
{
	cout<<"============================"<<endl;
	if(tempQueue.empty())
	{
		cout<<"Queue is Empty"<<endl;
	}
	else
	{
		while(tempQueue.size() > 0)
		{
			cout<<tempQueue.front()<<endl;
			tempQueue.pop();
		}
	}
}

void ClearQueue(queue<int>& realQueue)
{
	while(!realQueue.empty())
	{
		realQueue.pop();
	}
}
void ReverseQueue(queue<int>& realQueue, queue<int> tempQueue)
{
	ClearQueue(realQueue);
	stack<int> tempStack;
	while(!tempQueue.empty())
	{
		tempStack.push(tempQueue.front());
		tempStack.pop();
	}
	while(!tempStack.empty())
	{
		realQueue.push(tempStack.top());
		tempStack.pop();
	}


}
/*
// doesn't work
void DescendQueue(queue<int>& realQueue, queue<int> tempQueue, int queueLenght)
{
	ClearQueue(realQueue);
	int tempArray[20];
	while(!tempQueue.empty())
	{
		for(int i=0; i<=20; i++)
		{
		tempArray[i] = tempQueue.front();
		i=20;
		}
		tempQueue.pop();
	}
		for(int i=0; i<=20; i++)
		{
		cout<<tempArray[i];
		}

	int temp=0;
	for(int l=0;l<5;l++)
	{
	  for(int j=0;j<5;j++)
	  {
		if(tempArray[l]<tempArray[j])
		{
		 temp = tempArray[l];
		 tempArray[l] = tempArray[j];
		 tempArray[j] = temp;
		}
	  }
	}

}*/

//void DescendQueue2(queue<int>& realQueue, queue<int> tempQueue)
//{
//		int tempArray[10];
//		int i = 0;
//		int j = 0;
//	ClearQueue(realQueue);
//	stack<int> tempStack;
//	while(!tempQueue.empty())
//	{
//
//
//		j = tempQueue.front();
//
//		tempArray[i] = tempQueue.front();
//		i++;		
//		cout<<j<<endl;
//		tempStack.push(tempQueue.front());
//		tempStack.pop();
//	}
//	while(!tempStack.empty())
//	{
//		realQueue.push(tempStack.top());
//		tempStack.pop();
//	}
//}



int _tmain(int argc, _TCHAR* argv[])
{
	bool isExit = false;
	do
	{
		int choice = -1;
		int value = 0;
		cout<<"The Queue Program"<<endl;
		Display(myQueue);
		cout<<"1. Add to the queue"<<endl;
		cout<<"2. Remove from the queue"<<endl;
		cout<<"3. Reverse the queue"<<endl;
		cout<<"4. Arrange in DSC"<<endl;
		cout<<"5. Exit the program"<<endl;
		cout<<"Your choice is :"<<endl;
		cin>>choice;
		cout<<"============================"<<endl;
		if (choice == 1)
		{
			cout<<"Value to be added :";
			cin>>value;
			myQueue.push(value);
		}
		else if (choice == 2)
		{
			if(!myQueue.empty())
			{
				myQueue.pop();
			}
			else
			{
				cout<<"Nothing to pop!"<<endl;
			}
		}
		else if (choice == 3)
		{
			ReverseQueue(myQueue, myQueue);
		}

		else if (choice == 4)
		{
//			DescendQueue(myQueue, myQueue);
		}
		else if (choice == 5)
		{
			exit(0);
		}
		system("PAUSE");
		system("CLS");
	}while(!isExit);
	return 0;
}

