// FactoryPattern.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "AEmployee.h"

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	AEmployee *emp = AEmployee::CreateEmployee(AEmployee::EMP_TYPE::MANAGER);
	cout << "Receivable For Manager " << endl;
	emp->PrintBenefits();
	cout << endl;
	delete emp;

	emp = AEmployee::CreateEmployee(AEmployee::EMP_TYPE::ENGINEER);
	cout << "Receivable For Engineer " << endl;
	emp->PrintBenefits();
	cout << endl;
	


	
	
	system("PAUSE");
	return 0;
}

