//! header file -> handle the class declaration
#include "DefaultHeaderFiles.h"
class GameObject
{
public:
    //! attribute or properties
    int x, y, xOld, yOld, row, col, speedX, speedY, colour;
    //! represent the 2-D sprite array
    //!     X
    //!     X->
    //!     X
    char** sprite;
    //! to check the visibility of each object
    bool isToBeDestroy;
    //! timer
    int updateDelayTimer;
    int updateDelayDuration;

    //! constructor
    GameObject();
    //! function

    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();

    void DrawTrail();
    void Draw();
    //! check the boundary collision detection
    bool CheckOverBoundary();
    //! pure virtual function
    //!-> no need to include the function body
    virtual void Update(int elapsedTimed);
};
