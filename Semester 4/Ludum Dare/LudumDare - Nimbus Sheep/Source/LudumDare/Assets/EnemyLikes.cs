﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyLikes : MonoBehaviour {
    public GameObject inOut;
    SendInputOnSpace datascript;
    int score;
    Text text;
	// Use this for initialization
	void Start () {
        datascript = inOut.GetComponent<SendInputOnSpace>();
        text = GetComponent<Text>();
	
	}
	
	// Update is called once per frame
	void Update () {
        score = datascript.enemyLikes;
        text.text = score.ToString();
	
	}
}
