#include "Node.h"

void Node::addChild(Node* node)
{
	//!node must not have a prent
	if (node->mParent) return;

	mChildren.push_back(node);
	node->mParent = this;
}

void Node::appendActor(Actor* actor)
{
	if (actor->mOwner) return;

	mActor.push_back(actor);
	actor->mOwner = this;
}

void Node::accept(ActorVisitor* visitor)
{
	size_t count = mActor.size();
	for (size_t i = 0; i < count; i++)
	{
		visitor->visit(mActor[i]);
	}
	count = mChildren.size();

	for (size_t i = 0; i < count; i++)
	{
		mChildren[i]->accept(visitor);
	}
}