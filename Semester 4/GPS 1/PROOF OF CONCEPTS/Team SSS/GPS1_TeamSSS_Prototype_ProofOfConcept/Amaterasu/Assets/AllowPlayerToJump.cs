﻿using UnityEngine;
using System.Collections;

public class AllowPlayerToJump : MonoBehaviour 
{
	GameObject player;
	Player playerScript;

	bool playerInZone=false;
	Rect textArea;

	// Use this for initialization
	void Start () 
	{
		player=GameObject.Find("/Player");
		playerScript = player.GetComponent<Player>();
		Vector2 screenPos= Camera.main.WorldToScreenPoint(transform.position);
		screenPos.y*=2;
		textArea=new Rect(screenPos.x,screenPos.y,200,200);	
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	void OnMouseDown()
	{
		if(playerInZone)
		{
			player.rigidbody2D.AddForce(new Vector2(0,0.025f));
		}
	}
	void OnTriggerStay2D(Collider2D other)
	{
		if(other.tag=="Player")
		{
			
			playerInZone=true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag=="Player")
		{
			playerInZone=false;
		}
	}
	
	void OnGUI()
	{
		if(playerInZone)
		{
			GUI.Label(textArea,"Climb");
		}
	}
}
