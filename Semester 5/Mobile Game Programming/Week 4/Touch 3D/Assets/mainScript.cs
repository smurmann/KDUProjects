﻿using UnityEngine;
using System.Collections;

public class mainScript : MonoBehaviour {

    Camera camera;
    public GameObject m_projectilePrefab;
    int count = 0;

	// Use this for initialization
	void Start () {
        camera = GetComponent<Camera>();
        
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.touchCount>0)
        {
            Touch myTouch = Input.GetTouch(0);

            foreach(Touch touch in Input.touches)
          //  if(touch.phase == TouchPhase.Began)
            {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);

                GameObject newObject = Instantiate(m_projectilePrefab, ray.origin, Quaternion.identity) as GameObject;
                count++;
                Debug.Log(count);
                Rigidbody newObjectRigidBody = newObject.GetComponent<Rigidbody>();
                newObjectRigidBody.velocity = ray.direction * 15.0f;
                
            }
        }
	
	}
}
