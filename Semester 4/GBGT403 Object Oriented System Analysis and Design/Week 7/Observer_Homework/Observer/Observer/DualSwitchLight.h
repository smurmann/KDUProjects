#ifndef _DUALSWITCHLIGHT_H
#define _DUALSWITCHLIGHT_H
#include "Observer.h"
#include "Trigger.h"
#include <iostream>

using namespace std;

class DualSwitchLight : public Observer
{
protected:
	//! Light switch 1
	Trigger* mSwitch1;
	//! Light switch 2
	Trigger* mSwitch2;
	//! bool of light (on or off)
	int mLightState;
	//! Light enable flag;
	bool mEnable;

public:
	DualSwitchLight(Trigger* switch1, Trigger* switch2) :
		mSwitch1(switch1), mSwitch2(switch2),
		mLightState(0), mEnable(true)
	{
		switch1->AddObserver(this);
		switch2->AddObserver(this);
	}
	//! Enable/Disable light
	//! add the Observer / remove the observer;
	void setEnable(bool enable)
	{
		if (mEnable == enable) return;
		mEnable = enable;
		if (enable)
		{
			mSwitch1->AddObserver(this);
			mSwitch2->AddObserver(this);

			cout << "Light Bulb has been connected" << endl;
			update(NULL);
		}
		else
		{
			mSwitch1->removeObserver(this);
			mSwitch2->removeObserver(this);

			cout << "Light Bulb has been disconnected" << endl;
			update(NULL);

			if (mLightState)
				cout << "The room is dark again" << endl;
		}
	}
protected:
	//! update function called when subject has notification event
	void update(Subject* subject)
	{
		int switch1State = mSwitch1->getState() ? 1 : 0;
		int switch2State = mSwitch2->getState() ? 1 : 0;

		//mLightState = ((switch1State || switch2State) && (switch1State && switch2State));
		mLightState = switch1State^switch2State;

		
		if (mLightState)
			cout << "Light is on!" << endl;
		else
			cout << "Light is off!" << endl;
		
	

	}

};

#endif