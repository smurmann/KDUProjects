#ifndef _DEFAULT_HEADER_FILES_H_
#define _DEFAULT_HEADER_FILES_H_

#include <iostream>
#include <windows.h>
#include <vector>
#include <time.h>
#include "conio_yp.h"

using namespace std;

//!game world settings
#define BOUNDARY_LEFT 4
#define BOUNDARY_RIGHT 77
#define BOUNDARY_TOP 4
#define BOUNDARY_BOTTOM 28

//! include 8 directions
//! which type of container is suitable to
//! store 8 directions
//! enumeration - user defined type
enum DIRECTION
{
    NORTH = 0,
    EAST,
    SOUTH,
    WEST,
    NORTH_WEST,
    NORTH_EAST,
    SOUTH_WEST,
    SOUTH_EAST
};

enum ENEMY_TYPE
{
    ENEMY_01 = 0,
    ENEMY_02,
    ENEMY_03
};

enum BULLET_TYPE
{
    STRAIGHT = 0,
    DUAL,
    TRIPLE
};

enum ITEM_TYPE
{
    HEALTH_PACK = 0,
    DUAL_PACK,
    TRIPLE_PACK,
    NO_ITEM,
    TOTAL_ITEM_TYPE
};
struct SpawnInfo
{
    ENEMY_TYPE type;
    ITEM_TYPE itemType;
    int time;
    bool isSpawned;
};


#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Enemy1.h"
#include "Enemy2.h"
#include "Enemy3.h"
#include "Item.h"
#include "Bullet.h"
#include "Border.h"


#endif //_DEFAULT_HEADER_FILES_H_
