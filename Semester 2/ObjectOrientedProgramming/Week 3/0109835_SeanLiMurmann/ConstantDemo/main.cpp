#include <iostream>

using namespace std;

class Rectangle
{
public:
    const int side = 100;

    //! constructor
    Rectangle()
    {
        cout<<"Side "<<side<<endl;
       // side += 100;
        cout<<"(After update) Side "<<side<<endl;
    }
};

int main()
{
    cout << "Hello world!" << endl;
    Rectangle rect;
    cout<<rect.side<<endl;
    return 0;
}
