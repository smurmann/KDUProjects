#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Critter
{
 private:
     string name;
 public:
    Critter(const string& mname ="");
    //!void setName(string name)
    string GetName() const;
};
Critter::Critter(const string& mname)
{
    name = mname;
}

string Critter::GetName() const
{
    return name;
}

class Farm
{
   private:
       //! vector is a type of container which allow you to store
       //! more than 1 item
       vector<Critter>mCritter;
   public:
       //!constructor -> accepting space value
       Farm(int space=1);
       //! add the critter object to the mCritter vector
       void Add(const Critter& aCritter);
       void RollCall();
};
//!implement the Farm class function
Farm::Farm(int space)
{
    //! reserve function -> build in function from the vector class
    //! to set the size of the container
    mCritter.reserve(space);
}
void Farm::Add(const Critter& aCritter)
{
    //!to add the critter object to the vector container
    //!push_back -> build in function from vector class
    //!is to push in the critter object to the vector
    mCritter.push_back(aCritter);
}

void Farm::RollCall()
{
    //!Display all the critter object name from the vector
    for(vector<Critter>:: const_iterator iter=mCritter.begin();
    iter!=mCritter.end();++iter)
    {
        cout<<iter->GetName()<<" here."<<endl;
    }
}


int main()
{
    Critter crit("Pikachu");
    cout<< "My first Critter's name is: "<<crit.GetName()<<endl;

    //! create a farm object with space size given
    Farm myFarm(3);

    cout<< "Adding three critter to the farm:"<<endl;

    myFarm.Add(Critter("Moe"));
    myFarm.Add(Critter("Larry"));
    myFarm.Add(Critter("Curly"));
    myFarm.Add(Critter("Purly"));

    cout<< "Calling Roll"<<endl;
    myFarm.RollCall();
    return 0;
}
