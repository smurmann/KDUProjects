#ifndef PLAYERCLASS_H
#define PLAYERCLASS_H

#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
#include "WeaponFactory.h"


using namespace std;

class Player
{
private:
	// Stuff that can be set
	string Name;
	int SkillPoints;

	//basic stats
	int Level;
	int Experience;

	int Strength;
	int Intelligence;
	int Dexterity;
	
	//combat stats
	int DmgPhysical;
	int DmgMagical;
	int Defense;
	int Health;
	int Mana;
	int EvadeChance;

	//equipment
	//vector<string> weapons;
	vector<string> equipment;
	vector<WeaponFactory> weapons;
	


public:
	void addToWeapons(const WeaponFactory& wep)
	{
		if (weapons.size() <= 1)
		{
			this->weapons.push_back(wep);
		}
		else
		{
			cout << "Sorry, but you only have two hands to carry weapons!!" << endl;
		}
	}
	// setStuff
	void setSkillPoints(const int &SKLL)
	{
		SkillPoints = SKLL;
	}

	void setName(const string &NAME)
	{
		Name = NAME;
	}

	void setLevel(const int &LVL)
	{
		Level = LVL;
	}

	void setExperience(const int &EXP)
	{
		Experience = EXP;
	}

	void setStrength(const int &STR)
	{
		Strength = STR;
	}

	void setIntelligence(const int &INT)
	{
		Intelligence = INT;
	}

	void setDexterity(const int &DEX)
	{
		Dexterity = DEX;
	}

	void setDmgPhysical(const int &DMGPhy)
	{
		DmgPhysical = DMGPhy;
	}

	void setDmgMagical(const int &DMGMgc)
	{
		DmgMagical = DMGMgc;
	}

	void setDefense(const int &DEF)
	{
		Defense = DEF;
	}

	void setHealth(const int &HP)
	{
		Health = HP;
	}

	void setMana(const int &MP)
	{
		Mana = MP;
	}

	void setEvadeChance(const int &EVADE)
	{
		EvadeChance = EVADE;
	}

	//get Stuff
	int getSkillPoints()
	{
		return SkillPoints;
	}

	string getName()
	{
		return Name;
	}

	int getLevel()
	{
		return Level;
	}

	int getExperience()
	{
		return Experience;
	}

	int getStrength()
	{
		return Strength;
	}

	int getIntelligence()
	{
		return Intelligence;
	}

	int getDexterity()
	{
		return Dexterity;
	}

	int getDmgPhysical()
	{
		return DmgPhysical;
	}
	
	int getDmgMagical()
	{
		return DmgMagical;
	}

	int getDefense()
	{
		return Defense;
	}

	int getHealth()
	{
		return Health;
	}

	int getMana()
	{
		return Mana;
	}

	int getEvadeChance()
	{
		return EvadeChance;
	}

	void getCurrentWeapon()
	{
		cout << "These are your weapons: " << endl;
		for (int i = 0; weapons.size() > i; i++)
		{
			weapons[i].showWeaponStats();
		}
	}

	void addCurrentWeaponToStat()
	{
		for (int i = 0; weapons.size() > i; i++)
		{
			setHealth(getHealth() + weapons[i].getHealth());
			setMana(getMana() + weapons[i].getMana());
			setDmgPhysical(getDmgPhysical() + weapons[i].getPDmg());
			setDmgMagical(getDmgMagical() + weapons[i].getMDmg());
			setEvadeChance(getEvadeChance() + weapons[i].getEvadeChance());
			setDefense(getDefense() + weapons[i].getDefense());
		}
	}

	void PlayerCombatStats()
	{
		cout << Name << "'s Stats (LEVEL " << getLevel() << ")" << endl;
		cout << endl;
		cout << "Health        : " << getHealth() << endl;
		cout << "Mana          : " << getMana() << endl;
		cout << "Dmg (MAGIC)   : " << getDmgMagical() << endl;
		cout << "Dmg (PHYSICAL): " << getDmgPhysical() << endl;
		cout << "Defense       : " << getDefense() << endl;
		cout << "EvadeChance   : " << getEvadeChance() << endl;
	}

	void PlayerFullStats()
	{
		cout << Name << "'s Stats (LEVEL " << getLevel() << ")" << endl;
		cout << endl;
		cout << "Str           : " << getStrength() << endl;
		cout << "Dex           : " << getDexterity() << endl;
		cout << "Int           : " << getIntelligence() << endl;
		cout << "Health        : " << getHealth() << endl;
		cout << "Mana          : " << getMana() << endl;
		cout << "Dmg (MAGIC)   : " << getDmgMagical() << endl;
		cout << "Dmg (PHYSICAL): " << getDmgPhysical() << endl;
		cout << "Defense       : " << getDefense() << endl;
		cout << "EvadeChance   : " << getEvadeChance() << endl;
	}

	//add stuff
	void bAddStr()
	{
		setSkillPoints(getSkillPoints() - 1);
		if (getSkillPoints() >= 0)
		{
			setStrength(getStrength() + 1);
			setHealth(getHealth() + 20);
			setMana(getMana() + 5);
			setDmgPhysical(getDmgPhysical() + 10);
			setDmgMagical(getDmgMagical() + 3);
			setEvadeChance(getEvadeChance() - 2);
			setDefense(getDefense() + 10);
		}
		else
		{
			setSkillPoints(0);
			cout << "Can't add Str. You don't have enough skillpoints." << endl;
		}

	}
	void bAddDex()
	{
		setSkillPoints(getSkillPoints() - 1);
		if (getSkillPoints() >= 0)
		{
			setDexterity(getDexterity() + 1);
			setHealth(getHealth() + 10);
			setMana(getMana() + 10);
			setDmgPhysical(getDmgPhysical() + 5);
			setDmgMagical(getDmgMagical() + 5);
			setEvadeChance(getEvadeChance() + 5);
			setDefense(getDefense() + 5);
		}
		else
		{
			setSkillPoints(0);
			cout << "Can't add Dex. You don't have enough skillpoints." << endl;
		}
	}
	void bAddInt()
	{
		setSkillPoints(getSkillPoints() - 1);
		if (getSkillPoints() >= 0)
		{
			setIntelligence(getIntelligence() + 1);
			setHealth(getHealth() + 5);
			setMana(getMana() + 20);
			setDmgPhysical(getDmgPhysical() + 3);
			setDmgMagical(getDmgMagical() + 10);
			setEvadeChance(getEvadeChance() + 2);
			setDefense(getDefense() + 3);
		}
		else
		{
			setSkillPoints(0);
			cout << "Can't add Int. You don't have enough skillpoints." << endl;
		}
	}
};

#endif