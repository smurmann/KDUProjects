#include <iostream>
#include "windows.h"

using namespace std;

int main()
{
    int value = 200;
    cout << "Value: \t\t\t" << value << endl;
    cout << "Address of value: \t" << &value << endl;
    cout << endl;

    value = 1337;
    cout << "Value: \t\t\t" << value << endl;
    cout << "Address of value: \t" << &value << endl;
    cout << endl;

    int newValue = value;
    int * pointerToValue = &value;
    cout << "New Value: \t\t" << newValue << endl;
    cout << "Address of new value: \t" << &newValue << endl;
    cout << endl;

    value = 101;
    cout << "New Value: \t\t" << newValue << endl;
    cout << "Address of new value: \t" << &newValue << endl;
    cout << endl;

    cout << "Pointer to value: \t\t" << pointerToValue << endl;
    cout << "*Pointer to value: \t\t" << *pointerToValue << endl;
    cout << "Address of Pointer to value: \t" << &pointerToValue << endl;

    cout << endl;

    *pointerToValue = 10;
    cout << "*Pointer to value: " << *pointerToValue << endl;
    cout << "Value: " << value << endl;

    system("PAUSE");
    return 0;
}
