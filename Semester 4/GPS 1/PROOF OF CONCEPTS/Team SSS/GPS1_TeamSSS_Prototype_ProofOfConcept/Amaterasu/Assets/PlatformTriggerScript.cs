﻿using UnityEngine;
using System.Collections;

public class PlatformTriggerScript : MonoBehaviour 
{
	[SerializeField] GameObject platform;
	bool collidingWithOrb;
	private bool activated;
	Rect textArea;
	private GameObject orb;
	OrbScript orbScript;
	// Use this for initialization
	void Start () 
	{
		collidingWithOrb=false;
		activated=false;
		
		Vector2 screenPos= Camera.main.WorldToScreenPoint(transform.position);
		screenPos.y*=2;
		textArea=new Rect(screenPos.x,screenPos.y,200,200);	
	}
	
	// Update is called once per frame
	void Update () 
	{
		orb = GameObject.Find("/Orb");
		orbScript=orb.GetComponent<OrbScript>();
		if(activated)
		{
		
		}
		
	}
	
	void OnTriggerStay2D(Collider2D other) 
	{
		if(other.gameObject.tag=="Orb")
		{
			collidingWithOrb=true;
		}
		
	}
	void OnTriggerExit2D(Collider2D other) 
	{
		if(other.gameObject.tag=="Orb")
		{
			collidingWithOrb=false;
		}
	}
	void OnGUI()
	{
		if(collidingWithOrb&!activated)
			GUI.Label(textArea,"Activate");
	}
	void OnMouseOver()
	{
		
		if(Input.GetMouseButtonDown(1)&!activated)
		{
			if(collidingWithOrb)
			{
				activated=true;
				Instantiate(platform,this.gameObject.transform.position,Quaternion.identity);
			
			}
		}
	}

}
