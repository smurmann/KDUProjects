#ifndef _TRIANGLE_DEMO_H
#define _TRIANGLE_DEMO_H

#include "demo_base.h"
#include "bitmap.h"

#define TEXTURE_COUNT 1

class TriangleDemo : public DemoBase
{
private:
	GLuint mtextureID[TEXTURE_COUNT];
	void loadTexture(const char* path, GLuint textureID)
	{
		CBitmap bitmap(path);

		// Create Linear FIltered Texture
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//aooky texture wrapping along horizontal part.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);//aooky texture wrapping along vertical part.
		/*
		// old school minecraft filtering.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //near filtering. (For When texture needs to scale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // far filtering. (For when texture needs to scale down on screen)
		*/

		// bilinear filtering.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR); //near filtering (For when texgure needs to scale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.GetWidth(), bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.GetBits());
	}
public: 
	float m_rotation;
	Vector m_translation;
public:
	void init()
	{
		glGenTextures(TEXTURE_COUNT, mtextureID);
		loadTexture("../media/rocks.bmp", mtextureID[0]);
		//m_rotation = 0.0f;
		//m_translation = Vector(-0.5f, -0.5f, -0.5f);
	}

	void drawTexturedPolygons()
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mtextureID[0]);

		glBegin(GL_TRIANGLES);							// Drawing Using triangles
		
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);				// Top
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 0.0f, 0.0f);				// Bottom Left
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, 0.0f);				// Bottom Right
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 1.0f, 0.0f);

		glTexCoord2f(1.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 1.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 1.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(0.0f, 1.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);

		glTexCoord2f(1.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 1.0f, 1.0f);
		glEnd();

		glDisable(GL_TEXTURE_2D);

	}

	void deinit()
	{

	}

	void drawPyramid()
	{
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glEnd();



	}

	void drawCone(float height, float radius, float xOffset, float yOffset, float zOffset, GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(red, green, blue);
		for (int i = 1; i<360; i++)
		{
			glVertex3f(0, height, 0);
			for (float i = 1; i< 360; i++)
			{
				glVertex3f(radius*cos(i) + xOffset, 0 + yOffset, radius*sin(i)+zOffset);
			}
		}
		glEnd();
	}

	void drawCylinder(float radius, float height, float xOffset, float yOffset, float zOffset, GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(red, green, blue);
		for (int i = 1; i<360; i++)
		{
			glVertex3f(0+xOffset, height+yOffset, 0+zOffset);
			glVertex3f(0+xOffset, 0+yOffset, 0+zOffset);
			for (float i = 1; i< 360; i++)
			{
				glVertex3f(radius*cos(i)+xOffset, height+yOffset, radius*sin(i)+zOffset);
				glVertex3f(radius*cos(i)+xOffset, 0+yOffset, radius*sin(i)+zOffset);
			}
		}
		glEnd();
	}

	void drawCube()
	{ 
#pragma region DrawCube
		glBegin(GL_TRIANGLES);							// Drawing Using triangles
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);				// Top
		glVertex3f(1.0f, 0.0f, 0.0f);				// Bottom Left
		glVertex3f(1.0f, 1.0f, 0.0f);				// Bottom Right
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 1.0f);

		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 1.0f);
		glEnd();
#pragma endregion cubespawn
	}

	void drawPlanet1(const Matrix& viewMatrix)
	{
		//m_rotation += 0.1f*;
		m_translation.set(cos(m_rotation), sin(m_rotation), -0.5f);

		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(0.0f, 1.0f, 0.0f));
		
		//translate to origin
		//Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);

		Matrix translate1 = Matrix::makeTranslationMatrix(2.0f, -0.5, -0.5f);

		Matrix modelMatrix = rotate1* translate1;

		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;

		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		drawPyramid();

	}

	void drawPlanet3(const Matrix& viewMatrix)
	{
		//m_rotation += 0.1f*;
		m_translation.set(cos(m_rotation), sin(m_rotation), -0.5f);

		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation/-2, Vector(0.0f, 1.0f, 0.0f));

		//translate to origin
		//Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);

		Matrix translate1 = Matrix::makeTranslationMatrix(7.0f, -0.5, -0.5f);

		Matrix modelMatrix = rotate1* translate1 * rotate1 * rotate1;

		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;

		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		drawCube();

	}

	void drawPlanet2(const Matrix& viewMatrix)
	{
		//m_rotation += 0.1f*;
		m_translation.set(cos(m_rotation), sin(m_rotation), -0.5f);

		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation/1.3, Vector(0.0f, 1.0f, 0.0f));

		//translate to origin
		//Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);

		Matrix translate1 = Matrix::makeTranslationMatrix(5.0f, -0.5, -0.5f);

		Matrix modelMatrix = rotate1* translate1 * rotate1 * rotate1;

		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;

		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		drawCube();

	}

	void draw(const Matrix& viewMatrix)
	{
		//drawaxis
		glLoadMatrixf((GLfloat*)viewMatrix.mVal);
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.3f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.3f, 1.0f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.3f, 0.3f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();

		Matrix rotate1 = Matrix::makeRotateMatrix(0, Vector(0.0f, 0.0f, 1.0f));
		Matrix translate1 = Matrix::makeTranslationMatrix(0.0f, 0.0f, 0.0f);
		//m_rotation += 0.1f;

//		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(-1.0f, -1.0f, -1.0f));
//		Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);
		//Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(0.0f, 0.0f, 0.0f));
		//Matrix translate1 = Matrix::makeTranslationMatrix(0.0f, 0.0f, 0.0f);

		// Note onn OpenGl Matrix model;
		// Screen = Proj * View * Model
		// Model = TranformA(3rd) * TransformB(2nd) * TransformC(1st) (transform could be Rotate, Scale, Translate, etc)

		// perform model transformation
		Matrix modelMatrix = translate1 * rotate1;
		//Matrix modelMatrix = translate1;
		//Matrix modelMatrix = rotate1;

		// convert model space to view space
		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;


		// Triangle
		//glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		//glColor3f(1.0f, 1.0f, 1.0f);
		//glBegin(GL_TRIANGLES);							// Drawing Using triangles
		//	glVertex3f(0.0f, 1.0f, 0.0f);				// Top
		//	glVertex3f(-0.3f, -1.0f, 0.0f);				// Bottom Left
		//	glVertex3f(0.3f, -1.0f, 0.0f);				// Bottom Right
		//glEnd();										// Finish Drawing the triangle


		// Rectangle
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		//drawPyramid();
		//drawCube();
		glColor3f(1.0f, 1.0f, 1.0f);


	//	drawTexturedPolygons();							// Finish Drawing the triangle
		//drawCylinder(0.1f, 0.2f,0.5f,0.0f,0.0f,1.0f,1.0f,1.0f);
		//drawCone(0.3f, 0.1f);
	}
};


#endif