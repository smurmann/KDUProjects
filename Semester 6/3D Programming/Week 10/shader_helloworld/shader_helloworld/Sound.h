#define SPECTRUM_SIZE 64

#include <fmod.hpp>

class FmodTest
{
private:
	FMOD::System* m_fmodSystem;
	FMOD::Sound* m_music;
	FMOD::Channel *m_musicChannel;

	//these are use for getting specturm data
public:
	float m_spectrumLeft[SPECTRUM_SIZE];
	float m_spectrumRight[SPECTRUM_SIZE];
};