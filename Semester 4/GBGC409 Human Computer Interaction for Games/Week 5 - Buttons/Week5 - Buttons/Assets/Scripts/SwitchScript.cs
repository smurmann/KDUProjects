﻿using UnityEngine;
using System.Collections;

public class SwitchScript : MonoBehaviour {

	bool enable = true;

	public Sprite Up;
	public Sprite Pressed;

	SpriteRenderer button;

	// Use this for initialization
	void Start () {
		enable = true;
		button = GetComponent<SpriteRenderer> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		if(enable)
		{
			button.sprite = Up;
		}
		if(!enable)
		{
			button.sprite = Pressed;
		}
	
	}

	void OnMouseDown()
	{
		Debug.Log ("MouseDown!");
		if(enable)
		{
			enable = false;
			GameObject.Find ("Button").GetComponent<ButtonScript>().Enable();
		}
		else if (!enable)
		{
			enable = true;
			GameObject.Find ("Button").GetComponent<ButtonScript>().Disable();
		}
	}

	public void ToggleOff()
	{
		if(!enable)
		{
			enable = true;
			button.sprite = Up;
		}
	}

}
