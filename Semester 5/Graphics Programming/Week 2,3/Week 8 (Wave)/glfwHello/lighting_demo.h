#ifndef _LIGHTING_DEMO_H
#define _LIGHTING_DEMO_H

#include "demo_base.h"
#include "bitmap.h"

#define TEXTURE_COUNT 3

class LightingDemo : public DemoBase
{

private:
	GLuint mTextureID[TEXTURE_COUNT];
	GLfloat mRectVertices[18];

	void loadTexture(const char* path, GLuint textureID)
	{
		CBitmap bitmap(path);

		// Create Linear Filtered Texture
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // apply texture wrapping along horizontal part.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // apply texture wrapping along vertical part.

		// old school (minecraft) filtering.
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // near filtering. (For when texture needs to scale up on screen)
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // far filtering. (For when texture need to scale down on screen)

		
		// bilinear filtering.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // near filtering. (For when texture needs to scale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // far filtering. (For when texture need to scale down on screen)
		

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.GetWidth(), bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.GetBits());
	}

public:
	void init()
	{
		glGenTextures(TEXTURE_COUNT, mTextureID);
		loadTexture("../media/glass.bmp", mTextureID[0]);
		loadTexture("../media/background.bmp", mTextureID[1]);
		loadTexture("../media/rocks.bmp", mTextureID[2]);

		//initLights();
	}

	void deinit()
	{
	}

	void updateLights()
	{
		// set up light colors (ambient, diffuse, specular)
		GLfloat lightAmbient[] = {0.1f, 0.1f, 0.1f, 1.0f}; 
		GLfloat lightDiffuse[] = {0.7f, 0.7f, 0.7f, 1.0f};  
		GLfloat lightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};  
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

		// position the light
		float lightPos[4] = {0, 5, 5, 0}; // positional light, 4th component is ignored
		glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

		glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
	}

	void drawWorldOriginLines(const Matrix& viewMatrix)
	{
		// draw axis.
		glLoadMatrixf((GLfloat*)viewMatrix.mVal);
		glBegin(GL_LINES);
			glColor3f(1.0f, 0.3f, 0.3f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glVertex3f(1.0f, 0.0f, 0.0f);

			glColor3f(0.3f, 1.0f, 0.3f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 1.0f, 0.0f);

			glColor3f(0.3f, 0.3f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();
	}

	void drawCube()
	{

		static GLfloat vertices[] = { 1, 1, 1,  -1, 1, 1,  -1,-1, 1,      // v0-v1-v2 (front)
									   -1,-1, 1,   1,-1, 1,   1, 1, 1,      // v2-v3-v0

										1, 1, 1,   1,-1, 1,   1,-1,-1,      // v0-v3-v4 (right)
										1,-1,-1,   1, 1,-1,   1, 1, 1,      // v4-v5-v0

										1, 1, 1,   1, 1,-1,  -1, 1,-1,      // v0-v5-v6 (top)
									   -1, 1,-1,  -1, 1, 1,   1, 1, 1,      // v6-v1-v0

									   -1, 1, 1,  -1, 1,-1,  -1,-1,-1,      // v1-v6-v7 (left)
									   -1,-1,-1,  -1,-1, 1,  -1, 1, 1,      // v7-v2-v1

									   -1,-1,-1,   1,-1,-1,   1,-1, 1,      // v7-v4-v3 (bottom)
										1,-1, 1,  -1,-1, 1,  -1,-1,-1,      // v3-v2-v7

										1,-1,-1,  -1,-1,-1,  -1, 1,-1,      // v4-v7-v6 (back)
									   -1, 1,-1,   1, 1,-1,   1,-1,-1 };    // v6-v5-v4

	
		//static GLfloat normals[]  = { 0, 0, 1,   0, 0, 1,   0, 0, 1,      // v0-v1-v2 (front)
		//								0, 0, 1,   0, 0, 1,   0, 0, 1,      // v2-v3-v0

		//								1, 0, 0,   1, 0, 0,   1, 0, 0,      // v0-v3-v4 (right)
		//								1, 0, 0,   1, 0, 0,   1, 0, 0,      // v4-v5-v0

		//								0, 1, 0,   0, 1, 0,   0, 1, 0,      // v0-v5-v6 (top)
		//								0, 1, 0,   0, 1, 0,   0, 1, 0,      // v6-v1-v0

		//							   -1, 0, 0,  -1, 0, 0,  -1, 0, 0,      // v1-v6-v7 (left)
		//							   -1, 0, 0,  -1, 0, 0,  -1, 0, 0,      // v7-v2-v1

		//								0,-1, 0,   0,-1, 0,   0,-1, 0,      // v7-v4-v3 (bottom)
		//								0,-1, 0,   0,-1, 0,   0,-1, 0,      // v3-v2-v7

		//								0, 0,-1,   0, 0,-1,   0, 0,-1,      // v4-v7-v6 (back)
		//								0, 0,-1,   0, 0,-1,   0, 0,-1 };    // v6-v5-v4

		static GLfloat normals[]  = { 1, 1, 1,  -1, 1, 1,  -1,-1, 1,      // v0-v1-v2 (front)
									   -1,-1, 1,   1,-1, 1,   1, 1, 1,      // v2-v3-v0

										1, 1, 1,   1,-1, 1,   1,-1,-1,      // v0-v3-v4 (right)
										1,-1,-1,   1, 1,-1,   1, 1, 1,      // v4-v5-v0

										1, 1, 1,   1, 1,-1,  -1, 1,-1,      // v0-v5-v6 (top)
									   -1, 1,-1,  -1, 1, 1,   1, 1, 1,      // v6-v1-v0

									   -1, 1, 1,  -1, 1,-1,  -1,-1,-1,      // v1-v6-v7 (left)
									   -1,-1,-1,  -1,-1, 1,  -1, 1, 1,      // v7-v2-v1

									   -1,-1,-1,   1,-1,-1,   1,-1, 1,      // v7-v4-v3 (bottom)
										1,-1, 1,  -1,-1, 1,  -1,-1,-1,      // v3-v2-v7

										1,-1,-1,  -1,-1,-1,  -1, 1,-1,      // v4-v7-v6 (back)
									   -1, 1,-1,   1, 1,-1,   1,-1,-1};    // v6-v5-v4


		static GLfloat colors[]   = { 255, 255, 255,   255, 255, 255,   255, 255, 255,      // v0-v1-v2 (front)
										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v2-v3-v0

										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v0-v3-v4 (right)
										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v4-v5-v0

										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v0-v5-v6 (top)
										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v6-v1-v0

										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v1-v6-v7 (left)
										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v7-v2-v1

										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v7-v4-v3 (bottom)
										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v3-v2-v7

										255, 255, 255,   255, 255, 255,   255, 255, 255,      // v4-v7-v6 (back)
										255, 255, 255,   255, 255, 255,   255, 255, 255 };    // v6-v5-v4





		glEnable(GL_LIGHTING);

		glBindTexture(GL_TEXTURE_2D, mTextureID[0]);


		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glColorPointer(3, GL_UNSIGNED_BYTE, 0, colors);
		glNormalPointer(GL_FLOAT, 0, normals);

		glDrawArrays(GL_TRIANGLES, 0, 36);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);

		glDisable(GL_LIGHTING);
	}

	void draw(const Matrix& viewMatrix)
	{
		// draw axis.
		drawWorldOriginLines(viewMatrix);

		

		Matrix rotate1 = Matrix::makeRotateMatrix(0, Vector(0.0f, 0.0f, 1.0f));
		Matrix translate1 = Matrix::makeTranslationMatrix(0.0f, 0.0f, 0.0f);

		// NOTE on OpenGL Matrix model:
		// Screen = Proj * View * Model
		// Model = TransformA(3rd) * TransformB(2nd) * TransformC(1st) (Trasnform could be Rotate, Scale, Translate, etc)
		
		// perform model transformation
		Matrix modelMatrix = translate1 * rotate1;

		// convert model space to view space
		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;

		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		
		updateLights();

		drawCube();

	}
};

#endif
