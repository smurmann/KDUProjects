#include <iostream>
using namespace std;

class Hero
{
private:
    int health;
public:
    inline void SetHealth(int h)
    {
        health = h;
    }
    inline int GetHealth()
    {
        return health;
    }
};

int main()
{
    Hero h1,h2;
    h1.SetHealth(100);
    h2.SetHealth(200);
    cout<<"Hero1's health "<<h1.GetHealth()<<endl;
    cout<<"Hero2's health "<<h2.GetHealth()<<endl;
    return 0;
}
