// CompositeDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Component
{

protected:
	string fullName;
	double valueSalary;
public:
	Component(string name, double salary) :fullName(name), valueSalary(salary)
	{

	}

	virtual void printSalary(int level) = 0;

};

//! primitive class
class Worker :public Component
{

public:
	Worker(string name, double salary) :Component(name, salary)
	{

	}
	void printSalary(int level)
	{
		for (int j = 0; j < level; j++)
		{
			cout << "\t";
		}
		cout << "Worker : " << fullName << " , salary : " << valueSalary << endl;
	}


};

//! composite Class
class Manager :public Component
{
private:
	vector<Component*> children;
public:
	Manager(string name, double salary) :Component(name, salary)
	{

	}
	void add(Component *cmp)
	{
		children.push_back(cmp);
	}
	void printSalary(int level)
	{
		for (int j = 0; j < level; j++)
		{
			cout << "\t";
		}
		cout << "Manager : " << this->fullName << " , salary RM " << valueSalary << endl;

		if (!children.empty())
		{
			for (int x = 0; x < level; ++x)
			{
				cout << "\t";
			}
			cout << "Subbordinates of " << fullName << " : " << endl;
			++level;
			for (int i = 0; i < children.size(); i++)
			{
				children[i]->printSalary(level);
			}
		}
	}


};
int _tmain(int argc, _TCHAR* argv[])
{
	
	Manager CEO("Sam Kong" , 100000.0);
	Manager EnginneringManager(" TJ ", 50000.0);
	Manager ProductionManager("Mody", 80000.0);
	Manager TechicalLead("Azri", 100000.0);
	Manager ReseachLead("Max", 20000.0);

	Worker programmer1("Sean", 50000.0);
	Worker programmer2("WC", 50000.0);

	Worker research1("Vince", 10000.0);
	Worker research2("Kris", 15000.0);

	CEO.add(&EnginneringManager);
	CEO.add(&ProductionManager);

	ProductionManager.add(&TechicalLead);

	EnginneringManager.add(&ReseachLead);

	ReseachLead.add(&research1);
	ReseachLead.add(&research2);

	TechicalLead.add(&programmer1);
	TechicalLead.add(&programmer2);

	cout << "The Hierachy of the Company " << endl;
	CEO.printSalary(0);
	
	
	system("PAUSE");
	return 0;
}

