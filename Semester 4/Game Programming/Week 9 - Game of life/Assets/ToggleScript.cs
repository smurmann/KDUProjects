﻿using UnityEngine;
using System.Collections;

public class ToggleScript : MonoBehaviour {
    public bool toggleSize;
    SpriteRenderer sr;

	// Use this for initialization
	void Start () {
        toggleSize = false;
        sr = gameObject.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (toggleSize)
        {

            sr.color = Color.red;
        }
        else
        {
            sr.color = Color.white;
        }
	
	}

    void OnMouseDown()
    {
        toggleSize = !toggleSize;
    }
}
