#include <iostream>
#include "windows.h"
using namespace std;

void printArray(const int [][3]); //! function prototype
const int rows = 2;
const int colums = 3;

int main()
{
    //! define the 2D array
    int array1[rows][colums] = {{1,2,3},{4,5,6}};
    int array2[rows][colums] = {1,2,3,4,5};
    int array3[rows][colums] = {{1,2},{4}};
    cout << "Value in Array 1 by row are:" << endl;
    printArray(array1);

    cout << "Value in Array 2 by row are:" << endl;
    printArray(array2);

    cout << "Value in Array 3 by row are:" << endl;
    printArray(array3);

    system("PAUSE");
    return 0;
}

void printArray(const int a[][colums])
{
    //! loops based on number of rows
    for(int i = 0; i < rows; i++)
    {
        //! loops based on the row
        for(int j = 0; j<colums; j++)
        {
            cout << a[i][j]<<" ";
        }
        cout << endl;
    }
};
