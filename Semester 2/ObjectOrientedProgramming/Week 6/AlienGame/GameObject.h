//! function header & class declaration here
//! super class

class GameObject
{
    //!attribute or properties
    int x, y, xOld, yOld, row, col, speedX, speedY, color;

    //! 2D sprite array
    char** sprite;
    //! check visibility
    bool isToBeDestoryed;
    //! timer
    int updateDelayTimer;
    int updateDelayDuration;
    //!constructor
    GameObject();
    //!function

    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();

    void DrawTrail();
    void Draw();

    //! boundry collision detection
    bool CheckOverBoundary();

    virtual void Update(int elapsedTime)= 0;
};
