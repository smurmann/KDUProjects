﻿using UnityEngine;
using System.Collections;

public class AtomScript : MonoBehaviour {

	private float m_smileyHalfW;
	private float m_smileyHalfH;

	float dirX = 0.1f;
	float dirY = 0.1f;

	private Vector3 target;
	private Vector3 lastpos;
	private Vector3 markerPos;

	Vector3 smileyPos;
	private SpriteRenderer m_spriteRenderer;

	public bool activeAtom = false;

	public Vector3 originalPos;
	public GameObject marker;

	Vector3 direction;

	bool finddirection;

	public float speed;

	public GameObject Atom;

	public float timer = 0.2f;
	float currenttime;

	bool spawned = false;



	CircleCollider2D collider2D;



	// Use this for initialization
	void Start () {
		originalPos = transform.position;

		activeAtom = GameObject.Find("Switch").GetComponent<SwitchScript>().activeAtoms;

		finddirection = true;

		GameObject.FindGameObjectWithTag("AtomCounter").GetComponent<AtomCounterScript>().atomCounter++;



	


	
	}
	
	// Update is called once per frame
	void Update () {


		m_spriteRenderer = gameObject.renderer as SpriteRenderer;
		Sprite mySprite = m_spriteRenderer.sprite;
		m_smileyHalfW = mySprite.bounds.size.x * 0.5f;
		m_smileyHalfH = mySprite.bounds.size.y * 0.5f;

		smileyPos = transform.position;
		markerPos = GameObject.FindGameObjectWithTag("Marker").GetComponent<MarkerScript>().MarketPos;;
		
		Vector3 WorldSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0.0f));
		float halfWidth = WorldSize.x;
		float halfHeight = WorldSize.y;

		target = Camera.main.ScreenToWorldPoint(markerPos);
		target.z = transform.position.z;
		lastpos = transform.position;









//		direction.x += dirX;
//		direction.y += dirY;


		//if (transform.position.x >= halfWidth - m_smileyHalfW)
		if (transform.position.x >= halfWidth)
		{
			//set speed negative
			direction.x = -direction.x;
			dirX = -dirX;
			Duplicate();
			//dirXbool = false;		
		}
		
		//if (transform.position.y >= halfHeight - m_smileyHalfH)
		if (transform.position.y >= halfHeight)
		{
			//set speed negative 
			//dirYBool = false;
			direction.y = -direction.y;
			dirY = -dirY;
			Duplicate();
		}
		
		//if (transform.position.y <= -halfHeight + m_smileyHalfH)
		if (transform.position.y <= -halfHeight)
		{
			//dirYBool = true;
			direction.y = -direction.y;
			dirY = -dirY;
			Duplicate();
		}
		//if (transform.position.x <= -halfWidth + m_smileyHalfW)
		if (transform.position.x <= -halfWidth)
		{
			//dirXbool = true;
			direction.x = -direction.x;
			dirX = -dirX;
			Duplicate();
		}

		if(activeAtom)
		{


			if(finddirection)
			{
				finddirection = false;
				direction = (markerPos*10 - smileyPos).normalized;
			}


			transform.position += direction *speed * Time.deltaTime;
			//transform.Translate(direction * Time.deltaTime);

		}

		currenttime += Time.deltaTime;

		if (spawned)
		{
			if (currenttime >= timer)
			{
				Instantiate(Atom, new Vector3((transform.position.x),transform.position.y, transform.position.z), Quaternion.identity);
				currenttime = 0.0f;
				spawned = false;
			}

		}


	
	}
	public void Duplicate()
	{


		GameObject cell;
		cell = gameObject;
		collider2D = cell.GetComponent<CircleCollider2D>();

		if(cell.transform.localScale.x >= 0.015625f*2)
		{
			float scaleFactor = 0.7f;
			//cell.transform.localScale = Vector3.one * scaleFactor;
			cell.transform.localScale = cell.transform.localScale * scaleFactor;
			collider2D.radius = collider2D.radius * (scaleFactor + (scaleFactor/1));

			currenttime = 0.0f;

			spawned = true;


		}




	}

	public void Destroy()
	{
		Destroy(this.gameObject);
	}
}
