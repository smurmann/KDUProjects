#include "week1.h"

int Week1()
{
	osg::ref_ptr<osg::Node> root = osgDB::readNodeFile("../media/robot.osg");
	osgViewer::Viewer viewer;
	viewer.setSceneData(root.get());
	//viewer.setSceneData(osgDB::readNodeFile("../media/cessna.osg"));
	return viewer.run();
}