﻿using UnityEngine;
using System.Collections;

public class boatController : MonoBehaviour {
	
	public float speed = 0.1f;
	public float negativespeed = -0.1f;
	public float speedboat = 0.1f;
	public float negativespeedboat = -0.1f;

	float dirX = 0.1f;
	float dirY = 0.1f;
	float dirXStickMan = 0.1f;
	//float dirYStickMan = 0.1f;
	
	private float m_smileyHalfW;
	private float m_smileyHalfH;
	private float m_stickManHalfH;
	private float m_stickManHalfW;
	private GameObject m_stickman;
	
	
	private SpriteRenderer m_spriteRenderer;
	private Sprite mySprite;
	private SpriteRenderer m_spriteBoatRenderer;
	private Sprite StickManSprite;

	
	
	// Use this for initialization
	void Start () {
		m_stickman = transform.FindChild ("stickman_sad").gameObject;
		
		m_spriteRenderer = gameObject.renderer as SpriteRenderer;
		mySprite = m_spriteRenderer.sprite;
		m_spriteBoatRenderer = m_stickman.renderer as SpriteRenderer;
		StickManSprite = m_spriteBoatRenderer.sprite;

		m_smileyHalfW = mySprite.bounds.size.x * 0.5f;
		m_smileyHalfH = mySprite.bounds.size.y * 0.5f;


		m_stickManHalfW = StickManSprite.bounds.size.x * 0.5f;
		m_stickManHalfH = StickManSprite.bounds.size.y * 0.5f; 
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 WorldSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0.0f));
		float halfWidth = WorldSize.x;
		float halfHeight = WorldSize.y;

		float halfWidthBoat = mySprite.bounds.size.x;
		float halfHeightBoat = mySprite.bounds.size.y;
		
		
		//Debug.Log ("size :" + halfWidth + ", " + halfHeight);
		
		
		Vector3 sadmanPosition = m_stickman.transform.localPosition;
		Vector3 myPosition = transform.position;
		sadmanPosition.x += dirXStickMan;
		//sadmanPosition.y = 0.0f;
		myPosition.x += dirX;
		//myPosition.y += dirY;
		
		Debug.Log ("Pos x :" + sadmanPosition.x + ", Pos y :" + sadmanPosition.y);
		
		
		if (myPosition.x >= halfWidth - m_smileyHalfW)
		{
			//set speed negative
			dirX = negativespeed;
			myPosition.x += dirX;
		}
		
		if (myPosition.y >= halfHeight - m_smileyHalfH)
		{
			//set speed negative 
			dirY = negativespeed;
			myPosition.y += dirY;
		}
		
		if (myPosition.y <= -halfHeight + m_smileyHalfH)
		{
			dirY = speed;
			myPosition.y += dirY;
		}
		if (myPosition.x <= -halfWidth + m_smileyHalfW)
		{
			dirX = speed;
			myPosition.x += dirX;
		}

		//sadman

		if (sadmanPosition.x >= halfWidthBoat - m_stickManHalfW)
		{
			//set speed negative
			dirXStickMan = negativespeedboat;
			sadmanPosition.x += dirXStickMan;
		}
		
		if (sadmanPosition.y >= halfHeightBoat - m_stickManHalfH)
		{
			//set speed negative 
			//dirY = negativespeed;
			//sadmanPosition.y += dirY;
		}
		
		if (sadmanPosition.y <= -halfHeightBoat + m_stickManHalfH)
		{
			//dirY = speed;
			//sadmanPosition.y += dirY;
		}
		if (sadmanPosition.x <= -halfWidthBoat + m_stickManHalfW)
		{
			dirXStickMan = speedboat;
			sadmanPosition.x += dirXStickMan;
		}
		
		

		m_stickman.transform.localPosition = sadmanPosition;
		transform.position = myPosition;

	}
}
