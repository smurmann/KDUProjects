#ifndef _TRIANGLE_DEMO_H
#define _TRIANGLE_DEMO_H

#include "demo_base.h"

class TriangleDemo : public DemoBase
{
public:
	float m_rotation;
	Vector m_translation;
public:
	void init()
	{
		m_rotation = 0.0f;
		m_translation = Vector(-0.5f, -0.5f, -0.5f);
	}

	void deinit()
	{

	}

	void drawPyramid()
	{
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glEnd();



	}

	void drawCone(float height, float radius,  GLfloat red, GLfloat green, GLfloat blue)
	{
		glColor3f(red,green,blue);
		for (int i = 1; i<360; i++)
		{
			glVertex3f(0, height, 0);
			for (float i = 0; i< 360; i++)
			{
				glVertex3f(radius*cos(i), 0, radius*sin(i));
			}
		}
		glEnd();
	}

	void drawCylinder(float height, float radius, GLfloat red, GLfloat green, GLfloat blue)
	{
		glColor3f(red, green, blue);

		glBegin(GL_TRIANGLE_FAN);
		for (int i = 1; i<360; i++)
		{
			glVertex3f(0, height, 0);
			glVertex3f(0, 0, 0);
			for (float i = 0; i< 360; i++)
			{
				glVertex3f(radius*cos(i), height, radius*sin(i));
				glVertex3f(radius*cos(i), 0, radius*sin(i));
			}
		}
		glEnd();
	}

	void drawCube(float cubeSize,GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLES);							// Drawing Using triangles
		glColor3f(red, green, blue);
		glVertex3f(0.0f, 0.0f, 0.0f);				// Top
		glVertex3f(cubeSize, 0.0f, 0.0f);				// Bottom Left
		glVertex3f(cubeSize, cubeSize, 0.0f);				// Bottom Right
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(0.0f, cubeSize, 0.0f);

		glVertex3f(0.0f, cubeSize, 0.0f);
		glVertex3f(0.0f, cubeSize, cubeSize);
		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(0.0f, cubeSize, cubeSize);

		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(cubeSize, 0.0f, 0.0f);
		glVertex3f(cubeSize, 0.0f, cubeSize);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(cubeSize, 0.0f, 0.0f);

		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(cubeSize, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(cubeSize, 0.0f, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(cubeSize, 0.0f, 0.0f);

		glVertex3f(cubeSize, 0.0f, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(0.0f, cubeSize, cubeSize);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);

		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, cubeSize, 0.0f);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(0.0f, cubeSize, 0.0f);
		glVertex3f(0.0f, cubeSize, cubeSize);
		glEnd();
	}

	void draw(const Matrix& viewMatrix)
	{
		//drawaxis
		glLoadMatrixf((GLfloat*)viewMatrix.mVal);
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.3f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.3f, 1.0f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.3f, 0.3f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();

		//Matrix rotate1 = Matrix::makeRotateMatrix(90, Vector(0.0f, 0.0f, 1.0f));
		//Matrix translate1 = Matrix::makeTranslatisonMatrix(2.0f, 0.0f, 0.0f);
		m_rotation += 0.1f;

		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(-1.0f, -1.0f, -1.0f));
		Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);

		// perform model transformation
		Matrix modelMatrix = translate1 * rotate1;

		// convert model space to view space
		Matrix viewSpaceMatrix = viewMatrix;// *modelMatrix;

		// Rectangle
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);

		drawPyramid();
		
		// Finish Drawing the triangle
	}
};


#endif