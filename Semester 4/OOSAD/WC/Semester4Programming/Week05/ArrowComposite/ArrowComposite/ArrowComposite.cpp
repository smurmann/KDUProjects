// ArrowComposite.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Arrow
{
protected:
	double weightArrow;
public:
	Arrow(double arrowCount) :weightArrow(arrowCount)
	{


	}
	virtual void printArrow(int level) = 0;

};

class StandardArrow :public Arrow
{
public:
	StandardArrow(double arrowCount) :Arrow(arrowCount)
	{

	}
	void printArrow(int level)
	{
		for (int i = 0; i < level; i++)
		{
			cout << "\t";
		}
		cout << "Standard Arrow Count : " << weightArrow << endl;

	}

};
class FireArrow :public Arrow
{
public:
	FireArrow(double arrowCount) :Arrow(arrowCount)
	{

	}
	void printArrow(int level)
	{
		for (int i = 0; i < level; i++)
		{
			cout << "\t";
		}
		cout << "Fire Arrow Count : " << weightArrow << endl;

	}

};

class ArrowBundle :public Arrow
{
private:
	vector<Arrow*> children;
public:
	ArrowBundle(double arrowCount) :Arrow(arrowCount)
	{

	}
	void add(Arrow* arw)
	{
		children.push_back(arw);
	}
	void printArrow(int level)
	{
		for (int i = 0; i < level; i++)
		{
			cout << "\t";
		}
		cout << "ArrowCount : " << weightArrow << endl;

		if (!children.empty())
		{
			for (int x = 0; x < level; x++)
			{
				cout << "\t";
			}
			cout << "Arrow inBundle : " << weightArrow<< endl;
			++level;
			for (int i = 0; i < children.size(); i++)
			{
				children[i]->printArrow(level);
			}
		}
	}

};

int _tmain(int argc, _TCHAR* argv[])
{
	ArrowBundle bundle1(100);
	ArrowBundle bundle2(100);
	ArrowBundle bundle3(100);


	FireArrow firearw1(2);
	StandardArrow standard1(2);

	
	bundle1.add(&firearw1);
	bundle1.add(&standard1);
	

	bundle1.printArrow(0);

	system("PAUSE");
	return 0;
}

