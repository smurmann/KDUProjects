﻿using UnityEngine;
using System.Collections;

public class BgScript : MonoBehaviour {
    public GameObject box;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {

        Vector3 mousepos = Input.mousePosition;
        mousepos.z = 0;

        Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(mousepos);
        mouseWorld.z = 1;
        Instantiate(box, mouseWorld, Quaternion.identity);
    }
}
