#define GLFW_INCLUDE_ES2 1
#define GLFW_DLL 1
//#define GLFW_EXPOSE_NATIVE_WIN32 1
//#define GLFW_EXPOSE_NATIVE_EGL 1

#include <GLES2/gl2.h>
#include <EGL/egl.h>

#include <GLFW/glfw3.h>
//#include <GLFW/glfw3native.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include "bitmap.h"

#include "angle_util\Matrix.h"
#include "angle_util\geometry_utils.h"

#include "Sound.h"
#include <fmod.hpp>


#define TEXTURE_COUNT 2
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

GLint GprogramID = -1;
GLuint GTextureID[TEXTURE_COUNT];

GLFWwindow* window;

FMOD::System* m_fmodSystem;
FMOD::Sound* m_music;
FMOD::Channel *m_musicChannel;

FmodTest test;


Matrix4 gPerspectiveMatrix;
Matrix4 gViewMatrix;



void loadTexture(const char* path, GLuint textureID)
{
	CBitmap bitmap(path);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.GetWidth(), bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.GetBits());

}

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

GLuint LoadShader(GLenum type, const char *shaderSrc)
{
	GLuint shader;
	GLint compiled;

	// Create the shader object
	shader = glCreateShader(type);

	if (shader == 0)
		return 0;

	// Load the shader source
	glShaderSource(shader, 1, &shaderSrc, NULL);

	// Compile the shader
	glCompileShader(shader);

	// Check the compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled)
	{
		GLint infoLen = 0;

		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			// char* infoLog = malloc (sizeof(char) * infoLen );
			char infoLog[512];
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			printf("Error compiling shader:\n%s\n", infoLog);

			// free ( infoLog );
		}

		glDeleteShader(shader);
		return 0;
	}

	return shader;

}

GLuint LoadShaderFromFile(GLenum shaderType, std::string path)
{
	GLuint shaderID = 0;
	std::string shaderString;
	std::ifstream sourceFile(path.c_str());

	if (sourceFile)
	{
		shaderString.assign((std::istreambuf_iterator<char>(sourceFile)), std::istreambuf_iterator<char>());
		const GLchar* shaderSource = shaderString.c_str();

		return LoadShader(shaderType, shaderSource);
	}
	else
		printf("Unable to open file %s\n", path.c_str());

	return shaderID;
}

int Init(void)
{




	FMOD_RESULT result;
	unsigned int version;

	result = FMOD::System_Create(&m_fmodSystem);
	
	result = m_fmodSystem->getVersion(&version);

	if (version < FMOD_VERSION)
	{
		printf("STUFF");
	}

	result = m_fmodSystem->init(32, FMOD_INIT_NORMAL, 0);

	result = m_fmodSystem->createStream("../media/track.mp3", FMOD_SOFTWARE, 0, &m_music);

	result = m_fmodSystem->playSound(FMOD_CHANNEL_FREE, m_music, false, &m_musicChannel);

	//UserData *userData = esContext->userData;
   /* char vShaderStr[] =
	   "attribute vec4 vPosition;    \n"
	   "void main()                  \n"
	   "{                            \n"
	   "   gl_Position = vPosition;  \n"
	   "}                            \n";

	char fShaderStr[] =
	   "precision mediump float;\n"\
	   "void main()                                  \n"
	   "{                                            \n"
	   "  gl_FragColor = vec4 ( 1.0, 1.0, 0.0, 1.0 );\n"
	   "}                                            \n";*/

	


	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint programObject;
	GLint linked;

	glGenTextures(TEXTURE_COUNT, GTextureID);
	loadTexture("../media/rocks.bmp", GTextureID[0]);
	loadTexture("../media/glass.bmp", GTextureID[1]);

	// Load From file
	vertexShader = LoadShaderFromFile(GL_VERTEX_SHADER, "../vertextShader1.vert");
	fragmentShader = LoadShaderFromFile(GL_FRAGMENT_SHADER, "../fragmentShader1.frag");

	// Load the vertex/fragment shaders
	//vertexShader = LoadShader ( GL_VERTEX_SHADER, vShaderStr );
	//fragmentShader = LoadShader ( GL_FRAGMENT_SHADER, fShaderStr );

	// Create the program object
	programObject = glCreateProgram();

	if (programObject == 0)
		return 0;

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// Bind vPosition to attribute 0   
	glBindAttribLocation(programObject, 0, "vPosition");
	glBindAttribLocation(programObject, 1, "vColor");
	glBindAttribLocation(programObject, 2, "vTexCoord");

	// Link the program
	glLinkProgram(programObject);

	// Check the link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		GLint infoLen = 0;

		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			//char* infoLog = malloc (sizeof(char) * infoLen );
			char infoLog[512];
			glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
			printf("Error linking program:\n%s\n", infoLog);

			//free ( infoLog );
		}

		glDeleteProgram(programObject);
		return 0;
	}

	// Store the program object
	GprogramID = programObject;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	gPerspectiveMatrix = Matrix4::perspective(60.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.5f, 30.0f);
	gViewMatrix = Matrix4::translate(Vector3(0.0f, 0.0f, -2.0f));



	return 1;
}

float rf(float temp, float factor1)
{
	return sin(temp + factor1)*0.5 - 0.5;
}

void UpdateFmod(void)
{
	m_fmodSystem->update();
	
	m_musicChannel->getSpectrum(test.m_spectrumLeft, SPECTRUM_SIZE, 0, FMOD_DSP_FFT_WINDOW_RECT);
	m_musicChannel->getSpectrum(test.m_spectrumRight, SPECTRUM_SIZE, 1, FMOD_DSP_FFT_WINDOW_RECT);

	printf("data : %f, %f, %f\n", test.m_spectrumRight[0], test.m_spectrumRight[1], test.m_spectrumRight[60]);
}


void DrawSquare(void)
{
	GLfloat vVertices[] = { 0.5f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.0f };

	static GLfloat vColours[] = { 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f
	};

	GLfloat vTexCoords[] =
	{
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	glBindTexture(GL_TEXTURE_2D, GTextureID[1]);
	/*
	GLfloat vVertices[] = {0.0f,  1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f,  0.0f};*/



	// Use the program object
	glUseProgram(GprogramID);

	// Load the vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vVertices);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, vColours);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, vTexCoords);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

}

void UpdateCamera(void)
{
	static float yaw = 0.0f;
	static float pitch = 0.0f;
	static float distance = 3.0f;

	if (glfwGetKey(window, 'A')) pitch -= 1.0f;
	if (glfwGetKey(window, 'D')) pitch += 1.0f;
	if (glfwGetKey(window, 'W')) yaw -= 1.0f;
	if (glfwGetKey(window, 'S')) yaw -= 1.0f;

	if (glfwGetKey(window, 'R'))
	{
		distance -= 0.06f;
		if (distance < 1.0f)
			distance = 1.0f;
	}
	if (glfwGetKey(window, 'F')) distance += 0.06f;

	gViewMatrix = Matrix4::translate(Vector3(0.0f, 0.0f, -distance)) *
		Matrix4::rotate(yaw, Vector3(1.0f, 0.0f, 0.0f)) *
		Matrix4::rotate(pitch, Vector3(0.0f, 1.0f, 0.0f));
}



void Draw(void)
{
	// Set the viewport
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	// Clear the color buffer
	glClear(GL_COLOR_BUFFER_BIT);


	glUniform1i(glGetUniformLocation(GprogramID, "sampler2d"), 0);

	UpdateFmod();
	static float factor1 = 0.0f;
	static float factor2 = 0.0f;
	factor1 += 0.01f;
	//factor2 += abs(sin(1 * 0.05));
	factor2 += sin(1 * 0.05);
	UpdateCamera();

	//factor1 += abs(cos(12))/100;
	//factor2 += abs(sin(20))/100;// (rand() % 1 + 100) / 100));

	GLint factor1Loc = glGetUniformLocation(GprogramID, "Factor1");
	GLint factor2Loc = glGetUniformLocation(GprogramID, "Factor2");



	if (factor1Loc != -1)
	{
		glUniform1f(factor1Loc, factor1);
		glUniform1f(factor2Loc, factor2);
	}


	static float modelRotation = 0.0f;
	modelRotation += 0.5f;
	if (modelRotation > 360.0f)
		modelRotation -= 360.0f;


	Matrix4 modelMartix, mvpMatrix;
	modelMartix = Matrix4::translate(Vector3(-1.0f, 0.0f, 0.0f)) * Matrix4::rotate(-modelRotation, Vector3(0.0f, 1.0f, 0.0f));
	mvpMatrix = gPerspectiveMatrix * gViewMatrix * modelMartix;
	glUniformMatrix4fv(glGetUniformLocation(GprogramID, "uMvpMatrix"), 1, GL_FALSE, mvpMatrix.data);
	DrawSquare();


	modelMartix = Matrix4::translate(Vector3(1.0f, 0.0f, 0.0f)) * Matrix4::rotate(-modelRotation, Vector3(1.0f, 0.0f, 0.0f));
	mvpMatrix = gPerspectiveMatrix * gViewMatrix * modelMartix;
	glUniformMatrix4fv(glGetUniformLocation(GprogramID, "uMvpMatrix"), 1, GL_FALSE, mvpMatrix.data);
	DrawSquare();
}




int main(void)
{
	glfwSetErrorCallback(error_callback);

	// Initialize GLFW library
	if (!glfwInit())
		return -1;

	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// Create and open a window
	window = glfwCreateWindow(WINDOW_WIDTH,
		WINDOW_HEIGHT,
		"Hello World",
		NULL,
		NULL);

	if (!window)
	{
		glfwTerminate();
		printf("glfwCreateWindow Error\n");
		exit(1);
	}

	glfwMakeContextCurrent(window);

	Init();

	// Repeat
	while (!glfwWindowShouldClose(window)) {


		Draw();
 		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
