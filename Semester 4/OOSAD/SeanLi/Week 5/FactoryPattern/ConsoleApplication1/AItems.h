#pragma once
#include <string>
using namespace std;

class AItems
{
	string name;

public:
	AItems(string n) : name(n)
	{

	}
	string GetName()
	{
		return name;
	}
};

class CellPhone : public AItems
{
public:
	CellPhone() : AItems("CellPhone"){}
};

class Car : public AItems
{
public:
	Car() : AItems("Car"){}
};

class Computer : public AItems
{
public:
	Computer() : AItems("Computer"){}
};

class Laptop: public AItems
{
public:
	Laptop() : AItems("Laptop"){}
};

class Salary : public AItems
{
public:
	Salary() : AItems("Salary"){}
};

class Allowance : public AItems
{
public:
	Allowance() : AItems("Allowance"){}
};