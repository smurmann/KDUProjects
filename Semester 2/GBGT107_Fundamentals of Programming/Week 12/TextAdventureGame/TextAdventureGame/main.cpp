#include <iostream>
#include "windows.h"
#include "conio_yp.h"
#include <cstdlib>
#include <ctime>

using namespace std;

//! global variable
const int mapRow = 27;
const int mapCol = 36;
char map[mapRow][mapCol];
bool isExit = false;
int mapOffSetX = 4;
int mapOffSetY = 3;
int startPosX = 5;
int startPosY = 5;

#define tree 5
#define wall 64
#define trap 84
#define enemy01 69
#define item 43
#define home 72

//! enumeration
enum TYPE
{
    WARRIOR= 0,
    ARCHER,
    MAGE,
    TOTAL
};

struct Hero
{
    TYPE type;
    char avatar;
    int row;
    int column;
    int rowOld;
    int columnOld;
    int health;
    int attack;

    void Initialize();
};

void Hero::Initialize()
{
    //type = v;
    if(type == WARRIOR)
    {
        avatar = 'X';
        row = 5;
        column = 5;
        sshealth = 100;
        attack = 35;
    }
    else if(type == ARCHER)
    {
        avatar = 'Q';
        row = 5;
        column = 5;
        health = 80;
        attack = 30;
    }
    else if(type == MAGE)
    {
        avatar = 'Z';
        row = 5;
        column = 5;
        health = 50;
        attack = 50;
    }

}

Hero player;

struct Enemy
{
    TYPE type;
    char avatar;
    int row;
    int column;
    int rowOld;
    int columnOld;
    int health;
    int attack;

    void Initialize();
};
void Enemy::Initialize()
{
    type = (TYPE)(rand()%TOTAL);
    if(type == WARRIOR)
    {

    }
    else if(type == ARCHER)
    {

    }
    else if(type == MAGE)
    {

    }
}

Enemy theEnemy;
void InitializeMap()
{
    char tempMap[mapRow][mapCol] =
    {
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0',item,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0',trap,'0','0','0','0','0','0','0','0','0',trap,'0','0','0','0','0',item,'0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0',trap,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',item,'0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0',trap,'0','0','0','0','0','0',trap,'0',trap,'0','0',item,'0','0','0',item,'0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0',enemy01,'0','0','0',trap,'0','0','0','0','0','0','0','0','0','0','0','0','0',item,'0','0','0','0',item,'0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0',enemy01,'0',trap,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#',trap,trap,trap,trap,trap,trap,'0','0','0','0','0','0','0','0','0','0',item,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',enemy01,enemy01,enemy01,enemy01,enemy01,enemy01,enemy01,enemy01,enemy01,enemy01,'0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0',item,item,item,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0',item,item,item,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0',item,item,item,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0',enemy01,'0','0',enemy01,'0','0','0','0',enemy01,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0',enemy01,'0',enemy01,'0','0','0',enemy01,'0','0','0','0',enemy01,'0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',enemy01,'0','0',enemy01,'0','0','0','0',enemy01,'0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0',enemy01,'0','0','0','0',enemy01,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',trap,'0',trap,trap,trap,trap,trap,trap,'#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',trap,'0','0','0','0','0','0','0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',trap,'0','0','0','0',home,'0','#'},
        {'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',trap,trap,'0','0','0','0','#'},
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}
    };
    for (int i=0; i<mapRow; i++)
    {
        for(int j=0; j<mapCol; j++)
            {
                map[i][j] = tempMap[i][j];
            }
    }

}

void ShowMap()
{
    //int mapOffSetX = 4;
    //int mapOffSetY = 3;

    for(int i=0; i<mapRow; i++)
    {
        for(int j=0; j<mapCol; j++)
        {
            gotoxy(mapOffSetX + j, mapOffSetY + i);
            if(map[i][j] == tree)
            {
                textcolor(GREEN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == trap)
            {
                textcolor(LIGHTRED);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == '#')
            {
                textcolor(YELLOW);
                cout << map[i][j];
                textcolor(WHITE);
            }
            /*else if (map[i][j] == player)
            {
                textcolor(LIGHTCYAN);
                cout << map[i][j];
                textcolor(WHITE);
            }*/
            else if (map[i][j] == enemy01)
            {
                textcolor(DARKGRAY);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == trap)
            {
                textcolor(RED);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == item)
            {
                textcolor(LIGHTGREEN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else if (map[i][j] == home)
            {
                textcolor(LIGHTCYAN);
                cout << map[i][j];
                textcolor(WHITE);
            }
            else
            {
                textcolor(WHITE);
                cout << map[i][j];
                textcolor(WHITE);
            }
        }
        cout << endl;
    }
    //! draw player
    gotoxy(mapOffSetX + player.column,
           mapOffSetY + player.row);
    textcolor(GREEN);
    cout << player.avatar;
    textcolor(WHITE);
}

void ShowPlayerInfo()
{
    gotoxy(42,3);
    cout << "Player Information";
    gotoxy(42,4);
    cout << "==================";

    gotoxy(42,5);
    cout << "Health : " << player.health;
    gotoxy(42,6);
    cout << "Attack : " << player.attack;

    gotoxy(55,6);
    cout << "  ___    __    __  __  ____ /\ p";
    gotoxy(55,7);
    cout << " / __)  /__\  (  \/  )( ___))( ";
    gotoxy(55,8);
    cout << "( (_-. /(__)\  )    (  )__) \/ ";
    gotoxy(55,9);
    cout << " \___/(__)(__)(_/\/\_)(____)() ";
}

void UpdateInput()
{
    //! store the old position before the input
    player.rowOld = player.row;
    player.columnOld = player.column;

    if(GetAsyncKeyState(VK_LEFT))
    {
        if(player.column != 1)
        {
                player.column--;
        }

    }
    else if(GetAsyncKeyState(VK_RIGHT))
    {
        if(player.column != mapCol - 2)
        {
                player.column++;
        }

    }
    else if(GetAsyncKeyState(VK_UP))
    {
        if(player.row != 1)
        {
            player.row--;
        }
    }
    else if(GetAsyncKeyState(VK_DOWN))
    {
        if(player.row != mapRow - 2)
        {
                player.row++;
        }

    }
    else if(GetAsyncKeyState(VK_ESCAPE))
    {
        isExit = true;
    }
}

void BattleMenu()
{
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
    cout << "BATTLE MENU";

    gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
    cout << "===========";

    gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
    cout << "1. FIGHT";

    gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
    cout << "2. RUN";

battleselection:
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
    cout << "Your Choice : ";

    float choice;
    cin >> choice;

        if(choice == 1)
        {
            //! call battle system
        }
        else if(choice == 2)
        {
            player.row = player.rowOld;
            player.column = player.columnOld;
        }
        else if(choice != 1 || choice != 2)
        {
            goto battleselection;
        }

}

void ClearMenuArea()
{
    for(int i=0; i<10; i++)
    {
        gotoxy(mapOffSetX,
                mapOffSetY + mapRow + i + 1);
               cout <<"                                 ";
    }
}
void ShowEnemyInfo()
{
    gotoxy(42,23);
    cout << "Enemy Information";
    gotoxy(42,24);
    cout << "=================";
    gotoxy(42,25);
    cout << "Health : " << theEnemy.health;
    gotoxy(42,26);
    cout << "Attack : " << theEnemy.attack;
}

void BattleSimulation()
{
    bool isBattleEnded = false;
    do
    {
        ClearMenuArea();
        ShowPlayerInfo();
        ShowEnemyInfo();
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
        cout << "BATTLE SIMULATION";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
        cout << "=================";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "1. ATTACK";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
        cout << "2. EVADE";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
        cout << "3. FLEE";
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 6);
        cout << "Your Choice : ";
        int choice;
        cin >> choice;
        if (choice == 1)
        {
            theEnemy.health -= player.attack;
            gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
            cout << "You Inflict " << player.attack << " damage to the enemy";
            if(theEnemy.health<= 0)
            {
                isBattleEnded = true;
            }
            else
            {
                player.health -= theEnemy.attack;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
                cout << "Enemy inflict " << theEnemy.attack<<" damage to you";
            }
        }
        else if (choice == 2)
        {
            if(rand()%2 == 0)
            {
                player.health -= theEnemy.attack;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
                cout << "Enemy inflict " << theEnemy.attack<<" damage to you";

            }
            else
            {
                player.health += 5;
                gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
                cout << "Evade Successful, some health gained";
            }

        }
        else if (choice == 3)
        {
            player.row = player.rowOld;
            player.column = player.columnOld;
            isBattleEnded = true;
        }
    }while(!isBattleEnded);


//! result screen

    ClearMenuArea();
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
    cout << "BATTLE RESULT";
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
    cout << "=============";
    if(player.health > 0)
    {
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "You Win";
    }
    else
    {
        gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
        cout << "you were beaten, the village saved you and has sent you back home!";
        player.row = startPosX;
        player.column = startPosY;
        player.health = 100;
    }
    map[player.row][player.column] = '0';
    gotoxy(mapOffSetX, mapOffSetY + mapRow + 13);
    system("PAUSE");
}

void CollisionDection()
{
    if(map[player.row][player.column]==(char)enemy01)
    {
        BattleMenu();
    }
    if(map[player.row][player.column]==(char)tree)
    {
        player.row = player.rowOld;
        player.column = player.columnOld;
    }
}


int main(int argc, char** argv)
{
    system("mode con: cols=100lines=70");
    textcolor(WHITE);
    srand(time(NULL));
    Enemy.Initialize();
    InitializeMap();
    Hero.Initialize(MAGE);


    do
    {
        system("CLS");
        UpdateInput();
        ShowMap();
        ShowPlayerInfo();
        BattleMenu();
        gotoxy(mapOffSetX, mapOffSetY + mapRow);
        system("PAUSE");
    }while(!isExit);
    system("PAUSE");
    cout << "Hello world!" << endl;
    return 0;
}
