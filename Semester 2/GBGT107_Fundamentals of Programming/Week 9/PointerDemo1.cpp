#include "windows.h"
#include <iostream>

using namespace std;


int a = 7;

int * aPointer = &a;


int main()
{
    cout << "The address of a is: " << &a << endl;
    cout << "The value of aPtr is: " << aPointer << endl;
    cout << "The value of a:" << a << endl;
    cout << "The value of *aPointer: " << *aPointer << endl;
    cout << "The address of *aPointer: " << &aPointer << endl;

    return 0;
}
