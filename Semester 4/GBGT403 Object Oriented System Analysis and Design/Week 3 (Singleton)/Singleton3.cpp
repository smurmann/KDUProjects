#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

class Singleton
{
private:
	static bool instanceFlag;
	static Singleton *single;
	Singleton()
	{
		//private constructor
	}
public:
	static Singleton* getInstance();
	void method();
	~Singleton()
	{
		instanceFlag = false;
	}
};

bool Singleton::instanceFlag = false;
Singleton* Singleton::single = NULL;
Singleton* Singleton::getInstance()
{
	if(instanceFlag)
	{
		single = new Singleton();
		instanceFlag = true;
		return single;
	}

	
}
void Singleton::method()
{
	cout << "Methods of the singleton class" << endl;
}


int _tmain(int argc, _TCHAR* argv[])
{
	Singleton *s1, *s2;
	s1 = Singleton::getInstance();
	s1->method();
	s2 = Singleton::getInstance();
	s2->method();
	system("PAUSE");
	return 0;
}