template < typename T >

T maximum(T value1, T value2, T value3)
{
    //! assume the value1 is maximum
    T maximumValue = value1;

    //! determine wether value is greater than maxValue
    if(value2 > maximumValue)
    {
        maximumValue = value2;
    }
    if(value3 > maximumValue)
    {
        maximumValue = value3;
    }
    return maximumValue;
}
