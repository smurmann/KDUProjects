// VisitorDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Node.h"
#include <Windows.h>
#include <iostream>

using namespace std;

class PrintHierarchyVisitor : public ActorVisitor
{
	void visit(Actor* actor)
	{
		Node* parent = actor->mOwner;
		int indentCount = 0;
		while (parent)
		{
			++indentCount;
			parent = parent->getParent();

		}
		for (int i = 0; i < indentCount; i++)
		{
			cout << " ";
			
		}
		cout << actor->mName << endl;
	}

	void GetRoot()
	{

	}
	void GetTotalElement()
	{

	}

};
class PrintVertHieracrhy : public ActorVisitor
{
	void visit(Actor* actor)
	{

	}
};


int _tmain(int argc, _TCHAR* argv[])
{
	//! x(ROOT)
	//! - A (Dog)
	//! x (Child)
	//! - A(Bone)
	//! - A(Hat)
	//! X (Child2)
	//! - A (Cat)

	Node root, child, child2, child3, child4;
	Actor dog("Dog"), bone("Bone"), hat("hat"), cat("cat"), fish("fish"), wolf("wolf");

	root.appendActor(&dog);
	root.addChild(&child);
	root.addChild(&child2);

	child.appendActor(&bone);
	child.appendActor(&hat);

	child2.appendActor(&cat);
	child2.addChild(&child3);
	child2.addChild(&child4);
	child3.appendActor(&fish);
	child4.appendActor(&wolf);

	PrintHierarchyVisitor visitor;

	system("PAUSE");
	return 0;
}

