﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour {

	private PlayerController m_playerScript;

	// Use this for initialization
	void Start () {
		GameObject player = GameObject.Find("/player");
		m_playerScript = (PlayerController) player.GetComponent(typeof(PlayerController));
	}
	
	// Update is called once per frame
	void Update () {
	
		float speed = m_playerScript.m_speed * Time.deltaTime;
		
		Vector3 position = transform.position;
		position.x -= speed;
		
		transform.position = position;
		
		if (transform.position.x < -8.0f)
			Destroy(this.gameObject);
	}
}
