#include <iostream>

using namespace std;

//! create the base class / super class
class Enemy
{
public:
    int mDamage;
    Enemy();
    void Attack();
};
//!constructor
Enemy::Enemy() :
    mDamage(10)
    {}

void Enemy::Attack()
{
    cout<<"Attack inflicts " <<mDamage<< " damage points!"<<endl;
}
//! create a specialzed / unique class named Boss
//!Sub class or derived class which inherit from the super class
class Boss : public Enemy
{
public:
    //!specialized attribute for the sub class
    //!noneed to redeclare the common attribute, which was already
    //! declared in the super classs
    int mDamageMultiplier;

    //!Constructor -> creat object and assign the default value
    //! to the attributes
    Boss();
    void SpecialAttack();

};

Boss::Boss():
    mDamageMultiplier(3)
{}
void Boss::SpecialAttack()
{
    cout<<"Special Attack inflicts "<<(mDamageMultiplier*mDamage);
    cout<<" damage points!"<<endl;
}

int main()
{
    //! creat the enemey object
    cout<<"Creatingan Enemy"<< endl;
    Enemy enemy1;
    enemy1.Attack();

    cout << "Creating a Boss" << endl;
    Boss boss1;
    boss1.Attack();
    boss1.SpecialAttack();

    return 0;
}
