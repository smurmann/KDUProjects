﻿using UnityEngine;
using System.Collections;

public class bgObj1Spawner : MonoBehaviour {

	public GameObject m_prefab;

	private PlayerController m_playerScript;
	private float m_distanceSinceLastSpawn;
	private float m_nextSpawnDistance;

	// Use this for initialization
	void Start ()
	{
		GameObject player = GameObject.Find("/player");
		m_playerScript = (PlayerController) player.GetComponent(typeof(PlayerController));

		m_distanceSinceLastSpawn = 0.0f;
		m_nextSpawnDistance = 0.0f;
	}

	// Update is called once per frame
	void Update ()
	{
		float speed = m_playerScript.m_speed * Time.deltaTime;
		m_distanceSinceLastSpawn += speed;
		if (m_distanceSinceLastSpawn >= m_nextSpawnDistance)
		{
			Vector3 pos = new Vector3(8.0f, Random.value*1.8f-2.6f, 0.0f);
			GameObject newObject = Instantiate(m_prefab, pos, Quaternion.identity) as GameObject;
			newObject.transform.parent = this.transform;
			
			m_nextSpawnDistance = Random.value*4.0f + 3.0f; //same as Random.Range(3.0f, 7.0f);
			m_distanceSinceLastSpawn = 0.0f;
		}
	}
}
