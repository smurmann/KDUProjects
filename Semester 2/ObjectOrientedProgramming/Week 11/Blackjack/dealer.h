#include "random.h"

class Dealer
{
private:
    int mTotal;
    int mInsurance;
public:
    //! Constructor
    Dealer();
    int deal(int);
    int game(int);
    int getTotal();
    void setTotal(int);
    void setInsurance(int);
    int getInsurance();
    //! OOP concept - encapsulation
    //! make the data attribute private
    //! provide set and get method to manipulate the values
};
