﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DiagonalToggle : MonoBehaviour {

	// Use this for initialization
    void Start()
    {
        UpdateText();
    }

	public void UpdateText()
    {
        GetComponent<Text>().text = "Diagonal: " +  GameObject.Find("Array").GetComponent<Pathfinding>().diagonal.ToString();
    }
}
