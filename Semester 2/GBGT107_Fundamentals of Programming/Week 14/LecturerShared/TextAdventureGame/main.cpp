#include <iostream>
#include <cstdlib>
#include <ctime>
#include "windows.h"
#include "conio_yp.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;

//! global variable
const int mapRow = 27;
const int mapCol = 36;
char map[mapRow][mapCol];
bool isExit = false;
int mapOffSetX = 4;
int mapOffSetY = 3;
int startPosX;
int startPosY;

#define tree 5
#define wall 64
#define trap 21
#define enemy 128
#define item1 15

//! enumeration
enum TYPE
{
	WARRIOR =0,
	ARCHER,
	MAGE,
	TOTAL	
};

struct Hero
{
	TYPE type;
	char avatar;
	int row;
	int column;
	int rowOld;
	int columnOld;
	int health;
	int attack;
	
	void Initialize(TYPE v);
};

void Hero::Initialize(TYPE v)
{
	type = v;
	if(type == WARRIOR)
	{
		avatar = 'X';
		row = 5;
		column = 5;	
		health = 100;
		attack = 35;
	}
	else if (type == ARCHER)
	{
		avatar = 'Q';
		row = 5;
		column = 5;
		health = 80;
		attack = 30;
	}
	else if (type == MAGE)
	{
		avatar = 'Z';
		row = 5;
		column = 5;
		health = 50;
		attack = 50;
	}
}

Hero player;

struct Enemy
{
	TYPE type;
	char avatar;
	int row;
	int column;
	int rowOld;
	int columnOld;
	int health;
	int attack;
	
	void Initialize();
};
void Enemy::Initialize()
{
	type = (TYPE)(rand()%TOTAL);
	if(type == WARRIOR)
	{
		avatar = 'T';
		row = 5;
		column = 5;
		health = 100;
		attack = 35;
	}
	else if(type == ARCHER)
	{
		avatar = 'Y';
		row = 5;
		column = 5;
		health = 85;
		attack = 30;
	}
	else if(type == MAGE)
	{
		avatar = 'U';
		row = 5;
		column = 5;
		health = 66;
		attack = 40;
	}
}

Enemy theEnemy;

void InitializeMap()
{
	char tempMap[mapRow][mapCol] =
	{
		{'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0',tree,'0',trap,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0',enemy,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0',tree,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0',' ',' ',' ',' ','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0',' ',' ',' ',' ','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','#'},
		{'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}
	};
	for(int i=0; i<mapRow; i++)
	{
		for(int j=0; j<mapCol; j++)
		{
			map[i][j] = tempMap[i][j];
		}
	}
}

void ShowMap()
{
	//int mapOffSetX = 4;
	//int mapOffSetY = 3;
	
	for(int i=0; i<mapRow; i++)
	{
		for(int j=0; j<mapCol; j++)
		{
			gotoxy(mapOffSetX + j, mapOffSetY + i);
			
			if(i<player.row - 5 ||
				i> player.row +5 ||
				j<player.column -5 ||
				j>player.column +5)
				{
					textcolor(GREEN);
					cout<<" ";
					textcolor(WHITE);
					continue;
				}
			
			if(map[i][j] == tree)
			{
				textcolor(GREEN);
				cout<<map[i][j];
				textcolor(WHITE);
			}
			else if (map[i][j] == trap)
			{
				textcolor(LIGHTRED);
				cout<<map[i][j];
				textcolor(WHITE);
			}
			else if (map[i][j] == '#')
			{
				textcolor(YELLOW);
				cout<<map[i][j];
				textcolor(WHITE);
			}
			else if(map[i][j] == (char)enemy)
			{
				textcolor(LIGHTRED);
				cout<<map[i][j];
				textcolor(WHITE);
			}
			else
			{
				textcolor(WHITE);
				cout<<map[i][j];
				textcolor(WHITE);
			}
		}
		cout<<endl;
	}
	//! draw player
	gotoxy(mapOffSetX + player.column, mapOffSetY + player.row);
	textcolor(GREEN);
	cout<<player.avatar;
	textcolor(WHITE);
	
}

void ShowPlayerInfo()
{
	gotoxy(42,3);
	cout<<"Player Information";
	gotoxy(42,4);
	cout<<"==================";
	
	gotoxy(42,5);
	cout<<"Health : "<<player.health;
	gotoxy(42,6);
	cout<<"Attack : "<<player.attack;
	
	gotoxy(55,6);
	cout<<"               ,";
	gotoxy(55,7);
	cout<<"       ,      |\\ ,_____";
	
}

void UpdateInput()
{
	//! store the old position before the input
	player.rowOld = player.row;
	player.columnOld = player.column;
	
	if(GetAsyncKeyState(VK_LEFT))
	{
		if(player.column > 1)
		{
			player.column--;
		}
	}
	else if (GetAsyncKeyState(VK_RIGHT))
	{
		if(player.column < mapCol -2)
		{
			player.column++;
		}
	}
	if(GetAsyncKeyState(VK_UP))
	{
		if(player.row > 1)
		{
			player.row--;	
		}
	}
	else if (GetAsyncKeyState(VK_DOWN))
	{
		if(player.row < mapRow - 2)
		{
			player.row++;
		}
	}
}



void ClearMenuArea()
{
	for(int i=0; i<10; i++)
	{
		gotoxy(mapOffSetX, 
		mapOffSetY + mapRow + i + 1);
		cout<<"                                        ";
	}
}
void ShowEnemyInfo()
{
	gotoxy(42,23);
	cout<<"Enemy Information";
	gotoxy(42,24);
	cout<<"=================";
	gotoxy(42,25);
	cout<<"Health : "<< theEnemy.health;
	gotoxy(42,26);
	cout<<"Attack : "<< theEnemy.attack;
}

void BattleSimulation()
{
	bool isBattleEnded = false;
	do
	{
		ClearMenuArea();
		ShowPlayerInfo();
		ShowEnemyInfo();
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
		cout<<"BATTLE SIMULATION";
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
		cout<<"=================";
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
		cout<<"1. ATTACK";
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
		cout<<"2. EVADE";
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
		cout<<"3. FLEE";
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 6);
		cout<<"Your Choice : ";
		int choice;
		cin>>choice;
		if(choice == 1)
		{
			theEnemy.health -= player.attack;
			gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
			cout<<"You Inflict "<<player.attack<<" damage to the enemy";
			if(theEnemy.health<=0)
			{
				isBattleEnded = true;
			}
			else
			{
				player.health -= theEnemy.attack;
				gotoxy(mapOffSetX, mapOffSetY + mapRow + 9);
				cout<<"Enemy inflict "<<theEnemy.attack<<" damage to you";
			}
		}
		else if(choice == 2)
		{
			if(rand()%2 == 0)
			{
				player.health -= theEnemy.attack;
				gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
				cout<<"Enemy inflict "<<theEnemy.attack<<" damage to you";
			}
			else
			{
				player.health += 5;
				gotoxy(mapOffSetX, mapOffSetY + mapRow + 8);
				cout<<"Evade Successful, some health gained";
			}
		}
		else if(choice == 3)
		{
			player.row = player.rowOld;
			player.column = player.columnOld;
			isBattleEnded = true;
		}
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 13);
		system("PAUSE");
	}while(!isBattleEnded);
	
	//! result screen
	ClearMenuArea();
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
	cout<<"BATTLE RESULT";
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
	cout<<"=============";
	if(player.health>0)
	{
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
		cout<<"You Win";
	}
	else
	{
		gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
		cout<<"you were beaten, the village saved you and send you back home!";
		player.row = startPosX;
		player.column = startPosY;
		player.health = 100;
	}
	map[player.row][player.column] = '0';
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 13);
	system("PAUSE");
}
void BattleMenu()
{
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 1);
	cout<<"BATTLE MENU";	
	
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 2);
	cout<<"===========";
	
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 3);
	cout<<"1. FIGHT";
	
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 4);
	cout<<"2. RUN";
	
	gotoxy(mapOffSetX, mapOffSetY + mapRow + 5);
	cout<<"Your Choice : ";
	
	int choice;
	cin>>choice;
	if(choice == 1)
	{
		//! call battle system
		BattleSimulation();
	}
	else if(choice == 2)
	{
		player.row = player.rowOld;
		player.column = player.columnOld;
	}
}
void CollisionDetection()
{	
	if(map[player.row][player.column]==(char)enemy)
	{
		BattleMenu();
	}	
	if(map[player.row][player.column]==(char)tree)
	{
		player.row = player.rowOld;
		player.column = player.columnOld;
	}
	
}
int main(int argc, char** argv) {
	system("mode con: cols=80 lines=50");
	textcolor(WHITE);
	srand(time(NULL));
	player.Initialize(WARRIOR);
	theEnemy.Initialize();
	InitializeMap();
	do
	{
		system("CLS");
		UpdateInput();
		ShowMap();
		ShowPlayerInfo();
		CollisionDetection();
		gotoxy(mapOffSetX, mapOffSetY + mapRow);
		system("PAUSE");
	}while(!isExit);

	system("PAUSE");
	return 0;
}
