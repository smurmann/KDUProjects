//! concreate Subject class
#ifndef _TRIGGER_H
#define _TRIGGER_H

#include "Subject.h"

class Trigger : public Subject
{
protected:
	//! trigger state
	bool mIsOn;
public:
	Trigger() : mIsOn(false)
	{

	}

	//! Get state of trigger
	inline bool getState()
	{
		return mIsOn;
	}
	//! set the trigget state
	void setState(bool on)
	{
		if (mIsOn == on) return;
		mIsOn = on;
		Notify();
	}
};

#endif