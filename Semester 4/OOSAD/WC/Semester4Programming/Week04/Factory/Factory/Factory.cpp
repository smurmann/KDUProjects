// Factory.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include "PlayerList.h"

 using namespace std;



int _tmain(int argc, _TCHAR* argv[])
{
	int choice;
	bool isExit = false;
	do{
		cout<<"Choose Player :"<<endl;
		cout<<"1. Human"<<endl;
		cout<<"2. Soldier"<<endl;

		cin>>choice;
		cout<<endl;
		if(choice == 1)
		{
			PlayerList *p1 = PlayerList::CreatePlayer(PlayerList::PLAYER::HUMAN);
			cout<<"Human :"<<endl;
			cout<<"Weapon List"<<endl;
			p1->PrintWeapon();
			cout<<endl;
			cout<<"Armor List"<<endl;
			p1->PrintArmor();
			cout<<endl;
		
		}
		else if(choice == 2)
		{

			PlayerList *p1 = PlayerList::CreatePlayer(PlayerList::PLAYER::SOLDIER);
			cout<<"Soldier :"<<endl;
			cout<<"Weapon List"<<endl;
			p1->PrintWeapon();
			cout<<endl;
			cout<<"Armor List"<<endl;
			p1->PrintArmor();
			cout<<endl;
		
		}
		else
		{
			isExit = true;
		}

		system("PAUSE");


	}while(!isExit);
		
	system("PAUSE");
	return 0;
}

