#ifndef CREATORPATTERN_H
#define CREATORPATTERN_H

#include "BuilderWarrior.h"
#include "BuilderEnemy.h"
#include "BuilderMage.h"
#include "BuilderThief.h"

using namespace std;

class CreatorPattern
{
private:
	PlayerBuilder *playerBuilder;
public:
	//create player
	void setPlayerBuilder(PlayerBuilder* pBuilder)
	{
		playerBuilder = pBuilder;
	}

	Player* getPlayer()
	{
		return playerBuilder->getPlayer();
	}

	void createPlayer()
	{
		playerBuilder->createNewPlayer();
		playerBuilder->bStrength();
		playerBuilder->bDexerity();
		playerBuilder->bIntelligence();
		playerBuilder->bHealth();
		playerBuilder->bMana();
		playerBuilder->bDmgPhysical();
		playerBuilder->bDmgMagical();
		playerBuilder->bEvadeChance();
		playerBuilder->bDefense();
		playerBuilder->bExperience();
		playerBuilder->bLevel();
		playerBuilder->bName();
		playerBuilder->bSkillPoints();
	}

	//create enemy
};




#endif 
