﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

    public Sprite idle;
    public Sprite hover;
    public bool mouseDown;

    SpriteRenderer sr;

    public GameObject Credits1;
    public GameObject Credits2;
    public string button;
    float timer = 1.5f;
    float time;

	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = idle;
        mouseDown = false;
	
	}
	
	// Update is called once per frame
	void Update () {
        if(button == "credits")
        {
            if (time > timer)
            {
                Credits2.SetActive(true);
                Credits1.SetActive(false);
            }
            else
            {
                Credits2.SetActive(false);
            }


        }     
	
	}

    void OnMouseOver()
    {        
        sr.sprite = hover;

        if (button == "credits")
        {
            Credits1.SetActive(true);
            time += Time.deltaTime;
                
        }

        if(button == "Tutorial")
        {
            Credits1.SetActive(true);
        }
    }

    void OnMouseExit()
    {
        sr.sprite = idle;
        time = 0.0f;

        if (button == "credits")
        {
            Credits1.SetActive(false);
            Credits2.SetActive(false);
        }

        if(button == "Tutorial")
        {
            Credits1.SetActive(false);
        }
    }
    
    void OnMouseDown()
    {
        mouseDown = true;

        if (button == "DB")
        {
            Application.LoadLevel("DB_Gameplay");
        }
        else if(button == "TW")
        {
            Application.LoadLevel("TW_Gameplay");
        }
        else if(button == "Tutorial")
        {
            Application.LoadLevel("Tutorial");
        }
        else if (button == "Exit")
        {
            Application.Quit();
        }
    }

    void OnMouseUp()
    {
        mouseDown = false;
    }
}
