#include <iostream>
#include "ClassTemplate.h"

using namespace std;

int main()
{
    //! define the myPair object

    mypair<int> myIntObj(118,23);
    mypair<double> myDoubleObject(2.5,4.1);
    mypair<char> myCharObject('a','b');
    myIntObj.Display();
    cout << endl;
    cout << endl;
    myDoubleObject.Display();
    cout << endl;
    cout << endl;
    myCharObject.Display();
    cout << endl;
    cout << endl;
    //cout << "Hello world!" << endl;
    return 0;
}
