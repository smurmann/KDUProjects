#include <iostream>
#include "CardDeck.h"

using namespace std;

int main ()
{
    CardDeck box;
    box.deal();
    cout << endl;
    box.shuffle();
    box.deal();
    return 0;
}
