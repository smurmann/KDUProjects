#include <iostream>
#include "windows.h"
#include "time.h"

using namespace std;

struct Hero
{
    string name;
    int health;
    int attack;

    void initialize(string heroName)
    {
        name = heroName;
        health = rand()%51 + 50;
        attack = rand()%21 + 10;
    }

    void display()
    {
        cout << "Name:\t\t " << name << endl;
        cout << "Health: \t" << health << endl;
        cout << "Attack Damage: \t" << attack << endl;
        cout << endl;
    }
};

int main ()
{
    srand(time(NULL));

    Hero h1, h2, h3;
    h1.initialize("Azri");
    h2.initialize("Sam");
    h3.initialize("TJ");

    h1.display();
    h2.display();
   // h3.display();

        cout << "===================" << endl;
        cout << "LET BATTLE COMMENCE" << endl;
        cout << "===================" << endl << endl;
    bool battleStatus = false;
    do
    {
        h1.health -= h2.attack;
        h2.health -= h1.attack;

        if(h1.health <= 0 || h2.health <= 0)
        {
            battleStatus = true;
            if (h1.health == h2.health)
            {
                cout << "It's a draw!" << endl;
            }
            else if (h1.health < h2.health)
            {
                cout << h2.name << " wins!" << endl;
            }
            else
            {
                cout << h1.name << " wins!" << endl;
            }
        }
    }while (!battleStatus);

        cout << endl << "===================" << endl;
        cout << "BATTLE AFTERMATH!!!" << endl;
        cout << "===================" << endl;
        h1.display();
        h2.display();


}
