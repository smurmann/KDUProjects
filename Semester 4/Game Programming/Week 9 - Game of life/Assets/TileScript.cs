﻿using UnityEngine;
using System.Collections;

public class TileScript : MonoBehaviour {

	public Sprite m_SpriteState_0;
	public Sprite m_SpriteState_1;
	public bool m_state;
    bool toggle;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setState(bool state)
	{
		SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
		if(state == false)
		{
			sr.sprite = m_SpriteState_0;
			m_state = false;
		}
		else if(state == true)
		{
			sr.sprite = m_SpriteState_1;
			m_state = true;
		}

   }

    public bool getState()
    {
        return m_state;
    }


    void OnMouseOver()
    {
        if(Input.GetMouseButton(0))
        {
            toggle = !toggle;
            setState(toggle);
           // Debug.Log("Mousepressed");
        }

    }

    public void Kill()
    {
        Destroy(this.gameObject);
    }



}
