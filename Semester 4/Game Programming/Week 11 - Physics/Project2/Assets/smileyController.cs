﻿using UnityEngine;
using System.Collections;

public class smileyController : MonoBehaviour {

    private Rigidbody2D rb;
    public GameObject door1;
    public GameObject door2;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	
	}
	
	// Update is called once per frame
	void Update () {

        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        rb.angularVelocity += -moveHorizontal * 35.0f;
        if (rb.angularVelocity > 700.0f)
            rb.angularVelocity = 700.0f;
        else if(rb.angularVelocity < -700.0f)
        {
            rb.angularVelocity = -700.0f;
        }

        if(Input.GetButtonDown("Jump"))
        {
            Vector2 direction = new Vector2(0.0f, -1.0f);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 0.25f);
            if(hit == true)
            {
                Debug.Log(hit.distance );
                Vector3 vel = rb.velocity;
                vel.y += 5.0f;
                rb.velocity = vel;
            }
        }
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject == door1)
        {
            transform.position = door2.transform.position;
        }
    }
}
