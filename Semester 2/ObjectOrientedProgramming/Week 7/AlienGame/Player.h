#include "DefaultHeaderFiles.h"

class Player :  public GameObject
{
    public:
    bool enableShooting;
    int score;

    int shootDelayTimer;
    int shootDelayDuration;

    //! Constructor
    Player();

    //! update function prototype
    void Update(int elapsedTime);
};
