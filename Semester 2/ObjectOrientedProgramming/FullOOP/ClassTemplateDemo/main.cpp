#include <iostream>
#include "ClassTemplate.h"

using namespace std;

int main()
{
    //! define the mypair object
    //! mypair myObject
    mypair<int> myIntObj(118,23);
    mypair<double> myDoubleObject(2.5,4.1);
    mypair<char> myCharObj('a','b');
    myIntObj.Display();
    cout<<endl;
    cout<<endl;
    myDoubleObject.Display();
    cout<<endl;
    cout<<endl;
    myCharObj.Display();
    //cout << "Hello world!" << endl;
    return 0;
}
