#include <iostream>
#include <typeinfo>
#include <windows.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <GL/GLU.h>

#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/TrackballManipulator>
class SeanEngine
{
private:

public:
	SeanEngine();
	virtual ~SeanEngine();

	void run(void);
};