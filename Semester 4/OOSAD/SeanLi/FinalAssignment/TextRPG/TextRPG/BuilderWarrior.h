#ifndef BUILDERWARRIOR_H
#define BUILDERWARRIOR_H

#include "PlayerClass.h"
#include "PlayerBuilder.h"

using namespace std;

class BuilderWarrior : public PlayerBuilder
{
public:
	void bStrength()
	{
		player->setStrength(0);
	}
	void bDexerity()
	{
		player->setDexterity(0);
	}
	void bIntelligence()
	{
		player->setIntelligence(0);
	}
	void bHealth()
	{
		player->setHealth(200);
	}
	void bMana()
	{
		player->setMana(30);
	}
	void bDmgPhysical()
	{
		player->setDmgPhysical(70);
	}
	void bDmgMagical()
	{
		player->setDmgMagical(10);
	}
	void bEvadeChance()
	{
		player->setEvadeChance(10);
	}
	void bDefense()
	{
		player->setDefense(50);
	}
	void bExperience()
	{
		player->setExperience(0);
	}
	void bLevel()
	{
		player->setLevel(0);
	}
	void bName()
	{
		player->setName("Warrior");
	}
	void bSkillPoints()
	{
		player->setSkillPoints(10);
	}
};

#endif