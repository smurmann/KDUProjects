#include "week2.h"

int Week2()
{
	osg::ref_ptr<osg::Node> root = osgDB::readNodeFile("../media/robot.osg");
	osgViewer::Viewer viewer;
	viewer.setSceneData(root.get());
	//viewer.setSceneData(osgDB::readNodeFile("../media/cessna.osg"));
	return viewer.run();

	//
//#include <osg/ref_ptr>
//#include <osg/Referenced>
//#include <iostream>
//#include <Windows.h>
//
//
//class MonitoringTarget : public osg::Referenced
//{
//public:
//MonitoringTarget(int id) : _id(id)
//{
//std::cout<<"Constructing target "<<_id<<std::endl;
//}
//
//protected:
//int _id;
//virtual ~MonitoringTarget()
//{
//std::cout << "Destroying target "<<_id<<std::endl;
//}
//};
//
//int main(int argc, char** argv)
//{
////test 1
//osg::ref_ptr<MonitoringTarget> target = new MonitoringTarget(0);
//std::cout<<"Referenced count A : "<<target->referenceCount()<<std::endl;
//
//{
//osg::ref_ptr<MonitoringTarget> anotherTarget = target;
//std::cout << "Referenced count B : "<<target->referenceCount()<<std::endl;
//}
//
//std::cout << "Referenced count C : "<<target->referenceCount()<<std::endl;
//
//target = NULL;
////anotherTarget = NULL;
//
///*
////No Smart Pointer
//MonitoringTarget* target = new MonitoringTarget(0);
//
//{
//MonitoringTarget* anotherTarget = target;
//}
//*/
//
////test 2
//for(int i = 1; i<10; ++i)
//{
//osg::ref_ptr<MonitoringTarget> subTarget = new MonitoringTarget(i);
////MonitoringTarget* subTarget = new MonitoringTarget(i);
//}
//
//std::cout<<"===============end of program=================="<<std::endl;
//
//system("pause");
//}

}