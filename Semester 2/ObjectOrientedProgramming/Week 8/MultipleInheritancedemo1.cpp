#include <iostream>
#include <string>
#include "windows.h"

using namespace std;

class USBDevice
{
private:
    long id;
public:
    USBDevice(int ID)
    :id(ID)
    {}
    int GetID(){return id;}
};

class NetworkDevice
{
private:
    long id;
public:
    NetworkDevice(int ID)
    :id(ID)
    {}
    int GetID(){return id;}
};

class WirelessAdaptor : public USBDevice, public NetworkDevice
{
public:
    WirelessAdaptor(long UID, long NID)
    : USBDevice(UID), NetworkDevice(NID)
    {}
};

int main ()
{
    WirelessAdaptor adaptor1(5442, 181742);
    cout << adaptor1.NetworkDevice::GetID() << endl;
    cout << adaptor1.USBDevice::GetID() << endl;
    return 0;
}
