#include <iostream>
#include <vector>
#include "windows.h"

using namespace std;

//! define and declare the vector varibale
//! select the type of vector

vector<int> myVector;

void Display(vector<int> tempVector)
{
	cout << "==============================" << endl;
	if(tempVector.empty())
	{
		cout << "Vector is EMPTY" << endl;
		
	}
	else
	{
		for(int i=0; i<tempVector.size(); i++)
		{
			cout << tempVector[i]<< endl;
		}
	}
}

int main()
{
	bool isExit = false;
	do
	{
		int choice = -1;
		int value = 0;
		
		Display(myVector);
		
		cout << "1.Add to vector" << endl;
		cout << "Your Choice: " << endl;
		cin >> choice;
		
		if(choice == 1)
		{
			cout << "Value to be added: ";
			
			cin >> value;
			myVector.push_back(value);
		}
		else if(choice == 4)
		{
			exit(0);
		}
		system("PAUSE");
		system("CLS");
		
	}while(!isExit);
	
	
	
	system("PAUSE");
	return 0;
	
}
