#include <iostream>
#include "windows.h"

using namespace std;

//! declare the function protype
void f(const int*);

int main()
{
    int y;
    f(&y);

    system("PAUSE");
    return 0;
}

void f(const int* xPtr)
{
    *xPtr = 100; //! illegal value update
}
