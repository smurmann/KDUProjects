#include <iostream>\
#include "Employee.h"
#include "Manager.h"

using namespace std;

int main()
{
    Employee emp1("Azri", 30.0);

    cout << "Name: \t" << emp1.getName() << endl;
    cout << "Pay: \t" << emp1.pay(40.0) << endl;
    cout << endl;

    Manager mng("David", 5000.00, false);
    cout << "Name: \t" << mng.getName() << endl;
    cout << "Pay: \t" << mng.pay(10.0) << endl;
    cout << endl;

    //! array -> employee -> Manager
    //!                   -> Supervisor
    //!                   -> Worker
    Manager* employeeArray[3];
    employeeArray[0] = new Manager("John", 6000.0, true);
    employeeArray[1] = new Manager("Azri", 100.0, false);
    employeeArray[2] = new Manager("Sam", 4000.0, true);

    float hours = 0.0;
    for(int i=0; i<3; i++)
    {
        cout << "Name: \t" << employeeArray[i] -> getName() << endl;
        cout << "Please enter your hours worked: ";
        cin >> hours;
        cout << "Pay: \t" << employeeArray[i] -> pay(hours) << endl;

    }
    return 0;
}
