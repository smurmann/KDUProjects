// Stack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <stack>

using namespace std;

stack<int> myStack;


void Display(stack<int> tempStack)
{
	cout<<"======================"<<endl;
	if(tempStack.empty())
	{
		cout<<"Stack is EMPTY"<<endl;
	}
	else
	{
		while(tempStack.size() > 0)
		{
			cout<<tempStack.top()<<endl;
			tempStack.pop();
		}
	}
}
void ClearStack(stack<int>& realStack)
{
	while(!realStack.empty())
	{
		realStack.pop();
	}
}
void ReverseStack(stack<int>& realStack, stack<int> tempStack)
{
	ClearStack(realStack);
	while(!tempStack.empty())
	{
		realStack.push(tempStack.top());
		tempStack.pop();
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	bool isExit = false;
	do
	{
		int choice = -1;
		int value = 0;
		cout<<"The Stack Program"<<endl;
		Display(myStack);
		cout<<"1. Add to the stack"<<endl;
		cout<<"2. Remove from the stack"<<endl;
		cout<<"3. Reverse the stack"<<endl;
		cout<<"4. Arrange in ASC"<<endl;
		cout<<"5. Exit the program"<<endl;
		cout<<"Your choice is :"<<endl;
		cin>>choice;
		cout<<"============================"<<endl;
		if (choice == 1)
		{
			cout<<"Value to be added :";
			cin>>value;
			myStack.push(value);
		}
		else if (choice == 2)
		{
			if(!myStack.empty())
			{
				myStack.pop();
			}
			else
			{
				cout<<"Nothing to pop!"<<endl;
			}
		}
		else if (choice == 3)
		{
			ReverseStack(myStack, myStack);
		}
		else if (choice == 4)
		{
			exit(0);
		}
		system("PAUSE");
		system("CLS");
	}while(!isExit);
	return 0;
}

