#ifndef _SUBJECT_H
#define _SUBJECT_H
#include "Observer.h"

#include <vector>
using namespace std;
class Subject
{
protected:
	//! collection of registered listener
	typedef vector<Observer*> ObserverVector;
	ObserverVector mobServer;
public:
	//! Add Observer
	void AddObserver(Observer * observer)
	{
		mobServer.push_back(observer);

	}

	void RemoveObserver(Observer* observer)
	{

		ObserverVector::iterator iter = mobServer.begin();
		while (iter != mobServer.end())
		{
			if (*iter == observer)
			{
				mobServer.erase(iter);
			}
			++iter;
		}
	}
	//! notify listener to update
	void Notify()
	{

		ObserverVector::iterator iter = mobServer.begin();
		while (iter != mobServer.end())
		{
			(*iter)->update(this);
			++iter;
		}
	}
};


#endif