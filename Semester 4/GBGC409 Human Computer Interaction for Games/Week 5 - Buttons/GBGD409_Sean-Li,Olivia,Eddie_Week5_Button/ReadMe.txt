Instructions:

Be carful with the Volume levers.
It is recommended to keep them low and only
increase them after the game has started.

1.) Launch the .exe file
2.) Select 1280x720 resolution
3.) Select windowed mode

To exit:

Click the X button on the top right of the program.

or

Press Alt + F4

or

Press ESC.