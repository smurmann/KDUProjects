#include "windows.h"
#include <iostream>

using namespace std;

void DisplayEven()
{
    for(int counter = 1; counter<=100; counter++)
    {
        if(counter%2 == 0)
        cout << counter << endl;
    }
}

void DisplayOdd()
{
    for(int counter = 1; counter<=100; counter++)
    {
        if (counter%2 != 0)
        cout << counter << endl;
    }
}

void DisplayMultipleOfTen()
{
    for(int counter = 1; counter<=100; counter++)
    {
            if (counter%10 == 0)
               cout << counter << endl;

    }
}
void DisplayNine()
{
    for(int counter = 1; counter <=100; counter++)
    {
        if (counter > 9)
            cout << counter << endl;
    }
}


int main()
{
    bool isExit = false;
    int option = 0;
    do
        {
cout<<"Please select your option:"<<endl;
cout<<"1. Display Even Numbers"<<endl;
cout<<"2. Display Odd Numbers"<<endl;
cout<<"3. All Numbers (multiple of 10)"<<endl;
cout<<"4. Display all numbers bigger than 9"<<endl;
cout<<"5. Exit"<<endl;
cout<<"Please enter you option: ";
cin>>option;

if(option == 1)
{
    DisplayEven();
}
else if(option == 2)
{
    DisplayOdd();
}
else if(option == 3)
{
    DisplayMultipleOfTen();
}
else if(option == 4)
{
    DisplayNine();
}
else if(option == 5)
{
   isExit = true;
}
    system("PAUSE");
    system("CLS");
    } while(!isExit);
    return 0;
}
