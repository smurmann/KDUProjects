#include <iostream>
#include "windows.h"

using namespace std;

struct character {
int id;
const char * name;
int hp;
int mana;
};

int damage;

int main (){

    //! array size
    int num[10];
    //! assign value to array
    for (int i=0; i<10 ; i++)
    {
        num[i] = i + 1;
        cin >> num;
    }




    struct character Hero = {01, "Alberto", 100, 200};
    struct character Enemy = {02, "Dino", 300, 500};

    cout << "Struct Practise" << endl << endl;
    cout << "Character ID: \t\t" << Hero.id << endl;
    cout << "Character Name: \t" << Hero.name << endl;
    cout << "Character HP: \t\t" << Hero.hp << endl;
    cout << "Character MP: \t\t" << Hero.mana << endl;
    cout << "Please input damage value: ";
    //cin >> damage;

    if(damage > Hero.hp)
    {
        damage = Hero.hp;
    }

    Hero.hp = Hero.hp - damage;

    cout << "Hero HP - damage: \t\t" << Hero.hp << endl;


    if(Hero.hp == 100)
    {
        cout << "Hero is at full health!";
    } else if (Hero.hp == 0)
    {
        cout << "You hero has died!";
    } else
    {
        cout << "Hero has taken damage!";
    }

    cout << endl;

    cout << "Character ID: \t\t" << Enemy.id << endl;
    cout << "Character Name: \t" << Enemy.name << endl;
    cout << "Character HP: \t\t" << Enemy.hp << endl;
    cout << "Character MP: \t\t" << Enemy.mana << endl;

    return 0;

}
