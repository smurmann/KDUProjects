#include <iostream>
#include "windows.h"

using namespace std;

//! declare the function type

void cubeByReference(int);

int main()
{
    int num = 5;
    cout << "Num Value: " << num << endl;
    cubeByReference(num);
    cout << "New num value is: " << num << endl;
    system("PAUSE");
    return 0;
}

void cubeByReference(int nPointer)
{
 nPointer = nPointer * nPointer * nPointer;
}

