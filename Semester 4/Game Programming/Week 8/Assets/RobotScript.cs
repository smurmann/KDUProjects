﻿using UnityEngine;
using System.Collections;

public class RobotScript : MonoBehaviour {

	float speed = 4.0f;
	Vector3 posX;
	private Animator playerAnimator = null;	
	bool facingRight = true;

	// Use this for initialization
	void Start () {
		posX = transform.position;
		playerAnimator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		float horizontal = Input.GetAxisRaw("Horizontal");

		if(horizontal != 0.0f)
		{
			playerAnimator.SetBool("isWalking",true);
		}
		else
		{
			playerAnimator.SetBool("isWalking",false);

		}

		if(Input.GetKey(KeyCode.LeftArrow))
		{
			posX.x += -speed * Time.deltaTime;

		}
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			if(facingRight)
			{
				Flip ();
			}
		}
		if(Input.GetKeyDown(KeyCode.RightArrow))
		{
			if(!facingRight)
			{
				Flip ();
			}
		}
		if(Input.GetKey(KeyCode.RightArrow))
		{
			posX.x += speed * Time.deltaTime;	
		}
		transform.position = posX;
	}

	void Flip()
	{
		// Switch the way the player is labelled as facing
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
