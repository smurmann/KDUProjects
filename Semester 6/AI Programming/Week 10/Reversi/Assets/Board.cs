﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class Board
{

    public int BlackCount
    {
        get { return this.blackCount; }
    }
    public int WhiteCount
    {
        get { return this.whiteCount; }
    }
    public int EmptyCount
    {
        get { return this.emptyCount; }
    }
    public int BlackFrontierCount
    {
        get { return this.blackFrontierCount; }
    }
    public int WhiteFrontierCount
    {
        get { return this.whiteFrontierCount; }
    }
    public int BlackSafeCount
    {
        get { return this.blackSafeCount; }
    }
    public int WhiteSafeCount
    {
        get { return this.whiteSafeCount; }
    }


    public Tile[,] tileArray;
    private bool[,] safeDiscs;


    private int boardSize;
    private int blackCount;
    private int whiteCount;
    private int emptyCount;
    private int blackFrontierCount;
    private int whiteFrontierCount;
    private int blackSafeCount;
    private int whiteSafeCount;

    public Board(int boardSize, Texture[] textureList)
    {
        InitializeBoard(boardSize, textureList);
    }


    //create a copy board
    public Board(Board board)
    {
        this.tileArray = new Tile[board.boardSize, board.boardSize];
        this.safeDiscs = new bool[board.boardSize, board.boardSize];

        for (int i = 0; i < board.boardSize; i++)
        {
            for (int j = 0; j < board.boardSize; j++)
            {
                this.tileArray[i, j] = new Tile(board.tileArray[i,j].getState(), board.tileArray[i, j].getTextureList());
                //this.tileArray[i, j] = board.tileArray[i, j];
                this.safeDiscs[i, j] = board.safeDiscs[i, j];
            }
        }
        this.UpdateCounts();

        this.boardSize = board.boardSize;
        this.blackCount = board.blackCount;
        this.whiteCount = board.whiteCount;
        this.emptyCount = board.emptyCount;
        this.blackSafeCount = board.blackSafeCount;
        this.whiteSafeCount = board.whiteSafeCount;
    }

    private void InitializeBoard(int size, Texture[] textureList)
    {
        this.tileArray = new Tile[size, size];
        this.safeDiscs = new bool[size, size];

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                this.tileArray[i, j] = new Tile(TileState.Empty, textureList);
                this.safeDiscs[i, j] = false;
            }
        }



        if(size == 8)
        {
            this.tileArray[3, 3].setState(TileState.Black);
            this.tileArray[4, 4].setState(TileState.Black);
            this.tileArray[3, 4].setState(TileState.White);
            this.tileArray[4, 3].setState(TileState.White);
        }
        else if(size == 14)
        {
            this.tileArray[6, 6].setState(TileState.Black);
            this.tileArray[7, 7].setState(TileState.Black);
            this.tileArray[6, 7].setState(TileState.White);
            this.tileArray[7, 6].setState(TileState.White);
        }


        this.boardSize = size;
        this.UpdateCounts();
    }

    public TileState returnTileState(int x, int y)
    {
        return TileState.Black;
    }

    public bool isValidMove(TileState currentColor,int x, int y)
    {
        var tile = this.tileArray[x, y];
        //the tile must be empty
        if (tile.getState() != TileState.Empty)
            return false;

        //able to flip at least one of the opponent dots.
        int dr, dc;
        for (dr = -1; dr <= 1; dr++)
            for (dc = -1; dc <= 1; dc++)
                if (!(dr == 0 && dc == 0) && this.isOutflanking(currentColor, x, y, dr, dc))
                    return true;

        // No opponents could be flipped.
        return false;

    }

    public void MakeMove(TileState color, int row, int col)
    {
        Debug.Log(row + " "+  col);
        this.tileArray[row, col].setState(color);;

        // Flip any flanked opponents.
        int dr, dc;
        int i, j;
        for (dr = -1; dr <= 1; dr++)
            for (dc = -1; dc <= 1; dc++)
                // Are there any outflanked opponents?
                if (!(dr == 0 && dc == 0) && this.isOutflanking(color, row, col, dr, dc))
                {
                    i = row + dr;
                    j = col + dc;
                    // Flip 'em.
                    while (this.tileArray[i, j].getState() != color)
                    {
                        this.tileArray[i, j].setState(color);
                        i += dr;
                        j += dc;
                    }
                }
        this.UpdateCounts();

    }


    public bool HasAnyValidMove(TileState color)
    {
        // Check all board positions for a valid move.
        int i, j;
        for (i = 0; i < 8; i++)
            for (j = 0; j < 8; j++)
                if (this.isValidMove(color, i, j))
                    return true;

        // None found.
        return false;
    }

    //Able to flank enemy
    private bool isOutflanking(TileState state, int row, int col, int directionRow, int directionColumn)
    {
        // Move in the given direction as long as we stay on the board and
        // land on a disc of the opposite color.
        int i = row + directionRow;
        int j = col + directionColumn;
        while (i >= 0 && i < boardSize && j >= 0 && j < boardSize && this.tileArray[i, j].getState() != state && this.tileArray[i,j].getState() != TileState.Empty)
        {
            i += directionRow;
            j += directionColumn;
        }

        // If we ran off the board, only moved one space or didn't land on
        // a disc of the same color, return false.
        if (i < 0 || i > boardSize-1 || j < 0 || j > boardSize-1 || (i - directionRow == row && j - directionColumn == col) || this.tileArray[i, j].getState() != state)
            return false;

        // Otherwise, return true;
        return true;
    }

    public int GetValidMoveCount(TileState currentColor)
    {
        int n = 0;

        for (int i = 0; i < boardSize; i++)
        {
            for (int j = 0; j < boardSize; j++)
            {
                if (this.isValidMove(currentColor, i, j))
                    n++;
            }
        }

        return n;
    }

    private void UpdateAllCounts()
    {

        // Reset all counts.
        this.blackCount = 0;
        this.whiteCount = 0;
        this.emptyCount = 0;

        for (int i = 0; i < this.boardSize; i++)
        {
            for (int j = 0; j < this.boardSize; j++)
            {
                if (this.tileArray[i, j].getState() == TileState.Black)
                {
                    this.blackCount++;
                }
                else if (this.tileArray[i, j].getState() == TileState.White)
                {
                    this.whiteCount++;
                }
                else
                    this.emptyCount++;
            }
        }
        
    }

    private void UpdateCounts()
    {
        // Reset all counts.
        this.blackCount = 0;
        this.whiteCount = 0;
        this.emptyCount = 0;
        this.blackFrontierCount = 0;
        this.whiteFrontierCount = 0;
        this.whiteSafeCount = 0;
        this.blackSafeCount = 0;

        int i, j;

        // Update the safe disc map.
        //
        // All currently unsafe discs are checked to see if they are still
        // outflankable. Those that are not are marked as safe.
        // If any new safe discs were found, the process is repeated
        // because this change may have made other discs safe as well. The
        // loop exits when no new safe discs are found.
        bool statusChanged = true;
        while (statusChanged)
        {
            statusChanged = false;
            for (i = 0; i < 8; i++)
                for (j = 0; j < 8; j++)
                    if (this.tileArray[i, j].getState() != TileState.Empty && !this.safeDiscs[i, j] && !this.IsOutflankable(i, j))
                    {
                        this.safeDiscs[i, j] = true;
                        statusChanged = true;
                    }
        }

        // Tally the counts.
        int dr, dc;
        for (i = 0; i < 8; i++)
            for (j = 0; j < 8; j++)
            {
                // If there is a disc at this position, determine if it is
                // on the frontier (i.e., adjacent to an empty square).
                bool isFrontier = false;
                if (this.tileArray[i, j].getState() != TileState.Empty)
                {
                    for (dr = -1; dr <= 1; dr++)
                        for (dc = -1; dc <= 1; dc++)
                            if (!(dr == 0 && dc == 0) && i + dr >= 0 && i + dr < 8 && j + dc >= 0 && j + dc < 8 && this.tileArray[i+dr, j+dc].getState() == TileState.Empty)
                                isFrontier = true;
                }

                // Update the counts.
                if (this.tileArray[i, j].getState() == TileState.Black)
                {
                    this.blackCount++;
                    if (isFrontier)
                        this.blackFrontierCount++;
                    if (this.safeDiscs[i, j])
                        this.blackSafeCount++;
                }
                else if (this.tileArray[i, j].getState() == TileState.White)
                {
                    this.whiteCount++;
                    if (isFrontier)
                        this.whiteFrontierCount++;
                    if (this.safeDiscs[i, j])
                        this.whiteSafeCount++;
                }
                else
                    this.emptyCount++;
            }
    }

    private bool IsOutflankable(int row, int col)
    {
        // Get the disc color.
        int color = (int)this.tileArray[row, col].getState();

        // Check each line through the disc.
        // NOTE: A disc is outflankable if there is an empty square on
        // both sides OR if there is an empty square on one side and an
        // opponent or unsafe (outflankable) disc of the same color on the
        // other side.
        int i, j;
        bool hasSpaceSide1, hasSpaceSide2;
        bool hasUnsafeSide1, hasUnsafeSide2;

        // Check the horizontal line through the disc.
        hasSpaceSide1 = false;
        hasUnsafeSide1 = false;
        hasSpaceSide2 = false;
        hasUnsafeSide2 = false;
        // West side.
        for (j = 0; j < col && !hasSpaceSide1; j++)
            if (this.tileArray[row, j].getState() == TileState.Empty)
                hasSpaceSide1 = true;
            else if ((int)this.tileArray[row, j].getState() != color || !this.safeDiscs[row, j])
                hasUnsafeSide1 = true;
        // East side.
        for (j = col + 1; j < 8 && !hasSpaceSide2; j++)
            if (this.tileArray[row, j].getState() == TileState.Empty)
                hasSpaceSide2 = true;
            else if ((int)this.tileArray[row, j].getState() != color || !this.safeDiscs[row, j])
                hasUnsafeSide2 = true;
        if ((hasSpaceSide1 && hasSpaceSide2) ||
            (hasSpaceSide1 && hasUnsafeSide2) ||
            (hasUnsafeSide1 && hasSpaceSide2))
            return true;

        // Check the vertical line through the disc.
        hasSpaceSide1 = false;
        hasSpaceSide2 = false;
        hasUnsafeSide1 = false;
        hasUnsafeSide2 = false;
        // North side.
        for (i = 0; i < row && !hasSpaceSide1; i++)
            if (this.tileArray[i, col].getState() == TileState.Empty)
                hasSpaceSide1 = true;
            else if ((int)this.tileArray[i, col].getState() != color || !this.safeDiscs[i, col])
                hasUnsafeSide1 = true;
        // South side.
        for (i = row + 1; i < 8 && !hasSpaceSide2; i++)
            if (this.tileArray[i, col].getState() == TileState.Empty)
                hasSpaceSide2 = true;
            else if ((int)this.tileArray[i, col].getState() != color || !this.safeDiscs[i, col])
                hasUnsafeSide2 = true;
        if ((hasSpaceSide1 && hasSpaceSide2) ||
            (hasSpaceSide1 && hasUnsafeSide2) ||
            (hasUnsafeSide1 && hasSpaceSide2))
            return true;

        // Check the Northwest-Southeast diagonal line through the disc.
        hasSpaceSide1 = false;
        hasSpaceSide2 = false;
        hasUnsafeSide1 = false;
        hasUnsafeSide2 = false;
        // Northwest side.
        i = row - 1;
        j = col - 1;
        while (i >= 0 && j >= 0 && !hasSpaceSide1)
        {
            if (this.tileArray[i, j].getState() == TileState.Empty)
                hasSpaceSide1 = true;
            else if ((int)this.tileArray[i, j].getState() != color || !this.safeDiscs[i, j])
                hasUnsafeSide1 = true;
            i--;
            j--;
        }
        // Southeast side.
        i = row + 1;
        j = col + 1;
        while (i < 8 && j < 8 && !hasSpaceSide2)
        {
            if (this.tileArray[i, j].getState() == TileState.Empty)
                hasSpaceSide2 = true;
            else if ((int)this.tileArray[i, j].getState() != color || !this.safeDiscs[i, j])
                hasUnsafeSide2 = true;
            i++;
            j++;
        }
        if ((hasSpaceSide1 && hasSpaceSide2) ||
            (hasSpaceSide1 && hasUnsafeSide2) ||
            (hasUnsafeSide1 && hasSpaceSide2))
            return true;

        // Check the Northeast-Southwest diagonal line through the disc.
        hasSpaceSide1 = false;
        hasSpaceSide2 = false;
        hasUnsafeSide1 = false;
        hasUnsafeSide2 = false;
        // Northeast side.
        i = row - 1;
        j = col + 1;
        while (i >= 0 && j < 8 && !hasSpaceSide1)
        {
            if (this.tileArray[i, j].getState() == TileState.Empty)
                hasSpaceSide1 = true;
            else if ((int)this.tileArray[i, j].getState() != color || !this.safeDiscs[i, j])
                hasUnsafeSide1 = true;
            i--;
            j++;
        }
        // Southwest side.
        i = row + 1;
        j = col - 1;
        while (i < 8 && j >= 0 && !hasSpaceSide2)
        {
            if (this.tileArray[i, j].getState() == TileState.Empty)
                hasSpaceSide2 = true;
            else if ((int)this.tileArray[i, j].getState() != color || !this.safeDiscs[i, j])
                hasUnsafeSide2 = true;
            i++;
            j--;
        }
        if ((hasSpaceSide1 && hasSpaceSide2) ||
            (hasSpaceSide1 && hasUnsafeSide2) ||
            (hasUnsafeSide1 && hasSpaceSide2))
            return true;

        // All lines are safe so the disc cannot be outflanked.
        return false;
    }




}