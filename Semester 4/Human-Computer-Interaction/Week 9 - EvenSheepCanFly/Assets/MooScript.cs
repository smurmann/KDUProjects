﻿using UnityEngine;
using System.Collections;

public class MooScript : MonoBehaviour {
    Animator anim;
    bool licking;
    public int clicks = 0;
    float curTime;
    bool finishlicking;
    public GameObject yum;
    public GameObject slurp;
    public GameObject moo;
    public GameObject light2;
    public GameObject number;
    public GameObject canvas;
    bool lighton;
    public GameObject numberspawn;

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
        
	
	}
	
	// Update is called once per frame
	void Update () {

        anim.SetInteger("Clicks", clicks);
        anim.SetBool("Licking", licking);
        if(finishlicking)
        {
            curTime += Time.deltaTime;
            if(curTime > 0.05f)
            {
                finishlicking = false;
                licking = false;
            }
        }

        if(lighton)
        {
           light2.SetActive(true);
        }
        else if (!lighton)
        {
            light2.SetActive(false);
        }

        
	}

    void OnMouseDown()
    {
        //Debug.Log("MOOOOO");
        licking = true;
        //anim.Play("Licking");
        clicks++;
        float randomNum = Random.value;

        if(randomNum <= 0.8f )
        {
            float randomNum2 = Random.value;

            if(randomNum2 <= 0.4f)
            {
                Instantiate(yum, new Vector3(3.71889f, -1.857392f, 0f), Quaternion.identity);
            }
            else if (randomNum2 <= 0.8f && randomNum2 > 0.4f)
            {
                Instantiate(slurp, new Vector3(-0.51f, -0.6f, 0f), Quaternion.identity);
            }
            else if (randomNum2 >= 0.9f)
            {
                Instantiate(moo, new Vector3(3.543949f, -0.24f, 0f), Quaternion.identity);
            }

        }
        GameObject newobject = Instantiate(number, numberspawn.transform.position, Quaternion.identity) as GameObject;
        newobject.transform.parent = canvas.transform;

    }
    void OnMouseUp()
    {
        //anim.Play("State1Idle");
        //licking = false;
        curTime = 0.0f;
        finishlicking = true;
    }
    void OnMouseOver()
    {
        lighton = true;
        //Debug.Log("mouse is hover");
    }
    void OnMouseExit()
    {
        lighton = false;
    }
}
