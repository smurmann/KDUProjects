﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;
using System;
using Random = UnityEngine.Random;


public class SendInputOnSpace : MonoBehaviour {
   // public Text myText;
   // public Text wordTyped_Text;
    private string currentInput;
    private InputField inputField;
    private char last;
    public GameObject Player;
    public PlayerDataScript datascript;
    public GameObject boxSpawn;

    public Sprite enemy;
    public Sprite friendly;

    public int Difficulty = 0;
    public bool DifficultyBool = false;
    //scores
   


    List<string> WordVector;
    string[] DictionaryVector;
    string[] VulgarDictionaryVector;
    string[] enemyWordList;
    int wordcount;
    public bool playerTurn;

    string[] VulgarSentences;
    int vulgarSentenceCounter = 0;
    public int enemyScore = 0;

    public bool started = false;
    float timer = 2.0f;
    float counter;
    string datapath;

    public int playerLikes  = 0;
    public int enemyLikes = 0;



	// Use this for initialization
	void Start () {
      
        LoadDictionary();
        
        playerTurn = true;
        datascript = Player.GetComponent<PlayerDataScript>();
        inputField = GetComponent<InputField>();
        if(inputField != null)
        {
            //inputField.Select();
            
        }
        inputField.ActivateInputField();
        currentInput = "1";
        inputField.Select();
        //WordVector.Initialize();
       // myText.text = Regex.Replace(myText.text, @"[^a-zA-Z0-9!@#$%^&*<>?,.'|\s|\r|\n|]", "");
        spawnEnemyComment();

    }
	
	// Update is called once per frame
    void Update()
    {
        if(Input.anyKeyDown)
        {
            started = true;
        }


        if(started)
        {
            if (playerTurn)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    wordcount++;
                }

                if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl) ||Input.GetKeyDown(KeyCode.Return) || GameObject.Find("Timer").GetComponent<TimerScript>().timer > GameObject.Find("Timer").GetComponent<TimerScript>().timeToFinish || GameObject.Find("SubmitButton").GetComponent<ButtonScript>().mouseDown)
                {
                    datascript.Score = 0;
                    SendComment();
                    checkScore();
                    GameObject.Find("Timer").GetComponent<TimerScript>().timer = 0.0f;
                    playerTurn = false;
                    GameObject.Find("SubmitButton").GetComponent<ButtonScript>().mouseDown = false;
                    GameObject.Find("FBSounds").GetComponent<AudioSource>().Play();

                    //compare then delete
                }
                
            }
            else
            {
                spawnEnemyComment();
                playerTurn = true;
                inputField.value = " ";
            }

        }




//        Debug.Log(playerTurn);

    }
    void checkScore()
    {
        if (datascript.Score < GameObject.Find("EnemyScore").GetComponent<EnemyScoreScript>().score)
        {
            //datascript.Life--;
            enemyLikes++;
        }
        else
        {
            playerLikes++;
        }
    }


    void LoadDictionary()
    {

        datapath = Path.Combine(Application.dataPath, "English350k.txt");
        DictionaryVector = File.ReadAllLines(datapath);
        Debug.Log("DoneLoading Dictionary");

        datapath = Path.Combine(Application.dataPath, "VulgarList.txt");
        VulgarDictionaryVector = File.ReadAllLines(datapath);
        Debug.Log("DoneLoading VulgarDictinary");

        datapath = Path.Combine(Application.dataPath, "VulgarSentenceList.txt");
        VulgarSentences = File.ReadAllLines(datapath);
        enemyWordList = VulgarSentences;
        Debug.Log("DoneLoading VulgarSentences");

        

    }

    bool LookForWord(string word, bool enemy)
    {

        word = word.ToLower();
        word = Regex.Replace(word, @"[^A-Za-z0-9]+", "");
        int findSlot = 0;
        int findSlot2 = 0;

        findSlot = Array.IndexOf(VulgarDictionaryVector, word);
        findSlot2 = Array.IndexOf(DictionaryVector, word);

        if(findSlot != -1)
        {
            if(VulgarDictionaryVector.GetValue(findSlot).Equals(word))
            {
                Debug.Log("vulgar true");
                for (int i = 0; word.Length > i; i++)
                {
                    //ADD FOR VULGAR FUN!
                    //Debug.Log(TextSend);
                    if(!enemy)
                    {
                        datascript.Score++;
                        datascript.Score++;
                    }
                    else
                    {
                        enemyScore++;
                        enemyScore++;
                    }
                    
                }
                return true;
            }
            else
            {
                Debug.Log("false");
               // datascript.Life--;

                return false;
            }
        }
        if(findSlot2 != -1)
        {
            Debug.Log(findSlot2);
               if (DictionaryVector.GetValue(findSlot2).Equals(word))
                    {
                        for (int i = 0; word.Length > i; i++)
                        {
                            if(!enemy)
                            {
                                datascript.Score++;
                            }
                            else
                            {
                                enemyScore++;
                            }
                            //Debug.Log(TextSend);
                            //ADD FOR NOT VULGAR FUN!
                        }
                        Debug.Log("true");
                        return true;
                    }
               else
               {
                   //datascript.Life--;

                   return false;
               }

        }
        else
        if (findSlot2 == -1 && findSlot == -1)
        {
            if(!enemy)
            {
                datascript.Life--;

            }

            Debug.Log("false");
            return true;
        }
        else
        {
            return false;
        }




    }

    void ClearInput()
    {
        inputField.value = "";
        currentInput = "";
        inputField.Select();
        inputField.ActivateInputField();
        Debug.Log("Cleared");
    }

    public void SendComment()
    {
        GameObject kill = GameObject.FindGameObjectWithTag("FriendlyBox");
        if(kill != null)
        {
            kill.GetComponent<SlideBoxScript>().posState = 3;

        }
        //moveUp();
        string TextSend = GetComponent<InputField>().value;

        foreach (string word in TextSend.Split(' '))
        {
            LookForWord(word, false);
        }
        //GameObject enemyBox = GameObject.FindGameObjectWithTag("EnemyBox");
        //if(enemyBox != null)
        //{
        //    enemyBox.GetComponent<SlideBoxScript>().posState = 2;
        //}
        GameObject newBox = Instantiate(boxSpawn, new Vector3(-60.7f, -10.97f, 0.0f), Quaternion.identity) as GameObject;
        newBox.tag = "FriendlyBox";
        GameObject newBoxText = GameObject.FindGameObjectWithTag("CommentBoxLarge");
        //newBoxText.tag = "TextBox";
        newBoxText.tag = "FriendlyTextBox";
        newBoxText.GetComponent<Text>().text = TextSend;
        newBox.GetComponent<SlideBoxScript>().posState = 1;
        GameObject spriteBox = GameObject.FindGameObjectWithTag("SpriteBox");
        spriteBox.GetComponent<SpriteRenderer>().sprite = friendly;
        spriteBox.tag = "DoneSprite";


        ClearInput();
       // moveUp();

    }

    public void spawnEnemyComment()
    {
        if(DifficultyBool)
        {
            Difficulty = Random.Range(0, 100);
            if (Difficulty <= 10)
            {
                vulgarSentenceCounter = Random.Range(0, 21);
            }
            else if (Difficulty > 10 && Difficulty <= 70)
            {
                vulgarSentenceCounter = Random.Range(22, 50);

            }
            else if (Difficulty > 70)
            {
                vulgarSentenceCounter = Random.Range(51, 57);
            }
        }
        else
        {
            Difficulty = Random.Range(0, 100);
            if (Difficulty <= 60)
            {
                vulgarSentenceCounter = Random.Range(0, 21);
            }
            else
            {
                vulgarSentenceCounter = Random.Range(22, 50);

            }
        }

        GameObject kill = GameObject.FindGameObjectWithTag("EnemyBox");
        if (kill != null)
        {
            kill.GetComponent<SlideBoxScript>().posState = 3;

        }
       // moveUp();
        enemyScore = 0;
        foreach (string word in enemyWordList[vulgarSentenceCounter].Split(' '))
        {
            LookForWord(word, true);
        }           
        GameObject newBox = Instantiate(boxSpawn, new Vector3(10.7f, -10.97f, 0.0f), Quaternion.identity) as GameObject;
        newBox.tag = "EnemyBox";
        GameObject newBoxText = GameObject.FindGameObjectWithTag("CommentBoxLarge");
        //newBox.tag = "TextBox";
        newBoxText.tag = "EnemyTextBox";
        newBoxText.GetComponent<Text>().text = enemyWordList[vulgarSentenceCounter];
        vulgarSentenceCounter++;
        newBox.GetComponent<SlideBoxScript>().posState = 2;

        GameObject spriteBox = GameObject.FindGameObjectWithTag("SpriteBox");
        spriteBox.GetComponent<SpriteRenderer>().sprite = enemy;
        spriteBox.tag = "DoneSprite";
        

        //GameObject friendlyBox = GameObject.FindGameObjectWithTag("FriendlyBox");
        //if (friendlyBox != null)
        //{
        //    friendlyBox.GetComponent<SlideBoxScript>().posState = 3;
        //}
      //  Debug.Log(enemyScore);
        

    }

    public void moveUp()
    {
        GameObject[] boxes;
        boxes = GameObject.FindGameObjectsWithTag("CommentBoxLarge");
        foreach(GameObject box in boxes)
        {
            box.GetComponent<SlideBoxScript>().posState++;
        }
    }

}