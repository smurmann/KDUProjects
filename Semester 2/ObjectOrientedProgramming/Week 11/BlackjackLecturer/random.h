#ifndef RANDOM_H
#define RANDOM_H

#include "range.h"

//! function overload
int Random(Range r);
int Random(int, int);

#endif // RANDOM_H
