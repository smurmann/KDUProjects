﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerScript : MonoBehaviour {

    private Text text;
    public float timer;
    public float timeToFinish = 15.0f;
    GameObject obj;
    bool started = false;
    

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();	
        obj = GameObject.FindGameObjectWithTag("InputBox");
	}
	
	// Update is called once per frame
	void Update () {

        if(started)
        {
            timer += Time.deltaTime;

            text.text = "Timer: " + string.Format("{0:0.00}", timeToFinish - timer);

            if (timer > timeToFinish)
            {
                //timer = 0.0f;

                //obj.GetComponent<SendInputOnSpace>().SendComment();
            }
        }

        started = obj.GetComponent<SendInputOnSpace>().started;

	
	}

    public void DestoryPreviousBox()
    {
        GameObject.FindGameObjectWithTag("FriendlyTextBox").GetComponent<SlideBoxScript>().posState = 3;
        GameObject.FindGameObjectWithTag("EnemyTextBox").GetComponent<SlideBoxScript>().posState = 3;
        timer = 15.0f;
    }
}
