      precision mediump float;
	  varying vec4 fColor;
	  uniform float Factor1;
	  uniform float Factor2;
	  
	  varying vec2 fTexCoord;
	  
	  uniform sampler2D sampler2d;
	  
	 
	  
	  const float boxSize = 5.0;
	  
	  float gaussianFunction2D(float x, float y)
	  {
		  float variance = 0.25;
		  
		  float alpha = -(x*x+y*y) / (2.0*variance);
		  return exp(alpha);
	  }	  
	  

	  
	  
      void main()             
      {		
	   
	   float imageWidth = 128.0;
	   float imageHeight = 128.0;
	   
		float radiusSize = 4.0;
		float diameter = radiusSize * 2.0;
		float radiusSqr = radiusSize * radiusSize;
	  
		float totalWeight = 0.0;
		float x;
		float y;
	 	vec4 combinedColor;
			
		
		for(x=-radiusSize; x<= radiusSize; x+=1.0)
		{
			float u = fTexCoord.x + x/imageWidth;
			if(u >=0.0 && u <= 1.0)
			{
				for(y =- radiusSize; y<=radiusSize; y+=1.0)
				{
					if(x*x + y*y <= radiusSqr)
					{
						float v = fTexCoord.y + y/imageHeight;
						if(v>= 0.0 && v <=1.0)
						{
							float weight = gaussianFunction2D(x/radiusSize, y/radiusSize);
							combinedColor += texture2D(sampler2d, vec2(u,v)) * weight;
							totalWeight += weight;
						}
					}
				}
			}
		}
		
			
		vec4 resultColor;
		resultColor = combinedColor / totalWeight;

		
		gl_FragColor = resultColor;
		
		//gl_FragColor = textu
		
		
		//gl_FragColor = texture2D(sampler2d, fTexCoord);
       }                                            