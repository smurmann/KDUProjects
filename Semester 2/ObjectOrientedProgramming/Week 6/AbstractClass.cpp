#include <iostream>
using namespace std;

class Vehicle
{
public:
    int speed;
    //! pure virtual functions -> abstract class
    virtual void Move() = 0;
};
//! car inherits from abstract Vehicle class
//! car class required to implement all virtual functions
//! from the super class
class Car: public Vehicle
{
private:
    int gear;
public:
    //! Override the parent's implementations -> move()
    void Move()
    {
        cout << "Drive at " << gear << "KMH" << endl;
    }
    void SetGear(int g)
    {
        gear = g;
    }
};

class Heli: public Vehicle
{
private:
    int fanSpeed;
public:
    void Move()
    {
        cout << "Fly at " << fanSpeed << "KMH" << endl;
    }
    void SetFanSpeed(int fs)
    {
        fanSpeed = fs;
    }
};

int main()
{
    Car myCar;
    myCar.SetGear(4);
    myCar.Move();

    Heli myHeli;
    myHeli.SetFanSpeed(5);
    myHeli.Move();

    return 0;
}
