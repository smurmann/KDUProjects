#ifndef _TRIANGLE_DEMO_H
#define _TRIANGLE_DEMO_H

#include "demo_base.h"
#include "bitmap.h"

#define TEXTURE_COUNT 1

class TriangleDemo : public DemoBase
{
private:
	GLuint mtextureID[TEXTURE_COUNT];
	void loadTexture(const char* path, GLuint textureID)
	{
		CBitmap bitmap(path);

		// Create Linear FIltered Texture
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//aooky texture wrapping along horizontal part.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);//aooky texture wrapping along vertical part.
		/*
		// old school minecraft filtering.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //near filtering. (For When texture needs to scale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // far filtering. (For when texture needs to scale down on screen)
		*/

		// bilinear filtering.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //near filtering (For when texgure needs to scale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.GetWidth(), bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.GetBits());
	}
public:
	float m_rotation;
	Vector m_translation;
public:
	void init()
	{
		glGenTextures(TEXTURE_COUNT, mtextureID);
		loadTexture("../media/rocks.bmp", mtextureID[0]);
		//m_rotation = 0.0f;
		//m_translation = Vector(-0.5f, -0.5f, -0.5f);
	}

	void drawTexturedPolygons()
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mtextureID[0]);

		glBegin(GL_TRIANGLES);							// Drawing Using triangles

		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);				// Top
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 0.0f, 0.0f);				// Bottom Left
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, 0.0f);				// Bottom Right
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 1.0f, 0.0f);

		glTexCoord2f(1.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 1.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 1.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(0.0f, 1.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);

		glTexCoord2f(1.0f, 0.0f); glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 1.0f, 1.0f);
		glEnd();

		glDisable(GL_TEXTURE_2D);

	}

	void deinit()
	{

	}

	void drawPyramid(GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLES);
		glColor3f(red, green, blue);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glEnd();



	}

	void drawCone(float height, float radius, float xOffset, float yOffset, float zOffset, GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(red, green, blue);
		for (int i = 1; i < 360; i++)
		{
			glVertex3f(0, height, 0);
			for (float i = 1; i < 360; i++)
			{
				glVertex3f(radius*cos(i) + xOffset, 0 + yOffset, radius*sin(i) + zOffset);
			}
		}
		glEnd();
	}

	void drawCylinder(float radius, float height, float xOffset, float yOffset, float zOffset, GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(red, green, blue);
		for (int i = 1; i < 360; i++)
		{
			glVertex3f(0 + xOffset, height + yOffset, 0 + zOffset);
			glVertex3f(0 + xOffset, 0 + yOffset, 0 + zOffset);
			for (float i = 1; i < 360; i++)
			{
				glVertex3f(radius*cos(i) + xOffset, height + yOffset, radius*sin(i) + zOffset);
				glVertex3f(radius*cos(i) + xOffset, 0 + yOffset, radius*sin(i) + zOffset);
			}
		}
		glEnd();
	}

	void drawCube(float cubeSize, GLfloat red, GLfloat green, GLfloat blue)
	{
		glBegin(GL_TRIANGLES);							// Drawing Using triangles
		glColor3f(red, green, blue);
		glVertex3f(0.0f, 0.0f, 0.0f);				// Top
		glVertex3f(cubeSize, 0.0f, 0.0f);				// Bottom Left
		glVertex3f(cubeSize, cubeSize, 0.0f);				// Bottom Right
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(0.0f, cubeSize, 0.0f);

		glVertex3f(0.0f, cubeSize, 0.0f);
		glVertex3f(0.0f, cubeSize, cubeSize);
		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(0.0f, cubeSize, cubeSize);

		glVertex3f(cubeSize, cubeSize, 0.0f);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(cubeSize, 0.0f, 0.0f);
		glVertex3f(cubeSize, 0.0f, cubeSize);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(cubeSize, 0.0f, 0.0f);

		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(cubeSize, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(cubeSize, 0.0f, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(cubeSize, 0.0f, 0.0f);

		glVertex3f(cubeSize, 0.0f, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(0.0f, cubeSize, cubeSize);
		glVertex3f(cubeSize, cubeSize, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);

		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, cubeSize, 0.0f);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(0.0f, 0.0f, cubeSize);
		glVertex3f(0.0f, cubeSize, 0.0f);
		glVertex3f(0.0f, cubeSize, cubeSize);
		glEnd();
	}


	void draw(const Matrix& viewMatrix)
	{
		//drawaxis
		glLoadMatrixf((GLfloat*)viewMatrix.mVal);
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.3f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.3f, 1.0f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.3f, 0.3f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();

		Matrix rotate1 = Matrix::makeRotateMatrix(0, Vector(0.0f, 0.0f, 1.0f));
		Matrix translate1 = Matrix::makeTranslationMatrix(3.0f, 0.0f, 0.0f);
		//m_rotation += 0.1f;

		//		Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(-1.0f, -1.0f, -1.0f));
		//		Matrix translate1 = Matrix::makeTranslationMatrix(-0.5f, -0.5f, -0.5f);
		//Matrix rotate1 = Matrix::makeRotateMatrix(m_rotation, Vector(0.0f, 0.0f, 0.0f));
		//Matrix translate1 = Matrix::makeTranslationMatrix(0.0f, 0.0f, 0.0f);

		// Note onn OpenGl Matrix model;
		// Screen = Proj * View * Model
		// Model = TranformA(3rd) * TransformB(2nd) * TransformC(1st) (transform could be Rotate, Scale, Translate, etc)

		// perform model transformation
		Matrix modelMatrix = translate1 * rotate1;
		//Matrix modelMatrix = translate1;
		//Matrix modelMatrix = rotate1;

		// convert model space to view space
		Matrix viewSpaceMatrix = viewMatrix * modelMatrix;

		// Finish Drawing the triangle


		// Rectangle
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		//drawPyramid();
		//drawCube();
		glColor3f(1.0f, 1.0f, 1.0f);


		drawTexturedPolygons();							// Finish Drawing the triangle
		//drawCylinder(0.1f, 0.2f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);



		//HEAD
		rotate1 = Matrix::makeRotateMatrix(0, Vector(0.0f, 0.0f, 1.0f));
		translate1 = Matrix::makeTranslationMatrix(0.0f, 0.0f, 0.0f);
		modelMatrix = translate1 * rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawPyramid(1.0f, 1.0f, 0.0f);

		//EYE RIGHT
		translate1 = Matrix::makeTranslationMatrix(0.2f, -0.3f, 0.5f);
		modelMatrix = translate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCube(0.4f, 0.0f, 0.0f, 1.0f);

		//EYE LEFT
		translate1 = Matrix::makeTranslationMatrix(-0.7f, -0.3f, 0.5f);
		modelMatrix = translate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCube(0.4f, 0.0f, 0.0f, 1.0f);

		//EYEBall RIGHT
		translate1 = Matrix::makeTranslationMatrix(0.3f, -0.15f, 0.8f);
		modelMatrix = translate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCube(0.2f, 1.0f, 1.0f, 1.0f);

		//EYEBall LEFT
		translate1 = Matrix::makeTranslationMatrix(-0.6f, -0.15f, 0.8f);
		modelMatrix = translate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCube(0.2f, 1.0f, 1.0f, 1.0f);

		//NOSE
		translate1 = Matrix::makeTranslationMatrix(0.0f, -0.5f, 0.65f);
		rotate1 = Matrix::makeRotateMatrix(-90.0f, Vector(1.0f, 0.0f, 0.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCone(0.3f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

		//NECK
		translate1 = Matrix::makeTranslationMatrix(0.0f, -2.0f, 0.0f);
		rotate1 = Matrix::makeRotateMatrix(0.0f, Vector(1.0f, 0.0f, 0.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCylinder(0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);

		//BODY
		translate1 = Matrix::makeTranslationMatrix(0.0f, -1.8f, 0.0f);
		rotate1 = Matrix::makeRotateMatrix(180.0f, Vector(1.0f, 0.0f, 0.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCone(2.5f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.5f);

		//Ear
		translate1 = Matrix::makeTranslationMatrix(1.0f, 0.3f, 0.0f);
		rotate1 = Matrix::makeRotateMatrix(-135.0f, Vector(0.0f, 0.0f, 1.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCone(1.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.3f, 0.1f, 0.5f);

		//Ear2
		translate1 = Matrix::makeTranslationMatrix(-1.0f, 0.3f, 0.0f);
		rotate1 = Matrix::makeRotateMatrix(135.0f, Vector(0.0f, 0.0f, 1.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCone(1.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.3f, 0.1f, 0.5f);

		//foot
		translate1 = Matrix::makeTranslationMatrix(-0.7f, -4.75f, 0.0f);
		rotate1 = Matrix::makeRotateMatrix(-45.0f, Vector(0.0f, 0.0f, 1.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCube(1.0f, 0.0f, 1.0f, 1.0f);

		//foot2
		translate1 = Matrix::makeTranslationMatrix(0.0f, -4.15f, 0.0f);
		rotate1 = Matrix::makeRotateMatrix(45.0f, Vector(0.0f, 0.0f, 1.0f));
		modelMatrix = translate1*rotate1;
		viewSpaceMatrix = viewMatrix * modelMatrix;
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawCube(1.0f, 0.0f, 1.0f, 1.0f);


	}
};


#endif