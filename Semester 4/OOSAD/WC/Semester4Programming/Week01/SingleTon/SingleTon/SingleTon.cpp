// SingleTon.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>

using namespace std;

class GlobalClass

{

	int value;
public:
	GlobalClass(int v = 0)
	{
		value = v;
	}
	int getValue()
	{

		return value;
	}
	void setValue(int v)
	{
		value = v;

	}

};


//! Default Initialize

GlobalClass *global_ptr = 0;

void foo(void)
{
	// initialzie on firstTime
	if (!global_ptr)
	{
		global_ptr = new GlobalClass;


	}
	global_ptr->setValue(1);
	cout << "foo : global_prt is " << global_ptr->getValue() << endl;
}

void boo(void)
{
	// initialzie on firstTime
	if (!global_ptr)
	{
		global_ptr = new GlobalClass;


	}
	global_ptr->setValue(2);
	cout << "boo : global_prt is " << global_ptr->getValue() << endl;
}



int _tmain(int argc, _TCHAR* argv[])
{
	
	
	
	if (!global_ptr)
	{
		global_ptr = new GlobalClass;
		
	}

	cout << "main : global_ptr is " << global_ptr->getValue() << endl;
	
	foo();
	boo();

	system("PAUSE");
	return 0;

	
}

