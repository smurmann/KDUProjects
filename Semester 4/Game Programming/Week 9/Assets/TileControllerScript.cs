﻿using UnityEngine;
using System.Collections;

public class TileControllerScript : MonoBehaviour {

	public GameObject m_tilePrefab;
	private GameObject[,] m_tiles;

	private int m_playerX;
	private int m_playerY;
	private int m_cellNumX;
	private int m_cellNumY;
	private float m_cellSize;

	TileScript playerScript;
	
	// Use this for initialization
	void Start () {
		m_cellNumX = 30;
		m_cellNumY = 30;
		m_cellSize = 0.17f;

		float mapWidth = m_cellNumX * m_cellSize;
		float mapHeight = m_cellNumY * m_cellSize;

		m_tiles = new GameObject[m_cellNumX,m_cellNumY];

		int i, j;
		for(i=0; i< m_cellNumX; i++)
		{
			for(j=0; j<m_cellNumY; j++)
			{
				Vector3 pos = new Vector3(i*m_cellSize-mapWidth*0.5f, j*m_cellSize-mapWidth*0.5f,0);
				GameObject theObject = Instantiate(m_tilePrefab, pos, Quaternion.identity) as GameObject;

				TileScript tileScript = (TileScript) theObject.GetComponent(typeof(TileScript));

				tileScript.setState(0);

				m_tiles[i,j] =  theObject;
			}
		}

		m_playerX = 10;
		m_playerY = 10;
		playerScript = (TileScript) m_tiles[m_playerX, m_playerY].GetComponent(typeof(TileScript));
		playerScript.setState(1);
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			SetWhite();
			m_playerY++;

			if(m_playerY >= 29)
			{
				m_playerY = 29;
			}
		}

		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			SetWhite();
			m_playerY--;

			if(m_playerY <= 0)
			{
				m_playerY = 0;
			}
		}

		if(Input.GetKeyDown(KeyCode.RightArrow))
		{
			SetWhite();
			m_playerX++;

			if(m_playerX >= 29)
			{
				m_playerX = 29;
			}
		}

		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			SetWhite();
			m_playerX--;
			if(m_playerX <= 0)
			{
				m_playerX = 0;
			}
		}

		playerScript = (TileScript) m_tiles[m_playerX, m_playerY].GetComponent(typeof(TileScript));
		playerScript.setState(1);
	
	}

	void SetWhite()
	{	
		playerScript = (TileScript) m_tiles[m_playerX, m_playerY].GetComponent(typeof(TileScript));
		playerScript.setState(0);
	}
}
