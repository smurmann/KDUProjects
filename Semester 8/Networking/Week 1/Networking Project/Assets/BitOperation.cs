﻿using UnityEngine;
using System.Collections;

public class BitOperation : MonoBehaviour
{
    uint num1 = 3; //0-15, 4 bits
    uint num2 = 1; //0-1, 1 bit
    uint num3 = 27;//0-31, 5bits
    uint num4 = 0; //0-1, 1bit

    uint data = 0x00000000000;
    uint data2 = 0x00000000000;


    uint c, d, e;
    void Start()
    {
        BitPacker packer = new BitPacker();

        packer.pack(num1, 4);
        packer.pack(num2, 1);
        packer.pack(num3, 5);
        packer.pack(num4, 1);

        Debug.Log(packer.extract(1));
        Debug.Log(packer.extract(5));
        Debug.Log(packer.extract(1));
        Debug.Log(packer.extract(4));


        UIntSplitter splitter = new UIntSplitter();
        splitter.storeUInt(15);

        Debug.Log("Splitter" + splitter.getInt());

    }

    void BasicBit()
    {
        data = data | num1;

        data = data << 1;
        data = data | num2;

        data = data << 4;
        data = data | num3;

        data = data << 1;
        data = data | num4;
        Debug.Log(System.Convert.ToString(num3, 2));


        Debug.Log(System.Convert.ToString(data, 2));

        //data2 = data;

        //num1
        Debug.Log((data >> 6));// | data2);

        //num2
        Debug.Log((data >> 5) & 1);


        //num3
        Debug.Log((data >> 1) & 31);
        Debug.Log(System.Convert.ToString((data >> 1) & 31, 2));

        //11111100 &
        //00111110 = 00111100
        //00111100 >> 1
        //00011110 = 30 ?

        //Debug.Log((data & 000111110) >> 1 | data2);
        //Debug.Log(System.Convert.ToString((data & 000111110) >> 1 | data2, 2));

        //num4
        Debug.Log(data & 1);




        //num4
        Debug.Log(data & data2);
    }
}
