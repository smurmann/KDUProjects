#include <iostream>

using namespace std;

class Hero
{
private:
    int health;
public:
    inline void SetHealth(int h)
    {
        health = h;
    }
    inline int GetHealth()
    {
        return health;
    }
};

int userhealth;


int main ()
{
    Hero h1, h2;

    cout << "Input Hero1's health: ";
    cin >> userhealth    ;
    h1.SetHealth(userhealth);
    h2.SetHealth(200);
    cout << "Hero1's health " << h1.GetHealth() << endl;
    cout << "Hero2's health " << h2.GetHealth() << endl;

return 0;
}
