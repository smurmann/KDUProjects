#include <iostream>
#include <string>
#include <vector>
using namespace std;

//! card structure definition
struct Card
{
    string face;
    string suit;
};

class CardDeck
{
public:
    static const int numberOfCards = 52;
    static const int faces = 13;
    static const int suit = 4;
    CardDeck();
    void shuffle();
    void deal() const;
private:
    vector<Card> deck;
};
