#include <iostream>
#include "windows.h"
#include <ctime>
#include <cstdlib>
#include <iomanip>

using namespace std;
const int RACE_END = 70;

//! declare the required function prototype
void moveTortoise(int *);
void moveHare(int *);
void moveBee(int *);
void moveMonkey(int *);
void printCurrentPosition(int*, int*, char*);

int main()
{
    system("mode con: cols=110 lines=80");
    //! declare the initial variables
    int tortoise = 1;
    bool isTortoise = false;

    int hare = 1;
    bool isHare = false;

    int bee = 1;
    bool isBee = false;

    int monkey = 1;
    bool isMonkey = false;

    int timer = 0;

    int choice = 0;
    int choiceT = 0;
    int choiceH = 0;
    int choiceB = 0;
    int choiceM = 0;

    int player = 0; // 0 = Tortoise
                    // 1 = Hare
                    // 2 = Bee
                    // 3 = Monkey
    char compChar = 'C'; //print out for computer



    cout<<"Which character would you like to use? \n";
    cout<<"1. Tortoise \n";
    cout<<"2. Hare \n";
    cout<<"3. Bee \n";
    cout<<"4. Monkey \n";
    cin>> choice;

    cout<<endl;
    cout << "The player is displayed as P" << endl;
    cout << "The computer is displayed with the animals initial" << endl;
    cout << "You are now playing as: ";

      switch(choice)
    {
        case 1:
            cout << "Tortoise!" << endl;
            cout << "Who would you like to go against? \n";
            cout << "1. Hare \n";
            cout << "2. Bee \n";
            cout << "3. Monkey \n";
            cin >> choiceT;
        switch (choiceT)
        {
        case 1:
            cout << "You playing against Hare!" << endl;
            isHare = true;
            break;
        case 2:
            cout << "You are going against Bee!" << endl;
            isBee = true;
            break;
        case 3:
            cout << "You are going against Monkey!" << endl;
            isMonkey = true;
            break;
        default:
            cout << "You didn't pick a valid opponent!" << endl;
            return 0;
        }
            isTortoise = true;
            player = 0;
            break;

        case 2:
            cout << "Hare!" << endl;
            cout << "Who would you like to go against? \n";
            cout << "1. Tortoise \n";
            cout << "2. Bee \n";
            cout << "3. Monkey \n";
            cin >> choiceH;
        switch (choiceH)
        {
        case 1:
            cout << "You playing against Tortoise!" << endl;
            isTortoise = true;
            break;
        case 2:
            cout << "You are going against Bee!" << endl;
            isBee = true;
            break;
        case 3:
            cout << "You are going against Monkey!" << endl;
            isMonkey = true;
            break;
        default:
            cout << "You didn't pick a valid opponent!" << endl;
            return 0;
        }
            isHare = true;
            player = 1;
            break;

        case 3:
            cout << "Bee!" << endl;
            cout << "Who would you like to go against? \n";
            cout << "1. Tortoise \n";
            cout << "2. Hare \n";
            cout << "3. Monkey \n";
            cin >> choiceB;
        switch (choiceB)
        {
        case 1:
            cout << "You playing against Tortoise!" << endl;
            isTortoise = true;
            break;
        case 2:
            cout << "You are going against Hare!" << endl;
            isHare = true;
            break;
        case 3:
            cout << "You are going against Monkey!" << endl;
            isMonkey = true;
            break;
        default:
            cout << "You didn't pick a valid opponent!" << endl;
            return 0;
        }
            isBee = true;
            player = 2;
            break;

        case 4:
            cout << "Monkey!" << endl;
            cout << "Who would you like to go against? \n";
            cout << "1. Tortoise \n";
            cout << "2. Hare \n";
            cout << "3. Bee \n";
            cin >> choiceM;
        switch (choiceM)
        {
        case 1:
            cout << "You are going against Tortoise!" << endl;
            isTortoise = true;
            break;
        case 2:
            cout << "You playing against Hare!" << endl;
            isHare = true;
            break;
        case 3:
            cout << "You are going against Bee!" << endl;
            isBee = true;
            break;
        default:
            cout << "You didn't pick a valid opponent!" << endl;
            return 0;
        }
            isMonkey = true;
            player = 3;
            break;
        default:
            cout << choice << endl;
            cout<<"You didn't pick a valid animal!" << endl;
            return 0;
    }
    //! random number seed generator
    srand(time(0));
    cout << "On your mark, get set" << endl << "Bang!" << endl;
    cout << endl << "and they're off !!!" << endl;



    if(player == 0)
    {
        if(isHare == true)
        {
            isHare = false;
            while(tortoise != RACE_END && hare != RACE_END)
            {
                compChar = 'H';
                moveTortoise(&tortoise);
                moveHare(&hare);
                printCurrentPosition(&tortoise,&hare,&compChar);
                ++timer;
            }
        }
        else if (isBee == true)
        {
            isBee = false;
            while(tortoise != RACE_END && bee != RACE_END)
            {
                compChar = 'B';
                moveTortoise(&tortoise);
                moveBee(&bee);
                printCurrentPosition(&tortoise,&bee,&compChar);
                ++timer;
            }

        }
        else if (isMonkey == true)
        {
            isMonkey = false;
            while(tortoise != RACE_END && monkey != RACE_END)
            {
                compChar = 'M';
                moveTortoise(&tortoise);
                moveMonkey(&monkey);
                printCurrentPosition(&tortoise,&monkey,&compChar);
                ++timer;
            }
        }

    }

    if(player == 1)
    {
        if(isTortoise == true)
        {
            isTortoise = false;
            while(hare != RACE_END && tortoise != RACE_END)
            {
                compChar = 'T';
                moveHare(&hare);
                moveTortoise(&tortoise);
                printCurrentPosition(&hare,&tortoise,&compChar);
                ++timer;
            }
        }
        else if (isBee == true)
        {
            isBee = false;
            while(hare != RACE_END && bee != RACE_END)
            {
                compChar = 'B';
                moveHare(&hare);
                moveBee(&bee);
                printCurrentPosition(&hare,&bee,&compChar);
                ++timer;
            }

        }
        else if (isMonkey == true)
        {
            isMonkey = false;
            while(hare != RACE_END && monkey != RACE_END)
            {
                compChar = 'M';
                moveHare(&hare);
                moveMonkey(&monkey);
                printCurrentPosition(&hare,&monkey,&compChar);
                ++timer;
            }
        }

    }

    if(player == 2)
    {
        if(isTortoise == true)
        {
            isTortoise = false;
            while(bee != RACE_END && tortoise != RACE_END)
            {
                compChar = 'T';
                moveBee(&bee);
                moveTortoise(&tortoise);
                printCurrentPosition(&bee,&tortoise,&compChar);
                ++timer;
            }
        }
        else if (isHare == true)
        {
            isHare = false;
            while(bee != RACE_END && hare != RACE_END)
            {
                compChar = 'H';
                moveMonkey(&bee);
                moveHare(&hare);
                printCurrentPosition(&bee,&hare,&compChar);
                ++timer;
            }

        }
        else if (isMonkey == true)
        {
            isMonkey = false;
            while(bee != RACE_END && monkey != RACE_END)
            {
                compChar = 'M';
                moveBee(&bee);
                moveMonkey(&monkey);
                printCurrentPosition(&bee,&monkey,&compChar);
                ++timer;
            }
        }

    }
    if(player == 3)
    {
        if(isTortoise == true)
        {
            isTortoise = false;
            while(monkey != RACE_END && tortoise != RACE_END)
            {
                compChar = 'T';
                moveMonkey(&monkey);
                moveTortoise(&tortoise);
                printCurrentPosition(&monkey,&tortoise,&compChar);
                ++timer;
            }
        }
        else if (isHare == true)
        {
            isHare = false;
            while(monkey != RACE_END && hare != RACE_END)
            {
                compChar = 'H';
                moveMonkey(&monkey);
                moveHare(&hare);
                printCurrentPosition(&monkey,&hare,&compChar);
                ++timer;
            }

        }
        else if (isBee == true)
        {
            isBee = false;
            while(monkey != RACE_END && bee != RACE_END)
            {
                compChar = 'B';
                moveMonkey(&monkey);
                moveBee(&bee);
                printCurrentPosition(&monkey,&bee,&compChar);
                ++timer;
            }
        }

    }
/*
    //!Control the race process
    while(tortoise != RACE_END && hare != RACE_END && bee != RACE_END && monkey != RACE_END)
    {
        //! call the moveTortoise function
        moveTortoise(&tortoise);

        //! call the moveHare function
        moveHare(&hare);

        moveBee(&bee);
        moveMonkey(&monkey);

        //! call the prinCurrentPosition Function
        printCurrentPosition(&tortoise,&hare,);
        ++timer;
        //cout << endl;
        cout << timer;
    }*/
    //! determine the winner
    if(tortoise >= hare && bee && monkey)
    {
        if(isTortoise == false)
        {
            cout << endl << "Tortoise WINS!" << endl;
        }
        else
        {
            cout << endl << "Player's Tortoise WINS!" << endl;
        }
    }
    else if(hare >= tortoise && bee && monkey)
    {
        if(isHare == false)
        {
            cout << endl << "Hare WINS!" << endl;
        }
        else
        {
            cout << endl << "Player's Hare WINS!" << endl;
        }
    }
    else if(bee >= tortoise&&hare&&monkey)
    {
        if(isBee == false)
        {
           cout << endl << "Bee WINS!" << endl;
        }
        else
        {
            cout << endl << "Player's Bee WINS!" << endl;
        }
    }
    else if(monkey >= tortoise&&hare&&bee)
    {
        if(isMonkey == false)
        {
            cout << endl << "Monkey WINS!" << endl;
        }

        {
            cout << endl << "Player's Monkey WINS!" << endl;
        }
    }

    system("PAUSE");
    return 0;
}
void moveTortoise(int *turtlePtr)
{
    //! random number from 1 to 10
    int x = 1+ rand() % 10;
    //! determine which move to make
    //! x -> from 1 to 5 -> fast plod -> move 3 (right)
    if(x >= 1 && x <= 5)
    {
    *turtlePtr += 3;
    }
    //! x -> from 6 to 7 -> slip -> move 6 (left)
    else if(x >= 6 && x <= 6)
    {
    *turtlePtr -= 9;
    }
    //! x -> from 8 to 10 -> fast plod -> move 1 (right)
    else if(x >= 8 && x <= 10)
    {
    ++(*turtlePtr);
    }

    //! write statements that test if tortoise is before
    //! the starting line or has finished the race
    if(*turtlePtr < 1)
    {
        *turtlePtr = 1;
    }
    else if (*turtlePtr > RACE_END)
    {
        *turtlePtr = RACE_END;
    }
    //cout << *turtlePtr << endl;
}
void moveBee(int *beePtr)
{
    int z = 1+ rand() % 10;

    if (z == 1)
    {
        *beePtr += 1;
    } else if (z == 2)
    {
        *beePtr += 2;
    } else if (z == 3)
    {
        *beePtr += 3;
    } else if (z == 4)
    {
        *beePtr += 4;
    } else if (z == 5)
    {
        *beePtr += 5;
    } else if (z == 6)
    {
        *beePtr += 6;
    } else if (z == 7)
    {
        *beePtr += 7;
    } else if (z == 8)
    {
        *beePtr += 8;
    } else if (z == 9)
    {
        *beePtr += 9;
    } else if (z == 10)
    {
        *beePtr += 10;
    }
}
void moveMonkey(int *monkeyPtr)
{
    int v = 1+ rand() % 10;

    if(v >= 1 && v <= 5)
    {
        *monkeyPtr += 8;
    } else if (v >= 6 && v <= 9)
    {
        *monkeyPtr -= 1;
    } else if(v = 10)
    {
        *monkeyPtr += 2;
    }
}
void moveHare(int *harePtr)
{
    int y = 1+ rand() % 10;

    if(y == 1 || y == 2)
    {
    *harePtr += 0;
    }
    else if(y == 3 || y == 4)
    {
    *harePtr += 9;
    }
    else if(y = 5)
    {
    *harePtr -= 12;
    }
    else if (y >= 6 && y <= 8)
    {
    ++(*harePtr);
    }
    else if(y == 9 || y == 10)
    {
    *harePtr += 2;
    }

    if(*harePtr < 1)
    {
        *harePtr = 1;
    }
    else if (*harePtr > RACE_END)
    {
        *harePtr = RACE_END;
    }
    //cout << *harePtr << endl;
}
void printCurrentPosition(int *playerPtr, int*compPtr, char*compChar)
{
    if(*playerPtr == *compPtr)
    {
        cout << setw(*playerPtr) << "Ouch!";
    } else if (*playerPtr < *compPtr)
    {
        cout <<setw(*playerPtr) <<  "P" << setw(*compPtr - *playerPtr) << compChar;
    } else
    {
        cout <<setw(*playerPtr) <<  compChar << setw(*playerPtr - *compPtr) << "P";
    }
    cout << endl;




/*    for(int x = 0; x < *bunnyPtr; x++)
    {
        cout << *bunnyPtr;
    }
    cout << "H";

   // system ("PAUSE");

    for(int x = 0; x < *snapperPtr; x++)
    {
        cout << " ";
    }
    cout << "T";
    cout << "Endl";
    if (*bunnyPtr == *snapperPtr)
    {
        cout << "Ouch!!";
    }*/
}
