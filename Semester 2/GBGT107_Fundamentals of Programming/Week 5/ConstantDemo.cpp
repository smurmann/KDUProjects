#include <iostream>
#include "windows.h">
using namespace std;

int main()
{
    //! declare the constant
    const int MAX_HEALTH = 100;

    int health = 50;

    cout << "Health: "<< health << endl;

    //MAX_HEALTH = 0;

    health = MAX_HEALTH;

    cout << "Health: "<< health << endl;

    system("PAUSE");
}
