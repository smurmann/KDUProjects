﻿using UnityEngine;
using System.Collections;

public class SpawnerScript : MonoBehaviour {

	public enum Lane
	{
		Lane_1 = 0,
		Lane_2,
		Lane_3,
		Lane_4,
		Lane_5,
		Lane_6
	}

	public Lane lane = Lane.Lane_1;

	//Spawning stuff
	public bool enableSpawning = false;
	public float spawnDelayTimer = 0.0f;
	public float spawnDelayDuration = 1.0f;
	public int currentNoteIndex = 0;
	public GameObject[]notes;

	public bool hasStarted = false;

	public float hasStartedTimer = 0.0f;
	public float hasStartedTime = 12.0f;

	public float noteSpacing = 1.35f;
	public float noteOffsetX = 2.42f;

	public int spawnedNotes = 0;







	// Use this for initialization
	void Start () 
	{

	}

	public void Spawn(int laneNumber)
	{
		if(laneNumber == 1)
		{
			SpawnConvert(Lane.Lane_1);
		}
		
		if(laneNumber == 2)
		{
			SpawnConvert(Lane.Lane_2);
		}
		
		if(laneNumber == 3)
		{
			SpawnConvert(Lane.Lane_3);
		}
		
		if(laneNumber == 4)
		{
			SpawnConvert(Lane.Lane_4);
		}
		
		if(laneNumber == 5)
		{
			SpawnConvert(Lane.Lane_5);
		}
		
		if(laneNumber == 6)
		{
			SpawnConvert(Lane.Lane_6);
		}
	}

	public void SpawnConvert(Lane lane)
	{

		spawnedNotes++;


		if(lane == Lane.Lane_1)
		{
			Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
			//Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*2 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
			//Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*3 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
			//Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*4 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
			//Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*5 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
			//Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*6 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
		}
		
		if(lane == Lane.Lane_2)
		{
			Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*2 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
		}

		if(lane == Lane.Lane_3)
		{
			Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*3 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
		}

		if(lane == Lane.Lane_4)
		{
			Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*4 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
		}

		if(lane == Lane.Lane_5)
		{
			Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*5 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
		}

		if(lane == Lane.Lane_6)
		{
			Instantiate (notes [currentNoteIndex], (transform.position + new Vector3(noteSpacing*6 + noteOffsetX, 0.0f, 0.0f)), Quaternion.identity);
		}

		//GameObject.Find ("Note(Clone)").GetComponent<NoteScript>().ChangeSprite();



		notes [0].GetComponent<NoteScript> ().ChooseLane ();
		enableSpawning = false;
		currentNoteIndex = 0;
		if (notes.Length <= 0)
		{
			enableSpawning = false;
			Debug.LogError("You fked up somewhere, NoobieSean!");
		}

		//actual spawn of the note.
		//Instantiate (notes [currentNoteIndex], transform.position, transform.rotation);
	}
	//To delay inital spawn
	public void StartSpawning()
	{
		if(hasStarted == false)
		{
			hasStartedTimer += Time.deltaTime;
		}

		if((hasStartedTimer > hasStartedTime) && !hasStarted)
		{
			enableSpawning = true;
			hasStarted = true;
		}
	}
	// Update is called once per frame
	void Update () 
	{
		/*StartSpawning ();

		if (enableSpawning)
		{
			Spawn(Lane.Lane_1);
			enableSpawning = false;
		}
		if(!enableSpawning && hasStarted)
		{
			spawnDelayTimer += Time.deltaTime;
			if(spawnDelayTimer > spawnDelayDuration)
			{
				spawnDelayTimer = 0.0f;
				enableSpawning = true;
			}
		}
		*/


	}
}
