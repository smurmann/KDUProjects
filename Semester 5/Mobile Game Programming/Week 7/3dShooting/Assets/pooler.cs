﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class pooler : MonoBehaviour {

	public static pooler m_instance;

	public GameObject m_PooledObject;
	public int m_initialPoolSize;

	public List<GameObject> m_Pool;
	void Awake () {
		m_instance = this;

		DontDestroyOnLoad (m_instance.gameObject);
	}

	void Start()
	{
		m_Pool = new List<GameObject> ();
		for (int i=0; i<m_initialPoolSize; i++) {
			GameObject newObj = Instantiate(m_PooledObject) as GameObject;
			newObj.SetActive(false);
			m_Pool.Add (newObj);
		}
	}

	public GameObject newObject()
	{
		for (int i =0; i<m_Pool.Count; i++) {
			if(m_Pool[i].activeInHierarchy == false)
			{
				return m_Pool[i];
			}

		}
		return null;
		// run out of pooled objects,grow it here
//		GameObject newObj = Instantiate(m_PooledObject) as GameObject;
//		newObj.SetActive(false);
//		m_Pool.Add (newObj);
//		return newObj;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
