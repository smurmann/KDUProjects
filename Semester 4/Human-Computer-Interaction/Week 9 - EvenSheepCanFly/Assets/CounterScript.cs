﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CounterScript : MonoBehaviour {
    public GameObject sheep;
    int counter;
    Text text;

	// Use this for initialization
	void Start () {

        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        int counter = sheep.GetComponent<MooScript>().clicks;
        counter = 100 - counter;
        if(counter <= 0)
        {
            counter = 0;
        }
        text.text = counter.ToString();

	
	}
}
