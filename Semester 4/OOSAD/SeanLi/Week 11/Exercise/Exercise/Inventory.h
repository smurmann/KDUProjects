#pragma once
#include <vector>
#include <iostream>
#include "Weapons.h"

using namespace std;

enum  Weapon_TYPE { SWORD = 0, SHIELD, MASK, GUN };

class Inventory
{
public:
	vector<Weapons> CategorySword;
	vector<Weapons> CategoryShield;
	vector<Weapons> CategoryMask;
	vector<Weapons> CategoryGun;
	//void addWeapon(Weapons w, Weapon_TYPE);
	void addWeapon(Weapons *w, Weapon_TYPE);
	void removeWeapon(string name);
	void printInventory();
};