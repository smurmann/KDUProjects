//============================================================================
// Name        : 0109835_Sean-Li_Murmann_OOPExerciseWk4.cpp
// Author      : Sean-Li Murmann
// Version     : 1.0
// Copyright   : -
// Description : OOPExercise Week 4
//============================================================================

#include <iostream>
using namespace std;

class GameCharacter
{
protected:
	int totalHealth;
	int meleeDamage;

public:
	GameCharacter();
	void attack() const;
	void health() const;
};
GameCharacter::GameCharacter() :
		totalHealth(100),
		meleeDamage(10)
		{}
void GameCharacter::attack() const
{
	cout << "Basic attack deals " << meleeDamage << " damage!" << endl;
	cout << "Public access test for GameCharacter class" << endl;
}
void GameCharacter::health() const
{
	cout << "This character has " << totalHealth <<" total health." << endl;
}

class Hero : public GameCharacter
{
public:
	float heroSpecialAttackMultiplier;
	float heroHealthMultiplier;

	Hero();
	void heroSpecialAttack();
	void heroHealth();
	int specialAttackValue();
};
Hero::Hero():

		heroSpecialAttackMultiplier(2),
		heroHealthMultiplier(2.5)
{}
void Hero::heroSpecialAttack()
{
	cout << "Melee attack deals " << (heroSpecialAttackMultiplier*meleeDamage);
	cout << " damage!" << endl;
}
void Hero::heroHealth()
{
	cout << "Hero has " << (totalHealth*heroHealthMultiplier) << " total health.";
	cout << endl;
}

class Human : public GameCharacter
{
public:
	float humanSpecialAttackMultiplier;
	float humanHealthMultiplier;

	Human();
	void humanSpecialAttack();
	void humanHealth();
	int specialAttackValue();
};
Human::Human():
		humanSpecialAttackMultiplier(0.5),
		humanHealthMultiplier(0.5)
{}
void Human::humanSpecialAttack()
{
	cout << "Human poke deals " << (humanSpecialAttackMultiplier*meleeDamage);
	cout << " damage!" << endl;
}
void Human::humanHealth()
{
	cout << "Human has " << (totalHealth*humanHealthMultiplier) << " total health.";
}

//! Enemy Classes
class Enemy : public GameCharacter
{
public:
	float enemySpecialAttackMultiplier;
	float enemyHealthMultiplier;
	int enemyAttackValue;

	Enemy();
	void enemyAttack() const;
	void enemyHealth() const;
};
Enemy::Enemy() :
		enemySpecialAttackMultiplier(2.5),
		enemyHealthMultiplier(0.75),
		enemyAttackValue(25)
{}
void Enemy::enemyAttack() const
{
	cout<<"Enemy deals " <<(enemySpecialAttackMultiplier*meleeDamage)<< " damage!"<<endl;
}
void Enemy::enemyHealth() const
{
	cout<<"Enemy has " << (totalHealth*enemyHealthMultiplier) << " health." << endl;
}

class Boss : public Enemy
{
public:
	int bossDamageMultiplier;
	float bossHealthMultiplier;

	Boss();
	void SpecialAttack();
	int SpecialAttackValue();
	void bossHealth() const;
};
Boss::Boss():
	bossDamageMultiplier(3),
	bossHealthMultiplier(5)
{}
void Boss::SpecialAttack()
{
	cout<<"Special Attack deals "<<(bossDamageMultiplier*enemyAttackValue);
	cout<<" damage!"<<endl;
}
void Boss::bossHealth() const
{
	cout<<"Boss has " << (totalHealth*bossHealthMultiplier) << " health." << endl;
}
int Boss::SpecialAttackValue()

{
	return bossDamageMultiplier*enemyAttackValue;
}

class FinalBoss : public Boss
{
public:
	int mSpecialAttackMultiplier;
	float finalBossHealthMultiplier;
	FinalBoss();
	void MegaAttack();
	void finalBossHealth() const;
};
FinalBoss::FinalBoss():
	mSpecialAttackMultiplier(10),
	finalBossHealthMultiplier(7.5)
{}
void FinalBoss::MegaAttack()
{
	cout<<"Mega Attack deals "<<mSpecialAttackMultiplier*SpecialAttackValue();
	cout<<" damage!"<<endl;
}
void FinalBoss::finalBossHealth() const
{
	cout<<"FinalBoss has " << (totalHealth*finalBossHealthMultiplier) << " health." << endl;
}


int main()
{
	cout << "Class test: GameCharacter" << endl;
	GameCharacter gameCharacter1;
	gameCharacter1.attack();
	gameCharacter1.health();
	cout << endl;

	cout << "Spawning Hero" << endl;
	Hero hero1;
	hero1.attack();
	hero1.heroSpecialAttack();
	hero1.heroHealth();
	cout << endl;

	cout <<"Spawning Human" << endl;
	Human human1;
	human1.attack();
	human1.humanSpecialAttack();
	human1.humanHealth();
	cout << endl << endl;

	cout<<"Spawning Enemy"<< endl;
	Enemy enemy1;
	enemy1.attack();
	enemy1.enemyAttack();
	enemy1.enemyHealth();
	cout << endl;

	cout << "Spawning Boss" << endl;
	Boss boss1;
	boss1.attack();
	boss1.enemyAttack();
	boss1.SpecialAttack();
	boss1.bossHealth();
	cout << endl;

	cout << "Spawning FinalBoss" << endl;
	FinalBoss finalboss1;
	boss1.attack();
	finalboss1.enemyAttack();
	finalboss1.SpecialAttack();
	finalboss1.MegaAttack();
	finalboss1.finalBossHealth();
	cout << endl;
return 0;
}
