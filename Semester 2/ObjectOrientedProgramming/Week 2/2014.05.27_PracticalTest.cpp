#include <iostream>
#include <cmath>

using namespace std;

class Point
{
private:
    float x;
    float y;
public:
    Point(int x, int y)
    {
        this->x = x;
        this->y = y;
    }
    float distanceFromOrigin(Point *p1, Point *p2);

};
float Point::distanceFromOrigin(Point *p1, Point *p2)
{
    //root((x2-x1)^2+(y2-y1)^2)
    float result =
    sqrt(pow((p2->x-p1->x),2)+pow((p2->y-p1->y),2));
    return result;
}

int main()
{
    Point* p1 = new Point(4,6);
    Point* p2 = new Point(8,9);
    float distance = p1->distanceFromOrigin(p1,p2);
    cout<<"The distance between 2 points is: "<<distance<<endl;
}
