﻿using UnityEngine;
using System.Collections.Generic;

public class MultitouchTest: MonoBehaviour
{
	public GameObject mTouchSpritePrefab;
	Dictionary<int, Transform> mTouchSprites = new Dictionary<int, Transform>();

    int mTouchCount;
    TouchManager.State mFirstTouch = new TouchManager.State();
    TouchManager.State mSecondTouch = new TouchManager.State();
    float mLastDistance;
    private Vector2 mTouchedDown2ndTo1stVector;
    private Quaternion mTouchedDownRoataion;
    GameObject boat;

    void OnEnable()
	{
		TouchManager.instance.EvtOnTouchDown += OnTouchDown;
		TouchManager.instance.EvtOnTouchDrag += OnTouchDrag;
		TouchManager.instance.EvtOnTouchUp += OnTouchUp;

         boat = GameObject.Find("player");

        mTouchCount = 0;
        mLastDistance = -1.0f;
	}
	
	void OnDisable()
	{
		TouchManager.instance.EvtOnTouchDown -= OnTouchDown;
		TouchManager.instance.EvtOnTouchDrag -= OnTouchDrag;
		TouchManager.instance.EvtOnTouchUp -= OnTouchUp;
	}

    void checkChangeOfDistance()
    {
        //Debug.Log("this is called");
        Vector2 vec = mSecondTouch.position - mFirstTouch.position;
        float distance = vec.magnitude;

        float angle =  Vector2.Angle(mFirstTouch.position, mSecondTouch.position);
        float prevTurn = Vector2.Angle(mFirstTouch.position - mFirstTouch.deltaPosition,
                                        mSecondTouch.position - mSecondTouch.deltaPosition);
        //float turnAngleDelta = Mathf.DeltaAngle(prevTurn, angle);
        //Debug.Log(turnAngleDelta);
        ////Debug.Log(prevTurn);

        //if(turnAngleDelta != 0)
        //{
        //    GameObject boat = GameObject.Find("player");
        //    float scale = boat.transform.localEulerAngles.z;
        //    scale += turnAngleDelta * 0.01f ;
        //    //boat.transform.localEulerAngles = new Vector3(0.0f,0.0f,scale);
        //    boat.transform.Rotate(0, 0, -turnAngleDelta);
        //}
        float distanceDelta = distance - mLastDistance;
        mLastDistance = distance;

        if (distanceDelta != 0.0f)
        {
            //GameObject boat = GameObject.Find("player");
            float scale = boat.transform.localScale.x;
            scale += distanceDelta * 0.01f;
            boat.transform.localScale = new Vector3(scale, scale, 1.0f);

            Vector2 current2ndTo1stVector = mSecondTouch.position - mFirstTouch.position;
            Quaternion rotationDifference = new Quaternion();
            rotationDifference.SetFromToRotation(mTouchedDown2ndTo1stVector, current2ndTo1stVector);
            boat.transform.rotation = rotationDifference * boat.transform.rotation;

        }
    }


	void OnTouchDown(TouchManager.State state, ref bool processed)
	{
        if(mTouchCount == 0)
        {
            mFirstTouch.copyFrom(state);
        }
        else if (mTouchCount == 1)
        {
            mSecondTouch.copyFrom(state);
            mTouchedDown2ndTo1stVector = mSecondTouch.position - mFirstTouch.position;
            mTouchedDownRoataion = boat.transform.rotation;
            Vector2 vec = mSecondTouch.position - mFirstTouch.position;
            mLastDistance = vec.magnitude;
        }
		GameObject sprite = (GameObject)Instantiate(mTouchSpritePrefab);
		Transform spriteTrans = sprite.transform;
	
		Vector3 screenPos = state.position;
		Vector3 thePos = Camera.main.ScreenToWorldPoint(screenPos);
		thePos.z = 0.0f;
		spriteTrans.position = thePos;

		mTouchSprites[state.fingerID] = spriteTrans;

        mTouchCount++;
	}
	
	void OnTouchDrag(TouchManager.State state, ref bool processed)
	{
        Debug.Log(mTouchCount);
        if(mTouchCount >= 2)
        {
            if(state.fingerID == mFirstTouch.fingerID)
            {
                mFirstTouch.copyFrom(state);
                checkChangeOfDistance();
            }
            else if(state.fingerID == mSecondTouch.fingerID)
            {
                mSecondTouch.copyFrom(state);
                checkChangeOfDistance();
            }
        }
		Transform spriteTrans = mTouchSprites[state.fingerID];
		Vector3 screenPos = state.position;
		Vector3 thePos = Camera.main.ScreenToWorldPoint(screenPos);
		thePos.z = 0.0f;
		spriteTrans.position = thePos;
	}
	
	void OnTouchUp(TouchManager.State state, ref bool processed)
	{
		Transform spriteTrans = mTouchSprites[state.fingerID];
		Destroy(spriteTrans.gameObject);
		mTouchSprites.Remove(state.fingerID);

        mTouchCount--;
	}
}
