﻿using UnityEngine;
using System.Collections;

public class KillAfter : MonoBehaviour {

    private float time;
    private float timer = 1.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        time += Time.deltaTime;

        if(time > timer)
        {
            Destroy(this.gameObject);
        }
	
	}
}
