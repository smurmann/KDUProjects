﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooling : MonoBehaviour
{

    public static ObjectPooling m_instance;

    public GameObject m_poolObject;
    public int m_initialPoolSize;

    public List<GameObject> m_pool;
    // Use this for initialization

    void Awake()
    {
        // set the singelton instance before the start;
        m_instance = this;

        //optional, call this if you dont want to pooler to be destroyed
        DontDestroyOnLoad(m_instance.gameObject);

    }
    void Start()
    {
        m_pool = new List<GameObject>();
        for (int i = 0; i < m_initialPoolSize; i++)
        {
            GameObject newObj = Instantiate(m_poolObject) as GameObject;
            newObj.SetActive(false);
            m_pool.Add(newObj);
        }
    }

    public GameObject NewObject(Vector3 position, Quaternion rotation)
    {
        for(int i = 0; i< m_pool.Count; i++)
        {
            if(m_pool[i].activeInHierarchy == false)
            {
                return returnObject(m_pool[i],position,rotation);
            }
        }

        // whe  it reaches the code here, it means it has run out of pooled objects, grow it here
        GameObject newObj = Instantiate(m_poolObject) as GameObject;
        newObj.SetActive(false);
        m_pool.Add(newObj);
        return returnObject(newObj, position, rotation);

    }

    public GameObject returnObject(GameObject temp, Vector3 position, Quaternion rotation)
    {

        temp.transform.position = position;
        temp.transform.rotation = rotation;
        return temp;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
