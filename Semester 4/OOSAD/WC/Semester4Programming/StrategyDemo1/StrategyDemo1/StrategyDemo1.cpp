// StrategyDemo1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "windows.h"

using namespace std;

class SortBehavior
{
public:
	virtual void sort() = 0;

};
class Merge : public SortBehavior
{
public: 
	void sort()
	{
		cout << "Merge Sort " << endl;
	}
};

class Quick : public SortBehavior
{
public:
	void sort()
	{
		cout << "Quick Sort " << endl;
	}
};

class Bubble : public SortBehavior
{
public:
	void sort()
	{
		cout << "Bubble Sort " << endl;
	}
};

class SearchBehavior
{
public:
	virtual void search() = 0;
};

class BinaryTree : public SearchBehavior
{
public:
	void search()
	{
		cout << "Binary Tree Search " << endl;
	}
};

class HashTable : public SearchBehavior
{
public:
	void search()
	{
		cout << "HashTable Search " << endl;
	}
};

class Sequantial : public SearchBehavior
{
public:
	void search()
	{
		cout << "Sequantial Search " << endl;
	}
};

class Collection
{
private:
	SortBehavior *mSort;
	SearchBehavior *mSearch;
public:
	Collection(){}
	void setSort(SortBehavior *s)
	{
		mSort = s;
	}
	void setSearch(SearchBehavior * s)
	{
		mSearch = s;
	}
	void sort()
	{
		mSort->sort();
	}
	void search()
	{
		mSearch->search();
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	Merge merge;
	Quick quick;
	Bubble bubble;

	BinaryTree bt;
	Sequantial sequential;
	HashTable ht;

	Collection colA;
	colA.setSort(&merge);
	colA.setSearch(&sequential);
	colA.sort();
	colA.search();
	cout << endl;
	
	system("PAUSE");
	return 0;
}

