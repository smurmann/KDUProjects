//! promt user to enter answer 1 -> boolean value
//! promt user to enter answer 2 -> boolean value
//! pass the user inputs to if else statement to
//! determine the case value
//! the result from the if else statement (case value)
//! will pass to switch case
//! the switch case will print out the player profile
//! according to the pass in value

#include <iostream>
#include "windows.h"

using namespace std;




int main()
{
    //! Variable required
    //! if else statement
    //! switch case
    int casenumber = 0;
    bool meleeYN = true;
    bool magicYN = true;
    bool answer1 = true;
    bool answer2 = true;

    cout << "1 -> true ; 0 -> false\n";
    cout << "Do you like: Melee weapons?\n";
    cin >> meleeYN;
    cout << endl;
    cout << "Do you like magic? \n";
    cin >> magicYN;
    cout << endl;

    if(meleeYN == true && magicYN == true)
    {
        casenumber = 1;
        cout << "Casenumber value: "<<casenumber;
        cout << endl;
    }
    else if(meleeYN == true && magicYN == false)
    {
        casenumber = 2;
        cout << "Casenumber value: "<<casenumber;
        cout << endl;
    }
    else if(meleeYN == false && magicYN == true)
    {
        casenumber = 3;
        cout << "Casenumber value: "<<casenumber;
        cout << endl;
    }
    else if(meleeYN == false && magicYN == false)
    {
        casenumber = 4;
        cout << "Casenumber value: "<<casenumber;
        cout << endl;
    }
    else
    {
        casenumber = 5;
        cout << "Casenumber value: "<<casenumber;
        cout << endl;
    }

    switch(casenumber)
    {
    case 1:
        cout << "You might want to try Paladin. \n\n";
        break;
    case 2:
        cout << "You might want to try Warrior. \n\n";
        break;
    case 3:
        cout << "You might want to try Mage. \n\n";
        break;
    case 4:
        cout << "You might want to try Archer. \n\n";
        break;
    case 5:
        cout << "Something went wrong. \n\n";
        break;

    }


    system("PAUSE");
    return 0;
}
