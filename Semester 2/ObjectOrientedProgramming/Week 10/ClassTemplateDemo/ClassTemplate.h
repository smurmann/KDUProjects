#include <iostream>
using namespace std;

template <class T>
class mypair
{
    //! define any array without specifying any data type
    T values[2];
public:
    //!constructor / revceiving two objects (without specifying any type)
    mypair (T first, T second)
    {
        values[0] = first;
        values[1] = second;

    }

    void Display()
    {
        cout << values[0] << endl;
        cout << values[1] << endl;
    }
};
