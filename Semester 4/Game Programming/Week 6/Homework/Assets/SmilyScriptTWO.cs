using UnityEngine;
using System.Collections;

public class SmilyScriptTWO : MonoBehaviour {

	public float speed = 0.1f;
	public float negativespeed = -0.1f;
	float dirX = 0.1f;
	float dirY = 0.1f;

	bool dirXbool = true;
	bool dirYBool = true;

	private float m_smileyHalfW;
	private float m_smileyHalfH;

	private bool randomBool;

	public GameObject HalfSmiley;

	Vector3 smileyPos;



	private SpriteRenderer m_spriteRenderer;

	void  randomBoolean ()
	{
		if (Random.value >= 0.5)
		{
			randomBool = true;
		}
		else
		{
		randomBool = false;
		}

		//Debug.Log(Random.value);
	}

	// Use this for initialization
	void Start () {
		randomBoolean();

		m_spriteRenderer = gameObject.renderer as SpriteRenderer;
		Sprite mySprite = m_spriteRenderer.sprite;

		m_smileyHalfW = mySprite.bounds.size.x * 0.5f;
		m_smileyHalfH = mySprite.bounds.size.y * 0.5f;


		if(randomBool)
		{
			dirX = speed;
			dirXbool = true;
		}
		else if (!randomBool)
		{
			dirX = negativespeed;
			dirXbool = false;
		}

		randomBoolean();
		if(randomBool)
		{
			dirY = speed;
			dirYBool = true;
		}
		else if (!randomBool)
		{
			dirY = negativespeed;
			dirYBool = false;
		}


	
	}	
	// Update is called once per frame
	void Update () 
	{
		smileyPos = transform.position;

		Vector3 WorldSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0.0f));
		float halfWidth = WorldSize.x;
		float halfHeight = WorldSize.y;
		
		
		
		if (transform.position.x >= halfWidth - m_smileyHalfW)
		{
			//set speed negative
			dirX = -dirX;
			dirXbool = false;		
		}
		
		if (transform.position.y >= halfHeight - m_smileyHalfH)
		{
			//set speed negative 
			dirYBool = false;
			dirY = -dirY;
		}
		
		if (transform.position.y <= -halfHeight + m_smileyHalfH)
		{
			dirYBool = true;
			dirY = -dirY;
		}
		if (transform.position.x <= -halfWidth + m_smileyHalfW)
		{
			dirXbool = true;
			dirX = -dirX;
		}
		
		transform.Translate(dirX,dirY,0 * Time.deltaTime);



	}
	

	void OnCollisionEnter2D(Collision2D coll)
	{
		Debug.Log ("collided");
		//GameObject newSmiley2 = Instantiate(HalfSmiley, smileyPos, Quaternion.identity) as GameObject;
		//smileyPos.x += m_smileyHalfW;
		//GameObject newSmiley = Instantiate(HalfSmiley, smileyPos, Quaternion.identity) as GameObject;
		//Destroy(this.gameObject);

	}

}

