#include <iostream>
#include "windows.h"
using namespace std;

//Pass by Value
//! funtion declaration
 void Addition(int a, int b)
 {
 	cout<<"The value of a + b: "<<(a+b)<<endl;
 }
//! subtraction
 void Substraction(int a, int b)
 {
 	cout<<"The value of a - b: "<<(a-b)<<endl;
 }
 
//! multiplication
 void Multiplication (int a, int b)
 {
 	cout<<"The value of a * b: "<<(a*b)<<endl;
 }

//! division
 void Division (int a, int b)
 {
 	cout<<"The value of a / b: "<<(a/b)<<endl;
 }

void Display()
{
	cout<<"Welcome too my C++ Simple Calculator"<<endl;
}

int GetInput()
{
	int number = 0;
	cin>>number;
	if(number<=0)
	{
		return 1;
	}else
	{
		return number;
	}
}


//!pass by reference using &
void PowerofTwo(int  &x, int &y)
{
	x = x * x;
	y = y * y;
	
	cout<<"The value of a^2 is: "<< x <<endl;
	cout<<"The value of b^2 is: "<< y <<endl;
}

int main()
{
	Display();
//	int no1, no2;
	int no1 = 0, no2 = 0;
	cout<<"The first number: ";
	no1 = GetInput();
	cout<<"The second number: ";
	no2 = GetInput();
	
	/*cout<<"Please enter 2 numbers : "<<endl;
	cin>>no1>>no2;
	void result = Addition(no1, no2);
	cout<<"Addition of your 2 numbers is: "<<
					result<<endl;*/
					
	Addition(no1, no2);
	Substraction(no1, no2);
	Multiplication(no1, no2);
	Division(no1, no2);
	
	PowerofTwo(no1, no2);
	Addition(no1, no2);
	Substraction(no1, no2);	
	Multiplication(no1, no2);
	Division(no1, no2);
	

}

