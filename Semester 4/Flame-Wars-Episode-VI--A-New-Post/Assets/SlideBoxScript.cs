﻿using UnityEngine;
using System.Collections;

public class SlideBoxScript : MonoBehaviour {
    public float slideSpeed;
    Vector3 mypos;
    Vector3 pos2 = new Vector3(0.0f, 1.3f, 0.0f);
    Vector3 pos1 = new Vector3(0.0f, -1.97f, 0.0f);
    Vector3 pos0 = new Vector3(-6.7f, -1.97f, 0.0f);
    public int posState = 2;

    public bool isTwitter = false;
    SendInputOnSpace inputScript;
   // Animator anim;

	// Use this for initialization
	void Start () {
        if(isTwitter)
        {
            pos0 = new Vector3(0f, 0.68f, 0.0f);
            pos1 = new Vector3(0f, 0.25f, 0.0f);
            pos2 = new Vector3(0f, -1.87f, 0.0f);
        }
        inputScript = GameObject.FindGameObjectWithTag("InputBox").GetComponent<SendInputOnSpace>();
        mypos = transform.position;
        //anim = GetComponent<Animator>();
        if (gameObject.tag == "FriendlyBox")
        {
            //Destroy(this.gameObject);
            posState = 1;
        }
        else
        {
            posState = 2;
        }
        //posState = 1;
    
    }
	
	// Update is called once per frame
	void Update () {
        if(posState == 0)
        {
            transform.position = pos0;
        }
        else if(posState == 1)
        {
            transform.position = pos1;
        }
        else if(posState == 2)
        {
            transform.position = pos2;
        }
        else if(posState == 3)
        {
            Destroy(this.gameObject);
        }
       



	}




}
