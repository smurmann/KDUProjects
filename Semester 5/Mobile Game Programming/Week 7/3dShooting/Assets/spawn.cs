﻿using UnityEngine;
using System.Collections;

public class spawn : MonoBehaviour
{

	public GameObject projectilePrefab = null;
	public Color yellow;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
//		if (Input.touchCount > 0) {
//			Touch myTouch = Input.GetTouch (0);
//
//			if (myTouch.phase == TouchPhase.Began) {
//
//				Ray ray = Camera.current.ScreenPointToRay (myTouch.position);
//
//				GameObject newObject = Instantiate (projectilePrefab, ray.origin, Quaternion.identity)as GameObject;
//				Rigidbody newObjectRigidbody = newObject.GetComponent<Rigidbody> ();
//				newObjectRigidbody.velocity = ray.direction * 15.0f;
//			}
//
//		}

		if (Input.touchCount > 0) {
			Touch myTouch = Input.GetTouch (0);
						
			if (myTouch.phase == TouchPhase.Began) {
				Ray ray = Camera.current.ScreenPointToRay (myTouch.position);
				RaycastHit hitInfo;
				if (Physics.Raycast (ray.origin, ray.direction, out hitInfo)) {
					if (hitInfo.collider.gameObject.tag == "Pickable_Obj") {

						projectilePrefab = hitInfo.collider.gameObject;

					}
				}

			} else if (myTouch.phase == TouchPhase.Moved ||myTouch.phase == TouchPhase.Stationary) {
	
				Ray ray = Camera.current.ScreenPointToRay (myTouch.position);
				RaycastHit hitInfo;
				if (Physics.Raycast (ray.origin, ray.direction, out hitInfo)) {
					if (hitInfo.collider.gameObject.tag == "Pickable_Obj") {
					
						if (projectilePrefab != null) {
							ray = Camera.current.ScreenPointToRay (myTouch.position);
							
							Vector3 destPos = ray.origin + ray.direction * 1;
							hitInfo.transform.position = destPos;
						}

						
					}
				}
					
//					

			} 
			else if (myTouch.phase == TouchPhase.Ended) {
				if (projectilePrefab != null) {
					projectilePrefab = null;
					//Rigidbody newObjectRigidbody = newObject.GetComponent<Rigidbody> ();
					//newObjectRigidbody.velocity = ray.direction * 15.0f;
				}
			}


		}

	}

}
