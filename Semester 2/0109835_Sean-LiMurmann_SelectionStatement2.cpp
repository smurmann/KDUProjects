#include <iostream>
#include "windows.h"

using namespace std;

int main()
{
    int grade = 0;
    cout << "Please enter a grade:"<<endl;
    cin >> grade;

    if(grade > 100 || grade < 0)
    {
        cout << "Invalid Input \n";
    }
    else
    if(grade == 100)
    {
        cout << "A+ \n";
    }
    else
    if(grade >= 80)
    {
        cout << "A \n";
    }
    else
    if(grade >= 70)
    {
        cout << "B \n";
    }
    else
    if(grade >= 60)
    {
        cout << "C \n";
    }
    else
    if(grade >= 50)
    {
        cout << "D \n";
    }
    else
    {
        cout << "F \n";
    }

    system("PAUSE");
    return 0;
}
