#include "week4.h"
#include <typeinfo>
#include "Windows.h"


void Week4()
{
	 //Code1();
	 //Code2();
	Code3STL();
}

//]RTTI RUN TIME TYPE INFORMATION
void Code1()
{
	Manager manager;
	SoundManager soundManager1;
	SoundManager soundManager2;
	ControlManager* controlManager1 = new ControlManager();
	ControlManager controlManager2;

	Manager* genericManager = &soundManager1;

	if(typeid(soundManager1) == typeid(soundManager2))
		std::cout<<"same type 1"<<std::endl;
	else
		std::cout<<"different type1"<<std::endl;

	if(typeid(manager) == typeid(soundManager2))
			std::cout<<"same type 2"<<std::endl;
	else
		std::cout<<"different type2"<<std::endl;

	if(typeid(controlManager1) == typeid(controlManager2))
		std::cout<<"same type 3"<<std::endl;
	else
		std::cout<<"different type3"<<std::endl;

	std::cout<<typeid(*genericManager).name()<<std::endl;

	system("pause");


}

void Code2()
{
	Manager* a = new Manager;
	Manager* b = new SoundManager();

	SoundManager* c = dynamic_cast<SoundManager*>(b);
	if(c != NULL)
		std::cout<<"dynamic cast is successful"<<std::endl;
	else
		std::cout<<"dynamic cast failed"<<std::endl;


	SoundManager* c2 = dynamic_cast<SoundManager*>(a);
	if(c2 != NULL)
		std::cout<<"dynamic cast is successful"<<std::endl;
	else
		std::cout<<"dynamic cast failed"<<std::endl;

		SoloObject* d = dynamic_cast<SoloObject*>(a);
	if(d != NULL)
		std::cout<<"dynamic cast is successful"<<std::endl;
	else
		std::cout<<"dynamic cast failed"<<std::endl;

	system("pause");

}

//TEMPLATES
//C++ STL  (STANDARD TEMPLATE LIBRARY
//std::list<TYPE> m_container;
//std::vector<float> m_container2;
template <class T>
class Stack
{
private: T array[10];

public:
	void push(T const&)
	{
		std::cout<<"push"<<std::endl;
	}
	void pop()
	{
		std::cout<<"pop"<<std::endl;
	}
};

void Code3STL()
{
	//Stack<int> myStack;
	//myStack.push(10);
	//myStack.pop();

	//CircularBuffer<int> myBuffer(10);

	//myBuffer.CircularBuffer(10);//CircularBuffer<int>::CircularBuffer(10);// myBuffer;

	bool isExit = false;
	int choice = 0;

	std::cout << "Choose buffer size." << std::endl;
	std::cin >> choice;

	CircularBuffer<int> myBuffer(choice);

	choice = 0;
	do
	{
		myBuffer.DisplayArrayList();

		std::cout << "\n1. Add an obj." << std::endl;
		std::cout << "2. Pop Back." << std::endl;
		std::cout << "3. Pop Front." << std::endl;
		std::cout << "4. Get obj of index." << std::endl;
		std::cout << "5. AutoFill." << std::endl;
		std::cout << "6. Exit." << std::endl;
		std::cout << "Please input your choice ";

		std::cin >> choice;
		switch (choice)
		{
		case 1:
			std::cout << "Please input your data ";
				std::cin >> choice;

				myBuffer.push(choice);
			break;
		case 2:
			myBuffer.popBack();
			break;
		case 3:
			myBuffer.popFront();
			break;
		case 4:
			std::cout << "Please input your data ";
			std::cin >> choice;
			std::cout << "Value: " << myBuffer.getBufferValue(choice);
			system("Pause");
			break;
		case 5:
			for (int i = 1; i < 11; i++)
			{
				myBuffer.push(i);
			}
			break;
		case 6:
			isExit = true;
			break;
		default:
			std::cout << "Invalid Entry" << std::endl;
			break;
		}

		//system("Pause");
		system("cls");

	} while (!isExit);



}

///HOMEWORK
//Circular Buffer
// CircularBuffer
// - start
// - end
// + push 
// + popFront
// + popBack
// + int getSize
// + T getValue(int location)

