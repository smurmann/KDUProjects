﻿using UnityEngine;
using System.Collections;

public class LightScript : MonoBehaviour {
	[SerializeField] GameObject target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(target)
		{
			Vector3 position = target.transform.position;
			position.z=-1;

			transform.position=position;
		}
	
	}
}
