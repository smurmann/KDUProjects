// Exercise.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Inventory.h"
#include <Windows.h>


int _tmain(int argc, _TCHAR* argv[])
{
	Inventory inventory;

	inventory.addWeapon(new Sword("Dagger", 10), SWORD);
	inventory.addWeapon(new Shield("Door", 10), SHIELD);
	inventory.addWeapon(new Gun("Pistol1", 10), GUN);
	inventory.addWeapon(new Gun("Pistol2", 10), GUN);
	inventory.addWeapon(new Gun("Pistol3", 10), GUN);
	inventory.addWeapon(new Gun("Pistol4", 10), GUN);
	inventory.addWeapon(new Gun("Pistol5", 10), GUN);
	inventory.addWeapon(new Gun("Pistol6", 10), GUN);
	inventory.addWeapon(new Gun("Pistol7", 10), GUN);
	inventory.addWeapon(new Gun("Pistol8", 10), GUN);
	inventory.addWeapon(new Gun("Pistol9", 10), GUN);
	inventory.addWeapon(new Gun("Pistol10", 10), GUN);
	inventory.addWeapon(new Gun("Pistol11", 10), GUN);
	system("PAUSE");

	//11


	inventory.addWeapon(new Mask("MakeUp Mask", 10), MASK);

	inventory.printInventory();


	inventory.removeWeapon("Pistol3");
	
	system("PAUSE");

	inventory.printInventory();


	system("PAUSE");
	return 0;
}

