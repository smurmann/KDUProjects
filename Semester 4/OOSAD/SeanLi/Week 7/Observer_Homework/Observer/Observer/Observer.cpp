// Observer.cpp : Defines the entry point for the console application.
//
#include "DualSwitchLight.h"
#include "stdafx.h"
#include "MusicBox.h"
#include <iostream>
#include <windows.h>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Trigger trigger1;
	Trigger trigger2;

	MusicBox mBox;
	DualSwitchLight mLight(trigger1, trigger2);



	bool isExit = true;

	while (true)
	{
		char input;
		cout << "Input the state of the SECOND switch. 1 = True : 2 = False" << endl;
		cin >> input;
		if (input == '1')
		{
			trigger1.setState(true);
			//isExit = true;
		}
		else if (input == '2')
		{
			trigger1.setState(false);
			//isExit = true;
		}

		cout << "Input the state of the SECOND switch. 1 = True : 2 = False" << endl;
		cin >> input;
		if (input == '1')
		{
			trigger2.setState(true);
			isExit = true;
		}
		else if (input == '2')
		{
			trigger2.setState(false);
			isExit = true;
		}
	}while (!isExit)


	system("PAUSE");
	return 0;
}

