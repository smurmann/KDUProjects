#include "windows.h"
#include <iostream>

using namespace std;

int main()
{
    int x = 5, y;

    //! ptr is a constant poitner to a constant int
    //! ptr always point to the same location;
    //! the integer at that location cannot be modified

    const int* const ptr = &x;
    cout << *ptr << endl;
    ptr = &y;

    system("PAUSE");
    return 0;
}
