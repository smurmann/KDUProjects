﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScoreScript : MonoBehaviour {
    public GameObject inOut;
    PlayerDataScript datascript;
    int score;
    Text text;
	// Use this for initialization
	void Start () {
        datascript = GameObject.Find("PlayerData").GetComponent<PlayerDataScript>();
        text = GetComponent<Text>();
	
	}
	
	// Update is called once per frame
	void Update () {
        score = datascript.Score;
        text.text = "Your Score " + score.ToString();
	
	}
}
