#include <iostream>
using namespace std;

class Hero
{
private:
    int health;
public:
    Hero()
    {}
    //!setter method
    void SetHealth(int h)
    {
        if(h<100)
        {
            health = h;
        }
        else
        {
            health = 100;
        }

    }
    //!get method
    int GetHealth()
    {
        return health;
    }
};
int main()
{
    Hero h1, h2;
    h1.SetHealth(100);
    h2.SetHealth(200);
    cout<<"Hero 1 health : "<<h1.GetHealth()<<endl;
    cout<<"Hero 2 health : "<<h2.GetHealth()<<endl;
}
