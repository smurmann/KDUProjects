#include <iostream>
#include "windows.h"

using namespace std;

int main()
{
    int choice = 0;

    cout<<"which hero are you? \n";
    cout<<"1. Ultraman \n";
    cout<<"2. Batman \n";
    cout<<"3. Ironman \n";
    cout<<"4. Thor \n";
    cout<<"5. Spiderman \n";

    cin>> choice;

    cout<<endl;

    //! switch case -> multiple selection with
    //! specific range of input
    switch(choice)
    {
        case 1:
            cout<<"The red light is blinking!"<<endl;
            break;
        case 2:
            cout<<"Gotta Catch'em All"<<endl;
            break;
        case 3:
        case 4:
            cout<<"We are the Avengers"<<endl;
            break;
        case 5:
            cout<<"Pew pew pew"<<endl;
            break;
        default:
            cout<<"You are a homosapien"<<endl;
            break;

    }
system("pause");

}
