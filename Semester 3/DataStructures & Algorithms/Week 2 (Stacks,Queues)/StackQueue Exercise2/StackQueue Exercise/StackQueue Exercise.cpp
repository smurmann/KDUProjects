// StackQueue Exercise.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "windows.h"
#include "stack"
#include "queue"
#include "string"

using namespace std;

stack<char> charStack;
stack<char> checkStack;



int _tmain(int argc, _TCHAR* argv[])
{
	//! store the user string input
	string str;
	string check;
	cout<<"Please enter a word: "<<endl;
	// cin>>str; problem with spaces
	getline(cin,str);
	//! get each char from the string to be inserted
	for(int i=0; i<str.length(); i++)
	{
		//! push the char to stack
		charStack.push(str[i]);
	}
	while(charStack.size()>0)
	{
		cout<<charStack.top();
		//check.insert(0, 1, checkQueue.front());
		checkStack.push(charStack.top());
		charStack.pop();
	}
	while(checkStack.size()>0)
	{
		check.insert(0, 1, checkStack.top());
		checkStack.pop();
	}

	cout<<endl;
	//cout<<check<<endl;
	//cout<<str<<endl;
	if(check.compare(str) == 0)
	{
		cout<<"palindrome is TRUE"<<endl;
	}
	else if(check.compare(str) != 0)
	{
		cout<<"palindrome is FALSE"<<endl;
	}

	cout << endl;
	system("PAUSE");
	return 0;
}