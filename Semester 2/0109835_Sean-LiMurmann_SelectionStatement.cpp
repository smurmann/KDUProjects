#include <iostream>
#include "windows.h"

using namespace std;

int main()
{
    int grade = 0;
    cout << "Please enter a grade:"<<endl;
    cin >> grade;

    if(grade == 100 && grade < 101)
    {
        cout << "A+ \n";
    }
    else
    if(grade >= 80 && grade < 100)
    {
        cout << "A \n";
    }
    else
    if(grade >= 70 && grade < 80)
    {
        cout << "B \n";
    }
    else
    if(grade >= 60 && grade < 70)
    {
        cout << "C \n";
    }
    else
    if(grade >= 50 && grade < 60)
    {
        cout << "D \n";
    }
    else
    if(grade >= 0 && grade < 50)
    {
        cout << "F \n";
    }
    else
    if(grade > 100 || grade < 0)
    {
        cout << "Invalid Input \n";
    }
    system("PAUSE");
    return 0;
}
