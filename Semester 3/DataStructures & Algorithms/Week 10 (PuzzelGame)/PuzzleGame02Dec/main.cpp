#include <iostream>
#include <windows.h>
#include <time.h>
#include "conio_yp.h"

using namespace std;

enum DIRECTION
{
    NORTH = 0,
    EAST,
    SOUTH,
    WEST,
    NONE,
    TOTAL_DIRECTION
};


enum GRID_TYPE
{
    SOURCE = 0,
    TARGET,
    EMPTY,
    STRAIGHT_VERTICAL,
    STRAIGHT_HORIZONTAL,
    LSHAPE_0_DEGREE,
    LSHAPE_90_DEGREE,
    LSHAPE_180_DEGREE,
    LSHAPE_270_DEGREE,
    TSHAPE_0_DEGREE,
    TSHAPE_90_DEGREE,
    TSHAPE_180_DEGREE,
    TSHAPE_270_DEGREE,
    CROSSSHAPE,
    TOTAL_TYPE
};

struct Node
{
    GRID_TYPE type;

    char** sprite;
    int color;

    //!handle the connection
    Node* northNode;
    Node* southNode;
    Node* eastNode;
    Node* westNode;
    bool isNorthConnectable;
    bool isSouthConnectable;
    bool isEastConnectable;
    bool isWestConnectable;


    void Initialize(GRID_TYPE type);
    void SetType(GRID_TYPE type);
    void CopySprite(char tempSprite[][3]);
};

Node** gridNode;
int totalRow = 0;
int totalCol = 0;
int sourceRow = 0;
int sourceCol = 0;
int targetCounter = 0;
//!HERE store the row and col value for each target
bool isSolved = false;

void Node::Initialize(GRID_TYPE type)
{
    for(int i=0; i<3; i++)
    {
        sprite = new char*[3];
        for(int j=0; j<3; j++)
        {
            sprite[j]=new char[3];
        }
    }
    SetType(type);
}
void Node::SetType(GRID_TYPE type)
{
    this->type = type;

    if(type == SOURCE)
    {
        color = YELLOW;

        char tempSprite[3][3] = {
                                    {'#','#','#'},
                                    {'#','#','#'},
                                    {'#','#','#'}
                                };
        isNorthConnectable = true;
        isSouthConnectable = true;
        isEastConnectable = true;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }

    else if(type == TARGET)
    {
        color = LIGHTBLUE;

        char tempSprite[3][3] = {
                                    {'#','#','#'},
                                    {'#','#','#'},
                                    {'#','#','#'}
                                };
        isNorthConnectable = true;
        isSouthConnectable = true;
        isEastConnectable = true;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == EMPTY)
    {
        color = LIGHTBLUE;

        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {' ',' ',' '},
                                    {' ',' ',' '}
                                };
        isNorthConnectable = false;
        isSouthConnectable = false;
        isEastConnectable = false;
        isWestConnectable = false;
        CopySprite(tempSprite);
    }
    else if(type == STRAIGHT_VERTICAL)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {' ','#',' '},
                                    {' ','#',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = true;
        isEastConnectable = false;
        isWestConnectable = false;
        CopySprite(tempSprite);
    }
    else if(type == STRAIGHT_HORIZONTAL)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {'#','#','#'},
                                    {' ',' ',' '}
                                };
        isNorthConnectable = false;
        isSouthConnectable = false;
        isEastConnectable = true;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_0_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {' ','#','#'},
                                    {' ',' ',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = false;
        isEastConnectable = true;
        isWestConnectable = false;
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_90_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {' ','#','#'},
                                    {' ','#',' '}
                                };
        isNorthConnectable = false;
        isSouthConnectable = true;
        isEastConnectable = true;
        isWestConnectable = false;
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_180_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {'#','#',' '},
                                    {' ','#',' '}
                                };
        isNorthConnectable = false;
        isSouthConnectable = true;
        isEastConnectable = false;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_270_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {'#','#',' '},
                                    {' ',' ',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = false;
        isEastConnectable = false;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_0_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {'#','#','#'},
                                    {' ','#',' '}
                                };
        isNorthConnectable = false;
        isSouthConnectable = true;
        isEastConnectable = true;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_90_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {'#','#',' '},
                                    {' ','#',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = true;
        isEastConnectable = false;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_180_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {'#','#','#'},
                                    {' ',' ',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = false;
        isEastConnectable = true;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_270_DEGREE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {' ','#','#'},
                                    {' ','#',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = true;
        isEastConnectable = true;
        isWestConnectable = false;
        CopySprite(tempSprite);
    }
        else if(type == CROSSSHAPE)
    {
        color = LIGHTRED;

        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {'#','#','#'},
                                    {' ','#',' '}
                                };
        isNorthConnectable = true;
        isSouthConnectable = true;
        isEastConnectable = true;
        isWestConnectable = true;
        CopySprite(tempSprite);
    }
}

void Node::CopySprite(char tempSprite[][3])
{
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            sprite[i][j] = tempSprite[i][j];
        }
    }
}

void MarkLink(Node* node, DIRECTION direction)
{
    node->color = YELLOW;
    if(node->northNode !=NULL && direction !=NORTH)
    {
        MarkLink(node->northNode,SOUTH);
    }
    if(node->southNode !=NULL && direction != SOUTH)
    {
        MarkLink(node->southNode,NORTH);
    }
    if(node->eastNode !=NULL && direction != EAST)
    {
        MarkLink(node->eastNode,WEST);
    }
    if(node->westNode !=NULL && direction != WEST)
    {
        MarkLink(node->westNode,EAST);
    }
}

void Unlink()
{
    for(int i=0; i<totalRow; i++)
    {
        for(int j=0; j<totalCol; j++)
        {
            if(gridNode[i][j].type == TARGET)
            {
                gridNode[i][j].color = LIGHTBLUE;
            }
            else if(gridNode[i][j].type == SOURCE)
            {
                gridNode[i][j].color = YELLOW;
            }
            else
            {
                gridNode[i][j].color = LIGHTRED;
            }
            gridNode[i][j].northNode = NULL;
            gridNode[i][j].southNode = NULL;
            gridNode[i][j].eastNode = NULL;
            gridNode[i][j].westNode = NULL;
        }
    }
}

bool ParseLink()
{
    Node* currentNode = &gridNode[sourceRow][sourceCol];
    MarkLink(currentNode, NONE);
    for(int i=0; i<totalRow; i++)
    {
        for(int j=0; j<totalCol; j++)
        {
            if(gridNode[i][j].type == TARGET)
            {
                if(gridNode[i][j].color == YELLOW)
                {
                    targetCounter++;
                }
            }
        }
    }
    if(targetCounter >= 4)
    {
        return true;
    }
    else
    {
        targetCounter =0;
    }
    return false;
}

void ReinitializeLink()
{
for(int i=0; i<totalRow; i++)
{
for(int j=0; j<totalCol; j++)
{
if(i<totalRow -1)
{
if(gridNode[i][j].isSouthConnectable == true &&
gridNode[i+1][j].isNorthConnectable==true)
{
gridNode[i][j].southNode = &gridNode[i+1][j];
}
}
if(i>0)
{
if(gridNode[i][j].isNorthConnectable == true &&
gridNode[i-1][j].isSouthConnectable==true)
{
gridNode[i][j].northNode = &gridNode[i-1][j];
}
}
if(j<totalCol -1)
{
if(gridNode[i][j].isEastConnectable == true &&
gridNode[i][j+1].isWestConnectable==true)
{
gridNode[i][j].eastNode = &gridNode[i][j+1];
}
}
if(j>0)
{
if(gridNode[i][j].isWestConnectable == true &&
gridNode[i][j-1].isEastConnectable==true)
{
gridNode[i][j].westNode = &gridNode[i][j-1];
}
}
}
}
}

void InitializeGridHard()
{
    totalRow = 6;
    totalCol = 9;


    GRID_TYPE tempGrid[6][9] = {
                                    {               TARGET, TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,               EMPTY,               EMPTY, LSHAPE_270_DEGREE, STRAIGHT_HORIZONTAL,                TARGET},                        //!
                                    {               EMPTY,            EMPTY, STRAIGHT_HORIZONTAL,   LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,   TSHAPE_0_DEGREE,               EMPTY,                 EMPTY},
                                    {               EMPTY,LSHAPE_270_DEGREE,     TSHAPE_0_DEGREE,     TSHAPE_0_DEGREE,     TSHAPE_0_DEGREE,     TSHAPE_0_DEGREE,   TSHAPE_0_DEGREE,               EMPTY,                 EMPTY},
                                    {               TARGET, TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,               EMPTY, LSHAPE_270_DEGREE, STRAIGHT_HORIZONTAL,     LSHAPE_270_DEGREE},
                                    {               EMPTY,  TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE, STRAIGHT_HORIZONTAL,     TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE, LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,                TARGET},
                                    {               EMPTY,LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,              SOURCE,               EMPTY,             EMPTY,               EMPTY,                 EMPTY}
                               };

    gridNode = new Node*[totalRow];
    for(int i=0; i<totalRow; i++)
    {
        gridNode[i]= new Node[totalCol];
        for(int j=0; j<totalCol; j++)
        {
            Node tempNode;
            tempNode.Initialize(tempGrid[i][j]);
            if(tempGrid[i][j] == SOURCE)
            {
                sourceRow = i;
                sourceCol = j;
            }
            //!HERE set the row and col value for each target
            gridNode[i][j] = tempNode;
        }
    }
    ReinitializeLink();
    //ParseLink();
}

void InitializeGridMedium()
{
    totalRow = 6;
    totalCol = 9;


    GRID_TYPE tempGrid[6][9] = {
                                    {SOURCE             , TSHAPE_270_DEGREE  , STRAIGHT_HORIZONTAL, STRAIGHT_HORIZONTAL, STRAIGHT_VERTICAL  , TSHAPE_270_DEGREE  , EMPTY              , EMPTY              , EMPTY              },                        //!
                                    {EMPTY              , STRAIGHT_HORIZONTAL, EMPTY              , EMPTY              , EMPTY              , STRAIGHT_HORIZONTAL, EMPTY              , EMPTY              , EMPTY              },
                                    {EMPTY              , LSHAPE_0_DEGREE    , TSHAPE_180_DEGREE  , EMPTY              , EMPTY              , STRAIGHT_HORIZONTAL, EMPTY              , EMPTY              , EMPTY              },
                                    {EMPTY              , EMPTY              , STRAIGHT_HORIZONTAL, EMPTY              , EMPTY              , TSHAPE_180_DEGREE  , STRAIGHT_HORIZONTAL, STRAIGHT_HORIZONTAL, TARGET             },
                                    {EMPTY              , LSHAPE_90_DEGREE   , LSHAPE_270_DEGREE  , EMPTY              , EMPTY              , STRAIGHT_HORIZONTAL, EMPTY              , EMPTY              , EMPTY              },
                                    {EMPTY              , TARGET             , EMPTY              , EMPTY              , EMPTY              , TARGET             , EMPTY              , EMPTY              , EMPTY              },
                               };

    gridNode = new Node*[totalRow];
    for(int i=0; i<totalRow; i++)
    {
        gridNode[i]= new Node[totalCol];
        for(int j=0; j<totalCol; j++)
        {
            Node tempNode;
            tempNode.Initialize(tempGrid[i][j]);
            if(tempGrid[i][j] == SOURCE)
            {
                sourceRow = i;
                sourceCol = j;
            }
            //!HERE set the row and col value for each target
            gridNode[i][j] = tempNode;
        }
    }
    ReinitializeLink();
    //ParseLink();
}

void InitializeGridEasy()
{
    totalRow = 4;
    totalCol = 5;


    GRID_TYPE tempGrid[4][5] = {
                                    {               TARGET,       STRAIGHT_VERTICAL,            CROSSSHAPE,      STRAIGHT_VERTICAL,               TARGET},                        //!
                                    {               EMPTY,                    EMPTY,    STRAIGHT_HORIZONTAL,                 EMPTY,               EMPTY},
                                    {               EMPTY,                    EMPTY,    STRAIGHT_HORIZONTAL,                 EMPTY,               EMPTY},
                                    {               EMPTY,                    EMPTY,                 SOURCE,                 EMPTY,               EMPTY}
                               };

    gridNode = new Node*[totalRow];
    for(int i=0; i<totalRow; i++)
    {
        gridNode[i]= new Node[totalCol];
        for(int j=0; j<totalCol; j++)
        {
            Node tempNode;
            tempNode.Initialize(tempGrid[i][j]);
            if(tempGrid[i][j] == SOURCE)
            {
                sourceRow = i;
                sourceCol = j;
            }
            //!HERE set the row and col value for each target
            gridNode[i][j] = tempNode;
        }
    }
    ReinitializeLink();
    //ParseLink();
}

void DrawGrid()
{
    system("CLS");
    //! bottom col guide
    for(int i=0; i<totalCol; i++)
    {
        textcolor(WHITE);
        gotoxy(i * 4 + 3, 3 * totalRow + 1 + totalRow);
        cout<<"-----";
        gotoxy(i * 4 + 5, 3 * totalRow + 2 + totalRow);
        cout<<i;
    }
    //! left row guide
    for(int i=0; i<totalRow; i++)
    {
        textcolor(WHITE);
        gotoxy(1,i * 4 + 4);
        cout<<i;
    }

    for(int i=0; i<totalRow; i++)
    {
        for(int j=0; j<totalCol; j++)
        {
            textcolor(WHITE);
            int xPos = 2 * j + 3 + j;
            int yPos = 3 * i + 1 + i;
            gotoxy(xPos,yPos);
            cout<<"-------------";

            gotoxy(xPos + j,yPos+1);
            cout<<"|";
            gotoxy(xPos + j,yPos+2);
            cout<<"|";
            gotoxy(xPos + j,yPos+3);
            cout<<"|";

            textcolor(gridNode[i][j].color);
            for(int m=0; m<3; m++)
            {
                for(int n=0; n<3; n++)
                {
                    gotoxy(xPos + n + j + 1,yPos + m + 1);
                    cout<<gridNode[i][j].sprite[m][n];
                }
            }
        }
    }

    //! draw right border
    for(int i=0; i<totalRow; i++)
    {
        textcolor(WHITE);
        int xPos = 2 * totalCol + 3 + totalCol;
        int yPos = 3 * i + 1 + i;

        gotoxy(xPos + totalCol,yPos+1);
        cout<<"|";
        gotoxy(xPos + totalCol,yPos+2);
        cout<<"|";
        gotoxy(xPos + totalCol,yPos+3);
        cout<<"|";
    }
}

void RotateGrid(int row, int col)
{
    if(gridNode[row][col].type == STRAIGHT_VERTICAL)
    {
        gridNode[row][col].SetType(STRAIGHT_HORIZONTAL);
    }
    else if(gridNode[row][col].type == STRAIGHT_HORIZONTAL)
    {
        gridNode[row][col].SetType(STRAIGHT_VERTICAL);
    }
    //! lshape rotation
    else if(gridNode[row][col].type == LSHAPE_0_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_90_DEGREE);
    }
    else if(gridNode[row][col].type == LSHAPE_90_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_180_DEGREE);
    }
    else if(gridNode[row][col].type == LSHAPE_180_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_270_DEGREE);
    }
    else if(gridNode[row][col].type == LSHAPE_270_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_0_DEGREE);
    }
    //! tshape rotation
    else if(gridNode[row][col].type == TSHAPE_0_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_90_DEGREE);
    }
    else if(gridNode[row][col].type == TSHAPE_90_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_180_DEGREE);
    }
    else if(gridNode[row][col].type == TSHAPE_180_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_270_DEGREE);
    }
    else if(gridNode[row][col].type == TSHAPE_270_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_0_DEGREE);
    }
}

void SetDifficulty()
{
    int difficulty = 1;
    cout<<"Please select your difficulty: 1.Easy, 2.Medium, 3.Hard"<<endl;
    cout<<"Enter Number: ";
    cin>>difficulty;

    if(difficulty == 1)
    {
        InitializeGridEasy();
    }
    else if(difficulty == 2)
    {
        InitializeGridMedium();
    }
    else if(difficulty == 3)
    {
        InitializeGridHard();
    }
}

void RotateGridMenu()
{
    int xGrid = -1;
    int yGrid = -1;

    textcolor(WHITE);
    gotoxy(1, totalRow * 3 + totalRow + 4);
    cout<<"Grid to rotate [x y] eg. 1 3: ";
    cin>>xGrid>>yGrid;

    RotateGrid(xGrid, yGrid);
    Unlink();
    ReinitializeLink();
    isSolved = ParseLink();

    system("PAUSE");
}

int main()
{
    system("mode con: cols=80 lines=30");

    //InitializeGrid();
    SetDifficulty();
    DrawGrid();

    do
    {
        RotateGridMenu();
        DrawGrid();
    }while(!isSolved);

    return 0;
}
