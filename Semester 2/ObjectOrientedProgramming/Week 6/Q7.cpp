/*
 Category of the question :
    Correct syntax and logical errors

 Objective of the question :
    After the user entered the radius and height of a cylinder, the
    program will calculate and display the area and volume of the
    cylinder.

    However, there are some syntax and logical errors in the program
    and the program does not produced the desired output.

    What you need to do is to find the errors and correct them so
    that the desired output can be displayed as shown in the
    sample run.

 Correct Sample run 1:
    Input radius of a cylinder: 2
    Input height of a cylinder: 5
    Area = 87.92
    Volume = 62.80

 Correct Sample run 2:
    Input radius of a cylinder: 5
    Input height of a cylinder: 20
    Area = 785.00
    Volume = 1570.00

*********************************************************/
#include <cmath>
#include <iostream>
#include "windows.h"
using namespace std;



class Cylinder {
    public:
        Cylinder();
        void set(double cyl_radius, double cyl_height);
        double get_area();
        double get_volume();
    private:
        double radius;
        double height;
        double PI;
};

int main()
{
    Cylinder cylinder;
    double r, h;

    cout << "Input the radius of a cylinder: ";
    cin >> r;
    cout << "Input the height of a cylinder: ";
    cin >> h;

    cylinder.set(r,h);
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    cout << "Area = " << cylinder.get_area() << endl;
    cout << "Volume = " << cylinder.get_volume() << endl;

    system("pause");
    return 0;
}

Cylinder::Cylinder() {
    PI = 3.14;
    radius = 1.0;
    height = 1.0;
}

void Cylinder::set(double cyl_radius, double cyl_height) {
    PI = 3.14;
    radius = cyl_radius;
    height = cyl_height;
}

double Cylinder::get_area() {
    return (2 * PI * pow(radius,2)) + (2 * PI * radius * height);
}

double Cylinder::get_volume() {
    return PI * pow(radius,2) * height;
}

