// CompostieDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <vector>
#include <windowsx.h>

using namespace std;

class Component
{
protected:
	string fullName;
	double valueSalary;
public:
	Component(string name, double salary)
		:fullName(name), valueSalary(salary){}

	virtual void printSalary(int level) = 0;
};

class Worker : public Component
{
public: 
	Worker(string name, double salary)
		:Component(name, salary){}
	void printSalary(int level)
	{
		for (int j = 0; j < level; j++)
			cout << "\t";
		cout << fullName << ", salary : RM " << valueSalary << endl;
	}
};

//! composite class
class Manager : public Component
{
private:
	vector<Component*> children;

public:
	Manager(string name, double salary)
		:Component(name, salary)
	{}

	void add(Component *cmp)
	{
		children.push_back(cmp);
	}

	void printSalary(int level)
	{
		for (int j = 0; j < level; ++j) cout << "\t";
		cout << "Manager : " << this->fullName << "salary RM" << valueSalary << endl;

		if (!children.empty())
		{
			for (int x = 0; x < level; ++x)
				cout << "\t";
				cout << "Subbordinates of " << fullName << " : " << endl;
				++level;
			for (int i = 0; i < children.size(); i++)
			{
				children[i]->printSalary(level);
			}
		}
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	Manager CEO("Sam Kong", 100000.0);
	Manager EngineeringManager("TJ", 50000.0);
	Manager ProductionManager("Mody", 80000.0);
	Manager TechnicalLead("Azri", 100000.0);
	Manager ResearchLead("Max", 20000.0);

	Worker programmer1("Sean", 50000.0);
	Worker programmer2("WC", 50000.0);

	Worker research1("Vince", 10000.0);
	Worker research2("Cheesenaan", 15000.0);

	CEO.add(&EngineeringManager);
	CEO.add(&ProductionManager);
	EngineeringManager.add(&ResearchLead);
	ProductionManager.add(&TechnicalLead);
	ResearchLead.add(&research1);
	ResearchLead.add(&research2);
	TechnicalLead.add(&programmer1);
	TechnicalLead.add(&programmer2);

	cout << "The hierachy of the company" << endl;
	CEO.printSalary(0);

	system("PAUSE");
	return 0;
}

