// BubbleSort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <vector>

using namespace std;
vector<int> numberList;

void DisplayNumber()
{
	for(int i=0; i<numberList.size(); i++)
	{
		cout<<numberList[i]<<" ";
	}
}
void GenerateRandomNumbers()
{
	for(int i=0; i<10; i++)
	{
		numberList.push_back(rand()%100);
	}
}
void BubbleSortAcd()
{
	bool isStillSorting = false;
	do
	{//! 67 23 78 41
	isStillSorting = false;
	for(int i=0; i<numberList.size()-1; i++)
	{
		if(numberList[i]>numberList[i+1])
		{
			//!swapping
			int tempValue = numberList[i];
			numberList[i] = numberList[i+1];
			numberList[i+1] = tempValue;
			isStillSorting = true;
		}
	}
	}while(isStillSorting);
}
void BubbleSortDcs()
{
	bool isStillSorting = false;
	do
	{//! 67 23 78 41
	isStillSorting = false;
	for(int i=0; i<numberList.size()-1; i++)
	{
		if(numberList[i]<numberList[i+1])
		{
			//!swapping
			int tempValue = numberList[i];
			numberList[i] = numberList[i+1];
			numberList[i+1] = tempValue;
			isStillSorting = true;
		}
	}
	}while(isStillSorting);
}


int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	cout<<"Before Sorting"<<endl;
	GenerateRandomNumbers();
	DisplayNumber();
	cout<<endl<<"After Sorting (ACD)"<<endl;
	BubbleSortAcd();
	DisplayNumber();
	cout<<endl<<"After Sorting (DCS)"<<endl;
	BubbleSortDcs();
	DisplayNumber();
	system("PAUSE");
	return 0;
}

