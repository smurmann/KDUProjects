#pragma once
#include <vector>
#include <iostream>
#include "AItems.h"

using namespace std;

class AEmployee
{
public: 
	vector<AItems> benefits;
	enum EMP_TYPE
	{
		SOLDIER,
		HUMAN
	};
	void CreateAndAddBenefit(AItems ben);
	void PrintBenefits();
	static AEmployee* CreateEmployee(EMP_TYPE empType);
};