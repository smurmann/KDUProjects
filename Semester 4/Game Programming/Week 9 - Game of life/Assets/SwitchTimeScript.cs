﻿using UnityEngine;
using System.Collections;

public class SwitchTimeScript : MonoBehaviour {

    public bool toggle = false;
    SpriteRenderer sr;

    // Use this for initialization
    void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        if (toggle)
        {
            sr.color = Color.red;
        }
        else
        {
            sr.color = Color.white;
        }

    }

    void OnMouseDown()
    {
        toggle = !toggle;
    }
}
