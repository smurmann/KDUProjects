#include "DefaultHeaderFiles.h"

//! define several game variables
int startTime = 0;
int currentTime = 0;
int elapsedTime = 0;
int lastUpdateTime = 0;
bool isExit = false;

//! define a player object
Player player;
vector<Enemy*> enemyList;
vector<SpawnInfo> spawnList;
vector<Bullet> playerBulletList;
vector<Bullet> enemyBulletList;
void DrawBorder()
{
    for(int i=BOUNDARY_LEFT; i<BOUNDARY_RIGHT; i++)
    {
        gotoxy(i, BOUNDARY_TOP);
        cout<<char(205);
    }
    for(int i=BOUNDARY_LEFT; i<BOUNDARY_RIGHT; i++)
    {
        gotoxy(i,BOUNDARY_BOTTOM);
        cout<<char(205);
    }
    for(int i=BOUNDARY_TOP; i<BOUNDARY_BOTTOM; i++)
    {
        gotoxy(BOUNDARY_LEFT,i);
        cout<<char(186);
    }
    for(int i=BOUNDARY_TOP; i<BOUNDARY_BOTTOM; i++)
    {
        gotoxy(BOUNDARY_RIGHT,i);
        cout<<char(186);
    }
    gotoxy(BOUNDARY_LEFT,BOUNDARY_TOP);
    cout<<char(201);
    gotoxy(BOUNDARY_LEFT,BOUNDARY_BOTTOM);
    cout<<char(200);
    gotoxy(BOUNDARY_RIGHT,BOUNDARY_TOP);
    cout<<char(187);
    gotoxy(BOUNDARY_RIGHT,BOUNDARY_BOTTOM);
    cout<<char(188);

}
void DrawHUD()
{
    gotoxy(BOUNDARY_LEFT, BOUNDARY_TOP - 1);
    cout<<"                                ";
    gotoxy(BOUNDARY_LEFT,BOUNDARY_TOP - 2);
    cout<<"Score : "<<player.score;
    gotoxy(BOUNDARY_LEFT + 30,BOUNDARY_TOP - 2);
    cout<<"                                ";
    gotoxy(BOUNDARY_LEFT + 50,BOUNDARY_TOP - 2);
    cout<<"Bullet : "<<playerBulletList.size();
}

void SpawnEnemy(ENEMY_TYPE type)
{
    Enemy* tempEnemy;
    if(type == ENEMY_01)
    {
        tempEnemy = new Enemy01();
    }
    else if (type == ENEMY_02)
    {
        tempEnemy = new Enemy02();
    }
    else if(type == ENEMY_03)
    {
        tempEnemy = new Enemy03();
    }
    enemyList.push_back(tempEnemy);
}
//! set the type of enemy and the time to spawn in SpawnInfo struct
void AddEnemy(ENEMY_TYPE type, int time)
{
    SpawnInfo tempSpawnInfo;
    tempSpawnInfo.type = type;
    tempSpawnInfo.time = time;
    tempSpawnInfo.isSpawned = false;

    spawnList.push_back(tempSpawnInfo);
}
void InitializeSpawnList()
{
    AddEnemy(ENEMY_01, 1000);
    AddEnemy(ENEMY_02, 1000);
    AddEnemy(ENEMY_03, 1000);

    AddEnemy(ENEMY_03, 3000);
    AddEnemy(ENEMY_03, 3400);
    AddEnemy(ENEMY_03, 3600);
    AddEnemy(ENEMY_03, 3800);

    AddEnemy(ENEMY_01, 2000);
    AddEnemy(ENEMY_02, 4000);
    AddEnemy(ENEMY_03, 2800);
}

void UpdateSpawn()
{
    for(int i=0; i<spawnList.size(); i++)
    {
        if(spawnList[i].isSpawned == true)
        {
            continue;
        }
        if(currentTime - startTime > spawnList[i].time)
        {
            SpawnEnemy(spawnList[i].type);
            spawnList[i].isSpawned = true;
        }
    }
}

void Shoot(bool isPlayer, GameObject& object)
{
    BULLET_TYPE tempBulletType;
    vector<Bullet>* tempBulletList;
    bool tempIsReverseDirection = false;
    //! is to determine the pass in object is Player or enemy
    if(isPlayer)
    {
        tempBulletType = ((Player*)&object)->bulletType;
        tempBulletList = &playerBulletList;
        player.enableShooting = false;
        //player.sprite
    }
    else
    {
        tempBulletType = ((Enemy*)&object)->bulletType;
        tempBulletList = &enemyBulletList;
        ((Enemy*)&object)->enableShooting = false;
        tempIsReverseDirection = true;
    }

    if(tempBulletType == STRAIGHT)
    {
        Bullet tempBullet;
        tempBullet.x = object.x + 1;
        tempBullet.y = object.y;
        tempBullet.direction =  NORTH;
        tempBullet.isReverseDirection = tempIsReverseDirection;

        tempBulletList->push_back(tempBullet);
    }
    else if (tempBulletType == DUAL)
    {
        Bullet tempBullet;
        tempBullet.x = object.x - 2;
        tempBullet.y = object.y;
        tempBullet.direction =  NORTH;
        tempBullet.isReverseDirection = tempIsReverseDirection;

        tempBulletList->push_back(tempBullet);

        Bullet tempBullet02;
        tempBullet02.x = object.x + 2;
        tempBullet02.y = object.y;
        tempBullet02.direction =  NORTH;
        tempBullet02.isReverseDirection = tempIsReverseDirection;

        tempBulletList->push_back(tempBullet02);
    }
    else if(tempBulletType == TRIPLE)
    {
        Bullet tempBullet;
        tempBullet.x = object.x + 1;
        tempBullet.y = object.y;
        tempBullet.direction =  NORTH;
        tempBullet.isReverseDirection = tempIsReverseDirection;

        tempBulletList->push_back(tempBullet);

        Bullet tempBullet02;
        tempBullet02.x = object.x - 2;
        tempBullet02.y = object.y;
        tempBullet02.direction =  NORTH;
        tempBullet02.isReverseDirection = tempIsReverseDirection;

        tempBulletList->push_back(tempBullet02);

        Bullet tempBullet03;
        tempBullet03.x = object.x + 2;
        tempBullet03.y = object.y;
        tempBullet03.direction =  NORTH;
        tempBullet03.isReverseDirection = tempIsReverseDirection;

        tempBulletList->push_back(tempBullet03);
    }
   // player.enableShooting = false;
}

bool CheckBoundingBoxCollision(GameObject a, GameObject b)
{
    if(a.x + a.col< b.x ||
       a.x > b.x +b.col ||
       a.y + a.row < b.y ||
       a.y > b.y + b.row)
    {
        return false;
    }
    return true;
}
void CheckCollision()
{
    //!enemy vs bullet
    for(int i=0; i<enemyList.size(); i++)
    {
        for(int j=0; j<playerBulletList.size(); j++)
        {
            if(CheckBoundingBoxCollision(*enemyList[i], playerBulletList[j]))
            {
                enemyList[i]->isToBeDestroy = true;
                playerBulletList[j].isToBeDestroy = true;
                player.score += 10;
            }
        }
    }
    //!enemy vs player
    //!enemy bullet vs player
    //!player vs Items
}
int main()
{
    //! console window size
    system("mode con: cols=80 lines=30");
    srand(time(NULL)); //! time for random generator
    DrawBorder();
    InitializeSpawnList();
   // cout << "Hello world!" << endl;
   //! set the start time to current clock tick
   startTime = clock();

   do
   {
       //! set the current time to current clock tick
       currentTime = clock();
        UpdateSpawn();

       player.Update(elapsedTime);

       if(player.enableShooting)
       {
           Shoot(true, player);
       }

       for(int i=0; i<enemyList.size(); i++)
       {
           enemyList[i]->Update(elapsedTime);
           if(enemyList[i]->enableShooting)
           {
               Shoot(false, *enemyList[i]);
           }
       }
        for(int i=0; i<playerBulletList.size(); i++)
        {
            playerBulletList[i].Update(elapsedTime);
        }
        for(int i=0; i<enemyBulletList.size(); i++)
        {
            enemyBulletList[i].Update(elapsedTime);
        }

        CheckCollision();

       player.Draw();

       for(int i=0; i<enemyList.size(); i++)
       {
           if(enemyList[i]->isToBeDestroy)
           {
               enemyList[i]->DrawTrail();
               enemyList.erase(enemyList.begin()+ i);
               i--;
           }
           else
           {
               enemyList[i]->Draw();

           }
       }
       for(int i=0; i<playerBulletList.size(); i++)
       {
           if(playerBulletList[i].isToBeDestroy)
           {
               playerBulletList[i].DrawTrail();
               playerBulletList.erase(playerBulletList.begin()
                                      + i);
                i--;
           }
           else
           {
               playerBulletList[i].Draw();
           }
       }

       for(int i=0; i<enemyBulletList.size(); i++)
       {
           if(enemyBulletList[i].isToBeDestroy)
           {
               enemyBulletList[i].DrawTrail();
               enemyBulletList.erase(enemyBulletList.begin()
                                      + i);
                i--;
           }
           else
           {
               enemyBulletList[i].Draw();
           }
       }
       DrawHUD();
       elapsedTime = (clock() - lastUpdateTime)*1000 /
       CLOCKS_PER_SEC;
       lastUpdateTime = clock();
       Sleep(250);
   }while(!isExit);

   cout<<endl;

    return 0;
}
