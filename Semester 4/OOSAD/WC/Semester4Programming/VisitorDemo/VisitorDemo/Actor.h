#ifndef _ACTOR_H
#define _ACTOR_H
#include <string>

using namespace std;
class Node;

class Actor
{
public:
	string mName;
	Node* mOwner;
	Actor(string name) : mName(name), mOwner(NULL)
	{}
};

class ActorVisitor
{
public:
	virtual void visit(Actor* actor) = 0;
};

#endif