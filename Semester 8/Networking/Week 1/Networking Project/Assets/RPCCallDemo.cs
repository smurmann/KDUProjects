﻿using UnityEngine;
using System.Collections;

public class RPCCallDemo : Photon.MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("1.0");

    }

    void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");
        PhotonNetwork.JoinOrCreateRoom("Name", new RoomOptions(), TypedLobby.Default);
    }

    void OnJoinedRoom()
    {
        var cube = PhotonNetwork.Instantiate("cube", Vector3.zero, Quaternion.identity, 0);
        cube.transform.GetChild(0).gameObject.SetActive(true);

        cube.GetComponent<MeshRenderer>().material.color = Color.blue;
    }

}
